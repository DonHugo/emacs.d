# Initialize Emacs for non-interactive cli operations

# Increas garbage collector to 128MB
# Prioritize non-byte-compiled files
# Ensure this .emacs.d is used as directory
# Load init-core.el
# Load init-cli.el
EMACS = emacs -Q --batch --eval "(setq gc-cons-threshold 134217728)" \
	--eval "(setq load-prefer-newer t)" \
	--eval "(setq user-emacs-directory default-directory)" \
	--eval "(load (expand-file-name \"lisp/init-core.el\" user-emacs-directory) nil t)" \
	--eval "(require 'init-cli)"

bold := $(shell tput bold)
black := $(shell tput setaf 0)
red := $(shell tput setaf 1)
green := $(shell tput setaf 2)
yellow := $(shell tput setaf 3)
blue := $(shell tput setaf 4)
magenta := $(shell tput setaf 5)
cyan := $(shell tput setaf 6)
white := $(shell tput setaf 7)
sgr0 := $(shell tput sgr0)

ifdef MYNAME
MYNAME := $(MYNAME)
else
MYNAME := $(MAKE)
endif

.PHONY: help
help: ## Show this message
## Two tabs to align printed text
	@echo "${bold}Usage: $(MYNAME) <target>${sgr0}" >&2
	@echo 
	@printf "${green}[d]eploy${sgr0}		Deploy Emacs configuration\n"
	@printf "${green}[e]nv${sgr0}			Generate an env file\n"
	@printf "${green}[r]efresh${sgr0}		Refresh configuration:\n"
	@printf "			(Re)build packages if necessary and (re)generate autoloads\n"
	@printf "${green}[s]ync${sgr0}			Sync configuration:\n"
	@printf "			Sync package versions, (re)build packages if necessary,\n"
	@printf "			(re)generate autoloads and cleanup repositories\n"
	@printf "${green}[f]reeze${sgr0}		Freeze package versions\n"
	@printf "${green}[c]ompile${sgr0}		Byte-compile configuration\n"
	@printf "${green}[cl]ean${sgr0}			Clean .elc files\n"
	@printf "${green}[cl]ean-[s]traight${sgr0}	Delete straight repo and build dir\n"
	@printf "${green}[v]alidate${sgr0}		Validate el-patches\n"
	@echo
	@printf "Shortcuts in brackets, which can be combined:\n"
	@printf "$(MYNAME) rc : refresh, compile\n"
	@printf "$(MYNAME) clssc : clean straight, sync, compile\n"


## Shortcuts
d: deploy
e: env
r: refresh
s: sync
f: freeze
c: compile
cl: clean
cls: clean-straight
v: validate
dc: deploy compile
rc: refresh compile
sc: sync compile
clsr: clean-straight refresh
clss: clean-straight sync
clsrc: clean-straight refresh compile
clssc: clean-straight sync compile

.PHONY: deploy
deploy: ## Deploy Emacs configuration
	@$(EMACS) -f +emacs-cli-deploy

.PHONY: env
env: ## Generate env file
	@$(EMACS) --eval "(+emacs-cli-reload-env-file t)"

.PHONY: refresh
refresh: ## Refresh Emacs config:\n
	@$(EMACS) --eval "(+emacs-cli-refresh)"

.PHONY: sync
sync: ## Sync Emacs config:\n
	@$(EMACS) --eval "(+emacs-cli-sync)"

.PHONY: freeze
freeze: ## Freeze package versions
	@$(EMACS) --eval "(+emacs-cli-freeze-packages)"

.PHONY: compile
compile: ## Byte-compile
	@$(EMACS) --eval "(+emacs-cli-byte-compile)"

.PHONY: clean
clean: ## Clean .elc files
	@rm -fv $(CURDIR)/lisp/*.elc
	@rm -fv $(CURDIR)/lisp/modules/*.elc
	@rm -fv $(CURDIR)/site-lisp/*.elc

.PHONY: clean-straight
clean-straight: ## Delete straight repo and build dir
	@rm -rf $(CURDIR)/straight/repos/straight.el
	@rm -rf $(CURDIR)/straight/build/straight
	@echo Now run 'make refresh' or 'make sync'

.PHONY: validate
validate: ## Validate applied patches
	@$(EMACS) --eval "(setq +emacs-interactive-p t)" \
	--eval "(+emacs-initialize 'force)" \
    --eval "(el-patch-validate-all)"
