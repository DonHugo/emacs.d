;; lisp/init-core-modules.el -*- lexical-binding: t; -*-

;; Detect stale bytecode
;; If Emacs version changed, the bytecode is no longer valid and we must
;; recompile.
(eval
 `(unless (equal
           (list
            (emacs-version)
            (concat
             (file-name-sans-extension (file!)) ".el"))
           ',(eval-when-compile
               (list
                (emacs-version)
                (file!))))
    (throw 'stale-bytecode nil)))

(eval-when-compile
  (require 'el-patch))

;;
;;; completion

;;;; company

(module! (:completion company)

  (use-package company
    :commands (company-complete-common
               company-complete-common-or-cycle
               company-manual-begin
               company-grab-line)
    :hook (+emacs-first-input . global-company-mode)
    :init
    (setq company-minimum-prefix-length 2
          company-tooltip-limit 14
          ;; When candidates in the autocompletion tooltip have additional
          ;; metadata, like a type signature, align that information to the
          ;; right-hand side. This usually makes it look neater.
          company-tooltip-align-annotations t
          company-require-match 'never
          company-global-modes
          '(not erc-mode message-mode help-mode gud-mode vterm-mode)
          company-frontends '(company-pseudo-tooltip-frontend
                              company-echo-metadata-frontend)
          company-backends '(company-capf)
          ;; Only search the current buffer for `company-dabbrev', prevents
          ;; Company from causing lag once a lot of buffers are open.
          company-dabbrev-other-buffers nil
          ;; Make `company-dabbrev' fully case-sensitive
          company-dabbrev-ignore-case nil
          company-dabbrev-downcase nil)
    :config
    (when (featurep! :editor evil)
      (add-hook 'company-mode-hook #'evil-normalize-keymaps)
      (unless (featurep! :completion company +childframe)
        (defhook! +company--hook-abort ()
          "Don't persist company popups when switching back to
normal mode. `company-box' aborts on mode switch so it doesn't
need this."
          'evil-normal-state-entry-hook
          ;; HACK `company-abort' doesn't no-op if company isn't active; causing
          ;;      unwanted side-effects, like the suppression of messages in the
          ;;      echo-area.
          (when company-candidates
            (company-abort))))

      (defadvice! +company--advice-abort-previous (&rest _)
        "Allow switching of backends on the fly."
        :before #'company-begin-backend
        (company-abort)))

    (add-hook 'after-change-major-mode-hook #'+company--hook-init-backends 'append)

    ;; NOTE Ensure `company-emulation-alist' is the first item of
    ;;      `emulation-mode-map-alists', thus higher priority than keymaps of
    ;;      evil-mode. We raise the priority of company-mode keymaps
    ;;      unconditionally even when completion is not activated. This should
    ;;      not cause problems, because when completion is activated, the value
    ;;      of `company-emulation-alist' is ((t . company-my-keymap)), when
    ;;      completion is not activated, the value is ((t . nil)).
    (add-hook! 'evil-local-mode-hook
      (when (memq 'company-emulation-alist emulation-mode-map-alists)
        (company-ensure-emulation-alist)))

    ;; Allow eldoc to trigger after completions.
    (after! eldoc
      (eldoc-add-command 'company-complete-selection
                         'company-complete-common
                         'company-abort))

    ;; Allow `company-dabbrev' and `company-dabbrev-code' to work with other
    ;; backends
    ;; From https://github.com/dsedivec/dot-emacs-d/blob/master/recipes/company-dabbrev-code-work-with-other-prefixes.el
    (defvar +company-dabbrev--grouped-completions-prefix nil
      "Completion prefix already selected. Set, by advice, only
within a call to `company--multi-backend-adapter-candidates'.")

    (defadvice! +company-dabbrev--advice-grouped-completions-prefix (orig-fun backends prefix &rest args)
      "TODO"
      :around
      #'company--multi-backend-adapter-candidates
      (let ((+company-dabbrev--grouped-completions-prefix prefix))
        (apply orig-fun backends prefix args)))

    (defadvice! +company-dabbrev--advice-always-add-candidates-in-grouped-completion
      (orig-fun &optional command &rest args)
      "TODO"
      :around
      '(company-dabbrev-code company-dabbrev)
      (if (not +company-dabbrev--grouped-completions-prefix)
          ;; Don't behave any differently than usual when used in contexts other
          ;; than during a call to `company--multi-backend-adapter-candidates'.
          ;; See our advice, above.
          (apply orig-fun command args)
        (cl-case command
          (prefix
           (if (apply orig-fun command args)
               ;; Use whatever prefix was already chosen. See previous advice on
               ;; `company--multi-backend-adapter-candidates'.
               +company-dabbrev--grouped-completions-prefix
             ;; `company-dabbrev-code' doesn't want to play here at all, so
             ;; return nil for prefix, meaning we won't be consulted for
             ;; candidates next.
             nil))
          (candidates
           ;; Use the prefix that `company-dabbrev-code' would normally use
           ;; instead of whatever else was chosen. Need to then graft on the
           ;; rest of the chosen prefix to match any completions from
           ;; `company-dabbrev-code' because `company--insert-candidate' is
           ;; going to expect to replace that previously-chosen prefix if we
           ;; insert one of these candidates. (Plus the candidates with
           ;; different (e.g. missing) prefix from `company-dabbrev-code' will
           ;; look dumb next to others in the list.)
           (let* ((real-prefix +company-dabbrev--grouped-completions-prefix)
                  (dabbrev-prefix (funcall orig-fun 'prefix)))
             ;; For the case that the backend returns 'stop
             (setq dabbrev-prefix (if (symbolp dabbrev-prefix)
                                      (symbol-name dabbrev-prefix)
                                    dabbrev-prefix))
             (mapcar (lambda (cand)
                       ;; For speed, not copying text properties here. May be a
                       ;; bad idea? Right now `company-dabbrev-code' doesn't use
                       ;; any text properties AFAIK, and AFAIK the prefix
                       ;; shouldn't have any properties either.
                       (concat real-prefix
                               (substring cand (length dabbrev-prefix))))
                     (apply orig-fun command dabbrev-prefix (cdr args)))))
          (t
           (apply orig-fun command args)))))

    ;; Remove duplicates from candidate list
    ;; From https://github.com/dsedivec/dot-emacs-d/blob/master/recipes/company-remove-duplicates-ignoring-annotations.el
    (defun +company-remove-duplicates-ignoring-annotations (candidates)
      "Reorder company candidates, removing any duplicates.
cand-1 is a duplicate of cand-2 if (string= cand-1 cand-2). Note
that this ignores text properties, such as the company-backend
text property as well as any annotation-related properties. This
is desirable to, for example, remove duplicate candidates when
using `company-dabbrev-code' grouped with other, (presumably)
more intelligent backends. In fact, this function will also
replace a candidate from `company-dabbrev-code' with any other
`string=' candidate. Order of candidates is preserved (which is
usually important/desirable, particularly when using something
like company-prescient). If a `company-dabbrev-code' candidate
has a duplicate later in the of candidates, the
`company-dabbrev-code' candidate will be replaced by the
candidate that appears later in the list."
      (let* ((default-backend (if (listp company-backend)
                                  (car company-backend)
                                company-backend))
             (best-cands (make-hash-table :test #'equal))
             has-duplicates)
        ;; First pass: Put the best candidate in hash table best-cands.
        ;; Candidates from `company-dabbrev-code' backend are worse than all
        ;; other candidates. Aside from that rule, first candidate == best
        ;; candidate.
        (dolist (cand candidates)
          (pcase-let* ((cand-backend (or (get-text-property 0 'company-backend cand)
                                         default-backend))
                       (cand-prio (cond
                                   ((or (eq cand-backend 'company-dabbrev-code)
                                        (eq cand-backend 'company-dabbrev)) -10)
                                   (t 0)))
                       (`(,best-cand . ,best-prio) (gethash cand best-cands)))
            (if (not best-cand)
                (puthash cand (cons cand cand-prio) best-cands)
              (setq has-duplicates t)
              (when (> cand-prio best-prio)
                (puthash cand (cons cand cand-prio) best-cands)))))
        (if has-duplicates
            ;; Second pass: Remove duplicates. Replace the first instance of a
            ;; given candidate with its best candidate, e.g. replace a
            ;; `company-dabbrev-code' candidate with a duplicate candidate from
            ;; any other backend.
            (cl-loop
             for cand in candidates
             for (best-cand . best-prio) = (gethash cand best-cands)
             if best-cand
             collect best-cand
             and do (remhash cand best-cands))
          ;; There were no duplicates (maybe a common case), we can just return
          ;; the original list.
          candidates)))

    (add-to-list 'company-transformers #'+company-remove-duplicates-ignoring-annotations))


;;;;; company-tng

  (use-package company-tng
    :when (featurep! :completion company +tng)
    :when (not (featurep! :completion company +childframe))
    :after-call post-self-insert-hook
    :config
    (add-to-list 'company-frontends 'company-tng-frontend)
    (define-key! company-active-map
      "RET"       #'company-complete-selection
      [return]    #'company-complete-selection
      "TAB"       #'company-select-next
      [tab]       #'company-select-next
      [backtab]   #'company-select-previous))


;;;;; company-files

  (after! company-files
    ;; Fix `company-files' completion for org file:* links
    (pushnew! company-files--regexps
              "file:\\(\\(?:\\.\\{1,2\\}/\\|~/\\|/\\)[^\]\n]*\\)"))


;;;;; company-dict

  (use-package company-dict
    :defer t
    :config
    (setq company-dict-dir (expand-file-name "dicts" +emacs-private-dir))
    (defhook! +company--hook-enable-project-dicts (mode &rest _)
      "Enable per-project dictionaries."
      '+emacs-project-hook
      (if (symbol-value mode)
          (add-to-list 'company-dict-minor-mode-list mode nil #'eq)
        (setq company-dict-minor-mode-list (delq mode company-dict-minor-mode-list))))))


;;;; helm-base

(when-compiletime! (or (featurep! :org org +ref)
                       (featurep! :org org +org-ql))

  ;; If this gets loaded, set module
  (modules! :completion helm-base)

  (after! helm
    (setq helm-candidate-number-limit 50
          ;; Show input on top of helm
          helm-echo-input-in-header-line t
          ;; Allow mouse selection
          helm-allow-mouse t
          ;; Remove extraineous helm UI elements
          helm-display-header-line nil
          helm-mode-line-string nil
          helm-ff-auto-update-initial-value nil
          helm-find-files-doc-header nil
          ;; Default helm window sizes
          helm-display-buffer-default-width nil
          helm-display-buffer-default-height 0.25
          ;; When calling `helm-semantic-or-imenu', don't immediately jump to
          ;; symbol at point
          helm-imenu-execute-action-at-once-if-one nil
          ;; disable special behavior for left/right, M-left/right keys.
          helm-ff-lynx-style-map nil)

    (when (featurep! :editor evil)
      (setq helm-default-prompt-display-function #'+helm--set-prompt-display))

    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*helm" :vslot -100 :size 0.22 :ttl nil))

    ;; Hide the modeline
    (defun +helm--hide-mode-line (&rest _)
      (with-current-buffer (helm-buffer-get)
        (unless helm-mode-line-string
          (hide-mode-line-mode +1))))
    (add-hook 'helm-after-initialize-hook #'+helm--hide-mode-line)
    (advice-add #'helm-display-mode-line :override #'+helm--hide-mode-line)
    (advice-add #'helm-ag-show-status-default-mode-line :override #'ignore)

    (defhook! +helm-hide-minibuffer-maybe ()
      "Hide minibuffer in Helm session if we use the header line
as input field."
      'helm-minibuffer-set-up-hook
      (when (with-helm-buffer helm-echo-input-in-header-line)
        (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
          (overlay-put ov 'window (selected-window))
          (overlay-put ov 'face
                       (let ((bg-color (face-background 'default nil)))
                         `(:background ,bg-color :foreground ,bg-color)))
          (setq-local cursor-type nil)))))


;;;;; helm-bookmark

  (after! helm-bookmark
    (setq helm-bookmark-show-location t))


;;;;; helm-files

  (after! helm-files
    (setq helm-boring-file-regexp-list
          (append (list "\\.projects$" "\\.DS_Store$")
                  helm-boring-file-regexp-list)))


;;;;; helm-locate

  (defvar helm-generic-files-map (make-sparse-keymap))
  (after! helm-locate
    (when (and IS-MAC
               (null helm-locate-command)
               (executable-find "mdfind"))
      (setq helm-locate-command "mdfind -name %s"))
    (set-keymap-parent helm-generic-files-map helm-map))


;;;;; helm-org

  (after! helm-org
    :config
    (setq helm-org-format-outline-path t)))


;;;; ivy

(module! (:completion ivy)

  (defvar +ivy-buffer-preview nil
    "If non-nil, preview buffers while switching, à la `counsel-switch-buffer'.
When nil, don't preview anything.
When non-nil, preview non-virtual buffers.
When 'everything, also preview virtual buffers")

  (defvar +ivy-task-tags
    '(("TODO" warning bold)
      ("FIXME" error bold)
      ("HACK" font-lock-constant-face bold)
      ("REVIEW" font-lock-keyword-face bold)
      ("PATCH" font-lock-variable-name-face bold)
      ("NOTE" success bold)
      ("BUG" error bold)
      ("DEPRECATED" font-lock-doc-face bold)
      ("XXX" font-lock-constant-face bold))
    "An alist of tags for `+ivy/tasks' to include in its search,
whose CDR is the face to render it with.")

  (defvar +ivy-project-search-engines '(rg ag)
    "What search tools for `+ivy/project-search' (and
`+ivy-file-search' when no ENGINE is specified) to try, and in
what order. To disable a particular tool, remove it from this
list. To prioritize a tool over others, move it to the front of
the list. Later duplicates in this list are silently ignored. If
you want to already use git-grep or grep, set this to nil.")

  (defvar +ivy-buffer-unreal-face 'font-lock-comment-face
    "The face for unreal buffers in `ivy-switch-to-buffer'.")

  (defvar +ivy-edit-functions nil
    "A plist mapping ivy/counsel commands to commands that
generate an editable results buffer.")

  (defvar +ivy-standard-search
    #'ivy--regex-plus
    "Function to use for non-fuzzy search commands.")

  (defvar +ivy-alternative-search
    (cond (#'ivy--regex-ignore-order))
    "Function to use for fuzzy search commands.")

  (use-package ivy
    :hook (+emacs-first-input . ivy-mode)
    :init
    ;; Ignore order for non-fuzzy searches by default
    (setq ivy-re-builders-alist
          `(,@(cl-loop for cmd in '(counsel-ag
                                    counsel-rg
                                    counsel-grep
                                    swiper
                                    swiper-isearch)
                       collect (cons cmd +ivy-standard-search))
            ;; Ignore order for non-fuzzy searches by default
            (t . ,+ivy-alternative-search))
          ivy-more-chars-alist
          '((counsel-rg . 1)
            (counsel-search . 2)
            (t . 3)))
    (define-key!
      [remap switch-to-buffer]              #'+ivy/switch-buffer
      [remap switch-to-buffer-other-window] #'+ivy/switch-buffer-other-window
      [remap persp-switch-to-buffer]        #'+ivy/switch-workspace-buffer
      [remap evil-show-jumps]               #'+ivy/jump-list)
    :config
    ;; slightly longer ivy completions list
    (setq ivy-height 15
          ;; wrap around at end of completions list
          ivy-wrap t
          ;; use consistent height for ivy
          ivy-fixed-height-minibuffer t
          ;; don't show recent files in switch-buffer
          ivy-use-virtual-buffers nil
          ;; ...but if that ever changes, show their full path
          ivy-virtual-abbreviate 'full
          ;; don't quit minibuffer on delete-error
          ivy-on-del-error-function #'ignore
          ;; enable ability to select prompt
          ivy-use-selectable-prompt t
          ivy-count-format "(%d/%d) ")

    (setf (alist-get 't ivy-format-functions-alist)
          #'ivy-format-function-line)

    ;; Ensure a jump point is registered before jumping to new locations with
    ;; ivy
    (setf (alist-get 't ivy-hooks-alist)
          (lambda ()
            (with-ivy-window
              (setq +ivy--origin (point-marker)))))

    (defhook! +ivy--hook-set-jump-point-maybe ()
      "Set jump point from ivy."
      'minibuffer-exit-hook
      (when (markerp (bound-and-true-p +ivy--origin))
        (unless (equal (ignore-errors (with-ivy-window (point-marker)))
                       +ivy--origin)
          (with-current-buffer (marker-buffer +ivy--origin)
            (better-jumper-set-jump +ivy--origin)))
        (set-marker +ivy--origin nil))
      (setq +ivy--origin nil))

    (after! yasnippet
      (add-hook 'yas-prompt-functions #'+ivy-yas-prompt))

    (define-key ivy-minibuffer-map (kbd "C-c C-e") #'+ivy/woccur)

    ;; "Better" dispatching function
    (defun +ivy-dispatching-done ()
      "Select one of the available actions via
`ivy-read-action-ivy' and call `ivy-done'. This variant keeps
track of the original `ivy--directory' and sets it accordingly."
      (interactive)
      (let ((ivy-exit 'ivy-dispatching-done)
            (ivy-read-action-function #'ivy-read-action-ivy)
            (dir ivy--directory))
        (when (ivy-read-action)
          (setq ivy--directory dir)
          (ivy-done)))
      (ivy-shrink-after-dispatching)))


;;;;; counsel

  (use-package counsel
    :defer t
    :init
    (define-key!
      [remap apropos]                  #'counsel-apropos
      [remap bookmark-jump]            #'counsel-bookmark
      [remap compile]                  #'+ivy/compile
      [remap describe-bindings]        #'counsel-descbinds
      [remap describe-face]            #'counsel-faces
      [remap describe-function]        #'counsel-describe-function
      [remap describe-variable]        #'counsel-describe-variable
      [remap evil-ex-registers]        #'counsel-evil-registers
      [remap evil-show-marks]          #'counsel-mark-ring
      [remap execute-extended-command] #'counsel-M-x
      [remap find-file]                #'counsel-find-file
      [remap find-library]             #'counsel-find-library
      [remap imenu]                    #'counsel-imenu
      [remap info-lookup-symbol]       #'counsel-info-lookup-symbol
      [remap load-theme]               #'counsel-load-theme
      [remap locate]                   #'counsel-locate
      [remap org-goto]                 #'counsel-org-goto
      [remap org-set-tags-command]     #'counsel-org-tag
      [remap projectile-compile-project] #'+ivy/project-compile
      [remap recentf-open-files]       #'counsel-recentf
      [remap set-variable]             #'counsel-set-variable
      [remap swiper]                   #'counsel-grep-or-swiper
      [remap insert-char] #'counsel-unicode-char
      [remap yank-pop]                 #'counsel-yank-pop)
    :config
    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*ivy-occur" :size 0.35 :ttl 0 :quit nil))
    (setq ivy-initial-inputs-alist nil
          ;; style outline headings
          counsel-outline-face-style 'org
          counsel-org-goto-all-outline-path-prefix 'file-name-nondirectory)
    (after! savehist
      ;; Persist `counsel-compile' history
      (add-to-list 'savehist-additional-variables 'counsel-compile-history))

    (defadvice! +ivy--advice-counsel-file-jump-use-fd-rg (args)
      "Change `counsel-file-jump' to use fd or ripgrep, if they
are available."
      :override #'counsel--find-return-list
      (cl-destructuring-bind (find-program . args)
          (cond ((when-let (fd (executable-find (or +emacs-projectile-fd-binary "fd") t))
                   (append (list fd "-H" "-I" "--color=never" "--type" "file" "--type" "symlink" "--follow")
                           (if IS-WINDOWS '("--path-separator=/")))))
                ((executable-find "rg" t)
                 (append (list "rg" "--files" "--follow" "--color=never" "--hidden" "-g!.git" "--no-messages")
                         (cl-loop for dir in projectile-globally-ignored-directories
                                  collect "--glob"
                                  collect (concat "!" dir))
                         (if IS-WINDOWS '("--path-separator=/"))))
                ((cons find-program args)))
        (unless (listp args)
          (user-error "`counsel-file-jump-args' is a list now, please customize accordingly."))
        (counsel--call
         (cons find-program args)
         (lambda ()
           (goto-char (point-min))
           (let (files)
             (while (< (point) (point-max))
               (push (buffer-substring (line-beginning-position) (line-end-position))
                     files)
               (forward-line 1))
             (nreverse files))))))

    ;; Integrate with `helpful'
    (setq counsel-describe-function-function #'helpful-callable
          counsel-describe-variable-function #'helpful-variable)

    (add-to-list 'swiper-font-lock-exclude #'+doom-dashboard-mode)
    ;; Record in jumplist when opening files via counsel-{ag,rg,pt,git-grep}
    (add-hook 'counsel-grep-post-action-hook #'better-jumper-set-jump)
    (add-hook 'counsel-grep-post-action-hook #'recenter)
    ;; Configure `counsel-find-file'
    (setq counsel-find-file-ignore-regexp nil)
    (ivy-add-actions
     'counsel-find-file
     '(("p" (lambda (path) (with-ivy-window (insert (file-relative-name path default-directory))))
        "insert relative path")
       ("P" (lambda (path) (with-ivy-window (insert path)))
        "insert absolute path")
       ("l" (lambda (path) (with-ivy-window (insert (format "[[./%s]]" (file-relative-name path default-directory)))))
        "insert relative org-link")
       ("L" (lambda (path) (with-ivy-window (insert (format "[[%s]]" path))))
        "Insert absolute org-link")))

    (ivy-add-actions
     'counsel-ag ; also applies to `counsel-rg'
     '(("O" +ivy-git-grep-other-window-action "open in other window")))

    (ivy-add-actions
     'counsel-outline
     '(("i" +counsel-outline-indirect-action
        "open heading in indirect buffer")))

    (ivy-add-actions
     'counsel-org-goto
     '(("i" +counsel-org-goto-indirect-action
        "open heading in indirect buffer")))

    (ivy-add-actions
     'counsel-org-goto-all
     '(("i" +counsel-org-goto-indirect-action
        "open heading in indirect buffer")))


    (map!
     :map ivy-minibuffer-map
     [escape] #'minibuffer-keyboard-quit))


;;;;; counsel-projectile

  (use-package counsel-projectile
    :defer t
    :init
    (define-key!
      [remap projectile-find-file]        #'+ivy/projectile-find-file
      [remap projectile-find-dir]         #'counsel-projectile-find-dir
      [remap projectile-switch-to-buffer] #'counsel-projectile-switch-to-buffer
      [remap projectile-grep]             #'counsel-projectile-grep
      [remap projectile-ag]               #'counsel-projectile-ag
      [remap projectile-switch-project]   #'counsel-projectile-switch-project)
    :config
    ;; A more sensible `counsel-projectile-find-file' that reverts to
    ;; `counsel-find-file' if invoked from $HOME, `counsel-file-jump' if invoked
    ;; from a non-project, `projectile-find-file' if in a big project (more than
    ;; `ivy-sort-max-size' files), or `counsel-projectile-find-file' otherwise.
    (setf (alist-get 'projectile-find-file counsel-projectile-key-bindings)
          #'+ivy/projectile-find-file)
    ;; no highlighting visited files; slows down the filtering
    (ivy-set-display-transformer #'counsel-projectile-find-file nil)

    ;; Sort projectile files with prescient
    (setq counsel-projectile-sort-files t))


;;;;; ivy-rich

  (use-package ivy-rich
    :after ivy
    :init
    ;; PATCH `ivy-rich-bookmark-filename'
    (el-patch-feature ivy-rich)
    (after! ivy-rich
      (el-patch-defun ivy-rich-bookmark-filename (candidate)
        (el-patch-swap
          (ivy-rich-bookmark-value candidate 'filename)
          (let ((filename (ivy-rich-bookmark-filename candidate)))
            (if (not filename) "" filename)))))

    :config
    (setq ivy-rich-parse-remote-buffer nil)

    ;; Include variable value in `counsel-describe-variable'
    (plist-put! ivy-rich-display-transformers-list
                'counsel-describe-variable
                '(:columns
                  ((counsel-describe-variable-transformer (:width 40)) ; the original transformer
                   (+ivy-rich-describe-variable-transformer (:width 50))
                   (ivy-rich-counsel-variable-docstring (:face font-lock-doc-face))))
                'counsel-M-x
                '(:columns
                  ((counsel-M-x-transformer (:width 60))
                   (ivy-rich-counsel-function-docstring (:face font-lock-doc-face))))
                ;; Apply switch buffer transformers to `counsel-projectile-switch-to-buffer' as well
                'counsel-projectile-switch-to-buffer
                (plist-get ivy-rich-display-transformers-list 'ivy-switch-buffer)
                'counsel-bookmark
                '(:columns
                  ((ivy-rich-candidate (:width 0.5))
                   (ivy-rich-bookmark-filename (:width 60)))))

    ;; Remove built-in coloring of buffer list; we do our own
    (setq ivy-switch-buffer-faces-alist nil)
    (ivy-set-display-transformer 'internal-complete-buffer nil)
    ;; Highlight buffers differently based on whether they're in the same
    ;; project as the current project or not.
    (when-let* ((plist (plist-get ivy-rich-display-transformers-list 'ivy-switch-buffer))
                (switch-buffer-alist (assq 'ivy-rich-candidate (plist-get plist :columns))))
      (setcar switch-buffer-alist '+ivy-rich-buffer-name))

    (ivy-rich-mode +1)
    (ivy-rich-project-root-cache-mode +1))


;;;;; ivy-hydra

  (use-package ivy-hydra
    :commands (ivy-dispatching-done ivy--matcher-desc ivy-hydra/body)
    :config
    ;; Tune the hydra a bit
    (defhydra+ hydra-ivy (:hint nil :color pink)
      "
 Move     ^^^^^^^^^^ | Call         ^^^^ | Cancel^^ | Options^^ | Action _w_/_s_/_a_: %-14s(ivy-action-name)
----------^^^^^^^^^^-+--------------^^^^-+-------^^-+--------^^-+---------------------------------
 _g_   ^ ^ _k_ ^ ^^^ | _f_orward _o_ccur | _i_nsert | _c_alling: %-5s(if ivy-calling \"on\" \"off\") _C_ase-fold: %-10`ivy-case-fold-search
 ^ ^   _h_ ^+^ _l_^^ | _RET_ done     ^^ | _q_uit   | _M_atcher: %-5s(ivy--matcher-desc) _T_runcate: %-11`truncate-lines
 _G_   ^ ^ _j_ ^ ^^^ | _TAB_ alt-done ^^ | ^ ^      | _<_/_>_: shrink/grow
"
      ;; arrows
      ("l" ivy-scroll-up-command)
      ("h" ivy-scroll-down-command)
      ("g" ivy-beginning-of-buffer)
      ("G" ivy-end-of-buffer)
      ;; actions
      ("a" (let ((ivy-read-action-function #'ivy-read-action-ivy))
             (ivy-read-action)))
      ("q" keyboard-escape-quit :exit t)
      ("<escape>" keyboard-escape-quit :exit t)
      ("TAB" ivy-alt-done :exit nil)
      ("RET" ivy-done :exit t)
      ("C-SPC" ivy-call-and-recenter :exit nil)
      ("f" ivy-call)
      ("o" ivy-occur :exit t))

    ;; ivy-hydra rebinds this, so we have to do so again
    (define-key ivy-minibuffer-map (kbd "M-o") #'hydra-ivy/body))


;;;;; swiper

  (after! swiper
    (setq swiper-action-recenter t)
    (map!
     :map swiper-map
     [escape] #'minibuffer-keyboard-quit))


;;;;; amx

  (after! amx
    (setq amx-save-file (concat +emacs-cache-dir "amx-items")))


;;;;; wgrep

  (use-package wgrep
    :commands wgrep-change-to-wgrep-mode
    :config
    (setq wgrep-auto-save-buffer t)))


;;
;;; ui

;;;; dashboard

(module! (:ui dashboard)

  (defvar +doom-dashboard-name "*dashboard*"
    "The name to use for the dashboard buffer.")

  (defvar +doom-dashboard-functions
    '(doom-dashboard-widget-banner
      doom-dashboard-widget-shortmenu
      doom-dashboard-widget-loaded
      doom-dashboard-widget-footer)
    "List of widget functions to run in the dashboard buffer to
construct the dashboard. These functions take no arguments and
the dashboard buffer is current while they run.")

  (defvar +doom-dashboard-banner-file nil ;;"default.png"
    "The path to the image file to be used in on the dashboard.
The path is relative to `+doom-dashboard-banner-dir'. If nil,
always use the ASCII banner.")

  (defvar +doom-dashboard-banner-dir (concat (dir!) "/banners/")
    "Where to look for `+doom-dashboard-banner-file'.")

  (defvar +doom-dashboard-banner-padding '(0 . 4)
    "Number of newlines to pad the banner with, above and below,
respectively.")

  (defvar +doom-dashboard-inhibit-refresh nil
    "If non-nil, the doom buffer won't be refreshed.")

  (defvar +doom-dashboard-inhibit-functions ()
    "A list of functions which take no arguments. If any of them
return non-nil, dashboard reloading is inhibited.")

  (defvar +doom-dashboard-pwd-policy 'last-project
    "The policy to use when setting the `default-directory' in the dashboard.
Possible values:
  'last-project  The `doom-project-root' of the last open buffer. Falls back
                 to `default-directory' if not in a project.
  'last          The `default-directory' of the last open buffer
  a FUNCTION     A function run with the `default-directory' of the last
                 open buffer, that returns a directory path
  a STRING       A fixed path
  nil            `default-directory' will never change")

  (defvar +doom-dashboard-menu-sections
    '(("Reload last session"
       :icon (all-the-icons-octicon "history" :face 'font-lock-keyword-face)
       :when (cond ((featurep! :ui workspaces)
                    (file-exists-p (expand-file-name persp-auto-save-fname persp-save-dir)))
                   ((require 'desktop nil t)
                    (file-exists-p (desktop-full-file-name))))
       :face (:inherit (font-lock-keyword-face bold))
       :action +emacs/quickload-session)
      ("Open org-agenda"
       :icon (all-the-icons-octicon "calendar" :face 'font-lock-keyword-face)
       :when (fboundp 'org-agenda)
       :action org-agenda)
      ("Recently opened files"
       :icon (all-the-icons-octicon "file-text" :face 'font-lock-keyword-face)
       :action recentf-open-files)
      ("Open project"
       :icon (all-the-icons-octicon "briefcase" :face 'font-lock-keyword-face)
       :action projectile-switch-project)
      ("Jump to bookmark"
       :icon (all-the-icons-octicon "bookmark" :face 'font-lock-keyword-face)
       :action bookmark-jump)
      ("Open local configuration"
       :icon (all-the-icons-octicon "tools" :face 'font-lock-keyword-face)
       :when (file-directory-p +emacs-dir)
       :action +emacs/open-local-config))
    "An alist of menu buttons used by
`doom-dashboard-widget-shortmenu'. Each element is a cons
cell (LABEL . PLIST). LABEL is a string to display after the icon
and before the key string. PLIST can have the following
properties:
  :icon FORM
    Uses the return value of FORM as an icon (can be literal string).
  :key STRING
    The keybind displayed next to the button.
  :when FORM
    If FORM returns nil, don't display this button.
  :face FACE
    Displays the icon and text with FACE (a face symbol).
  :action FORM
    Run FORM when the button is pushed.")

  (defvar +doom-dashboard--last-cwd nil)
  (defvar +doom-dashboard--width 80)
  (defvar +doom-dashboard--old-fringe-indicator fringe-indicator-alist)
  (defvar +doom-dashboard--pwd-alist ())
  (defvar +doom-dashboard--reload-timer nil)

  (defvar all-the-icons-scale-factor)
  (defvar all-the-icons-default-adjust)

;;;;; Bootstrap

  (defun +doom-dashboard--hook-init ()
    "Initializes Doom's dashboard."
    (unless noninteractive
      ;; Ensure the dashboard becomes Emacs' go-to buffer when there's nothing
      ;; else to show.
      (setq +emacs-fallback-buffer-name +doom-dashboard-name
            initial-buffer-choice #'+emacs-fallback-buffer)
      (when (equal (buffer-name) "*scratch*")
        (set-window-buffer nil (+emacs-fallback-buffer))
        (+doom-dashboard-reload)))
    ;; Ensure the dashboard is up-to-date whenever it is switched to or resized.
    (add-hook 'window-configuration-change-hook #'+doom-dashboard--hook-resize)
    (add-hook 'window-size-change-functions #'+doom-dashboard--hook-resize)
    (add-hook '+emacs-switch-buffer-hook #'+doom-dashboard--hook-reload-maybe)
    (add-hook 'delete-frame-functions #'+doom-dashboard--hook-reload-frame)
    ;; `persp-mode' integration: update `default-directory' when switching
    ;; perspectives
    (add-hook 'persp-created-functions #'+doom-dashboard--hook-record-project)
    (add-hook 'persp-activated-functions #'+doom-dashboard--hook-detect-project)
    ;; HACK In GUI daemon frames, the dashboard loses center alignment after
    ;;      switching (or killing) workspaces.
    (when (daemonp)
      (add-hook 'persp-activated-functions #'+doom-dashboard--hook-reload-maybe))
    (add-hook 'persp-before-switch-functions #'+doom-dashboard--hook-record-project))

  (add-hook '+emacs-init-ui-hook #'+doom-dashboard--hook-init 'append)

;;;;; Major mode

  (define-derived-mode +doom-dashboard-mode special-mode
    (format "Emacs v%s" emacs-version)
    "Major mode for the DOOM dashboard buffer."
    :syntax-table nil
    :abbrev-table nil
    (buffer-disable-undo)
    (setq truncate-lines t)
    (setq-local whitespace-style nil)
    (setq-local show-trailing-whitespace nil)
    (setq-local hscroll-margin 0)
    (setq-local tab-width 2)
    ;; Don't scroll to follow cursor
    (setq-local scroll-preserve-screen-position nil)
    (setq-local auto-hscroll-mode nil)
    (cl-loop for (car . _cdr) in fringe-indicator-alist
             collect (cons car nil) into alist
             finally do (setq-local fringe-indicator-alist alist))
    ;; Ensure point is always on a button
    (add-hook 'post-command-hook #'+doom-dashboard--hook-reposition-point nil 'local)
    ;; hl-line produces an ugly cut-off line highlight in the dashboard, so don't
    ;; activate it there (by pretending it's already active).
    (setq-local hl-line-mode t)
    ;; Mark `+doom-dashboard' as real
    (add-hook '+doom-dashboard-mode-hook #'+emacs--hook-mark-buffer-as-real))

  (define-key! +doom-dashboard-mode-map
    [left-margin mouse-1]   #'ignore
    [remap forward-button]  #'+doom-dashboard/forward-button
    [remap backward-button] #'+doom-dashboard/backward-button
    "n"       #'forward-button
    "p"       #'backward-button
    "C-n"     #'forward-button
    "C-p"     #'backward-button
    [down]    #'forward-button
    [up]      #'backward-button
    [tab]     #'forward-button
    [backtab] #'backward-button

    ;; Evil remaps
    [remap evil-next-line]     #'forward-button
    [remap evil-previous-line] #'backward-button
    [remap evil-next-visual-line]     #'forward-button
    [remap evil-previous-visual-line] #'backward-button
    [remap evil-paste-pop-next] #'forward-button
    [remap evil-paste-pop]      #'backward-button
    [remap evil-delete]         #'ignore
    [remap evil-delete-line]    #'ignore
    [remap evil-insert]         #'ignore
    [remap evil-append]         #'ignore
    [remap evil-replace]        #'ignore
    [remap evil-replace-state]  #'ignore
    [remap evil-change]         #'ignore
    [remap evil-change-line]    #'ignore
    [remap evil-visual-char]    #'ignore
    [remap evil-visual-line]    #'ignore)

;;;;; Hooks

  (defun +doom-dashboard--hook-reposition-point ()
    "Trap the point in the buttons."
    (when (region-active-p)
      (setq deactivate-mark t)
      (when (bound-and-true-p evil-local-mode)
        (evil-change-to-previous-state)))
    (or (ignore-errors
          (if (button-at (point))
              (forward-button 0)
            (backward-button 1)))
        (progn (goto-char (point-min))
               (forward-button 1))))

  (defun +doom-dashboard--hook-reload-maybe (&rest _)
    "Reload the dashboard or its state. If this isn't a dashboard
buffer, move along, but record its `default-directory' if the
buffer is real. See `+emacs-real-buffer-p' for an explanation for
what 'real' means. If this is the dashboard buffer, reload it
completely."
    (cond ((+doom-dashboard-p (current-buffer))
           (let (+doom-dashboard-inhibit-refresh)
             (ignore-errors (+doom-dashboard-reload))))
          ((+emacs-real-buffer-p (current-buffer))
           (setq +doom-dashboard--last-cwd default-directory)
           (+doom-dashboard-update-pwd))))

  (defun +doom-dashboard--hook-reload-frame (_frame)
    "Reload the dashboard after a brief pause. This is necessary for new frames,
whose dimensions may not be fully initialized by the time this is
run."
    (when (timerp +doom-dashboard--reload-timer)
      (cancel-timer +doom-dashboard--reload-timer)) ; in case this function is run rapidly
    (setq +doom-dashboard--reload-timer (run-with-timer 0.1 nil #'+doom-dashboard-reload t)))

  (defun +doom-dashboard--hook-resize (&rest _)
    "Recenter the dashboard, and reset its margins and fringes."
    (let (buffer-list-update-hook
          window-configuration-change-hook
          window-size-change-functions)
      (let ((windows (get-buffer-window-list (+emacs-fallback-buffer) nil t)))
        (dolist (win windows)
          (set-window-start win 0)
          (set-window-fringes win 0 0)
          (set-window-margins
           win (max 0 (/ (- (window-total-width win) +doom-dashboard--width) 2))))
        (when windows
          (with-current-buffer (+emacs-fallback-buffer)
            (save-excursion
              (with-silent-modifications
                (goto-char (point-min))
                (delete-region (line-beginning-position)
                               (save-excursion (skip-chars-forward "\n")
                                               (point)))
                (insert (make-string
                         (+ (max 0 (- (/ (window-height (get-buffer-window)) 2)
                                      (round (/ (count-lines (point-min) (point-max))
                                                2))))
                            (car +doom-dashboard-banner-padding))
                         ?\n)))))))))

  (defun +doom-dashboard--hook-detect-project (&rest _)
    "Check for a `last-project-root' parameter in the
perspective, and set the dashboard's `default-directory' to it if
it exists. This and `+doom-dashboard--hook-record-project'
provides `persp-mode' integration with the Doom dashboard. It
ensures that the dashboard is always in the correct
project (which may be different across perspective)."
    (when (bound-and-true-p persp-mode)
      (when-let* ((pwd (persp-parameter 'last-project-root)))
        (+doom-dashboard-update-pwd pwd))))

  (defun +doom-dashboard--hook-record-project (&optional persp &rest _)
    "Record the last `doom-project-root' for the current
perspective. See `+doom-dashboard--hook-detect-project' for more
information."
    (when (bound-and-true-p persp-mode)
      (set-persp-parameter
       'last-project-root (+emacs-project-root)
       (if (perspective-p persp)
           persp
         (get-current-persp)))))

;;;;; Library

  (defun +doom-dashboard-initial-buffer ()
    "Returns buffer to display on startup. Designed for
`initial-buffer-choice'."
    (let (buffer-list-update-hook)
      (get-buffer-create +doom-dashboard-name)))

  (defun +doom-dashboard-p (buffer)
    "Returns t if BUFFER is the dashboard buffer."
    (eq buffer (get-buffer +doom-dashboard-name)))

  (defun +doom-dashboard-update-pwd (&optional pwd)
    "Update `default-directory' in the Doom dashboard buffer.
What it is set to is controlled by `+doom-dashboard-pwd-policy'."
    (if pwd
        (with-current-buffer (+emacs-fallback-buffer)
          (+emacs-log "Changed dashboard's PWD to %s" pwd)
          (setq-local default-directory pwd))
      (let ((new-pwd (+doom-dashboard--get-pwd)))
        (when (and new-pwd (file-directory-p new-pwd))
          (unless (string-suffix-p "/" new-pwd)
            (setq new-pwd (concat new-pwd "/")))
          (+doom-dashboard-update-pwd new-pwd)))))

  (defun +doom-dashboard-reload (&optional force)
    "Update the DOOM scratch buffer (or create it, if it doesn't
exist)."
    (when (or (and (not +doom-dashboard-inhibit-refresh)
                   (get-buffer-window (+emacs-fallback-buffer))
                   (not (window-minibuffer-p (frame-selected-window)))
                   (not (run-hook-with-args-until-success '+doom-dashboard-inhibit-functions)))
              force)
      (with-current-buffer (+emacs-fallback-buffer)
        (+emacs-log "Reloading dashboard at %s" (format-time-string "%T"))
        (with-silent-modifications
          (let ((pt (point)))
            (unless (eq major-mode '+doom-dashboard-mode)
              (+doom-dashboard-mode))
            (erase-buffer)
            (run-hooks '+doom-dashboard-functions)
            (goto-char pt)
            (+doom-dashboard--hook-reposition-point))
          (+doom-dashboard--hook-resize)
          (+doom-dashboard--hook-detect-project)
          (+doom-dashboard-update-pwd)
          (current-buffer)))))

;;;;; Helpers

  (defun +doom-dashboard--center (len s)
    (concat (make-string (ceiling (max 0 (- len (length s))) 2) ? )
            s))

  (defun +doom-dashboard--get-pwd ()
    (let ((lastcwd +doom-dashboard--last-cwd)
          (policy +doom-dashboard-pwd-policy))
      (cond ((null policy)
             default-directory)
            ((stringp policy)
             (expand-file-name policy lastcwd))
            ((functionp policy)
             (funcall policy lastcwd))
            ((null lastcwd)
             default-directory)
            ((eq policy 'last-project)
             (or (+emacs-project-root lastcwd)
                 lastcwd))
            ((eq policy 'last)
             lastcwd)
            (t
             ((warn "`+doom-dashboard-pwd-policy' has an invalid value of '%s'"
                    policy))))))

;;;;; Widgets

  (defun doom-dashboard-widget-banner ()
    (let ((point (point)))
      (mapc (lambda (line)
              (insert (propertize (+doom-dashboard--center +doom-dashboard--width line)
                                  'face 'font-lock-comment-face) " ")
              (insert "\n"))
            '("___________   _____      _____   _________    _________"
              "\\_   _____/  /     \\    /  _  \\  \\_   ___ \\  /   _____/"
              " |    __)_  /  \\ /  \\  /  /_\\  \\ /    \\  \\/  \\_____  \\ "
              " |        \\/    Y    \\/    |    \\\\     \\____ /        \\"
              "/_______  /\\____|__  /\\____|__  / \\______  //_______  /"
              "        \\/         \\/         \\/         \\/         \\/ "
              ""
              ""
              "Yay!"
              ""
              ""))
      (when (and (stringp +doom-dashboard-banner-file)
                 (display-graphic-p)
                 ;; (file-exists-p! +doom-dashboard-banner-file
                 ;;                 +doom-dashboard-banner-dir)
                 )
        (let* ((image (create-image (expand-file-name +doom-dashboard-banner-file
                                                      +doom-dashboard-banner-dir)
                                    'png nil))
               (size (image-size image nil))
               (margin (+ 1 (/ (- +doom-dashboard--width (car size)) 2))))
          (add-text-properties
           point (point) `(display ,image rear-nonsticky (display)))
          (when (> margin 0)
            (save-excursion
              (goto-char point)
              (insert (make-string (truncate margin) ? )))))
        (insert (make-string (or (cdr +doom-dashboard-banner-padding) 0) ?\n)))))

  (defun doom-dashboard-widget-loaded ()
    (insert
     "\n\n"
     (propertize
      (+doom-dashboard--center
       +doom-dashboard--width
       (+emacs--hook-display-benchmark 'return))
      'face 'font-lock-comment-face)
     "\n"))

  (defun doom-dashboard-widget-shortmenu ()
    (let ((all-the-icons-scale-factor 1.45)
          (all-the-icons-default-adjust -0.02))
      (insert "\n")
      (dolist (section +doom-dashboard-menu-sections)
        (cl-destructuring-bind (label &key icon action when face) section
          (when (and (fboundp action)
                     (or (null when)
                         (eval when t)))
            (insert
             (+doom-dashboard--center
              (- +doom-dashboard--width 1)
              (let ((icon (if (stringp icon) icon (eval icon t))))
                (format (format "%s%%s%%-10s" (if icon "%3s\t" "%3s"))
                        (or icon "")
                        (with-temp-buffer
                          (insert-text-button
                           label
                           'action
                           `(lambda (_)
                              (call-interactively (or (command-remapping #',action)
                                                      #',action)))
                           'face (or face 'font-lock-keyword-face)
                           'follow-link t
                           'help-echo
                           (format "%s (%s)" label
                                   (propertize (symbol-name action) 'face 'font-lock-constant-face)))
                          (format "%-37s" (buffer-string)))
                        ;; Lookup command keys dynamically
                        (or (when-let (key (where-is-internal action nil t))
                              (with-temp-buffer
                                (save-excursion (insert (key-description key)))
                                (while (re-search-forward "<\\([^>]+\\)>" nil t)
                                  (let ((str (match-string 1)))
                                    (replace-match
                                     (upcase (if (< (length str) 3)
                                                 str
                                               (substring str 0 3))))))
                                (propertize (buffer-string) 'face 'font-lock-constant-face)))
                            ""))))
             (if (display-graphic-p)
                 "\n\n"
               "\n")))))))

  (defun doom-dashboard-widget-footer ()
    (insert
     "\n"
     (+doom-dashboard--center
      (- +doom-dashboard--width 2)
      (with-temp-buffer
        (insert-text-button (or (all-the-icons-octicon "octoface" :face 'all-the-icons-green :height 1.3 :v-adjust -0.15)
                                (propertize "github" 'face 'font-lock-keyword-face))
                            'action (lambda (_) (browse-url "https://gitlab.com/DonHugo/emacs.d"))
                            'follow-link t
                            'help-echo "Open Emacs config gitlab page")
        (buffer-string)))
     "\n")))

;;;; hl-todo

(module! (:ui hl-todo)

  (use-package hl-todo
    :hook (prog-mode . hl-todo-mode)
    :hook (yaml-mode . hl-todo-mode)
    :config
    (setq hl-todo-highlight-punctuation ":"
          hl-todo-keyword-faces
          `(;; For things that need to be done, just not today.
            ("TODO" warning bold)
            ;; For problems that will become bigger problems later if not fixed
            ;; ASAP.
            ("FIXME" error bold)
            ;; For tidbits that are unconventional and not intended uses of the
            ;; constituent parts, and may break in a future update.
            ("HACK" font-lock-constant-face bold)
            ;; For patches defined via `el-patch'
            ("PATCH" font-lock-variable-name-face bold)
            ;; For things that were done hastily and/or hasn't been thoroughly
            ;; tested. It may not even be necessary!
            ("REVIEW" font-lock-keyword-face bold)
            ;; For especially important gotchas with a given implementation,
            ;; directed at another user other than the author.
            ("NOTE" success bold)
            ;; For things that just gotta go and will soon be gone.
            ("DEPRECATED" font-lock-doc-face bold)
            ;; For a known bug that needs a workaround
            ("BUG" error bold)
            ;; For warning about a problematic or misguiding code
            ("XXX" font-lock-constant-face bold)))

    (defadvice! +hl-todo--advice-clamp-font-lock-fontify-region (orig-fn &rest args)
      "Fix an `args-out-of-range' error in some modes."
      :around #'hl-todo-mode
      (letf! (defun font-lock-fontify-region (beg end &optional loudly)
               (funcall font-lock-fontify-region (max beg 1) end loudly))
        (apply orig-fn args)))

    ;; Use a more primitive todo-keyword detection method in major modes that
    ;; don't use/have a valid syntax table entry for comments.
    (defhook! +hl-todo--hook-use-face-detection ()
      "Use a different, more primitive method of locating todo keywords.
This is useful for major modes that don't use or have a valid
syntax-table entry for comment start/end characters."
      (pug-mode haml-mode)
      (set (make-local-variable 'hl-todo-keywords)
           '(((lambda (limit)
                (let (case-fold-search)
                  (and (re-search-forward hl-todo-regexp limit t)
                       (memq 'font-lock-comment-face (+emacs-enlist (get-text-property (point) 'face))))))
              (1 (hl-todo-get-face) t t))))
      (when hl-todo-mode
        (hl-todo-mode -1)
        (hl-todo-mode +1)))))


;;;; evil-goggles

(module! (:ui evil-goggles)

  (use-package evil-goggles
    :hook (+emacs-first-input . evil-goggles-mode)
    :config
    (pushnew! evil-goggles--commands
              '(lispyville-delete :face evil-goggles-delete-face :switch evil-goggles-enable-delete :advice evil-goggles--generic-blocking-advice)
              '(lispyville-delete-line :face evil-goggles-delete-face :switch evil-goggles-enable-delete :advice evil-goggles--delete-line-advice)
              '(lispyville-change :face evil-goggles-change-face :switch evil-goggles-enable-change :advice evil-goggles--generic-blocking-advice)
              '(lispyville-change-line :face evil-goggles-change-face :switch evil-goggles-enable-change :advice evil-goggles--generic-blocking-advice)
              '(lispyville-change-whole-line :face evil-goggles-change-face :switch evil-goggles-enable-change :advice evil-goggles--generic-blocking-advice)
              '(lispyville-yank :face evil-goggles-yank-face :switch evil-goggles-enable-yank :advice evil-goggles--generic-async-advice)
              '(lispyville-yank-line :face evil-goggles-yank-face :switch evil-goggles-enable-yank :advice evil-goggles--generic-async-advice)
              '(lispyville-indent :face evil-goggles-indent-face :switch evil-goggles-enable-indent :advice evil-goggles--generic-async-advice)
              '(lispyville-join :face evil-goggles-join-face :switch evil-goggles-enable-join :advice evil-goggles--join-advice)
              '(lispyville-comment-or-uncomment :face evil-goggles-nerd-commenter-face :switch evil-goggles-enable-nerd-commenter :advice evil-goggles--generic-async-advice)
              '(lispyville-prettify :face evil-goggles-indent-face :switch evil-goggles-enable-indent :advice evil-goggles--generic-async-advice)
              '(evil-cp-yank :face evil-goggles-yank-face :switch evil-goggles-enable-yank :advice evil-goggles--generic-async-advice)
              '(evil-cp-delete :face evil-goggles-delete-face :switch evil-goggles-enable-delete :advice evil-goggles--generic-blocking-advice)
              '(evil-cp-change :face evil-goggles-change-face :switch evil-goggles-enable-change :advice evil-goggles--generic-blocking-advice)
              '(evil-cp-yank-line :face evil-goggles-yank-face :switch evil-goggles-enable-yank :advice evil-goggles--generic-async-advice)
              '(evil-cp-delete-line :face evil-goggles-delete-face :switch evil-goggles-enable-delete :advice evil-goggles--delete-line-advice)
              '(evil-cp-change-line :face evil-goggles-change-face :switch evil-goggles-enable-change :advice evil-goggles--generic-blocking-advice)
              '(evil-cp-change-whole-line :face evil-goggles-change-face :switch evil-goggles-enable-change :advice evil-goggles--generic-blocking-advice)
              '(evil-delete-backward-word :face evil-goggles-delete-face :switch evil-goggles-enable-delete :advice evil-goggles--generic-blocking-advice)
              '(evil-magit-yank-whole-line :face evil-goggles-yank-face :switch evil-goggles-enable-yank :advice evil-goggles--generic-async-advice)
              '(+evil:yank-unindented :face evil-goggles-yank-face :switch evil-goggles-enable-yank :advice evil-goggles--generic-async-advice)
              '(+eval:region :face evil-goggles-yank-face :switch evil-goggles-enable-yank :advice evil-goggles--generic-async-advice))

    (protect-macros!
      (custom-set-faces!
        '(evil-goggles-default-face          :inherit magit-blame-highlight)
        '(evil-goggles-delete-face           :inherit magit-diff-removed-highlight)
        '(evil-goggles-change-face           :inherit magit-diff-removed-highlight)
        '(evil-goggles-paste-face            :inherit magit-diff-added-highlight)
        '(evil-goggles-yank-face             :inherit magit-diff-base-highlight)
        '(evil-goggles-undo-redo-remove-face :inherit magit-diff-removed-highlight)
        '(evil-goggles-undo-redo-add-face    :inherit magit-diff-added-highlight)
        '(evil-goggles-undo-redo-change-face :inherit magit-diff-base-highlight)))))


;;;; highlight-symbol

(module! (:ui highlight-symbol)

  (use-package auto-highlight-symbol
    :commands (ahs-highlight-p)
    :init
    (setq ahs-case-fold-search nil
          ahs-default-range 'ahs-range-whole-buffer
          ahs-idle-interval 0.25
          ahs-inhibit-face-list nil)
    ;; Since we are creating our own maps, prevent the default keymap from
    ;; getting created
    (setq auto-highlight-symbol-mode-map (make-sparse-keymap))
    :config
    (defvar-local +highlight-symbol-last-ahs-highlight-p nil
      "Info on the last searched highlighted symbol.")
    (defvar-local +highlight-symbol--ahs-searching-forward t)

    (ahs-regist-range-plugin
        beginning-of-defun
      '((name          . "beginning of defun")
        (lighter       . "HSD")
        (face          . ahs-plugin-bod-face)
        (before-search . (lambda (symbol)
                           (save-excursion
                             (let ((pos (funcall ahs-plugin-bod-function)))
                               (if (not (consp pos))
                                   'abort
                                 (setq ahs-plugin-bod-start (car pos))
                                 (setq ahs-plugin-bod-end   (cdr pos)))))))
        (start         . ahs-plugin-bod-start)
        (end           . ahs-plugin-bod-end))
      "beginning-of-defun to end-of-defun."))

  (map!
   :nv "*" #'+highlight-symbol/start
   :nv "#" #'+highlight-symbol/start))


;;;; popup

(module! (:ui popup)

  (defconst +popup-window-parameters '(ttl quit select modeline popup)
    "A list of custom parameters to be added to `window-persistent-parameters'.
Modifying this has no effect, unless done before ui/popup
loads.")

  (defvar +popup-default-display-buffer-actions
    '(+popup-display-buffer-stacked-side-window)
    "The functions to use to display the popup buffer.")

  (defvar +popup-default-alist
    '((window-height . 0.16) ; remove later
      (reusable-frames . visible))
    "The default alist for `display-buffer-alist' rules.")

  (defvar +popup-default-parameters
    '((transient . t)   ; remove later
      (quit . t)        ; remove later
      (select . ignore) ; remove later
      (no-other-window . t))
    "The default window parameters.")

  (defvar +popup-margin-width 1
    "Size of the margins to give popup windows. Set this to nil
to disable margin adjustment.")

  (defvar +popup--inhibit-transient nil)
  (defvar +popup--inhibit-select nil)
  (defvar +popup--old-display-buffer-alist nil)
  (defvar +popup--remember-last t)
  (defvar +popup--last nil)
  (defvar-local +popup--timer nil)

;;;;; Global modes

  (defvar +popup-mode-map (make-sparse-keymap)
    "Active keymap in a session with the popup system enabled.
See `+popup-mode'.")

  (defvar +popup-buffer-mode-map
    (let ((map (make-sparse-keymap)))

      ;; For maximum escape coverage in emacs state buffers; this only works in
      ;; GUI Emacs, in tty Emacs use C-g instead
      (define-key map [escape] #'+emacs/escape)
      map)
    "Active keymap in popup windows. See `+popup-buffer-mode'.")

  (define-minor-mode +popup-mode
    "Global minor mode representing the popup management system."
    :init-value nil
    :global t
    :keymap +popup-mode-map
    (cond (+popup-mode
           (add-hook '+emacs-escape-hook #'+popup--hook-close-on-escape t)
           (setq +popup--old-display-buffer-alist display-buffer-alist
                 display-buffer-alist +popup--display-buffer-alist
                 window--sides-inhibit-check t)
           (dolist (prop +popup-window-parameters)
             (push (cons prop 'writable) window-persistent-parameters)))
          (t
           (remove-hook '+emacs-escape-hook #'+popup--hook-close-on-escape)
           (setq display-buffer-alist +popup--old-display-buffer-alist
                 window--sides-inhibit-check nil)
           (+popup-cleanup-rules-h)
           (dolist (prop +popup-window-parameters)
             (delq (assq prop window-persistent-parameters)
                   window-persistent-parameters)))))

  (define-minor-mode +popup-buffer-mode
    "Minor mode for individual popup windows.
It is enabled when a buffer is displayed in a popup window and
disabled when that window has been changed or closed."
    :init-value nil
    :keymap +popup-buffer-mode-map
    (if (not +popup-buffer-mode)
        (remove-hook 'after-change-major-mode-hook #'+popup--hook-set-modeline-on-enable t)
      (add-hook 'after-change-major-mode-hook #'+popup--hook-set-modeline-on-enable
                nil 'local)
      (when (timerp +popup--timer)
        (remove-hook 'kill-buffer-hook #'+popup--hook-kill-buffer-hook t)
        (cancel-timer +popup--timer)
        (setq +popup--timer nil))))

  (put '+popup-buffer-mode 'permanent-local t)
  (put '+popup-buffer-mode 'permanent-local-hook t)
  (put '+popup--hook-set-modeline-on-enable 'permanent-local-hook t)

;;;;; Macros

  (defmacro with-popup-rules! (rules &rest body)
    "Evaluate BODY with popup RULES. RULES is a list of popup
rules. Each rule should match the arguments of `+popup-define' or
the :popup setting."
    (declare (indent defun))
    `(let ((+popup--display-buffer-alist +popup--old-display-buffer-alist)
           display-buffer-alist)
       (set-popup-rules! ,rules)
       (when (bound-and-true-p +popup-mode)
         (setq display-buffer-alist +popup--display-buffer-alist))
       ,@body))

  (defmacro save-popups! (&rest body)
    "Sets aside all popups before executing the original
function, usually to prevent the popup(s) from messing up the
UI (or vice versa)."
    `(let* ((in-popup-p (+popup-buffer-p))
            (popups (+popup-windows))
            (+popup--inhibit-transient t)
            buffer-list-update-hook
            +popup--last)
       (dolist (p popups)
         (+popup/close p 'force))
       (unwind-protect
           (progn ,@body)
         (when popups
           (let ((origin (selected-window)))
             (+popup/restore)
             (unless in-popup-p
               (select-window origin)))))))

;;;;; Default popup rules & bootstrap

  (set-popup-rules!
    ;; more specific rules
    (when (featurep! :ui popup)
      '(("^\\*Completions" :ignore t)
        ("^\\*Local variables\\*$"
         :vslot -1 :slot 1 :size +popup-shrink-to-fit)
        ("^\\*\\(?:[Cc]ompil\\(?:ation\\|e-Log\\)\\|Messages\\)"
         :vslot -2 :size 0.3  :autosave t :quit t :ttl nil)
        ("^\\*\\(?:\\+emacs \\|Pp E\\)"  ; transient buffers (no interaction required)
         :vslot -3 :size +popup-shrink-to-fit :autosave t :select ignore :quit t :ttl 0)
        ("^\\*\\+emacs:"  ; editing buffers (interaction required)
         :vslot -4 :size 0.35 :autosave t :select t :modeline t :quit nil :ttl t)
        ("^\\*\\+emacs:\\(?:v?term\\|eshell\\)-popup"  ; editing buffers (interaction required)
         :vslot -5 :size 0.35 :select t :modeline t :quit nil :ttl nil)
        ("^\\*\\(?:Wo\\)?Man "
         :vslot -6 :size 0.45 :select t :quit t :ttl 0)
        ("^\\*Calc"
         :vslot -7 :side bottom :size 0.4 :select t :quit nil :ttl 0)
        ("^\\*Customize"
         :slot 2 :side right :size +popup-shrink-to-fit :select t :quit nil)
        ("^ \\*undo-tree\\*"
         :slot 2 :side left :size 20 :select t :quit t)
        ;; `help-mode', `helpful-mode'
        ("^\\*\\([Hh]elp\\|Apropos\\)"
         :slot 2 :vslot -8 :size 0.35 :select t)
        ("^\\*eww\\*"  ; `eww' (and used by dash docsets)
         :vslot -11 :size 0.42 :select t)
        ;; ("^\\*eww\\*"  ; `eww' (and used by dash docsets)
        ;;  :ignore t)
        ("^\\*info\\*$"  ; `Info-mode'
         :slot 2 :vslot 2 :size 0.45 :select t)))
    '(("^\\*Warnings" :vslot 99 :size 0.25)
      ("^\\*Backtrace" :vslot 99 :size 0.4 :quit nil)
      ("^\\*CPU-Profiler-Report "    :side bottom :vslot 100 :slot 1 :height 0.4 :width 0.5 :quit nil)
      ("^\\*Memory-Profiler-Report " :side bottom :vslot 100 :slot 2 :height 0.4 :width 0.5 :quit nil)
      ("^\\*\\(?:Proced\\|timer-list\\|Process List\\|Abbrevs\\|Output\\|Occur\\|unsent mail\\)\\*" :ignore t)))

  (add-hook '+emacs-init-ui-hook #'+popup-mode 'append)

  (add-hook! '+popup-buffer-mode-hook
             #'+popup--hook-adjust-fringes
             #'+popup--hook-adjust-margins
             #'+popup--hook-set-modeline-on-enable
             #'+popup--hook-unset-modeline-on-disable)

;;;;; Hacks

  ;; Hacks should be kept in alphabetical order, named after the feature they
  ;; modify, and should follow an `after!' or `use-package'.

;;;;;; Core functions

  (defadvice! +popup--advice-make-case-sensitive (orig-fn &rest args)
    "Make regexps in `display-buffer-alist' case-sensitive.
To reduce fewer edge cases and improve performance when
`display-buffer-alist' grows larger."
    :around #'display-buffer-assq-regexp
    (let (case-fold-search)
      (apply orig-fn args)))

  ;; Don't try to resize popup windows
  (advice-add #'balance-windows :around #'+popup--advice-save)

  (defun +popup/quit-window ()
    "The regular `quit-window' sometimes kills the popup buffer
and switches to a buffer that shouldn't be in a popup. We prevent
that by remapping `quit-window' to this commmand."
    (interactive)
    (let ((orig-buffer (current-buffer)))
      (quit-window)
      (when (and (eq orig-buffer (current-buffer))
                 (+popup-window-p))
        (+popup/close nil 'force))))
  (global-set-key [remap quit-window] #'+popup/quit-window)

  (defadvice! +popup--advice-override-display-buffer-alist (orig-fn &rest args)
    "When `pop-to-buffer' is called with non-nil ACTION, that
ACTION should override `display-buffer-alist'."
    :around '(switch-to-buffer-other-tab
              switch-to-buffer-other-window
              switch-to-buffer-other-frame)
    (let ((display-buffer-alist nil))
      (apply orig-fn args)))

;;;;;; External functions

;;;;;;; buff-menu

;;;###package buff-menu
  (define-key Buffer-menu-mode-map (kbd "RET") #'Buffer-menu-other-window)

;;;;;;; company

;;;###package company
  (defadvice! +popup--advice-dont-select-me (orig-fn &rest args)
    "Don't select popup window."
    :around #'company-show-doc-buffer
    (let ((+popup--inhibit-select t))
      (apply orig-fn args)))

;;;;;;; eshell

;;;###package eshell
  (progn
    (setq eshell-destroy-buffer-when-process-dies t)

    ;; When eshell runs a visual command (see `eshell-visual-commands'), it
    ;; spawns a term buffer to run it in, but where it spawns it is the
    ;; problem...
    (defadvice! +popup--advice-eshell-undedicate-popup (&rest _)
      "Force spawned term buffer to share with the eshell
popup (if necessary)."
      :before #'eshell-exec-visual
      (when (+popup-window-p)
        (set-window-dedicated-p nil nil)
        (add-transient-hook! #'eshell-query-kill-processes :after
                             (set-window-dedicated-p nil t)))))

;;;;;;; evil

;;;###package evil
  (progn
    ;; Make evil-mode cooperate with popups
    (defadvice! +popup--advice-evil-command-window (hist cmd-key execute-fn)
      "Monkey patch the evil command window to use
`pop-to-buffer' instead of `switch-to-buffer', allowing the popup
manager to handle it."
      :override #'evil-command-window
      (when (eq major-mode 'evil-command-window-mode)
        (user-error "Cannot recursively open command line window"))
      (dolist (win (window-list))
        (when (equal (buffer-name (window-buffer win))
                     "*Command Line*")
          (kill-buffer (window-buffer win))
          (delete-window win)))
      (setq evil-command-window-current-buffer (current-buffer))
      (ignore-errors (kill-buffer "*Command Line*"))
      (with-current-buffer (pop-to-buffer "*Command Line*")
        (setq-local evil-command-window-execute-fn execute-fn)
        (setq-local evil-command-window-cmd-key cmd-key)
        (evil-command-window-mode)
        (evil-command-window-insert-commands hist)))

    (defadvice! +popup--advice-evil-command-window-execute ()
      "Execute the command under the cursor in the appropriate
buffer, rather than the command buffer."
      :override #'evil-command-window-execute
      (interactive)
      (let ((result (buffer-substring (line-beginning-position)
                                      (line-end-position)))
            (execute-fn evil-command-window-execute-fn)
            (execute-window (get-buffer-window evil-command-window-current-buffer))
            (popup (selected-window)))
        (if execute-window
            (select-window execute-window)
          (user-error "Originating buffer is no longer active"))
        ;; (kill-buffer "*Command Line*")
        (delete-window popup)
        (funcall execute-fn result)
        (setq evil-command-window-current-buffer nil)))

    ;; Don't mess with popups
    (advice-add #'+evil--window-swap           :around #'+popup--advice-save)
    (advice-add #'evil-window-move-very-bottom :around #'+popup--advice-save)
    (advice-add #'evil-window-move-very-top    :around #'+popup--advice-save)
    (advice-add #'evil-window-move-far-left    :around #'+popup--advice-save)
    (advice-add #'evil-window-move-far-right   :around #'+popup--advice-save))

;;;;;;; help-mode

;;;###package help-mode
  (after! help-mode
    (defun +popup--switch-from-popup (location)
      (let (origin enable-local-variables)
        (save-popups!
         (switch-to-buffer (car location) nil t)
         (if (not (cdr location))
             (message "Unable to find location in file")
           (goto-char (cdr location))
           (recenter)
           (setq origin (selected-window))))
        (select-window origin)))

    ;; Help buffers use `pop-to-window' to decide where to open followed links,
    ;; which can be unpredictable. It should *only* replace the original buffer
    ;; we opened the popup from. To fix this these three button types need to be
    ;; redefined to set aside the popup before following a link.
    (define-button-type 'help-function-def
      :supertype 'help-xref
      'help-function
      (lambda (fun file)
        (require 'find-func)
        (when (eq file 'C-source)
          (setq file (help-C-file-name (indirect-function fun) 'fun)))
        (+popup--switch-from-popup (find-function-search-for-symbol fun nil file))))

    (define-button-type 'help-variable-def
      :supertype 'help-xref
      'help-function
      (lambda (var &optional file)
        (when (eq file 'C-source)
          (setq file (help-C-file-name var 'var)))
        (+popup--switch-from-popup (find-variable-noselect var file))))

    (define-button-type 'help-face-def
      :supertype 'help-xref
      'help-function
      (lambda (fun file)
        (require 'find-func)
        (+popup--switch-from-popup (find-function-search-for-symbol fun 'defface file)))))

;;;;;;; helpful

;;;###package helpful
  (defadvice! +popup--advice-helpful-open-in-origin-window (button)
    "Open links in non-popup, originating window rather than
helpful's window."
    :override #'helpful--navigate
    (let ((path (substring-no-properties (button-get button 'path)))
          enable-local-variables
          origin)
      (save-popups!
       (find-file path)
       (when-let (pos (get-text-property button 'position
                                         (marker-buffer button)))
         (goto-char pos))
       (setq origin (selected-window))
       (recenter))
      (select-window origin)))

;;;;;;; helm

;;;###package helm
;;;###package helm-ag
  (when (or (featurep! :completion helm)
            (featurep! :completion helm-base))
    (setq helm-default-display-buffer-functions '(+popup-display-buffer-stacked-side-window))

    ;; Fix #897: "cannot open side window" error when TAB-completing file links
    (defadvice! +popup--helm-hide-org-links-popup-a (orig-fn &rest args)
      "Fix \"cannot open side window\" error when TAB-completing
      file links"
      :around #'org-insert-link
      (letf! ((defun org-completing-read (&rest args)
                (when-let (win (get-buffer-window "*Org Links*"))
                  ;; While helm is opened as a popup, it will mistaken the *Org
                  ;; Links* popup for the "originated window", and will target
                  ;; it for actions invoked by the user. However, since *Org
                  ;; Links* is a popup too (they're dedicated side windows),
                  ;; Emacs complains about being unable to split a side window.
                  ;; The simple fix: get rid of *Org Links*!
                  (delete-window win)
                  ;; ...but it must exist for org to clean up later.
                  (get-buffer-create "*Org Links*"))
                (apply org-completing-read args)))
        (apply #'funcall-interactively orig-fn args)))

    ;; Fix left-over popup window when closing persistent help for `helm-M-x'
    (defadvice! +popup--helm-elisp--persistent-help-a (candidate _fun &optional _name)
      "Fix left-over popup window when closing persistent help
for `helm-M-x'"
      :before #'helm-elisp--persistent-help
      (let (win)
        (and (helm-get-attr 'help-running-p)
             (string= candidate (helm-get-attr 'help-current-symbol))
             (setq win (get-buffer-window (get-buffer (help-buffer))))
             (delete-window win)))))

;;;;;;; ibuffer

;;;###package ibuffer
  (setq ibuffer-use-other-window t)

;;;;;;; info

;;;###package Info
  (defadvice! +popup--advice-switch-to-info-window (&rest _)
    "Switch to info window."
    :after #'info-lookup-symbol
    (when-let (win (get-buffer-window "*info*"))
      (when (+popup-window-p win)
        (select-window win))))

;;;;;;; latex

;;;###package latex
  (defadvice! +popup--advice-use-popup-window-for-reftex-citation (fn &rest args)
    "Use popup for reftex."
    :around #'reftex-do-citation
    (letf! ((#'switch-to-buffer-other-window #'pop-to-buffer))
      (apply fn args)))

;;;;;;; org

;;;###package org
  (after! org
    (defvar +popup--disable-internal nil)
    ;; Org has a scorched-earth window management system I'm not fond of. i.e.
    ;; it kills all windows and monopolizes the frame. No thanks. We can do
    ;; better ourselves.
    (defadvice! +popup--advice-suppress-delete-other-windows (orig-fn &rest args)
      "Suppress `delete-other-windows'."
      :around '(org-add-log-note
                org-capture-place-template
                org-export--dispatch-ui
                org-agenda-get-restriction-and-command
                org-fast-tag-selection
                org-fast-todo-selection)
      (if +popup-mode
          (letf! ((#'delete-other-windows #'ignore)
                  (#'delete-window        #'ignore))
            (apply orig-fn args))
        (apply orig-fn args)))

    (defadvice! +popup--advice-org-fix-goto (orig-fn &rest args)
      "`org-goto' uses `with-output-to-temp-buffer' to display its help buffer,
for some reason, which is very unconventional, and so requires
these gymnastics to tame (i.e. to get the popup manager to handle
it)."
      :around #'org-goto-location
      (if +popup-mode
          (letf! (defun internal-temp-output-buffer-show (buffer)
                   (let ((temp-buffer-show-function
                          (+emacs-rpartial #'+popup-display-buffer-stacked-side-window nil)))
                     (with-current-buffer buffer
                       (+popup-buffer-mode +1))
                     (funcall internal-temp-output-buffer-show buffer)))
            (apply orig-fn args))
        (apply orig-fn args)))

    (defadvice! +popup--advice-org-fix-popup-window-shrinking (orig-fn &rest args)
      "Hides the mode-line in *Org tags* buffer so you can
actually see its content and displays it in a side window without
deleting all other windows. Ugh, such an ugly hack."
      :around '(org-fast-tag-selection
                org-fast-todo-selection)
      (if +popup-mode
          (letf! ((defun read-char-exclusive (&rest args)
                    (message nil)
                    (apply read-char-exclusive args))
                  (defun split-window-vertically (&optional _size)
                    (funcall split-window-vertically (- 0 window-min-height 1)))
                  (defun org-fit-window-to-buffer (&optional window max-height min-height shrink-only)
                    (when-let (buf (window-buffer window))
                      (with-current-buffer buf
                        (+popup-buffer-mode)))
                    (when (> (window-buffer-height window)
                             (window-height window))
                      (fit-window-to-buffer window (window-buffer-height window)))))
            (apply orig-fn args))
        (apply orig-fn args)))

    (defadvice! +popup--advice-org-edit-src-exit (orig-fn &rest args)
      "If you switch workspaces or the src window is recreated..."
      :around #'org-edit-src-exit
      (let* ((window (selected-window))
             (popup-p (+popup-window-p window)))
        (prog1 (apply orig-fn args)
          (when (and popup-p (window-live-p window))
            (delete-window window)))))

    ;; Ensure todo, agenda, and other minor popups are delegated to the popup
    ;; system.
    (defadvice! +popup--advice-org-pop-to-buffer (orig-fn buf &optional norecord)
      "Use `pop-to-buffer' instead of `switch-to-buffer' to open
buffer.'"
      :around #'org-switch-to-buffer-other-window
      (if +popup-mode
          (pop-to-buffer buf nil norecord)
        (funcall orig-fn buf norecord))))

;;;;;;; org-journal

;;;###package org-journal
  (defadvice! +popup--advice-use-popup-window (orig-fn &rest args)
    "Advice org-journal to work with popup system."
    :around #'org-journal-search-by-string
    (letf! ((#'switch-to-buffer #'pop-to-buffer))
      (apply orig-fn args)))

;;;;;;; persp-mode

;;;###package persp-mode
  (defadvice! +popup--advice-persp-mode-restore-popups (&rest _)
    "Restore popup windows when loading a perspective from file."
    :after #'persp-load-state-from-file
    (dolist (window (window-list))
      (when (+popup-parameter 'popup window)
        (+popup--init window nil))))

;;;;;;; pdf-tools

;;;###package pdf-tools
  (after! pdf-tools
    (setq tablist-context-window-display-action
          '((+popup-display-buffer-stacked-side-window)
            (side . left)
            (slot . 2)
            (window-height . 0.3)
            (inhibit-same-window . t))
          pdf-annot-list-display-buffer-action
          '((+popup-display-buffer-stacked-side-window)
            (side . left)
            (slot . 3)
            (inhibit-same-window . t))))

;;;;;;; profiler

;;;###package profiler
  (defadvice! +popup--advice-profiler-report-find-entry-in-other-window (orig-fn function)
    "Profiler report in other window."
    :around #'profiler-report-find-entry
    (letf! ((#'find-function #'find-function-other-window))
      (funcall orig-fn function)))

;;;;;;; wgrep

;;;###package wgrep
  (progn
    ;; close the popup after you're done with a wgrep buffer
    (advice-add #'wgrep-abort-changes :after #'+popup--advice-close)
    (advice-add #'wgrep-finish-edit :after #'+popup--advice-close))

;;;;;;; which-key

;;;###package which-key
  (after! which-key
    (when (eq which-key-popup-type 'side-window)
      (setq which-key-popup-type 'custom
            which-key-custom-popup-max-dimensions-function (lambda (_) (which-key--side-window-max-dimensions))
            which-key-custom-hide-popup-function #'which-key--hide-buffer-side-window
            which-key-custom-show-popup-function
            (lambda (act-popup-dim)
              (letf! ((defun display-buffer-in-side-window (buffer alist)
                        (+popup-display-buffer-stacked-side-window
                         buffer (append '((vslot . -9999)) alist))))
                ;; HACK Fix #2219 where the which-key popup would get cut off.
                (setcar act-popup-dim (1+ (car act-popup-dim)))
                (which-key--show-buffer-side-window act-popup-dim))))))

;;;;;;; windmove

;;;###package windmove
  ;; Users should be able to hop into popups easily, but Elisp shouldn't.
  (defadvice! +popup--advice-ignore-window-parameters (orig-fn &rest args)
    "Allow *interactive* window moving commands to traverse
popups."
    :around '(windmove-up windmove-down windmove-left windmove-right)
    (letf! (defun windmove-find-other-window (dir &optional arg window)
             (window-in-direction
              (pcase dir (`up 'above) (`down 'below) (_ dir))
              window (bound-and-true-p +popup-mode) arg windmove-wrap-around t))
      (apply orig-fn args))))

;;;; pretty-code

(module! (:ui pretty-code)

  (defvar +pretty-code-extra-symbols
    '(;; org
      :name          "»"
      :src_block     "»"
      :src_block_end "«"
      :quote         "“"
      :quote_end     "”"
      ;; Functional
      :lambda        "λ"
      :def           "ƒ"
      :composition   "∘"
      :map           "↦"
      ;; Types
      :null          "∅"
      :true          "𝕋"
      :false         "𝔽"
      :int           "ℤ"
      :float         "ℝ"
      :str           "𝕊"
      :bool          "𝔹"
      :list          "𝕃"
      ;; Flow
      :not           "￢"
      :in            "∈"
      :not-in        "∉"
      :and           "∧"
      :or            "∨"
      :for           "∀"
      :some          "∃"
      :return        "⟼"
      :yield         "⟻"
      ;; Other
      :union         "⋃"
      :intersect     "∩"
      :diff          "∖"
      :tuple         "⨂"
      :pipe          "" ;; FIXME: find a non-private char
      :dot           "•")
    "Maps identifiers to symbols, recognized by `set-pretty-symbols!'.
This should not contain any symbols from the Unicode Private
Area! There is no universal way of getting the correct symbol as
that area varies from font to font.")

  (defvar +pretty-code-extra-alist '((t))
    "A map of major modes to symbol lists (for
`prettify-symbols-alist').")

  (defvar +pretty-code-composition-alist
    '((?!  . "\\(?:!\\(?:==\\|[!=]\\)\\)")                                      ; (regexp-opt '("!!" "!=" "!=="))
      (?#  . "\\(?:#\\(?:###?\\|_(\\|[#(:=?[_{]\\)\\)")                         ; (regexp-opt '("##" "###" "####" "#(" "#:" "#=" "#?" "#[" "#_" "#_(" "#{"))
      (?$  . "\\(?:\\$>>?\\)")                                                  ; (regexp-opt '("$>" "$>>"))
      (?%  . "\\(?:%%%?\\)")                                                    ; (regexp-opt '("%%" "%%%"))
      (?&  . "\\(?:&&&?\\)")                                                    ; (regexp-opt '("&&" "&&&"))
      (?*  . "\\(?:\\*\\(?:\\*[*/]\\|[)*/>]\\)?\\)")                            ; (regexp-opt '("*" "**" "***" "**/" "*/" "*>" "*)"))
      (?+  . "\\(?:\\+\\(?:\\+\\+\\|[+:>]\\)?\\)")                              ; (regexp-opt '("+" "++" "+++" "+>" "+:"))
      (?-  . "\\(?:-\\(?:-\\(?:->\\|[>-]\\)\\|<[<-]\\|>[>-]\\|[:<>|}~-]\\)\\)") ; (regexp-opt '("--" "---" "-->" "--->" "->-" "-<" "-<-" "-<<" "->" "->>" "-}" "-~" "-:" "-|"))
      (?.  . "\\(?:\\.\\(?:\\.[.<]\\|[.=>-]\\)\\)")                             ; (regexp-opt '(".-" ".." "..." "..<" ".=" ".>"))
      (?/  . "\\(?:/\\(?:\\*\\*\\|//\\|==\\|[*/=>]\\)\\)")                      ; (regexp-opt '("/*" "/**" "//" "///" "/=" "/==" "/>"))
      (?:  . "\\(?::\\(?:::\\|[+:<=>]\\)?\\)")                                  ; (regexp-opt '(":" "::" ":::" ":=" ":<" ":=" ":>" ":+"))
      (?\; . ";;")                                                              ; (regexp-opt '(";;"))
      (?0  . "0\\(?:\\(x[a-fA-F0-9]\\).?\\)") ; Tries to match the x in 0xDEADBEEF
      ;; (?x . "x") ; Also tries to match the x in 0xDEADBEEF
      ;; (regexp-opt '("<!--" "<$" "<$>" "<*" "<*>" "<**>" "<+" "<+>" "<-" "<--" "<---" "<->" "<-->" "<--->" "</" "</>" "<<" "<<-" "<<<" "<<=" "<=" "<=<" "<==" "<=>" "<===>" "<>" "<|" "<|>" "<~" "<~~" "<." "<.>" "<..>"))
      (?<  . "\\(?:<\\(?:!--\\|\\$>\\|\\*\\(?:\\*?>\\)\\|\\+>\\|-\\(?:-\\(?:->\\|[>-]\\)\\|[>-]\\)\\|\\.\\(?:\\.?>\\)\\|/>\\|<[<=-]\\|=\\(?:==>\\|[<=>]\\)\\||>\\|~~\\|[$*+./<=>|~-]\\)\\)")
      (?=  . "\\(?:=\\(?:/=\\|:=\\|<[<=]\\|=[=>]\\|>[=>]\\|[=>]\\)\\)")         ; (regexp-opt '("=/=" "=:=" "=<<" "==" "===" "==>" "=>" "=>>" "=>=" "=<="))
      (?>  . "\\(?:>\\(?:->\\|=>\\|>[=>-]\\|[:=>-]\\)\\)")                      ; (regexp-opt '(">-" ">->" ">:" ">=" ">=>" ">>" ">>-" ">>=" ">>>"))
      (??  . "\\(?:\\?[.:=?]\\)")                                               ; (regexp-opt '("??" "?." "?:" "?="))
      (?\[ . "\\(?:\\[\\(?:|]\\|[]|]\\)\\)")                                    ; (regexp-opt '("[]" "[|]" "[|"))
      (?\\ . "\\(?:\\\\\\\\[\\n]?\\)")                                          ; (regexp-opt '("\\\\" "\\\\\\" "\\\\n"))
      (?^  . "\\(?:\\^==?\\)")                                                  ; (regexp-opt '("^=" "^=="))
      (?w  . "\\(?:wwww?\\)")                                                   ; (regexp-opt '("www" "wwww"))
      (?{  . "\\(?:{\\(?:|\\(?:|}\\|[|}]\\)\\|[|-]\\)\\)")                      ; (regexp-opt '("{-" "{|" "{||" "{|}" "{||}"))
      (?|  . "\\(?:|\\(?:->\\|=>\\||=\\|[]=>|}-]\\)\\)")                        ; (regexp-opt '("|=" "|>" "||" "||=" "|->" "|=>" "|]" "|}" "|-"))
      (?_  . "\\(?:_\\(?:|?_\\)\\)")                                            ; (regexp-opt '("_|_" "__"))
      (?\( . "\\(?:(\\*\\)")                                                    ; (regexp-opt '("(*"))
      (?~  . "\\(?:~\\(?:~>\\|[=>@~-]\\)\\)"))                                  ; (regexp-opt '("~-" "~=" "~>" "~@" "~~" "~~>"))
    "An alist of all ligatures used by `+pretty-code-extras-in-modes'.
The car is the character ASCII number, cdr is a regex which will
call `font-shape-gstring' when matched. Because of the underlying
code in :ui ligatures module, the regex should match a string
starting with the character contained in car.")

  (defvar +pretty-code-in-modes
    '(not special-mode comint-mode eshell-mode term-mode vterm-mode Info-mode)
    "List of major modes where ligatures should be enabled.
If t, enable it everywhere (except `fundamental-mode').
If the first element is 'not, enable it in any mode besides what
is listed.
If nil, don't enable ligatures anywhere.")

  (defvar +pretty-code-extras-in-modes t
    "List of major modes where extra ligatures should be enabled.
Extra ligatures are mode-specific substituions, defined in
`+pretty-code-extra-symbols' and assigned with
`set-pretty-symbols!'. This variable controls where these are
enabled.
If t, enable it everywhere (except `fundamental-mode').
If the first element is 'not, enable it in any mode besides what
is listed.
If nil, don't enable these extra ligatures anywhere.")

  (defvar +pretty-code--init-font-hook nil)

  (defun +ligatures--correct-symbol-bounds (ligature-alist)
    "Prepend non-breaking spaces to a ligature.
This way `compose-region' (called by `prettify-symbols-mode')
will use the correct width of the symbols instead of the width
measured by `char-width'."
    (let ((len (length (car ligature-alist)))
          (acc (list   (cdr ligature-alist))))
      (while (> len 1)
        (setq acc (cons #X00a0 (cons '(Br . Bl) acc))
              len (1- len)))
      (cons (car ligature-alist) acc)))

  (defun +pretty-code--enable-p (modes)
    "Return t if ligatures should be enabled in this buffer
depending on MODES."
    (unless (eq major-mode 'fundamental-mode)
      (or (eq modes t)
          (if (eq (car modes) 'not)
              (not (apply #'derived-mode-p (cdr modes)))
            (apply #'derived-mode-p modes)))))

  (defun +pretty-code--hook-init-buffer ()
    "Set up ligatures for the current buffer.
Extra ligatures are mode-specific substituions, defined in
`+pretty-code-extra-symbols', assigned with
`set-pretty-symbols!', and made possible with
`prettify-symbols-mode'. This variable controls where these are
enabled. See `+pretty-code-extras-in-modes' to control what major
modes this function can and cannot run in."
    (when after-init-time
      (let ((in-mode-p
             (+pretty-code--enable-p +pretty-code-in-modes))
            (in-mode-extras-p (+pretty-code--enable-p +pretty-code-extras-in-modes)))
        (when in-mode-p
          (run-hooks '+pretty-code--init-font-hook)
          (setq +pretty-code--init-font-hook nil))
        (when in-mode-extras-p
          (prependq! prettify-symbols-alist
                     (alist-get major-mode +pretty-code-extra-alist)))
        (when (and (or in-mode-p in-mode-extras-p)
                   prettify-symbols-alist)
          (when prettify-symbols-mode
            (prettify-symbols-mode -1))
          (prettify-symbols-mode +1)))))

;;;###package prettify-symbols
  ;; When you get to the right edge, it goes back to how it normally prints
  (setq prettify-symbols-unprettify-at-point 'right-edge)

  (defhook! +pretty-code--hook-init ()
    "Initialize ligatures and pretty code."
    '+emacs-init-ui-hook :append
    (add-hook 'after-change-major-mode-hook #'+pretty-code--hook-init-buffer)))


;;;; transient-state

(module! (:ui transient-state)

  (use-package hydra-examples
    :commands (hydra-move-splitter-up
               hydra-move-splitter-down
               hydra-move-splitter-right
               hydra-move-splitter-left))

  (after! hydra
    (setq lv-use-separator t)

    (defadvice! +hydra--advice-inhibit-window-switch-hooks (orig-fn)
      "Inhibit `+emacs-inhibit-switch-window-hooks'."
      :around #'lv-window
      (let (+emacs-switch-window-hook)
        (funcall orig-fn))))

  (defvar +transient-states-map
    (let ((map (make-sparse-keymap)))
      (define-key map (kbd "b") #'+emacs-buffers-transient-state/body)
      (define-key map (kbd "w") #'+emacs-windows-transient-state/body)
      (define-key map (kbd "x") #'+emacs-text-zoom-transient-state/body)
      (define-key map (kbd "g") #'+vc-gutter-hydra/body)
      (define-key map (kbd "G") #'+vc-smerge-hydra/body)
      map)
    "Keymap for misc transient-states.")
  (map! "<f6>" +transient-states-map))


;;;; treemacs

(module! (:ui treemacs)

  (defvar +treemacs-git-mode 'deferred
    "Type of git integration for `treemacs-git-mode'.

There are 3 possible values:

  1) `simple', which highlights only files based on their git
     status, and is slightly faster,
  2) `extended', which highlights both files and directories, but
     requires python,
  3) `deferred', same as extended, but highlights asynchronously.
This must be set before `treemacs' has loaded.")

  (use-package treemacs
    :defer t
    :init
    (setq treemacs-follow-after-init t
          treemacs-is-never-other-window t
          treemacs-sorting 'alphabetic-case-insensitive-asc
          treemacs-persist-file (concat +emacs-cache-dir "treemacs-persist")
          treemacs-last-error-persist-file (concat +emacs-cache-dir "treemacs-last-error-persist"))

    ;; HACK Otherwise `treemacs' always asks for project root
    (defadvice! +treemacs--advice-restore-persist-file (orig-fn)
      "Restore `treemacs-persist-file'."
      :around #'treemacs
      (+treemacs--restore-projects)
      (funcall-interactively orig-fn))

    (defadvice! +treemacs--advice-backup-persist-file (orig-fn)
      "Backup `treemacs-persist-file'."
      :around #'treemacs-display-current-project-exclusively
      (+treemacs--backup-projects)
      (add-transient-hook! 'treemacs-quit-hook (+treemacs--restore-projects))
      (add-transient-hook! 'treemacs-kill-hook (+treemacs--restore-projects))
      (funcall-interactively orig-fn))

    :config
    ;; Allow ace-window to target treemacs windows elsewhere
    (after! ace-window
      (delq! 'treemacs-mode aw-ignored-buffers))

    ;; Don't follow the cursor
    (treemacs-follow-mode -1)

    (when +treemacs-git-mode
      ;; If they aren't supported, fall back to simpler methods
      (when (and (memq +treemacs-git-mode '(deferred extended))
                 (not (executable-find "python3")))
        (setq +treemacs-git-mode 'simple))
      (treemacs-git-mode +treemacs-git-mode)
      (setq treemacs-collapse-dirs
            (if (memq treemacs-git-mode '(extended deferred))
                3
              0)))

    (map! :n "M-0" 'treemacs-select-window))

;;;;; treemacs-evil

  (use-package treemacs-evil
    :when (featurep! :editor evil)
    :defer t
    :init
    (after! treemacs (require 'treemacs-evil))
    (add-to-list '+emacs-evil-state-alist '(?T . treemacs))
    :config
    (define-key! evil-treemacs-state-map
      [return] #'treemacs-RET-action
      [tab]    #'treemacs-TAB-action
      "TAB"    #'treemacs-TAB-action))

;;;;; treemacs-projectile

  (use-package treemacs-projectile
    :after treemacs)

;;;;; treemacs-magit

  (use-package treemacs-magit
    :when (featurep! :tools magit)
    :after treemacs magit)

;;;;; treemacs-persp

  (use-package treemacs-persp
    :when (featurep! :ui workspaces)
    :after treemacs
    :config (treemacs-set-scope-type 'Perspectives)))

;;;; vc-gutter

(module! (:ui vc-gutter)

  (defvar +vc-gutter-in-remote-files nil
    "If non-nil, enable the vc gutter in remote files (e.g. open
through TRAMP).")

  (use-package git-gutter
    :commands git-gutter:revert-hunk git-gutter:stage-hunk
    :init
    (defhook! +vc-gutter--hook-init-maybe ()
      "Enable `git-gutter-mode' in the current buffer.
If the buffer doesn't represent an existing file,
`git-gutter-mode's activation is deferred until the file is
saved."
      'find-file-hook
      (let ((file-name (buffer-file-name (buffer-base-buffer))))
        (cond
         ((and (file-remote-p (or file-name default-directory))
               (not +vc-gutter-in-remote-files)))
         ;; If not a valid file, wait until it is written/saved to activate
         ;; git-gutter.
         ((not (and file-name (vc-backend file-name)))
          (add-hook 'after-save-hook #'+vc-gutter--hook-init-maybe nil 'local))
         ;; Allow git-gutter or git-gutter-fringe to activate based on the type
         ;; of frame we're in. This allows git-gutter to work for silly geese
         ;; who open both tty and gui frames from the daemon.
         ((if (and (display-graphic-p)
                   (require 'git-gutter-fringe nil t))
              (setq-local git-gutter:init-function      #'git-gutter-fr:init
                          git-gutter:view-diff-function #'git-gutter-fr:view-diff-infos
                          git-gutter:clear-function     #'git-gutter-fr:clear
                          git-gutter:window-width -1)
            (setq-local git-gutter:init-function      'nil
                        git-gutter:view-diff-function #'git-gutter:view-diff-infos
                        git-gutter:clear-function     #'git-gutter:clear-diff-infos
                        git-gutter:window-width 1))
          (git-gutter-mode +1)
          (remove-hook 'after-save-hook #'+vc-gutter--hook-init-maybe 'local)))))

    (setq git-gutter:disabled-modes '(fundamental-mode image-mode pdf-view-mode))

    :config
    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*git-gutter" :select nil :size '+popup-shrink-to-fit))

    ;; Update git-gutter on focus
    (add-hook 'focus-in-hook #'git-gutter:update-all-windows)

    (defhook! +vc-gutter--hook-update (&rest _)
      "Refresh git-gutter on ESC. Return nil to prevent shadowing
other `+emacs-escape-hook' hooks."
      '(+emacs-escape-hook +emacs-switch-window-hook) :append
      (ignore (or (memq this-command '(git-gutter:stage-hunk
                                       git-gutter:revert-hunk))
                  inhibit-redisplay
                  (if git-gutter-mode
                      (git-gutter)
                    (+vc-gutter--hook-init-maybe)))))

    ;; update git-gutter when using magit commands
    (advice-add #'magit-stage-file   :after #'+vc-gutter--hook-update)
    (advice-add #'magit-unstage-file :after #'+vc-gutter--hook-update)
    (defadvice! +vc-gutter--advice-fix-linearity-of-hunks (diffinfos is-reverse)
      "Fixes `git-gutter:next-hunk' and
`git-gutter:previous-hunk' sometimes jumping to random hunks."
      :override #'git-gutter:search-near-diff-index
      (cl-position-if (let ((lineno (line-number-at-pos))
                            (fn (if is-reverse #'> #'<)))
                        (lambda (line) (funcall fn lineno line)))
                      diffinfos
                      :key #'git-gutter-hunk-start-line
                      :from-end is-reverse)))


  (after! git-gutter-fringe
    ;; standardize default fringe width
    (if (fboundp 'fringe-mode) (fringe-mode '4))

    ;; places the git gutter outside the margins.
    (setq-default fringes-outside-margins t)
    ;; thin fringe bitmaps
    (define-fringe-bitmap 'git-gutter-fr:added [224]
      nil nil '(center repeated))
    (define-fringe-bitmap 'git-gutter-fr:modified [224]
      nil nil '(center repeated))
    (define-fringe-bitmap 'git-gutter-fr:deleted [128 192 224 240]
      nil nil 'bottom))

  ;; let diff have left fringe, flycheck can have right fringe
  (after! flycheck
    (setq flycheck-indication-mode 'right-fringe)
    ;; A non-descript, left-pointing arrow
    (define-fringe-bitmap 'flycheck-fringe-bitmap-double-arrow
      [16 48 112 240 112 48 16] nil nil 'center)))

;;;; window-select

(module! (:ui window-select)

  (use-package ace-window
    :init
    (define-key global-map [remap other-window] #'ace-window)
    :defer t
    :config
    (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)
          aw-scope 'frame
          aw-background t)))

;;;; workspaces

(module! (:ui workspaces)

  (defvar +workspaces-main "main"
    "The name of the primary and initial workspace, which cannot
be deleted.")

  (defvar +workspaces-switch-project-function #'+emacs-project-find-file
    "The function to run after `projectile-switch-project' or
`counsel-projectile-switch-project'. This function must take one
argument: the new project directory.")

  (defvar +workspaces-on-switch-project-behavior 'non-empty
    "Controls the behavior of workspaces when switching to a new project.
Can be one of the following:
t           Always create a new workspace for the project
'non-empty  Only create a new workspace if the current one already has buffers
            associated with it.
nil         Never create a new workspace on project switch.")

  ;; FIXME actually use this for wconf bookmark system
  (defvar +workspaces-data-file "_workspaces"
    "The basename of the file to store single workspace
perspectives. Will be stored in `persp-save-dir'.")

  (defvar +workspace--old-uniquify-style nil)

;;;;; persp-mode

  (use-package persp-mode
    :unless noninteractive
    :commands (persp-switch-to-buffer)
    :hook (+emacs-init-ui . persp-mode)
    :config
    (setq persp-autokill-buffer-on-remove 'kill-weak
          persp-reset-windows-on-nil-window-conf nil
          persp-nil-hidden t
          persp-auto-save-fname "autosave"
          persp-save-dir (concat +emacs-etc-dir "workspaces/")
          persp-set-last-persp-for-new-frames t
          persp-switch-to-added-buffer nil
          persp-kill-foreign-buffer-behaviour 'kill
          persp-remove-buffers-from-nil-persp-behaviour nil
          persp-auto-resume-time -1     ; Don't auto-load on startup
          persp-auto-save-opt (if noninteractive 0 1)) ; auto-save on kill

    (advice-add #'persp-asave-on-exit :around #'+workspaces--advice-autosave-real-buffers)

    (defhook! +workspaces--hook-ensure-no-nil-workspaces (&rest _)
      "Ensure no nil workspace is active."
      '(persp-mode-hook persp-after-load-state-functions)
      (when persp-mode
        (dolist (frame (frame-list))
          (when (string= (safe-persp-name (get-current-persp frame)) persp-nil-name)
            ;; Take extra steps to ensure no frame ends up in the nil
            ;; perspective
            (persp-frame-switch (or (cadr (hash-table-keys *persp-hash*))
                                    +workspaces-main)
                                frame)))))

    (defhook! +workspaces--hook-init-first-workspace (&rest _)
      "Ensure a main workspace exists."
      'persp-mode-hook
      (when persp-mode
        (let (persp-before-switch-functions)
          ;; The default perspective persp-mode creates is special and doesn't
          ;; represent a real persp object, so buffers can't really be assigned
          ;; to it, among other quirks. We hide the nil persp...
          (when (equal (car persp-names-cache) persp-nil-name)
            (pop persp-names-cache))
          ;; ...and create a *real* main workspace to fill this role, and hide
          ;; the nil perspective.
          (unless (or (persp-get-by-name +workspaces-main)
                      ;; Start from 2 b/c persp-mode counts the nil workspace
                      (> (hash-table-count *persp-hash*) 2))
            (persp-add-new +workspaces-main))
          ;; HACK Fix #319: the warnings buffer gets swallowed when creating
          ;;      `+workspaces-main', so display it ourselves, if it exists.
          (when-let (warnings (get-buffer "*Warnings*"))
            (save-excursion
              (display-buffer-in-side-window
               warnings '((window-height . shrink-window-if-larger-than-buffer))))))))

    (defhook! +workspaces--hook-init-persp-mode ()
      "Init persp-mode."
      'persp-mode-hook
      (cond (persp-mode
             ;; `uniquify' breaks persp-mode. It renames old buffers, which
             ;; causes errors when switching between perspective (their buffers
             ;; are serialized by name and persp-mode expects them to have the
             ;; same name when restored).
             (when uniquify-buffer-name-style
               (setq +workspace--old-uniquify-style uniquify-buffer-name-style))
             (setq uniquify-buffer-name-style nil)
             ;; Ensure `persp-kill-buffer-query-function' is last
             (remove-hook 'kill-buffer-query-functions #'persp-kill-buffer-query-function)
             (add-hook 'kill-buffer-query-functions #'persp-kill-buffer-query-function t)
             ;; Restrict buffer list to workspace
             (advice-add #'+emacs-buffer-list :override #'+workspace-buffer-list))
            (t
             (when +workspace--old-uniquify-style
               (setq uniquify-buffer-name-style +workspace--old-uniquify-style))
             (advice-remove #'+emacs-buffer-list #'+workspace-buffer-list))))

    ;; Per-workspace `winner-mode' history
    (add-to-list 'window-persistent-parameters '(winner-ring . t))

    (defhook! +workspaces--hook-save-winner-data (_)
      "TODO"
      'persp-before-deactivate-functions
      (when (and (bound-and-true-p winner-mode)
                 (get-current-persp))
        (set-persp-parameter
         'winner-ring (list winner-currents
                            winner-ring-alist
                            winner-pending-undo-ring))))

    (defhook! +workspaces--hook-load-winner-data (_)
      "TODO"
      'persp-activated-functions
      (when (bound-and-true-p winner-mode)
        (cl-destructuring-bind
            (currents alist pending-undo-ring)
            (or (persp-parameter 'winner-ring) (list nil nil nil))
          (setq winner-undo-frame nil
                winner-currents currents
                winner-ring-alist alist
                winner-pending-undo-ring pending-undo-ring))))

    (defhook! +workspaces--hook-add-current-buffer ()
      "Add current buffer to focused perspective."
      '+emacs-switch-buffer-hook
      (or (not persp-mode)
          (persp-buffer-filtered-out-p
           (or (buffer-base-buffer (current-buffer))
               (current-buffer))
           persp-add-buffer-on-after-change-major-mode-filter-functions)
          (persp-add-buffer (current-buffer) (get-current-persp) nil nil)))

    (add-hook 'persp-add-buffer-on-after-change-major-mode-filter-functions
              #'+emacs-unreal-buffer-p)

    (defadvice! +workspaces--advice-evil-alternate-buffer (&optional window)
      "Make `evil-alternate-buffer' ignore buffers outside the
current workspace."
      :override #'evil-alternate-buffer
      (let* ((prev-buffers (if persp-mode
                               (cl-remove-if-not #'persp-contain-buffer-p (window-prev-buffers)
                                                 :key #'car)
                             (window-prev-buffers)))
             (head (car prev-buffers)))
        (if (eq (car head) (window-buffer window))
            (cadr prev-buffers)
          head)))

    (defadvice! +workspaces--advice-remove-dead-buffers (persp)
      "HACK Selecting deleted buffer error when quitting Emacs or
on some buffer listing ops."
      :before #'persp-buffers-to-savelist
      (when (perspective-p persp)
        ;; HACK Can't use `persp-buffers' because of a race condition with its
        ;;      gv getter/setter not being defined in time.
        (setf (aref persp 2)
              (cl-delete-if-not #'persp-get-buffer-or-null (persp-buffers persp)))))

    ;; Delete the current workspace if closing the last open window
    (define-key! persp-mode-map
      [remap delete-window] #'+workspace/close-window-or-workspace
      [remap evil-window-delete] #'+workspace/close-window-or-workspace)

    ;; per-frame workspaces
    (setq persp-init-frame-behaviour t
          persp-init-new-frame-behaviour-override nil
          persp-interactive-init-frame-behaviour-override #'+workspaces--associate-frame
          persp-emacsclient-init-frame-behaviour-override #'+workspaces--associate-frame)
    (add-hook 'delete-frame-functions #'+workspaces--hook-delete-associated-workspace)
    (add-hook 'server-done-hook #'+workspaces--hook-delete-associated-workspace)

    ;; per-project workspaces, but reuse current workspace if empty
    (setq projectile-switch-project-action #'+workspaces--set-project-action
          counsel-projectile-switch-project-action
          '(1 ("o" +workspaces--hook-switch-to-project "open project in new workspace")
              ("O" counsel-projectile-switch-project-action "jump to a project buffer or file")
              ("f" counsel-projectile-switch-project-action-find-file "jump to a project file")
              ("d" counsel-projectile-switch-project-action-find-dir "jump to a project directory")
              ("D" counsel-projectile-switch-project-action-dired "open project in dired")
              ("b" counsel-projectile-switch-project-action-switch-to-buffer "jump to a project buffer")
              ("m" counsel-projectile-switch-project-action-find-file-manually "find file manually from project root")
              ("w" counsel-projectile-switch-project-action-save-all-buffers "save all project buffers")
              ("k" counsel-projectile-switch-project-action-kill-buffers "kill all project buffers")
              ("r" counsel-projectile-switch-project-action-remove-known-project "remove project from known projects")
              ("c" counsel-projectile-switch-project-action-compile "run project compilation command")
              ("C" counsel-projectile-switch-project-action-configure "run project configure command")
              ("e" counsel-projectile-switch-project-action-edit-dir-locals "edit project dir-locals")
              ("v" counsel-projectile-switch-project-action-vc "open project in vc-dir / magit / monky")
              ("s" (lambda (project)
                     (let ((projectile-switch-project-action
                            (lambda () (call-interactively #'+ivy/project-search))))
                       (counsel-projectile-switch-project-by-name project))) "search project")
              ("xs" counsel-projectile-switch-project-action-run-shell "invoke shell from project root")
              ("xe" counsel-projectile-switch-project-action-run-eshell "invoke eshell from project root")
              ("xt" counsel-projectile-switch-project-action-run-term "invoke term from project root")
              ("X" counsel-projectile-switch-project-action-org-capture "org-capture into project")))

    ;; Fix visual selection surviving workspace changes
    (add-hook 'persp-before-deactivate-functions #'deactivate-mark)

    ;; Fix #1017: stop session persistence from restoring a broken posframe
    (after! posframe
      (defhook! +workspaces--hook-delete-all-posframes (&rest _)
        "Stop session persistence from restoring a broken
posframe."
        'persp-after-load-state-functions
        (posframe-delete-all)))

    ;; FIXME: Ignore dead buffers in PERSP's buffer list
    (defhook! +workspaces--hook-dead-buffer-p (buf)
      "Ignore dead buffers in PERSP's buffer list."
      'persp-filter-save-buffers-functions
      (not (buffer-live-p buf)))

    ;; Otherwise, buffers opened via bookmarks aren't treated as "real" and are
    ;; excluded from the buffer list.
    (add-hook 'bookmark-after-jump-hook #'+workspaces--hook-add-current-buffer)

    ;; eshell
    (persp-def-buffer-save/load
     :mode 'eshell-mode :tag-symbol 'def-eshell-buffer
     :save-vars '(major-mode default-directory))
    ;; compile
    (persp-def-buffer-save/load
     :mode 'compilation-mode :tag-symbol 'def-compilation-buffer
     :save-vars
     '(major-mode default-directory compilation-directory compilation-environment compilation-arguments))
    ;; magit
    (persp-def-buffer-save/load
     :mode 'magit-status-mode :tag-symbol 'def-magit-status-buffer
     :save-vars '(default-directory)
     :load-function (lambda (savelist &rest _)
                      (cl-destructuring-bind (buffer-name vars &rest _rest) (cdr savelist)
                        (magit-status (alist-get 'default-directory vars)))))
    ;; Restore indirect buffers
    (defvar +workspaces--indirect-buffers-to-restore nil)
    (persp-def-buffer-save/load
     :tag-symbol 'def-indirect-buffer
     :predicate #'buffer-base-buffer
     :save-function (lambda (buf tag vars)
                      (list tag (buffer-name buf) vars
                            (buffer-name (buffer-base-buffer buf))))
     :load-function (lambda (savelist &rest _rest)
                      (cl-destructuring-bind (buf-name _vars base-buf-name &rest _)
                          (cdr savelist)
                        (push (cons buf-name base-buf-name)
                              +workspaces--indirect-buffers-to-restore)
                        nil)))
    (defhook! +workspaces--hook-reload-indirect-buffers (&rest _)
      "Reload indirect buffers."
      'persp-after-load-state-functions
      (dolist (ibc +workspaces--indirect-buffers-to-restore)
        (cl-destructuring-bind (buffer-name . base-buffer-name) ibc
          (let ((base-buffer (get-buffer base-buffer-name)))
            (when (buffer-live-p base-buffer)
              (when (get-buffer buffer-name)
                (setq buffer-name (generate-new-buffer-name buffer-name)))
              (make-indirect-buffer base-buffer buffer-name t)))))
      (setq +workspaces--indirect-buffers-to-restore nil)))

  (defadvice! +workspaces--advice-delete-workspace-after-project-kill (&rest_)
    "Delete empty workspace after corresponding project has been
killed."
    :after #'+emacs/kill-project-buffers
    (+workspace/delete (+workspace-current-name))))

;;; editor

;;;; cleverlispy

(module! (:editor cleverlispy)

  (defvar +cleverlispy-lispyville-modes nil
    "List of major modes in which `lispyville' should be used
instead of `evil-cleverparens'.")

  (defun +cleverlispy--hook-enable ()
    "Enable `lispyville' or `evil-cleverparens' according to
`+cleverlispy-lispyville-modes'."
    (if (memq major-mode +cleverlispy-lispyville-modes)
        (lispyville-mode +1)
      (evil-cleverparens-mode +1)))

  ;; Keybindings
  (map! :nie "M-n" #'+emacs-cleverlispy-transient-state/body)

  ;; evil-cleverparens
  (use-package evil-cleverparens
    :when (featurep! :editor evil)
    :defer t
    :init
    (setq evil-cleverparens-use-additional-bindings nil
          evil-cleverparens-use-additional-movement-keys nil
          evil-cleverparens-use-s-and-S nil)
    :config
    ;; Remove old keybinds
    (dolist (state '(normal visual))
      (evil-cp--populate-mode-bindings-for-state
       evil-cp-regular-bindings
       state
       nil))
    (evil-define-key* '(normal visual) evil-cleverparens-mode-map
      ;; `evil-cleverparens' regular bindings
      "d" #'evil-cp-delete
      "c" #'evil-cp-change
      "y" #'evil-cp-yank
      "D" #'evil-cp-delete-line
      "C" #'evil-cp-change-line
      "Y" #'evil-cp-yank-line
      "x" #'evil-cp-delete-char-or-splice
      "X" #'evil-cp-delete-char-or-splice-backwards)
    (add-hook 'evil-cleverparens-mode-hook #'evil-normalize-keymaps))

  ;; lispy
  (use-package lispyville
    :when (featurep! :editor evil)
    :defer t
    :init
    (setq lispyville-key-theme
          '((operators)
            c-w
            (prettify insert)))
    :config
    (lispyville-set-key-theme)))


;;;; evil

(module! (:editor evil)

  ;; `evil-collection'
  (load! "+editor-evil-collection")

  (defvar evil-want-C-g-bindings t)
  (defvar evil-want-C-i-jump nil)
  (defvar evil-want-C-u-scroll t)  ; moved the universal arg to <leader> u
  (defvar evil-want-C-u-delete t)
  (defvar evil-want-C-w-delete t)
  (defvar evil-want-Y-yank-to-eol t)
  (defvar evil-want-abbrev-expand-on-insert-exit nil)
  (defvar evil-respect-visual-line-mode nil)

  (use-package evil
    :hook (+emacs-init-modules . evil-mode)
    :demand t
    :preface
    ;; Restore emacs keybindings in insert mode
    (setq evil-disable-insert-state-bindings t
          evil-want-integration t
          evil-ex-search-vim-style-regexp t
          evil-respect-visual-line-mode t
          evil-ex-visual-char-range t
          evil-ex-interactive-search-highlight 'selected-window
          ;; Change defaults
          evil-cross-lines t
          evil-move-beyond-eol t
          evil-move-cursor-back nil
          evil-want-fine-undo t)
    ;; Set Cursor colors
    (setq evil-default-cursor '+evil-default-cursor
          evil-normal-state-cursor '(box)
          evil-insert-state-cursor '(bar)
          evil-emacs-state-cursor '(box +evil-emacs-cursor)
          evil-visual-state-cursor '(hollow))

    ;; Slow this down from 0.02 to prevent blocking in large or folded buffers
    ;; like magit while incrementally highlighting matches.
    (setq-hook! 'magit-mode-hook evil-ex-hl-update-delay 0.2)
    (setq-hook! 'so-long-minor-mode-hook evil-ex-hl-update-delay 0.25)
    :config
    ;; Matching parens highlighting in normal state
    (add-to-list 'evil-highlight-closing-paren-at-point-states 'normal 'append)

    (evil-select-search-module 'evil-search-module 'evil-search)

    ;; stop copying each visual state move to the clipboard:
    ;; https://bitbucket.org/lyro/evil/issue/336/osx-visual-state-copies-the-region-on
    ;; grokked from:
    ;; http://stackoverflow.com/questions/15873346/elisp-rename-macro
    (advice-add #'evil-visual-update-x-selection :override #'ignore)

    ;; Start help-with-tutorial in emacs state
    (advice-add #'help-with-tutorial :after (lambda (&rest _) (evil-emacs-state +1)))

    (when (featurep! :ui popup)
      (defhook! +evil--hook-init-popup-rules ()
        "Initialize popop rules."
        '+emacs-init-modules-hook
        (set-popup-rules!
          '(("^\\*evil-registers" :size 0.3)
            ("^\\*Command Line"   :size 8)))))
    ;; Change the cursor color in emacs mode
    (defvar +evil--default-cursor-color "#ffffff")
    (defvar +evil--emacs-cursor-color "#ff9999")

    ;; Change the cursor color in emacs mode
    (defhook! +evil--hook-update-cursor-color ()
      "Update cursor color."
      '+emacs-load-theme-hook
      (setq +evil--default-cursor-color (face-background 'cursor)
            +evil--emacs-cursor-color (face-foreground 'warning)))

    (defun +evil-default-cursor ()
      (evil-set-cursor-color +evil--default-cursor-color))
    (defun +evil-emacs-cursor ()
      (evil-set-cursor-color +evil--emacs-cursor-color))

    ;; Ensure `evil-shift-width' always matches `tab-width'
    (setq-hook! 'after-change-major-mode-hook evil-shift-width tab-width)

    (after! wgrep
      ;; A wrapper that invokes `wgrep-mark-deletion' across lines you use
      ;; `evil-delete' in wgrep buffers.
      (define-key wgrep-mode-map [remap evil-delete] #'+evil-delete))

    (after! eldoc
      ;; Allow eldoc to trigger directly after changing modes
      (eldoc-add-command 'evil-normal-state
                         'evil-insert
                         'evil-change
                         'evil-delete
                         'evil-replace))

    (defhook! +evil--hook-disable-highlights ()
      "Disable ex search buffer highlights."
      '+emacs-escape-hook
      (when (evil-ex-hl-active-p 'evil-ex-search)
        (evil-ex-nohighlight)
        t))

    ;; HACK '=' moves the cursor to the beginning of selection. Disable this,
    ;;      since it's more disruptive than helpful.
    (defadvice! +evil--advice-dont-move-cursor (orig-fn &rest args)
      "Don't move cursor after indent."
      :around #'evil-indent
      (save-excursion (apply orig-fn args)))

    ;; Make J (evil-join) remove comment delimiters when joining lines.
    (advice-add #'evil-join :around #'+evil--advice-join)

    ;; Make ESC (from normal mode) the universal escaper. See
    ;; `+emacs-escape-hook'.
    (advice-add #'evil-force-normal-state :after #'+evil--advice-escape)

    ;; Don't move cursor when indenting
    (defadvice! +evil--advice-static-reindent (orig-fn &rest args)
      "Don't move cursor on indent."
      :around #'evil-indent
      (save-excursion (apply orig-fn args)))

    ;; make `try-expand-dabbrev' (from `hippie-expand') work in minibuffer
    (add-hook 'minibuffer-inactive-mode-hook #'+evil--hook-fix-dabbrev-in-minibuffer)

    ;; Focus and recenter new splits
    (advice-add #'evil-window-split  :override #'+evil--advice-window-split)
    (advice-add #'evil-window-vsplit :override #'+evil--advice-window-vsplit)

    (defadvice! +evil--advice-make-numbered-markers-global (char)
      "In evil, registers 2-9 are buffer-local. In vim, they're
global, so..."
      :after-until #'evil-global-marker-p
      (and (>= char ?2) (<= char ?9)))

    (defadvice! +evil--advice-fix-local-vars (&rest _)
      "REVIEW Fix #2493: dir-locals cannot target
fundamental-mode when evil-mode is active. See
https://github.com/hlissner/doom-emacs/issues/2493. Revert this
if this is ever fixed upstream."
      :before #'turn-on-evil-mode
      (when (eq major-mode 'fundamental-mode)
        (hack-local-variables)))

    (defadvice! +evil--advice-fix-helpful-key-in-evil-ex (key-sequence)
      "HACK Invoking helpful from evil-ex throws a \"No recursive
edit is in progress\" error because, between evil-ex and
helpful,`abort-recursive-edit' gets called one time too many."
      :before #'helpful-key
      (when (evil-ex-p)
        (run-at-time 0.1 nil #'helpful-key key-sequence)
        (abort-recursive-edit)))

    ;; These arg types will highlight matches in the current buffer
    (evil-ex-define-argument-type regexp-match
      :runner (lambda (flag &optional arg) (+evil-ex-regexp-match flag arg 'inverted)))
    (evil-ex-define-argument-type regexp-global-match
      :runner +evil-ex-regexp-match)

    (defun +evil--regexp-match-args (arg)
      (when (evil-ex-p)
        (cl-destructuring-bind (&optional arg flags)
            (evil-delimited-arguments arg 2)
          (list arg (string-to-list flags)))))

    ;; Other commands can make use of this
    (evil-define-interactive-code "<//>"
      :ex-arg regexp-match
      (+evil--regexp-match-args evil-ex-argument))
    (evil-define-interactive-code "<//!>"
      :ex-arg regexp-global-match
      (+evil--regexp-match-args evil-ex-argument))

    ;; Forward declare these so that ex completion works, even if the autoloaded
    ;; functions aren't loaded yet.
    (evil-add-command-properties '+evil:align :ex-arg 'regexp-match)
    (evil-add-command-properties '+evil:align-right :ex-arg 'regexp-match)
    (evil-add-command-properties '+multiple-cursors:evil-mc :ex-arg 'regexp-global-match)

    ;; Lazy load evil ex commands
    (delq! 'evil-ex features)
    (add-transient-hook! 'evil-ex (provide 'evil-ex))
    (after! evil-ex (load! "+editor-evil-commands" +emacs-core-dir)))

;;;;; evil-nerd-commenter

  (use-package evil-nerd-commenter
    :commands (evilnc-comment-operator
               evilnc-inner-comment
               evilnc-outer-commenter)
    :general ([remap comment-line] #'evilnc-comment-or-uncomment-lines))

;;;;; evil-lion

  (use-package evil-lion
    :commands (evil-lion-left evil-lion-right))

;;;;; evil-snipe

  (use-package evil-snipe
    :commands
    (evil-snipe-mode
     evil-snipe-override-mode
     evil-snipe-local-mode
     evil-snipe-override-local-mode)
    :after-call pre-command-hook
    :init
    (setq evil-snipe-smart-case t
          evil-snipe-scope 'line
          evil-snipe-repeat-scope 'visible
          evil-snipe-char-fold t)
    :config
    (pushnew! evil-snipe-disabled-modes 'Info-mode 'calc-mode 'treemacs-mode 'dired-mode)
    (evil-snipe-mode +1)
    (evil-snipe-override-mode +1))

;;;;; evil-surround

  (use-package evil-surround
    :commands
    (global-evil-surround-mode
     evil-surround-edit
     evil-Surround-edit
     evil-surround-region)
    :config
    (global-evil-surround-mode 1))

;;;;; evil-traces

  (use-package evil-traces
    :after-call evil-ex
    :config
    (pushnew! evil-traces-argument-type-alist
              '(+evil:align . evil-traces-global)
              '(+evil:align-right . evil-traces-global))
    (evil-traces-mode))

;;;;; evil-visualstar

  (use-package evil-visualstar
    :commands (evil-visualstar/begin-search
               evil-visualstar/begin-search-forward
               evil-visualstar/begin-search-backward))

;;;;; Keybinds

  (map!
   :m  [C-i]   #'evil-jump-forward
   ;; Global evil
   :m  "]a"    #'evil-forward-arg
   :m  "[a"    #'evil-backward-arg
   :m  "]o"    #'outline-next-visible-heading
   :m  "[o"    #'outline-previous-visible-heading
   :n  "]b"    #'next-buffer
   :n  "[b"    #'previous-buffer
   :m  "]e"    #'next-error
   :m  "[e"    #'previous-error
   (:when (featurep! :ui hl-todo)
    :m  "]t"    #'hl-todo-next
    :m  "[t"    #'hl-todo-previous)
   (:when (featurep! :ui vc-gutter)
    :m  "]d"    #'git-gutter:next-hunk
    :m  "[d"    #'git-gutter:previous-hunk)
   :n  "g="    #'widen
   :v  "g="    #'+evil:narrow-buffer
   :nv "gc"    #'evilnc-comment-operator
   :nv "gO"    #'imenu
   :v  "gp"    #'+evil/paste-preserve-register
   (:when (featurep! :ui workspaces)
    :n "gt"   #'+workspace:switch-next
    :n "gT"   #'+workspace:switch-previous
    :n "]w"   #'+workspace/switch-right
    :n "[w"   #'+workspace/switch-left)

   (:when (featurep! :tools lookup)
    :nv "K"   #'+emacs-lookup-transient-state/body
    :ie "M-." #'+emacs-lookup-transient-state/body
    :nv "gd"  #'+lookup/definition
    :nv "gD"  #'+lookup/references
    :nv "gf"  #'+lookup/file
    :nv "gI"  #'+lookup/implementations
    :nv "gA"  #'+lookup/assignments)
   (:when (featurep! :tools eval)
    :nv "gr"  #'+eval:region
    :n  "gR"  #'+eval/buffer
    :v  "gR"  #'+eval:replace-region)
   ;; Restore these keybinds, since the blacklisted/overwritten gr/gR will undo
   ;; them:
   (:after helpful
    :map helpful-mode-map
    :n "gr" #'helpful-update)
   (:after compile
    :map (compilation-mode-map compilation-minor-mode-map)
    :n "gr" #'recompile)
   (:after dired
    :map dired-mode-map
    :n "gr" #'revert-buffer)

   ;; custom evil keybinds
   :n  "zn"    #'+evil:narrow-buffer
   :n  "zN"    #'+emacs/widen-indirectly-narrowed-buffer

   ;; don't leave visual mode after shifting
   :v  "<"     #'+evil/shift-left      ; vnoremap < <gv
   :v  ">"     #'+evil/shift-right      ; vnoremap > >gv

   ;; window management (prefix "C-w")
   (:map evil-window-map
    ;; Navigation
    "C-h"     #'evil-window-left
    "C-j"     #'evil-window-down
    "C-k"     #'evil-window-up
    "C-l"     #'evil-window-right
    "<left>"  #'evil-window-left
    "<right>" #'evil-window-right
    "<up>"    #'evil-window-up
    "<down>"  #'evil-window-down

    "C-w"     #'other-window
    ;; Swapping windows
    "H"       #'+evil/window-move-left
    "J"       #'+evil/window-move-down
    "K"       #'+evil/window-move-up
    "L"       #'+evil/window-move-right
    "C-S-w"   #'ace-swap-window
    ;; Window undo/redo
    "u"       #'winner-undo
    "C-u"     #'winner-undo
    "C-r"     #'winner-redo
    ;; Delete window
    "C-C"     #'ace-delete-window
    "d"       #'evil-window-delete)

   ;; `evil-surround'
   :v "S" #'evil-surround-region
   :o "s" #'evil-surround-edit
   :o "S" #'evil-Surround-edit

   ;; evil-lion
   :n "gl" #'evil-lion-left
   :n "gL" #'evil-lion-right
   :v "gl" #'evil-lion-left
   :v "gL" #'evil-lion-right

   ;; Omni-completion
   (:when (featurep! :completion company)
    (:prefix "C-x"
     :i "C-l"    #'+company/whole-lines
     :i "C-k"    #'+company/dict-or-keywords
     :i "C-f"    #'company-files
     :i "C-]"    #'company-etags
     :i "s"      #'company-ispell
     :i "C-s"    #'company-yasnippet
     :i "C-o"    #'company-capf
     :i "C-n"    #'+company/dabbrev
     :i "C-p"    #'+company/dabbrev-code-previous))))

;;;; file-templates

(module! (:editor file-templates)

  (defvar +file-templates-dir
    (concat +emacs-dir  "snippets/file-templates/")
    "The path to a directory of yasnippet folders to use for file
templates.")

  (defvar +file-templates-private-dir
    (concat +emacs-private-dir  "snippets/file-templates/")
    "The path to a directory in `+emacs-private-dir' of yasnippet
folders to use for file templates.")


  (defvar +file-templates-default-trigger "__"
    "The default yasnippet trigger key (a string) for file
template rules that don't have a :trigger property in
`+file-templates-alist'.")

  (defvar +file-templates-alist
    `((gitignore-mode)
      (dockerfile-mode)
      ("/docker-compose\\.yml$" :mode yaml-mode)
      ("/Makefile$"             :mode makefile-gmake-mode)
      ;; elisp
      ("/.dir-locals.el$")
      ("/packages\\.el$" :when +file-templates-in-emacs-dirs-p
       :trigger "__+emacs-packages"
       :mode emacs-lisp-mode)
      ("\\.el$" :when +file-templates-in-emacs-dirs-p
       :trigger "__+emacs-module"
       :mode emacs-lisp-mode)
      (emacs-lisp-mode :trigger "__initfile")
      (snippet-mode)
      ;; direnv
      ("/\\.envrc$" :trigger "__envrc" :mode direnv-envrc-mode)
      ;; Markdown
      (markdown-mode)
      ;; Nix
      ("/shell\\.nix$" :trigger "__shell.nix")
      (nix-mode)
      ;; Org
      (org-journal-mode :ignore t)
      (org-mode)
      ;; Python
      (python-mode)
      ;; Shell scripts
      ("\\.zunit$" :trigger "__zunit" :mode sh-mode)
      (fish-mode)
      (sh-mode))
    "An alist of file template rules. The CAR of each rule is
either a major mode symbol or regexp string. The CDR is a plist.
See `set-file-template!' for more information.")

;;;;; Library

  (defun +file-templates-in-emacs-dirs-p (file)
    "Returns t if FILE is in one Emacs' directories."
    (or (file-in-directory-p file +emacs-private-dir)
        (file-in-directory-p file +emacs-dir)))

  (defun +file-template-p (rule)
    "Return t if RULE applies to the current buffer."
    (let ((pred (car rule))
          (plist (cdr rule)))
      (and (or (and (symbolp pred)
                    (eq major-mode pred))
               (and (stringp pred)
                    (stringp buffer-file-name)
                    (string-match-p pred buffer-file-name)))
           (or (not (plist-member plist :when))
               (funcall (plist-get plist :when)
                        buffer-file-name))
           rule)))

  (defhook! +file-templates--hook-check ()
    "Check if the current buffer is a candidate for file template
expansion. It must be non-read-only, empty, and there must be a
rule in `+file-templates-alist' that applies to it."
    '+emacs-switch-buffer-hook
    (and buffer-file-name
         (not buffer-read-only)
         (bobp) (eobp)
         (not (member (substring (buffer-name) 0 1) '("*" " ")))
         (not (file-exists-p buffer-file-name))
         (not (buffer-modified-p))
         (null (buffer-base-buffer))
         (when-let (rule (cl-find-if #'+file-template-p +file-templates-alist))
           (apply #'+file-templates--expand rule))))

;;;;; Bootstrap

  (after! yasnippet
    (if (featurep! :editor snippets)
        (progn
          (add-to-list 'yas-snippet-dirs '+file-templates-dir 'append #'eq)
          (add-to-list 'yas-snippet-dirs '+file-templates-private-dir 'append #'eq))
      (setq yas-prompt-functions (delq #'yas-dropdown-prompt yas-prompt-functions)
            yas-snippet-dirs '(+file-templates-dir +file-templates-private-dir))
      ;; Exit snippets on ESC from normal mode
      (add-hook '+emacs-escape-hook #'yas-abort-snippet)
      ;; Ensure file templates in `+file-templates-dir' are visible
      (yas-reload-all))))

;;;; fold

(module! (:editor fold)

  (when (featurep! :editor evil)
    (define-key! 'global
      [remap evil-toggle-fold]   #'+fold/toggle
      [remap evil-close-fold]    #'+fold/close
      [remap evil-open-fold]     #'+fold/open
      [remap evil-open-fold-rec] #'+fold/open
      [remap evil-close-folds]   #'+fold/close-all
      [remap evil-open-folds]    #'+fold/open-all)
    (after! evil
      (evil-define-key* 'motion 'global
        "zj" #'+fold/next
        "zk" #'+fold/previous)))

  ;; Transient state
  (map! :leader
        :desc "Fold transient state" "-" #'+emacs-fold-transient-state/body)

;;;;; hideshow

  (use-package hideshow
    :commands (hs-toggle-hiding
               hs-hide-block
               hs-hide-level
               hs-show-all
               hs-hide-all
               hs-already-hidden-p)
    :config
    (setq hs-hide-comments-when-hiding-all nil
          ;; Nicer code-folding overlays (with fringe indicators)
          hs-set-up-overlay #'+fold-hideshow-set-up-overlay)

    (defadvice! +fold-hideshow--advice-ensure-mode (&rest _)
      "Ensure `hs-minor-mode' is enabled."
      :before '(hs-toggle-hiding
                hs-hide-block hs-hide-level
                hs-show-all hs-hide-all
                +fold/next
                +fold/previous)
      (unless (bound-and-true-p hs-minor-mode)
        (hs-minor-mode +1)))

    ;; extra folding support for more languages
    (unless (assq 't hs-special-modes-alist)
      (setq hs-special-modes-alist
            (append
             '((vimrc-mode "{{{" "}}}" "\"")
               (yaml-mode "\\s-*\\_<\\(?:[^:]+\\)\\_>"
                          ""
                          "#"
                          +fold-hideshow-forward-block-by-indent nil)
               (haml-mode "[#.%]" "\n" "/" +fold-hideshow-haml-forward-sexp nil)
               (ruby-mode "class\\|d\\(?:ef\\|o\\)\\|module\\|[[{]"
                          "end\\|[]}]"
                          "#\\|=begin"
                          ruby-forward-sexp)
               (enh-ruby-mode "class\\|d\\(?:ef\\|o\\)\\|module\\|[[{]"
                              "end\\|[]}]"
                              "#\\|=begin"
                              enh-ruby-forward-sexp nil)
               (matlab-mode "if\\|switch\\|case\\|otherwise\\|while\\|for\\|try\\|catch"
                            "end"
                            nil (lambda (_arg) (matlab-forward-sexp)))
               (nxml-mode "<!--\\|<[^/>]*[^/]>"
                          "-->\\|</[^/>]*[^/]>"
                          "<!--" sgml-skip-tag-forward nil)
               (latex-mode
                ;; LaTeX-find-matching-end needs to be inside the env
                ("\\\\begin{[a-zA-Z*]+}\\(\\)" 1)
                "\\\\end{[a-zA-Z*]+}"
                "%"
                (lambda (_arg)
                  ;; Don't fold whole document, that's useless
                  (unless (save-excursion
                            (search-backward "\\begin{document}"
                                             (line-beginning-position) t))
                    (LaTeX-find-matching-end)))
                nil))
             hs-special-modes-alist
             '((t))))))

;;;;; evil-vimish-fold

  (use-package evil-vimish-fold
    :when (featurep! :editor evil)
    :commands (evil-vimish-fold/next-fold
               evil-vimish-fold/previous-fold
               evil-vimish-fold/delete
               evil-vimish-fold/delete-all
               evil-vimish-fold/create
               evil-vimish-fold/create-line)
    :init
    (setq vimish-fold-dir (concat +emacs-cache-dir "vimish-fold/")
          vimish-fold-indication-mode 'right-fringe)
    (evil-define-key* 'motion 'global
      "zf" #'evil-vimish-fold/create
      "zF" #'evil-vimish-fold/create-line
      "zd" #'vimish-fold-delete
      "zE" #'vimish-fold-delete-all)
    :config
    (vimish-fold-global-mode +1))

;;;;; outshine

  (after! outshine
    (map!
     :map outline-minor-mode-map
     :n "z q" #'outline-hide-sublevels
     :n "z n" #'outshine-narrow-to-subtree
     :n "z w" #'widen
     :n "M-<return>" #'outline-insert-heading)
    (add-hook 'outshine-mode-hook 'evil-normalize-keymaps))

;;;;; outline-ivy

  (use-package outline-ivy
    :commands oi-jump
    :init
    ;; fix and tune outline-ivy
    ;; fix faces to follow ivy and outshine settings
    (defface oi-match-face
      '((t :inherit ivy-current-match))
      "Match face for ivy outline prompt."
      :group 'outline-ivy)
    (defface oi-face-1
      '((t :inherit outshine-level-1))
      "Ivy outline face for level 1"
      :group 'outline-ivy)
    (defface oi-face-2
      '((t :inherit outshine-level-2))
      "Ivy outline face for level 2"
      :group 'outline-ivy)
    (defface oi-face-3
      '((t :inherit outshine-level-3))
      "Ivy outline face for level 3"
      :group 'outline-ivy)

    ;; define the new faces
    (defface oi-face-4
      '((t :inherit outshine-level-4))
      "Ivy outline face for level 4"
      :group 'outline-ivy)
    (defface oi-face-5
      '((t :inherit outshine-level-5))
      "Ivy outline face for level 5"
      :group 'outline-ivy)
    (defface oi-face-6
      '((t :inherit outshine-level-6))
      "Ivy outline face for level 6"
      :group 'outline-ivy)
    (defface oi-face-7
      '((t :inherit outshine-level-7))
      "Ivy outline face for level 7"
      :group 'outline-ivy)
    (defface oi-face-8
      '((t :inherit outshine-level-8))
      "Ivy outline face for level 8"
      :group 'outline-ivy)

    :config/el-patch
    ;; PATCH `oi-format-name-pretty'
    (defun oi-format-name-pretty (str parents level)
      "Prepend invisible PARENTS to propertized STR at LEVEL."
      (concat (propertize
               (concat (when level (number-to-string level))
                       (apply 'concat parents))
               'invisible t)
              (propertize (oi-format-name str level)
                          'face (pcase level
                                  (1 'oi-face-1)
                                  (2 'oi-face-2)
                                  (3 'oi-face-3)
                                  ;; add rest of outshine levels
                                  (el-patch-add (4 'oi-face-4)
                                                (5 'oi-face-5)
                                                (6 'oi-face-6)
                                                (7 'oi-face-7)
                                                (8 'oi-face-8))))))
    :config
    (setq oi-height ivy-height)

    (ivy-add-actions
     'oi-jump
     '(("i" +counsel-outline-indirect-action
        "open heading in indirect buffer")))))

;;;; multiple-cursors

(module! (:editor multiple-cursors)

  (defvar +multiple-cursors-evil-mc-ex-global t
    "TODO")

  (defvar +multiple-cursors-evil-mc-ex-case nil
    "TODO")

;;;;; evil-multiedit

  (use-package evil-multiedit
    :when (featurep! :editor evil)
    :defer t
    :init
    ;; Otherwise byte-compiler will freak out
    (setq iedit-toggle-key-default nil))

;;;;; evil-mc

  (use-package evil-mc
    :when (featurep! :editor evil)
    :commands
    (evil-mc-make-cursor-here
     evil-mc-make-all-cursors
     evil-mc-undo-all-cursors
     evil-mc-pause-cursors
     evil-mc-resume-cursors
     evil-mc-make-and-goto-first-cursor
     evil-mc-make-and-goto-last-cursor
     evil-mc-make-cursor-in-visual-selection-beg
     evil-mc-make-cursor-in-visual-selection-end
     evil-mc-make-cursor-move-next-line
     evil-mc-make-cursor-move-prev-line
     evil-mc-make-cursor-at-pos
     evil-mc-has-cursors-p
     evil-mc-make-and-goto-next-cursor
     evil-mc-skip-and-goto-next-cursor
     evil-mc-make-and-goto-prev-cursor
     evil-mc-skip-and-goto-prev-cursor
     evil-mc-make-and-goto-next-match
     evil-mc-skip-and-goto-next-match
     evil-mc-skip-and-goto-next-match
     evil-mc-make-and-goto-prev-match
     evil-mc-skip-and-goto-prev-match)
    :init
    (defvar evil-mc-key-map (make-sparse-keymap))
    :config
    (global-evil-mc-mode +1)

    ;; REVIEW This is tremendously slow on macos and windows for some reason.
    (setq evil-mc-enable-bar-cursor (not (or IS-MAC IS-WINDOWS)))

    (after! smartparens
      ;; Make evil-mc cooperate with smartparens better
      (let ((vars (cdr (assq :default evil-mc-cursor-variables))))
        (unless (memq (car sp--mc/cursor-specific-vars) vars)
          (setcdr (assq :default evil-mc-cursor-variables)
                  (append vars sp--mc/cursor-specific-vars)))))

    ;; Whitelist more commands
    (dolist (fn '((backward-kill-word)
                  (company-complete-common . evil-mc-execute-default-complete)
                  (+emacs/backward-to-bol-or-indent . evil-mc-execute-default-call)
                  (+emacs/forward-to-last-non-comment-or-eol . evil-mc-execute-default-call)
                  ;; :editor evil
                  (evil-delete-back-to-indentation . evil-mc-execute-default-call)
                  ;; Have evil-mc work with explicit `evil-escape' (on C-g)
                  (evil-escape . evil-mc-execute-default-evil-normal-state)
                  (evil-digit-argument-or-evil-beginning-of-visual-line
                   (:default . evil-mc-execute-default-call)
                   (visual . evil-mc-execute-visual-call))
                  ;; :editor fold
                  (outshine-self-insert-command)
                  ;; :editor lispy
                  (lispy-space)
                  ;; :org org
                  ;; Add `evil-org' support
                  (evil-org-delete . evil-mc-execute-default-evil-delete)
                  (evil-org-delete-char . evil-mc-execute-default-evil-delete)
                  (evil-org-delete-backward-char . evil-mc-execute-default-evil-delete)
                  ;; :tools eval
                  (+eval:replace-region . +multiple-cursors-execute-default-operator)
                  ;; :lang ess
                  (ess-smart-comma . evil-mc-execute-call)))
      (setf (alist-get (car fn) evil-mc-custom-known-commands)
            (if (and (cdr fn) (listp (cdr fn)))
                (cdr fn)
              (list (cons :default
                          (or (cdr fn)
                              #'evil-mc-execute-default-call-with-count))))))

    ;; If we're entering insert mode, it's a good bet that we want to start
    ;; using our multiple cursors
    (add-hook 'evil-insert-state-entry-hook #'evil-mc-resume-cursors)

    ;; evil-escape's escape key sequence leaves behind extraneous characters
    (cl-pushnew 'evil-escape-mode evil-mc-incompatible-minor-modes)
    ;; Lispy commands don't register on more than 1 cursor. Lispyville is fine
    ;; though.
    (when (featurep! :editor lispy)
      (cl-pushnew 'lispy-mode evil-mc-incompatible-minor-modes))

    (defhook! +evil--hook-escape-multiple-cursors ()
      "Clear evil-mc cursors and restore state."
      '+emacs-escape-hook
      (when (evil-mc-has-cursors-p)
        (evil-mc-undo-all-cursors)
        (evil-mc-resume-cursors)
        t))

    (evil-add-command-properties '+evil:align :evil-mc t)
    (evil-add-command-properties '+multiple-cursors:evil-mc :evil-mc t)

    (map! :map evil-mc-key-map
          :nv "C-n" #'evil-mc-make-and-goto-next-cursor
          :nv "C-N" #'evil-mc-make-and-goto-last-cursor
          :nv "C-p" #'evil-mc-make-and-goto-prev-cursor
          :nv "C-P" #'evil-mc-make-and-goto-first-cursor)))

;;;; snippets

(module! (:editor snippets)

  (defvar +snippets-dir (expand-file-name "snippets/" +emacs-private-dir)
    "Directory where `yasnippet' will search for snippets.")

  (use-package yasnippet
    :defer-incrementally eldoc easymenu help-mode
    :commands
    (yas-minor-mode-on
     yas-expand
     yas-expand-snippet
     yas-lookup-snippet
     yas-insert-snippet
     yas-new-snippet
     yas-visit-snippet-file
     yas-activate-extra-mode
     yas-deactivate-extra-mode
     yas-maybe-expand-abbrev-key-filter)
    :init
    ;; Remove default ~/.emacs.d/snippets
    (defvar yas-snippet-dirs nil)

    ;; Lazy load yasnippet until it is needed
    (add-transient-hook! #'company-yasnippet (require 'yasnippet))
    :config
    ;; Add private snippets dir
    (add-to-list 'yas-snippet-dirs '+snippets-dir)

    (setq yas-verbosity (if +emacs-debug-p 3 2))

    ;; HACK Remove duplicates exist in `yas-snippet-dirs'.
    (advice-add #'yas-snippet-dirs :filter-return #'delete-dups)

    ;; Remove GUI dropdown prompt (prefer ivy/helm)
    (delq #'yas-dropdown-prompt yas-prompt-functions)
    ;; Prioritize private snippets in `+snippets-dir' over built-in ones if
    ;; there are multiple choices.
    (add-to-list 'yas-prompt-functions #'+snippets-prompt-private)

    ;; Register `def-project-mode!' modes with yasnippet. This enables project
    ;; specific snippet libraries (e.g. for Laravel, React or Jekyll projects).
    (add-hook '+emacs-project-hook #'+snippets--hook-enable-project-modes)

    ;; Exit snippets on ESC from normal mode
    (add-hook '+emacs-escape-hook #'yas-abort-snippet)

    (after! smartparens
      ;; tell smartparens overlays not to interfere with yasnippet keybinds
      (advice-add #'yas-expand :before #'sp-remove-active-pair-overlay))

    ;; evil visual-mode integration for `yas-insert-snippet'
    (advice-add #'yas-insert-snippet :around #'+snippets--advice-expand-on-region)

    ;; Remove TAB keybind
    (define-key! yas-minor-mode-map
      "<tab>" nil
      "TAB" nil)

    (define-key! snippet-mode-map
      "C-c C-k" #'+snippet--abort
      "C-c C-e" #'+snippet--edit)

    ;; Show keybind hints in snippet header-line
    (add-hook 'snippet-mode-hook #'+snippets--hook-show-hints-in-header-line)

    (map! (:map yas-keymap
           "C-e"         #'+snippets/goto-end-of-field
           "C-a"         #'+snippets/goto-start-of-field
           [M-right]     #'+snippets/goto-end-of-field
           [M-left]      #'+snippets/goto-start-of-field
           [M-backspace] #'+snippets/delete-to-start-of-field
           [backspace]   #'+snippets/delete-backward-char
           [delete]      #'+snippets/delete-forward-char-or-field
           ;; Replace commands with superior alternatives
           :map yas-minor-mode-map
           [remap yas-new-snippet]        #'+snippets/new
           [remap yas-visit-snippet-file] #'+snippets/edit)
          (:map snippet-mode-map
           "C-c C-k" #'+snippet--abort))

    ;; If in a daemon session, front-load this expensive work:
    (yas-global-mode +1))

;;;;; auto-yasnippet

  (use-package auto-yasnippet
    :defer t
    :commands
    (aya-create
     aya-persist-snippets)
    :init
    (setq aya-persist-snippets-dir (concat +emacs-etc-dir "auto-snippets/"))
    :config
    (defadvice! +snippets--advice-inhibit-yas-global-mode (orig-fn &rest args)
      "auto-yasnippet enables `yas-global-mode'. This is
obnoxious for folks like us who use yas-minor-mode and enable
yasnippet more selectively. This advice swaps `yas-global-mode'
with `yas-minor-mode'."
      :around '(aya-expand aya-open-line)
      (letf! ((#'yas-global-mode #'yas-minor-mode)
              (yas-global-mode yas-minor-mode))
        (apply orig-fn args)))))

;;; emacs

;;;; dired

(module! (:emacs dired)

  (use-package dired
    :commands dired-jump
    :init
    ;; don't prompt to revert; just do it
    (setq dired-auto-revert-buffer t
          ;; suggest a target for moving/copying intelligently
          dired-dwim-target t
          dired-hide-details-hide-symlink-targets nil
          ;; Always copy/delete recursively
          dired-recursive-copies  'always
          dired-recursive-deletes 'top
          ;; Ask whether destination dirs should get created when
          ;; copying/removing files.
          dired-create-destination-dirs 'ask
          ;; Where to store image caches
          image-dired-dir (concat +emacs-cache-dir "image-dired/")
          image-dired-db-file (concat image-dired-dir "db.el")
          image-dired-gallery-dir (concat image-dired-dir "gallery/")
          image-dired-temp-image-file (concat image-dired-dir "temp-image")
          image-dired-temp-rotate-image-file (concat image-dired-dir "temp-rotate-image")
          ;; Screens are larger nowadays, we can afford slightly larger
          ;; thumbnails
          image-dired-thumb-size 150)
    :config
    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*image-dired"
        :slot 20 :size 0.8 :select t :quit nil :ttl 0))
    (when (featurep! :editor evil)
      (set-evil-initial-state! 'image-dired-display-image-mode 'emacs))

    (let ((args (list "-ahl" "-v" "--group-directories-first")))
      (when IS-BSD
        ;; Use GNU ls as `gls' from `coreutils' if available. Add `(setq
        ;; dired-use-ls-dired nil)' to your config to suppress the Dired warning
        ;; when not using GNU ls.
        (if-let (gls (executable-find "gls"))
            (setq insert-directory-program gls)
          ;; BSD ls doesn't support -v or --group-directories-first
          (setq args (list (car args)))))
      (setq dired-listing-switches (string-join args " "))

      (defhook! +dired--hook-disable-gnu-ls-flags-maybe ()
        "Remove extraneous switches from `dired-actual-switches'
when it's uncertain that they are supported (e.g. over TRAMP or
on Windows)."
        'dired-mode-hook
        (when (or ;; (file-remote-p default-directory)
               (and (boundp 'ls-lisp-use-insert-directory-program)
                    (not ls-lisp-use-insert-directory-program)))
          (setq-local dired-actual-switches (car args)))))

    ;; Don't complain about this command being disabled when we use it
    (put 'dired-find-alternate-file 'disabled nil)

    (map! :map dired-mode-map
          ;; Kill all dired buffers on q
          :ng "q" #'+dired/quit
          ;; To be consistent with ivy/helm+wgrep integration
          "C-c C-e" #'wdired-change-to-wdired-mode
          :nv "." #'+emacs-dired-transient-state/body))

;;;;; dired-rsync

  (use-package dired-rsync
    :general (dired-mode-map "C-c C-r" #'dired-rsync))

;;;;; diredfl

  (use-package diredfl
    :hook (dired-mode . diredfl-mode))

;;;;; diff-hl

  (use-package diff-hl
    :hook (dired-mode . diff-hl-dired-mode-unless-remote)
    :hook (magit-post-refresh . diff-hl-magit-post-refresh)
    :config
    ;; use margin instead of fringe
    (diff-hl-margin-mode))

;;;;; dired-x

  (use-package dired-x
    :hook (dired-mode . dired-omit-mode)
    :config
    (setq dired-omit-verbose nil
          dired-omit-files
          (concat dired-omit-files
                  "\\|^\\.DS_Store\\'"
                  "\\|^\\.project\\(?:ile\\)?\\'"
                  "\\|^\\.\\(?:svn\\|git\\)\\'"
                  "\\|^\\.ccls-cache\\'"
                  "\\|\\(?:\\.js\\)?\\.meta\\'"
                  "\\|\\.\\(?:elc\\|o\\|pyo\\|swp\\|class\\)\\'"))
    ;; Disable the prompt about whether I want to kill the Dired buffer for a
    ;; deleted directory. Of course I do!
    (setq dired-clean-confirm-killing-deleted-buffers nil)
    ;; Let OS decide how to open certain files
    (when-let (cmd (cond (IS-MAC "open")
                         (IS-LINUX "xdg-open")
                         (IS-WINDOWS "start")))
      (setq dired-guess-shell-alist-user
            `(("\\.\\(?:docx\\|pdf\\|djvu\\|eps\\)\\'" ,cmd)
              ("\\.\\(?:jpe?g\\|png\\|gif\\|xpm\\)\\'" ,cmd)
              ("\\.\\(?:xcf\\)\\'" ,cmd)
              ("\\.csv\\'" ,cmd)
              ("\\.tex\\'" ,cmd)
              ("\\.\\(?:mp4\\|mkv\\|avi\\|flv\\|rm\\|rmvb\\|ogv\\)\\(?:\\.part\\)?\\'" ,cmd)
              ("\\.\\(?:mp3\\|flac\\)\\'" ,cmd)
              ("\\.html?\\'" ,cmd)
              ("\\.md\\'" ,cmd))))
    (map! :map dired-mode-map
          :localleader
          "h" #'dired-omit-mode))

;;;;; fd-dired

  (use-package fd-dired
    :when (executable-find +emacs-projectile-fd-binary)
    :defer t
    :init
    (global-set-key [remap find-dired] #'fd-dired)
    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*F\\(?:d\\|ind\\)\\*$" :ignore t)))

;;;;; dired-git-info

;;;###package dired-git-info
  (map! :after dired
        :map (dired-mode-map ranger-mode-map)
        :ng ")" #'dired-git-info-mode)
  (setq dgi-commit-message-format "%h %cs %s"
        dgi-auto-hide-details-p nil)
  (after! wdired
    ;; Temporarily disable `dired-git-info-mode' when entering wdired, due to
    ;; reported incompatibilities.
    (defvar +dired--git-info-p nil)
    (defadvice! +dired--advice-disable-git-info (&rest _)
      "Disable git info."
      :before #'wdired-change-to-wdired-mode
      (setq +dired--git-info-p (bound-and-true-p dired-git-info-mode))
      (when +dired--git-info-p
        (dired-git-info-mode -1)))
    (defadvice! +dired--advice-reactivate-git-info (&rest _)
      "Reactivate git info."
      :after '(wdired-exit
               wdired-abort-changes
               wdired-finish-edit)
      (when +dired--git-info-p
        (dired-git-info-mode +1)))))

;;;; electric

(module! (:emacs electric)

  (defvar-local +electric-indent-words '()
    "The list of electric words. Typing these will trigger
reindentation of the current line.")

  (after! electric
    (setq-default electric-indent-chars '(?\n ?\^?))

    (defhook! +electric-indent--hook-char (_c)
      "Electric indent."
      'electric-indent-functions
      (when (and (eolp) +electric-indent-words)
        (save-excursion
          (backward-word)
          (looking-at-p (concat "\\<" (regexp-opt +electric-indent-words))))))))

;;;; ibuffer

(module! (:emacs ibuffer)

  (after! ibuffer
    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*Ibuffer\\*$" :ignore t))

    (setq ibuffer-show-empty-filter-groups nil
          ibuffer-filter-group-name-face '(:inherit (success bold))
          ibuffer-formats
          `((mark modified read-only locked
                  (name 18 18 :left :elide)
                  " " (size 9 -1 :right)
                  " " (mode 16 16 :left :elide)
                  ,@(when (require 'ibuffer-vc nil t)
                      '(" " (vc-status 12 :left)))
                  " " filename-and-process)
            (mark " " (name 16 -1) " " filename))
          ibuffer-use-other-window nil)

    ;; Display buffer icons on GUI
    (define-ibuffer-column icon (:name "  ")
      (let ((icon (if (and (buffer-file-name)
                           (all-the-icons-auto-mode-match?))
                      (all-the-icons-icon-for-file (file-name-nondirectory (buffer-file-name)) :v-adjust -0.05)
                    (all-the-icons-icon-for-mode major-mode :v-adjust -0.05))))
        (if (symbolp icon)
            (setq icon (all-the-icons-faicon "file-o" :face 'all-the-icons-dsilver :height 0.8 :v-adjust 0.0))
          icon)))

    ;; Redefine size column to display human readable size
    (define-ibuffer-column size
      (:name "Size"
       :inline t
       :header-mouse-map ibuffer-size-header-map)
      (file-size-human-readable (buffer-size)))

    (when (featurep! :ui workspaces)
      (define-ibuffer-filter workspace-buffers
          "Filter for workspace buffers"
        (:reader (+workspace-get (read-string "workspace name: "))
         :description "workspace")
        (memq buf (+workspace-buffer-list qualifier)))

      (defun +ibuffer-workspace (workspace-name)
        "Open an ibuffer window for a workspace"
        (ibuffer nil (format "%s buffers" workspace-name)
                 (list (cons 'workspace-buffers (+workspace-get workspace-name)))))

      (defun +ibuffer/open-for-current-workspace ()
        "Open an ibuffer window for the current workspace"
        (interactive)
        (+ibuffer-workspace (+workspace-current-name))))

    (when (featurep! :completion ivy)
      (defadvice! +ibuffer--advice-use-counsel-maybe (_file &optional _wildcards)
        "Use `counsel-find-file' instead of `find-file'."
        :override #'ibuffer-find-file
        (interactive)
        (counsel-find-file
         (let ((buf (ibuffer-current-buffer)))
           (if (buffer-live-p buf)
               (with-current-buffer buf
                 default-directory)
             default-directory)))))

    (map! :map ibuffer-mode-map
          :ne "q"  #'kill-current-buffer
          :e "/ w" #'ibuffer-filter-by-workspace-buffers)

    (when (featurep! :editor evil)
      (set-evil-initial-state! 'ibuffer-mode 'emacs)))

;;;;; ibuffer-projectile

  (use-package ibuffer-projectile
    ;; Group ibuffer's list by project root
    :hook (ibuffer . ibuffer-projectile-set-filter-groups)))


;;;; imenu

(module! (:emacs imenu)

  (use-package imenu-list
    :commands imenu-list-smart-toggle
    :config
    (setq imenu-list-idle-update-delay 0.5
          imenu-list-focus-after-activation t)

    (when (featurep! :editor evil)
      (set-evil-initial-state! 'ibuffer-mode 'emacs))

    (map! :map imenu-list-major-mode-map
          "z r" #'+fold/open-all
          "z m" #'+fold/close-all
          "/" #'evil-ex-search-forward
          "n" #'evil-ex-search-next
          "N" #'evil-ex-search-previous)))


;;;; vc

(module! (:emacs vc)

  (when IS-WINDOWS
    (setenv "GIT_ASKPASS" "git-gui--askpass"))

  (after! vc-annotate
    (when (featurep! :ui popup)
      (set-popup-rules!
        ;; *vc-diff*
        '(("^\\vc-d" :select nil)
          ;; *vc-change-log*
          ("^\\vc-c" :select t))))
    (when (featurep! :editor evil)
      (set-evil-initial-state!
        '(vc-annotate-mode vc-git-log-view-mode)
        'normal))
    ;; Clean up after itself
    (define-key vc-annotate-mode-map [remap quit-window] #'kill-current-buffer))

;;;;; git-timemachine

  (after! git-timemachine
    (setq git-timemachine-show-minibuffer-details t)
    (advice-add #'git-timemachine--show-minibuffer-details :override
                #'+vc--advice-update-header-line)
    (when (featurep! :editor evil)
      (after! evil
        ;; rehash evil keybindings so they are recognized
        (add-hook 'git-timemachine-mode-hook #'evil-normalize-keymaps)))
    (add-transient-hook! #'git-timemachine-blame (require 'magit-blame))
    (map! :map git-timemachine-mode-map
          :n "C-p"  #'git-timemachine-show-previous-revision
          :n "C-n"  #'git-timemachine-show-next-revision
          :n "g b"  #'git-timemachine-blame
          :n "gtc" #'git-timemachine-show-commit))

;;;;; git-commit

  (use-package git-commit
    :hook (+emacs-first-file . global-git-commit-mode)
    :config
    (when (featurep! :editor snippets)
      (set-yas-minor-mode! 'git-commit-mode))

    ;; Enforce git commit conventions.
    ;; See https://chris.beams.io/posts/git-commit/
    (setq git-commit-summary-max-length 50
          git-commit-style-convention-checks '(overlong-summary-line non-empty-second-line))
    (setq-hook! 'git-commit-mode-hook fill-column 72)

    (defhook! +vc-start-in-insert-state-maybe ()
      "Start git-commit-mode in insert state if in a blank commit message,
otherwise in default state."
      'git-commit-setup-hook
      (when (and (bound-and-true-p evil-mode)
                 (not (evil-emacs-state-p))
                 (bobp) (eolp))
        (evil-insert-state)))))

;;; term

;;;; eshell

(module! (:term eshell)

  (defvar +eshell-config-dir
    (expand-file-name "eshell/" +emacs-private-dir)
    "Where to store eshell configuration files, as opposed to
`eshell-directory-name', which is where Emacs will store
temporary/data files.")

  (defvar eshell-directory-name (concat +emacs-etc-dir "eshell")
    "Where to store temporary/data files, as opposed to
`eshell-config-dir',which is where Emacs will store eshell
configuration files.")

  (defvar +eshell-enable-new-shell-on-split t
    "If non-nil, spawn a new eshell session after splitting from
an eshell buffer.")

  (defvar +eshell-kill-window-on-exit nil
    "If non-nil, eshell will close windows along with its eshell
buffers.")

  (defvar +eshell-aliases
    '(("q"  "exit")           ; built-in
      ("f"  "find-file $1")
      ("ff" "find-file-other-window $1")
      ("bd" "eshell-up $1")   ; `eshell-up'
      ("rg" "rg --color=always")
      ("ag" "ag --color=always")
      ("l"  "ls -lh --group-directories-first")
      ("ll" "ls -lah --group-directories-first")
      ("git" "git --no-pager $*")
      ("gg" "magit-status")
      ("cdp" "cd-to-project")
      ("clear" "clear-scrollback")) ; more sensible than default
    "An alist of default eshell aliases, meant to emulate useful
shell utilities,like fasd and bd. Note that you may overwrite
these in your `eshell-aliases-file'. This is here to provide an
alternative, elisp-centric way to define your aliases. You should
use `set-eshell-alias!' to change this.")

  ;; These files are exceptions, because they may contain configuration
  (defvar eshell-aliases-file (concat +eshell-config-dir "aliases"))
  (defvar eshell-rc-script    (concat +eshell-config-dir "profile"))
  (defvar eshell-login-script (concat +eshell-config-dir "login"))

  (defvar +eshell--default-aliases nil)

  ;; PATCH Workaround for https://github.com/company-mode/company-mode/issues/409
  (el-patch-feature em-cmpl)
  (after! em-cmpl
    (el-patch-defun eshell-complete-parse-arguments ()
      "Parse the command line arguments for `pcomplete-argument'."
      (when (and eshell-no-completion-during-jobs
                 (eshell-interactive-process))
        (el-patch-remove (insert-and-inherit "\t"))
        (throw 'pcompleted t))
      (let ((end (point-marker))
            (begin (save-excursion (eshell-bol) (point)))
            (posns (list t))
            args delim)
        (when (memq this-command '(pcomplete-expand
                                   pcomplete-expand-and-complete))
          (run-hook-with-args 'eshell-expand-input-functions begin end)
          (if (= begin end)
              (end-of-line))
          (setq end (point-marker)))
        (if (setq delim
                  (catch 'eshell-incomplete
                    (ignore
                     (setq args (eshell-parse-arguments begin end)))))
            (cond ((memq (car delim) '(?\{ ?\<))
                   (setq begin (1+ (cadr delim))
                         args (eshell-parse-arguments begin end)))
                  ((eq (car delim) ?\()
                   (eshell-complete-lisp-symbol)
                   (throw 'pcompleted t))
                  (t
                   (el-patch-remove (insert-and-inherit "\t"))
                   (throw 'pcompleted t))))
        (when (get-text-property (1- end) 'comment)
          (el-patch-remove (insert-and-inherit "\t"))
          (throw 'pcompleted t))
        (let ((pos begin))
          (while (< pos end)
            (if (get-text-property pos 'arg-begin)
                (nconc posns (list pos)))
            (setq pos (1+ pos))))
        (setq posns (cdr posns))
        (cl-assert (= (length args) (length posns)))
        (let ((a args)
              (i 0)
              l)
          (while a
            (if (and (consp (car a))
                     (eq (caar a) 'eshell-operator))
                (setq l i))
            (setq a (cdr a) i (1+ i)))
          (and l
               (setq args (nthcdr (1+ l) args)
                     posns (nthcdr (1+ l) posns))))
        (cl-assert (= (length args) (length posns)))
        (when (and args (eq (char-syntax (char-before end)) ? )
                   (not (eq (char-before (1- end)) ?\\)))
          (nconc args (list ""))
          (nconc posns (list (point))))
        (cons (mapcar
               (function
                (lambda (arg)
                  (let ((val
                         (if (listp arg)
                             (let ((result
                                    (eshell-do-eval
                                     (list 'eshell-commands arg) t)))
                               (cl-assert (eq (car result) 'quote))
                               (cadr result))
                           arg)))
                    (if (numberp val)
                        (setq val (number-to-string val)))
                    (or val ""))))
               args)
              posns))))


  (after! eshell
    (setq eshell-banner-message
          '(format "%s %s\n"
                   (propertize (format " %s " (string-trim (buffer-name)))
                               'face 'mode-line-highlight)
                   (propertize (current-time-string)
                               'face 'font-lock-keyword-face))
          eshell-scroll-to-bottom-on-input 'all
          eshell-scroll-to-bottom-on-output 'all
          eshell-kill-processes-on-exit t
          eshell-hist-ignoredups t
          ;; don't record command in history if prefixed with whitespace
          eshell-input-filter #'eshell-input-filter-initial-space
          ;; em-prompt
          eshell-prompt-regexp "^.* λ "
          eshell-prompt-function #'+eshell-default-prompt
          ;; em-glob
          eshell-glob-case-insensitive t
          eshell-error-if-no-glob t)
    ;; Consider eshell buffers real
    (add-hook 'eshell-mode-hook #'+emacs--hook-mark-buffer-as-real)

    ;; Keep track of open eshell buffers
    (add-hook 'eshell-mode-hook #'+eshell--hook-init)
    (add-hook 'eshell-exit-hook #'+eshell--hook-cleanup)

    ;; Enable autopairing in eshell
    (add-hook 'eshell-mode-hook #'smartparens-mode)

    ;; Persp-mode/workspaces integration
    (when (featurep! :ui workspaces)
      (add-hook 'persp-activated-functions #'+eshell--hook-switch-workspace)
      (add-hook 'persp-before-switch-functions #'+eshell--hook-save-workspace))

    ;; UI enhancements
    (defhook! +eshell--hook-remove-fringes ()
      "Remove fringes in eshell."
      'eshell-mode-hook
      (set-window-fringes nil 0 0)
      (set-window-margins nil 1 nil))

    (defhook! +eshell--hook-enable-text-wrapping ()
      "Enable text wrapping in eshell."
      'eshell-mode-hook
      (visual-line-mode +1)
      (set-display-table-slot standard-display-table 0 ?\ ))

    (add-hook 'eshell-mode-hook #'hide-mode-line-mode)

    ;; Don't auto-write our aliases! Let us manage our own `eshell-aliases-file'
    ;; or configure `+eshell-aliases' via elisp.
    (advice-add #'eshell-write-aliases-list :override #'ignore)

    ;; Visual commands require a proper terminal. Eshell can't handle that, so
    ;; it delegates these commands to a term buffer.
    (after! em-term
      (pushnew! eshell-visual-commands "tmux" "htop" "vim" "nvim" "ncmpcpp"))

    (after! em-alias
      (setq +eshell--default-aliases eshell-command-aliases-list
            eshell-command-aliases-list
            (append eshell-command-aliases-list
                    +eshell-aliases)))

    ;; Evil
    (when (featurep! :editor evil)
      (advice-add #'evil-collection-eshell-next-prompt-on-insert
                  :override #'+eshell--advice-goto-prompt-on-insert))

    ;; Syntax highlighting for cat. From github.com/manateelazycat/aweshell
    (defadvice! +eshell--advice-cat-with-syntax-highlight (filename)
      "Like cat(1) but with syntax highlighting."
      :override #'eshell/cat
      (let ((existing-buffer (get-file-buffer filename))
            (buffer (find-file-noselect filename)))
        (eshell-print
         (with-current-buffer buffer
           (if (fboundp 'font-lock-ensure)
               (font-lock-ensure)
             (with-no-warnings
               (font-lock-fontify-buffer)))
           (buffer-string)))
        (unless existing-buffer
          (kill-buffer buffer))
        nil))

    (defhook! +eshell--hook-init-keymap ()
      "Setup eshell keybindings. This must be done in a hook
because eshell-mode redefines its keys every time `eshell-mode'
is enabled."
      'eshell-first-time-mode-hook
      (map! :map eshell-mode-map
            :n "RET"     #'+eshell/goto-end-of-prompt
            :n [return]  #'+eshell/goto-end-of-prompt
            :ni "C-j"    #'eshell-next-matching-input-from-input
            :ni "C-k"    #'eshell-previous-matching-input-from-input
            :ig "C-d"    #'+eshell/quit-or-delete-char
            :i "C-c h"   #'evil-window-left
            :i "C-c j"   #'evil-window-down
            :i "C-c k"   #'evil-window-up
            :i "C-c l"   #'evil-window-right
            "TAB"   #'+company/complete
            [tab]   #'+company/complete
            "C-s"   #'+eshell/search-history
            ;; Emacs bindings
            "C-e"   #'end-of-line
            ;; Tmux-esque prefix keybinds
            "C-c s" #'+eshell/split-below
            "C-c v" #'+eshell/split-right
            "C-c x" #'+eshell/kill-and-close
            [remap split-window-below]  #'+eshell/split-below
            [remap split-window-right]  #'+eshell/split-right
            [remap +emacs/backward-to-bol-or-indent] #'eshell-bol
            [remap evil-delete-back-to-indentation] #'eshell-kill-input
            [remap evil-window-split]   #'+eshell/split-below
            [remap evil-window-vsplit]  #'+eshell/split-right
            ;; To emulate terminal keybinds
            "C-l"   #'eshell/clear
            (:localleader
             "b" #'eshell-insert-buffer-name
             "e" #'eshell-insert-envvar
             "s" #'+eshell/search-history))))

;;;;; eshell-up

  (use-package eshell-up
    :commands eshell-up eshell-up-peek)

;;;;; eshell-z

  (use-package eshell-z
    :after eshell
    :config
    ;; Use zsh's db if it exists, otherwise, store it in `+emacs-cache-dir'
    (unless (file-exists-p eshell-z-freq-dir-hash-table-file-name)
      (setq eshell-z-freq-dir-hash-table-file-name
            (expand-file-name "z" eshell-directory-name))))

;;;;; eshell-help

  (use-package esh-help
    :after eshell
    :config (setup-esh-help-eldoc))

;;;;; eshell-did-you-mean

  (use-package eshell-did-you-mean
    :after esh-mode ; Specifically esh-mode, not eshell
    :config
    (eshell-did-you-mean-setup)
    ;; HACK There is a known issue with `eshell-did-you-mean' where it does not
    ;;      work on first invocation, so we invoke it once manually by setting
    ;;      the last command and then calling the output filter.
    (setq eshell-last-command-name "catt")
    (eshell-did-you-mean-output-filter "catt: command not found"))

;;;;; eshell-syntax-highlighting

  (use-package eshell-syntax-highlighting
    :hook (eshell-mode . eshell-syntax-highlighting-mode))

;;;;; fish-completion

  (use-package fish-completion
    :hook (eshell-mode . fish-completion-mode)
    :init (setq fish-completion-fallback-on-bash-p t)
    :config
    (defadvice! +eshell--advice-fallback-to-bash (&rest _)
      "HACK Even with `fish-completion-fallback-on-bash-p'
        non-nil, fish must be installed for bash completion to
        work. How frustrating. This way we can at least get bash
        completion whether or not fish is present. "
      :before-until #'fish-completion--list-completions-with-desc
      (unless (executable-find "fish") ""))))

;;; tools

;;;; backups

(module! (:tools backups)

  (setq make-backup-files t ; Activate backups
        ;; If we backup, backup (nearly) everything
        vc-make-backup-files t))


;;;; eval

(module! (:tools eval)

  ;; remove ellipsis when printing sexps in message buffer
  (setq eval-expression-print-length nil
        eval-expression-print-level  nil)

  (when (featurep! :ui popup)
    (set-popup-rule!
      (lambda (bufname _)
        (when (boundp '+eval-repl-mode)
          (buffer-local-value '+eval-repl-mode (get-buffer bufname))))
      :ttl (lambda (buf)
             (unless (plist-get +eval-repl-plist :persist)
               (when-let (process (get-buffer-process buf))
                 (set-process-query-on-exit-flag process nil)
                 (kill-process process)
                 (kill-buffer buf))))
      :size 0.25 :quit nil))

  (after! quickrun
    (setq quickrun-focus-p nil)

    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*quickrun" :size 0.3 :ttl 0))

    (defadvice! +eval--advice-quickrun-fix-evil-visual-region ()
      "Make `quickrun-replace-region' recognize evil visual selections."
      :override #'quickrun--outputter-replace-region
      (let ((output (buffer-substring-no-properties (point-min) (point-max))))
        (with-current-buffer quickrun--original-buffer
          (cl-destructuring-bind (beg . end)
              ;; Because `deactivate-mark', the function, was used in
              ;; `quickrun--region-command-common' instead of `deactivate-mark',
              ;; the variable, the selection is disabled by this point.
              (if (bound-and-true-p evil-local-mode)
                  (cons evil-visual-beginning evil-visual-end)
                (cons (region-beginning) (region-end)))
            (delete-region beg end)
            (insert output))
          (setq quickrun-option-outputter quickrun--original-outputter))))

    (defadvice! +eval--advice-quickrun-auto-close (&rest _)
      "Silently re-create the quickrun popup when re-evaluating."
      :before '(quickrun quickrun-region)
      (when-let (win (get-buffer-window quickrun--buffer-name))
        (let ((inhibit-message t))
          (quickrun--kill-running-process)
          (message ""))
        (delete-window win)))

    (defhook! +eval--hook-quickrun-shrink-window ()
      "Shrink the quickrun output window once code evaluation is
complete."
      'quickrun-after-run-hook
      (when-let (win (get-buffer-window quickrun--buffer-name))
        (with-selected-window (get-buffer-window quickrun--buffer-name)
          (let ((ignore-window-parameters t))
            (shrink-window-if-larger-than-buffer)))))

    (defhook! +eval--hook-quickrun-scroll-to-bof ()
      "Ensures window is scrolled to BOF on invocation."
      'quickrun-after-run-hook
      (when-let (win (get-buffer-window quickrun--buffer-name))
        (with-selected-window win
          (goto-char (point-min)))))))


;;;; lookup

(module! (:tools lookup)

  (defvar +lookup-provider-url-alist
    (append '(("Google"            +lookup--online-backend-google "https://google.com/search?q=%s")
              ("Google images"     "https://www.google.com/images?q=%s")
              ("Google maps"       "https://maps.google.com/maps?q=%s")
              ("Project Gutenberg" "http://www.gutenberg.org/ebooks/search/?query=%s")
              ("DuckDuckGo"        +lookup--online-backend-duckduckgo "https://duckduckgo.com/?q=%s")
              ("DevDocs.io"        "https://devdocs.io/#q=%s")
              ("StackOverflow"     "https://stackoverflow.com/search?q=%s")
              ("Github"            "https://github.com/search?ref=simplesearch&q=%s")
              ("Youtube"           "https://youtube.com/results?aq=f&oq=&search_query=%s")
              ("Wolfram alpha"     "https://wolframalpha.com/input/?i=%s")
              ("Wikipedia"         "https://wikipedia.org/search-redirect.php?language=en&go=Go&search=%s")
              ("MDN"               "https://developer.mozilla.org/en-US/search?q=%s")))
    "An alist that maps online resources to either:

1. A search url (needs on '%s' to substitute with an url encoded
query),
2. A non-interactive function that returns the search url in #1,
3. An interactive command that does its own search for that
provider.

Used by `+lookup/online'.")

  (defvar +lookup-open-url-fn #'browse-url
    "Function to use to open search urls.")

  (defvar +lookup-definition-functions
    '(+lookup-xref-definitions-backend-fn
      +lookup-dumb-jump-backend-fn
      +lookup-project-search-backend-fn
      +lookup-evil-goto-definition-backend-fn)
    "Functions for `+lookup/definition' to try, before resorting to `dumb-jump'.
Stops at the first function to return non-nil or change the
current window/point.

If the argument is interactive (satisfies `commandp'), it is
called with `call-interactively' (with no arguments). Otherwise,
it is called with one argument: the identifier at point. See
`set-lookup-handlers!' about adding to this list.")

  (defvar +lookup-implementations-functions ()
    "Function for `+lookup/implementations' to try. Stops at the
first function to return non-nil or change the current
window/point.

If the argument is interactive (satisfies `commandp'), it is
called with `call-interactively' (with no arguments). Otherwise,
it is called with one argument: the identifier at point. See
`set-lookup-handlers!' about adding to this list.")

  (defvar +lookup-type-definition-functions ()
    "Functions for `+lookup/type-definition' to try. Stops at the
first function to return non-nil or change the current
window/point.

If the argument is interactive (satisfies `commandp'), it is
called with `call-interactively' (with no arguments). Otherwise,
it is called with one argument: the identifier at point. See
`set-lookup-handlers!' about adding to this list.")

  (defvar +lookup-references-functions
    '(+lookup-xref-references-backend-fn
      +lookup-project-search-backend-fn)
    "Functions for `+lookup/references' to try, before resorting to `dumb-jump'.
Stops at the first function to return non-nil or change the
current window/point.

If the argument is interactive (satisfies `commandp'), it is
called with `call-interactively' (with no arguments). Otherwise,
it is called with one argument: the identifier at point. See
`set-lookup-handlers!' about adding to this list.")

  (defvar +lookup-documentation-functions
    '(+lookup-online-backend-fn)
    "Functions for `+lookup/documentation' to try, before resorting
to `dumb-jump'. Stops at the first function to return non-nil or
change the current window/point.

If the argument is interactive (satisfies `commandp'), it is
called with `call-interactively' (with no arguments). Otherwise,
it is called with one argument: the identifier at point. See
`set-lookup-handlers!' about adding to this list.")

  (defvar +lookup-file-functions
    '(+lookup-bug-reference-backend-fn
      +lookup-ffap-backend-fn)
    "Function for `+lookup/file' to try, before restoring to `find-file-at-point'.
Stops at the first function to return non-nil or change the
current window/point.

If the argument is interactive (satisfies `commandp'), it is
called with `call-interactively' (with no arguments). Otherwise,
it is called with one argument: the identifier at point. See
`set-lookup-handlers!' about adding to this list.")


;;;;; dumb-jump

  (use-package dumb-jump
    :commands dumb-jump-result-follow
    :config
    (setq dumb-jump-default-project +emacs-dir
          dumb-jump-prefer-searcher 'rg
          dumb-jump-aggressive nil
          dumb-jump-selector
          (cond ((featurep! :completion ivy)  'ivy)
                ((featurep! :completion helm) 'helm)
                ('popup)))
    (add-hook 'dumb-jump-after-jump-hook #'better-jumper-set-jump))


;;;;; xref

  ;; The lookup commands are superior, and will consult xref if there are no
  ;; better backends available.
  (global-set-key [remap xref-find-definitions] #'+lookup/definition)
  (global-set-key [remap xref-find-references]  #'+lookup/references)

  (after! xref
    ;; We already have `projectile-find-tag' and `evil-jump-to-tag', no need for
    ;; xref to be one too.
    (remove-hook 'xref-backend-functions #'etags--xref-backend)
    ;; ...however, it breaks `projectile-find-tag', unless we put it back.
    (defadvice! +lookup--advice-projectile-find-tag (orig-fn)
      "Don't break projectile."
      :around #'projectile-find-tag
      (let ((xref-backend-functions '(etags--xref-backend t)))
        (funcall orig-fn)))

    ;; This integration is already built into evil
    (unless (featurep! :editor evil)
      ;; Use `better-jumper' instead of xref's marker stack
      (advice-add #'xref-push-marker-stack :around #'doom-set-jump-a))

    (use-package ivy-xref
      :when (featurep! :completion ivy)
      :config
      (set-popup-rule! "^\\*xref\\*$" :ignore t)
      (setq xref-show-definitions-function #'ivy-xref-show-defs
            xref-show-xrefs-function       #'ivy-xref-show-xrefs)

      ;; HACK Fix #4386: `ivy-xref-show-xrefs' calls `fetcher' twice, which has
      ;; side effects that breaks in some cases (i.e. on `dired-do-find-regexp').
      (defadvice! +lookup--advice-fix-ivy-xrefs (orig-fn fetcher alist)
        "TODO"
        :around #'ivy-xref-show-xrefs
        (when (functionp fetcher)
          (setf (alist-get 'fetched-xrefs alist)
                (funcall fetcher)))
        (funcall orig-fn fetcher alist)))

    (use-package helm-xref
      :when (featurep! :completion helm)))


;;;;; Dash docset integration

  (use-package dash-docs
    :defer t
    :init
    (add-hook '+lookup-documentation-functions #'+lookup-dash-docsets-backend-fn)
    :config
    (setq dash-docs-enable-debugging +emacs-debug-p
          dash-docs-docsets-path (concat +emacs-etc-dir "docsets/")
          dash-docs-min-length 2
          dash-docs-browser-func #'eww)

    (cond ((featurep! :completion helm)
           (require 'helm-dash nil t))
          ((featurep! :completion ivy)
           (require 'counsel-dash nil t)))))


;;;; magit

(module! (:tools magit)

  (defvar +magit-fringe-size '(13 . 1)
    "Size of the fringe in magit-mode buffers.
Can be an integer or a cons cell whose CAR and CDR are integer
widths for the left and right fringe. Only has an effect in GUI
Emacs.")

  (use-package magit
    :commands magit-file-delete
    :defer-incrementally (dash f s with-editor git-commit package eieio transient)
    :init
    (setq magit-auto-revert-mode nil ; we already use `global-auto-revert-mode'
          transient-levels-file  (concat +emacs-etc-dir "transient/levels")
          transient-values-file  (concat +emacs-etc-dir "transient/values")
          transient-history-file (concat +emacs-etc-dir "transient/history"))
    :config
    (setq transient-default-level 5
          magit-revision-show-gravatars '("^Author:     " . "^Commit:     ")
          magit-diff-refine-hunk t) ; show granular diffs in selected hunk
    (add-hook 'magit-process-mode-hook #'goto-address-mode)

    ;; The default location for git-credential-cache is in
    ;; ~/.cache/git/credential. However, if ~/.git-credential-cache/ exists,
    ;; then it is used instead. Magit seems to be hardcoded to use the latter,
    ;; so here we override it to have more correct behavior.
    (unless (file-exists-p "~/.git-credential-cache/")
      (setq magit-credential-cache-daemon-socket
            (+emacs-glob (or (getenv "XDG_CACHE_HOME")
                             "~/.cache/")
                         "git/credential/socket")))

    ;; Prevent scrolling when manipulating magit-status hunks. Otherwise you
    ;; must reorient yourself every time you stage/unstage/discard/etc a hunk.
    ;; Especially so on larger projects."
    (defvar +magit--pos nil)
    (defhook! +magit--hook-set-window-state ()
      "Prevent scrolling when manipulating magit-status hunks."
      'magit-pre-refresh-hook
      (setq-local +magit--pos (list (current-buffer) (point) (window-start))))

    (defhook! +magit--hook-restore-window-state ()
      "Prevent scrolling when manipulating magit-status hunks."
      'magit-post-refresh-hook
      (when (and +magit--pos (eq (current-buffer) (car +magit--pos)))
        (goto-char (cadr +magit--pos))
        (set-window-start nil (caddr +magit--pos) t)
        (kill-local-variable '+magit--pos)))

    ;; Leave it to `+magit-display-buffer' and `+magit-display-popup-buffer' to
    ;; manage popup windows.
    (setq transient-display-buffer-action '(display-buffer-below-selected)
          magit-display-buffer-function #'+magit-display-buffer)
    (when (featurep! :ui popup)
      (set-popup-rule! "^\\(?:\\*magit\\|magit:\\| \\*transient\\*\\)" :ignore t))

    ;; so magit buffers can be switched to (except for process buffers)
    (defun +magit-buffer-p (buf)
      (with-current-buffer buf
        (and (derived-mode-p 'magit-mode)
             (not (eq major-mode 'magit-process-mode)))))
    (add-to-list '+emacs-real-buffer-functions #'+magit-buffer-p nil #'eq)
    ;; modeline isn't helpful in magit
    (add-hook! '(magit-mode-hook magit-popup-mode-hook)
               #'hide-mode-line-mode)
    ;; properly kill leftover magit buffers on quit
    (define-key magit-status-mode-map [remap magit-mode-bury-buffer] #'+magit/quit)

    ;; Close transient with ESC
    (define-key transient-map [escape] #'transient-quit-one)

    (defhook! +magit--hook-enlargen-fringe ()
      "Make fringe larger in magit."
      'magit-mode-hook
      (add-hook! 'window-configuration-change-hook :local
        (and (display-graphic-p)
             (derived-mode-p 'magit-mode)
             +magit-fringe-size
             (let ((left  (or (car-safe +magit-fringe-size) +magit-fringe-size))
                   (right (or (cdr-safe +magit-fringe-size) +magit-fringe-size)))
               (set-window-fringes nil left right))))))


;;;;; magit-todos

  (use-package magit-todos
    :hook (magit-mode . magit-todos-mode)
    :config
    (setq magit-todos-keyword-suffix "\\(?:([^)]+)\\)?:?")
    (define-key magit-todos-section-map "j" nil))


;;;;; evil-collection-magit

  (use-package evil-collection-magit
    :when (featurep! :editor evil)
    :defer t
    :init
    (defvar evil-collection-magit-use-z-for-folds t)
    :config
    ;; Replaced by z1, z2, z3, etc
    (unmap! magit-mode-map "M-1" "M-2" "M-3" "M-4" "1" "2" "3" "4" "0")

    (evil-define-key* '(normal visual) magit-mode-map
      "zz" #'evil-scroll-line-to-center)

    ;; Fix these keybinds because they are blacklisted
    (map! (:map magit-mode-map
           :nv "]" #'magit-section-forward-sibling
           :nv "[" #'magit-section-backward-sibling
           :nv "gr" #'magit-refresh
           :nv "gR" #'magit-refresh-all)
          (:map magit-status-mode-map
           :nv "gz" #'magit-refresh)
          (:map magit-diff-mode-map
           :nv "gd" #'magit-jump-to-diffstat-or-diff))

    (define-key! 'normal
      (magit-status-mode-map
       magit-stash-mode-map
       magit-revision-mode-map
       magit-diff-mode-map)
      [tab] #'magit-section-toggle)

    (after! git-rebase
      (dolist (key '(("M-k" . "gk") ("M-j" . "gj")))
        (when-let (desc (assoc (car key) evil-collection-magit-rebase-commands-w-descriptions))
          (setcar desc (cdr key))))
      (evil-define-key* evil-collection-magit-state git-rebase-mode-map
        "gj" #'git-rebase-move-line-down
        "gk" #'git-rebase-move-line-up))))

;;; org

;;;; org

(module! (:org org)

  (defvar +org-babel-native-async-langs '(python)
    "Languages that will use `ob-comint' instead of `ob-async'
for `:async'.")

  (defvar +org-babel-mode-alist
    '((c . C)
      (cpp . C)
      (C++ . C)
      (D . C)
      (elisp . emacs-lisp)
      (sh . shell)
      (bash . shell)
      (matlab . octave))
    "An alist mapping languages to babel libraries. This is
necessary for babel libraries (ob-*.el) that don't match the name
of the language. For example, with (fish . shell) will cause
#+BEGIN_SRC fish to load ob-shell.el when executed.")

  (defvar +org-babel-load-functions ()
    "A list of functions executed to load the current executing
src block. They take one argument (the language specified in the
src block, as a string). Stops at the first function to return
non-nil.")

  (defvar +org-capture-todo-file "todo.org"
    "Default target for todo entries.
Is relative to `org-directory', unless it is absolute. Is used in
Doom's default `org-capture-templates'.")

  (defvar +org-capture-changelog-file "changelog.org"
    "Default target for changelog entries.
Is relative to `org-directory' unless it is absolute. Is used in
Doom's default `org-capture-templates'.")

  (defvar +org-capture-notes-file "notes.org"
    "Default target for storing notes.
Used as a fall back file for org-capture.el, for templates that
do not specify a target file. Is relative to `org-directory',
unless it is absolute. Is used in Doom's default
`org-capture-templates'.")

  (defvar +org-capture-journal-file "journal.org"
    "Default target for storing timestamped journal entries.
Is relative to `org-directory', unless it is absolute. Is used in
Doom's default `org-capture-templates'.")

  (defvar +org-capture-projects-file "projects.org"
    "Default, centralized target for org-capture templates.")

  (defvar +org-habit-graph-padding 2
    "The padding added to the end of the consistency graph")

  (defvar +org-habit-graph-window-ratio 0.3
    "The ratio of the consistency graphs relative to the window
width")

  (defvar +org-habit-min-width 30
    "Hides the consistency graph if the
`org-habit-graph-column'is less than this value")

  (defvar +org-agenda-local-files nil
    "Local directories which should be searched for agenda
entries.")


;;;;; org-load hooks

;;;;;; org-directory

  (defun +org--hook-init-org-directory ()
    (unless org-directory
      (setq-default org-directory "~/org"))
    (unless org-id-locations-file
      (setq org-id-locations-file (expand-file-name ".orgids" org-directory))))

;;;;;; agenda

  (defun +org--hook-init-agenda ()
    (unless org-agenda-files
      (setq-default org-agenda-files (list org-directory)))
    (defhook! +org-agenda--hook-set-agenda-files ()
      "Set `org-agenda-files'."
      'org-agenda-mode-hook
      (setq org-agenda-files (remove-duplicates
                              (append org-agenda-files
                                      +org-agenda-local-files)
                              :test #'string=)))
    (setq-default
     ;; Don't monopolize the whole frame just for the agenda
     org-agenda-window-setup 'current-window
     org-agenda-skip-unavailable-files t
     ;; Move the agenda to show the previous 3 days and the next 7 days for a
     ;; bit better context instead of just the current week which is a bit
     ;; confusing on, for example, a sunday
     org-agenda-span 10
     org-agenda-start-on-weekday nil
     org-agenda-start-day "-3d"))

;;;;;; appearance

  (defun +org--hook-init-appearance ()
    "Configures the UI for `org-mode'."
    (setq-default org-indirect-buffer-display 'current-window
                  org-eldoc-breadcrumb-separator "/"
                  org-fontify-quote-and-verse-blocks t
                  org-fontify-whole-heading-line t
                  org-hide-leading-stars t
                  org-imenu-depth 6
                  org-priority-faces
                  '((?A . error)
                    (?B . warning)
                    (?C . success))
                  org-startup-indented t
                  org-startup-with-inline-images t
                  org-tags-column 0
                  org-use-sub-superscripts '{})

    (setq org-refile-targets
          '((nil :maxlevel . 3)
            (org-agenda-files :maxlevel . 3))
          ;; Without this, completers like ivy/helm are only given the first
          ;; level of each outline candidates. i.e. all the candidates under the
          ;; "Tasks" heading are just "Tasks/". This is unhelpful. We want the
          ;; full path to each refile target! e.g. FILE/Task/heading/subheading
          org-refile-use-outline-path 'file
          org-outline-path-complete-in-steps nil)

    ;; Larger previews
    (plist-put org-format-latex-options :scale 1.5)

    ;; HACK Face specs fed directly to `org-todo-keyword-faces' don't respect
    ;;      underlying faces like the `org-todo' face does, so we define our own
    ;;      intermediary faces that extend from org-todo.
    (with-no-warnings
      (custom-declare-face '+org-todo-active '((t (:inherit (bold font-lock-constant-face org-todo)))) "")
      (custom-declare-face '+org-todo-project '((t (:inherit (bold font-lock-doc-face org-todo)))) "")
      (custom-declare-face '+org-todo-onhold '((t (:inherit (bold warning org-todo)))) "")
      (custom-declare-face '+org-todo-cancel  '((t (:inherit (bold error org-todo)))) ""))
    (setq org-todo-keywords
          '((sequence
             "TODO(t)"   ; A task that needs doing & is ready to do
             "PROJ(p)"   ; An ongoing project that cannot be completed in one step
             "STRT(s)"   ; A task that is in progress
             "WAIT(w)"   ; Something is holding up this task; or it is paused
             "|"
             "DONE(d)"    ; Task successfully completed
             "KILL(k)")   ; Task was cancelled, aborted or is no longer applicable
            (sequence
             "[ ](T)"                     ; A task that needs doing
             "[-](S)"                     ; Task is in progress
             "[?](W)"                     ; Task is being held up or paused
             "|"
             "[X](D)"))                   ; Task was completed
          org-todo-keyword-faces
          '(("[-]"  . +org-todo-active)
            ("STRT" . +org-todo-active)
            ("[?]"  . +org-todo-onhold)
            ("WAIT" . +org-todo-onhold)
            ("PROJ" . +org-todo-project)
            ("KILL" . +org-todo-cancel)))

    (defadvice! +org--advice-display-link-in-eldoc (orig-fn &rest args)
      "Display full link in minibuffer when cursor/mouse is over
it."
      :around #'org-eldoc-documentation-function
      (or (when-let (link (org-element-property :raw-link (org-element-context)))
            (format "Link: %s" link))
          (apply orig-fn args)))

    ;; Don't do automatic indent detection in org files
    (cl-pushnew 'org-mode +emacs-detect-indentation-excluded-modes :test #'eq))

;;;;;; attachments

  (defun +org--hook-init-attachments ()
    "Based on Doom's attachment system but works per project. Sets
up org's attachment system.
+ Use `+org-attach/sync' to index all attachments in `org-directory' that use
  the attach:* abbreviation and delete orphaned ones that are no longer
  referenced.
+ This compliments the +dragndrop flag which provides drag'n'drop support for
  images (with preview) and media files.
Some commands of interest:
+ `org-download-screenshot'
+ `+org-attach/file'
+ `+org-attach/url'
+ `+org-attach/sync'"
    ;; Store a link to attachments when they are attached
    (setq org-attach-store-link-p t
          ;; Inherit attachment properties from parent nodes
          org-attach-use-inheritance t)

    (use-package org-attach
      :commands (org-attach-new
                 org-attach-open
                 org-attach-open-in-emacs
                 org-attach-reveal-in-emacs
                 org-attach-url
                 org-attach-set-directory
                 org-attach-sync)
      :config
      (unless org-attach-id-dir
        ;; Centralized attachments directory by default
        (setq-default org-attach-id-dir (expand-file-name ".attach/" org-directory))))

    ;; Add inline image previews for attachment links
    (org-link-set-parameters "attachment" :image-data-fun #'+org-inline-image-data-fn)

    (unless org-attach-id-dir
      ;; Centralized attachments directory by default
      (setq-default org-attach-id-dir (expand-file-name ".attach/" org-directory)))

    (defadvice! +org--advice-dragndrop-download-fullname (orig-fn &rest args)
      "MDMD"
      :around #'org-download--fullname
      (let ((org-download-image-dir org-attach-id-dir)
            (org-download-heading-lvl nil))
        (apply orig-fn args)))

    (+org-define-basic-link "attach" (lambda () (or org-download-image-dir org-attach-id-dir "."))
      :requires 'org-download
      :image-data-fun #'+org-inline-image-data-fn)

    (defhook! +org-attach--hook-update-dirs ()
      "Update directories."
      '(org-mode-hook)
      ;; Only try to update if this is a file visiting buffer or a capture frame
      (when (and (eq major-mode 'org-mode)
                 (or (+org-capture-frame-p) (buffer-file-name)))
        (setq-local org-attach-id-dir (cond ((+org-capture-frame-p) (expand-file-name ".attach/" org-directory))
                                            ((+emacs-project-p) (+emacs-path (+emacs-project-root) ".attach/"))
                                            ((file-in-directory-p (file!) org-directory) (expand-file-name ".attach/" org-directory))
                                            (t (+emacs-path (file-name-directory (buffer-file-name)) ".attach/")))
                    org-link-abbrev-alist-local (+emacs-alist-set "attach" (abbreviate-file-name org-attach-id-dir) org-link-abbrev-alist)
                    org-download-image-dir org-attach-id-dir)))

    (after! projectile
      (add-to-list 'projectile-globally-ignored-directories org-attach-id-dir))

    (after! recentf
      (add-to-list 'recentf-exclude
                   (lambda (file) (file-in-directory-p file org-attach-id-dir)))))

;;;;;; babel

  (defun +org--hook-init-babel ()
    (setq org-src-preserve-indentation t  ; use native major-mode indentation
          org-src-tab-acts-natively t
          org-confirm-babel-evaluate nil
          org-link-elisp-confirm-function nil
          org-src-window-setup 'other-window)

    ;; I prefer C-c C-c over C-c ' (more consistent)
    (define-key org-src-mode-map (kbd "C-c C-c") #'org-edit-src-exit)

    ;; so `org-src-mode' buffers can be switched to
    (add-hook 'org-src-mode-hook #'+workspaces--hook-add-current-buffer)
    (defun +org-src-buffer-p (buf)
      (with-current-buffer buf
        (bound-and-true-p org-src-mode)))
    (add-to-list '+emacs-real-buffer-functions #'+org-src-buffer-p nil #'eq)

    ;; Don't process babel results asynchronously when exporting org, as they
    ;; won't likely complete in time, and will instead output an ob-async hash
    ;; instead of the wanted evaluation results.
    (after! ob
      (add-to-list 'org-babel-default-lob-header-args '(:sync)))

    (defadvice! +org-babel--advice-disable-async-maybe (orig-fn &optional fn arg info params)
      "Use ob-comint where supported, disable async altogether where it isn't.
We have access to two async backends: ob-comint or ob-async,
which have different requirements. This advice tries to pick the
best option between them, falling back to synchronous execution
otherwise. Without this advice, they die with an error; terrible
UX! Note: ob-comint support will only kick in for languages
listed in `+org-babel-native-async-langs'. Also adds support for
a `:sync' parameter to override `:async'."
      :around #'ob-async-org-babel-execute-src-block
      (if (null fn)
          (funcall orig-fn fn arg info params)
        (let* ((info (or info (org-babel-get-src-block-info)))
               (params (org-babel-merge-params (nth 2 info) params)))
          (if (or (assq :sync params)
                  (not (assq :async params))
                  (member (car info) ob-async-no-async-languages-alist)
                  ;; ob-comint requires a :session, ob-async does not, so fall
                  ;; back to ob-async if no :session is provided.
                  (unless (member (alist-get :session params) '("none" nil))
                    (unless (memq (let* ((lang (nth 0 info))
                                         (lang (cond ((symbolp lang) lang)
                                                     ((stringp lang) (intern lang)))))
                                    (or (alist-get lang +org-babel-mode-alist)
                                        lang))
                                  +org-babel-native-async-langs)
                      (message "Org babel: %s :session is incompatible with :async. Executing synchronously!"
                               (car info))
                      (sleep-for 0.2))
                    t))
              (funcall fn arg info params)
            (funcall orig-fn fn arg info params)))))

    (defadvice! +org--advice-fix-newline-and-indent-in-src-blocks (&optional indent _arg _interactive)
      "Mimic `newline-and-indent' in src blocks w/
lang-appropriate indentation."
      :after #'org-return
      (when (and indent
                 org-src-tab-acts-natively
                 (org-in-src-block-p t))
        (org-babel-do-in-edit-buffer
         (call-interactively #'indent-for-tab-command))))

    ;; Refresh inline images after executing src blocks (useful for plantuml or
    ;; ipython, where the result could be an image)
    (add-hook 'org-babel-after-execute-hook #'org-redisplay-inline-images)

    ;; Fix 'require(...).print is not a function' error from `ob-js' when
    ;; executing JS src blocks
    (setq org-babel-js-function-wrapper "console.log(require('util').inspect(function(){\n%s\n}()));")

    (after! python
      (setq org-babel-python-command
            (concat python-shell-interpreter " " python-shell-interpreter-args)))

    ;; Fix #2010: ob-async needs to initialize Emacs at least minimally for its
    ;; async babel sessions to run correctly. This cannot be a named function
    ;; because it is interpolated directly into a closure to be evaluated on the
    ;; async session.
    (defadvice! +org--advice-init-emacs-during-async-executation (orig-fn &rest args)
      "Init Emacs during async."
      :around #'ob-async-org-babel-execute-src-block
      (let ((ob-async-pre-execute-src-block-hook
             ;; Ensure our hook is always first
             (cons `(lambda () (load ,(concat +emacs-dir "init.el")))
                   ob-async-pre-execute-src-block-hook)))
        (apply orig-fn args))))

  (defun +org--hook-init-babel-lazy-loader ()
    "Load babel libraries lazily when babel blocks are executed."
    (defun +org--babel-lazy-load (lang &optional async)
      (cl-check-type lang (or symbol null))
      (unless (cdr (assq lang org-babel-load-languages))
        (when async
          ;; ob-async has its own agenda for lazy loading packages (in the child
          ;; process), so we only need to make sure it's loaded.
          (require 'ob-async nil t))
        (prog1 (or (run-hook-with-args-until-success '+org-babel-load-functions lang)
                   (require (intern (format "ob-%s" lang)) nil t)
                   (require lang nil t))
          (add-to-list 'org-babel-load-languages (cons lang t)))))

    ;; Lazy load babel packages for exporting
    (defadvice! +org--advcie-export-lazy-load-library ()
      "Lazy load babel packages for exporting."
      :before #'org-babel-exp-src-block
      (+org--advice-babel-lazy-load-library (org-babel-get-src-block-info)))

    (defadvice! +org--advice-src-lazy-load-library (lang)
      "Lazy load a babel package to ensure syntax highlighting."
      :before #'org-src--get-lang-mode
      (or (cdr (assoc lang org-src-lang-modes))
          (+org--babel-lazy-load lang)))

    (defadvice! +org--advice-babel-lazy-load-library (info)
      "Load babel libraries lazily when babel blocks are executed."
      :after-while #'org-babel-confirm-evaluate
      (let* ((lang (nth 0 info))
             (lang (cond ((symbolp lang) lang)
                         ((stringp lang) (intern lang))))
             (lang (or (cdr (assq lang +org-babel-mode-alist))
                       lang)))
        (+org--babel-lazy-load
         lang (and (not (assq :sync (nth 2 info)))
                   (assq :async (nth 2 info))))
        t))

    (advice-add #'org-babel-do-load-languages :override #'ignore))

;;;;;; capture

  (defun +org--hook-init-capture-defaults ()
    "Sets up some reasonable defaults, as well as two
`org-capture' workflows that I like:

1. The traditional way: invoking `org-capture' directly, via SPC
   X, or through the :cap ex command.

2. Through a org-capture popup frame that is invoked from outside
   Emacs (the ~/.emacs.d/bin/org-capture script). This can be
   invoked from qutebrowser, vimperator, dmenu or a global
   keybinding."
    (setq org-default-notes-file
          (expand-file-name +org-capture-notes-file org-directory)
          +org-capture-journal-file
          (expand-file-name +org-capture-journal-file org-directory)
          org-capture-templates
          `(("t" "Personal todo" entry
             (file+headline +org-capture-todo-file "Inbox")
             "* TODO %?\n%i\n%a" :prepend t)
            ("n" "Personal notes" entry
             (file+headline +org-capture-notes-file "Inbox")
             "* %u %?\n%i\n%a" :prepend t)

            ;; Will use {project-root}/{todo,notes,changelog}.org, unless a
            ;; {todo,notes,changelog}.org file is found in a parent directory.
            ;; Uses the basename from `+org-capture-todo-file',
            ;; `+org-capture-changelog-file' and `+org-capture-notes-file'.
            ("p" "Templates for projects")
            ("pt" "Project-local todo" entry  ; {project-root}/todo.org
             (file+headline +org-capture-project-todo-file "Inbox")
             "* TODO %?\n%i\n%a" :prepend t)
            ("pn" "Project-local notes" entry  ; {project-root}/notes.org
             (file+headline +org-capture-project-notes-file "Inbox")
             "* %U %?\n%i\n%a" :prepend t)
            ("pc" "Project-local changelog" entry  ; {project-root}/changelog.org
             (file+headline +org-capture-project-changelog-file "Unreleased")
             "* %U %?\n%i\n%a" :prepend t)

            ;; Will use {org-directory}/{+org-capture-projects-file} and store
            ;; these under {ProjectName}/{Tasks,Notes,Changelog} headings. They
            ;; support `:parents' to specify what headings to put them under,
            ;; e.g. :parents ("Projects")
            ("o" "Centralized templates for projects")
            ("ot" "Project todo" entry
             (function +org-capture-central-project-todo-file)
             "* TODO %?\n %i\n %a"
             :heading "Tasks"
             :prepend nil)
            ("on" "Project notes" entry
             (function +org-capture-central-project-notes-file)
             "* %U %?\n %i\n %a"
             :heading "Notes"
             :prepend t)
            ("oc" "Project changelog" entry
             (function +org-capture-central-project-changelog-file)
             "* %U %?\n %i\n %a"
             :heading "Changelog"
             :prepend t)))

    ;; Kill capture buffers by default (unless they've been visited)
    (after! org-capture
      (org-capture-put :kill-buffer t))

    ;; Fix #462: when refiling from org-capture, Emacs prompts to kill the
    ;; underlying, modified buffer. This fixes that.
    (add-hook 'org-after-refile-insert-hook #'save-buffer)

    (defadvice! +org--advice-remove-customize-option (orig-fn table title &optional prompt specials)
      "HACK Don't advertize `customize' as an option in
`org-capture's menu."
      :around #'org-mks
      (funcall orig-fn table title prompt
               (remove '("C" "Customize org-capture-templates")
                       specials)))

    (defadvice! +org--advice-capture-expand-variable-file (file)
      "If a variable is used for a file path in
`org-capture-template', it is used as is, and expanded relative
to `default-directory'. This changes it to be relative to
`org-directory', unless it is an absolute path."
      :filter-args #'org-capture-expand-file
      (if (and (symbolp file) (boundp file))
          (expand-file-name (symbol-value file) org-directory)
        file))

    (defhook! +org--hook-show-target-in-capture-header ()
      "Show target in capture header."
      'org-capture-mode-hook
      (setq header-line-format
            (format "%s%s%s"
                    (propertize (abbreviate-file-name (buffer-file-name (buffer-base-buffer)))
                                'face 'font-lock-string-face)
                    org-eldoc-breadcrumb-separator
                    header-line-format)))

    (when (featurep! :editor evil)
      (add-hook 'org-capture-mode-hook #'evil-insert-state)))

  (defun +org--hook-init-capture-frame ()
    (add-hook 'org-capture-after-finalize-hook #'+org-capture--hook-cleanup-frame)

    (defadvice! +org-capture--advice-refile-cleanup-frame (&rest _)
      "Cleanup frame after `org-capture-refile'."
      :after #'org-capture-refile
      (+org-capture--hook-cleanup-frame))

    (when (featurep! :ui dashboard)
      (add-hook '+doom-dashboard-inhibit-functions #'+org-capture-frame-p)))

;;;;;; custom links

  (defun +org--hook-init-custom-links ()
    ;; Highlight broken file links
    (org-link-set-parameters
     "file"
     :face (lambda (path)
             (if (or (file-remote-p path)
                     (file-exists-p path))
                 'org-link
               '(warning org-link))))

    ;; Add custom link types
    (pushnew! org-link-abbrev-alist
              '("github"      . "https://github.com/%s")
              '("youtube"     . "https://youtube.com/watch?v=%s")
              '("google"      . "https://google.com/search?q=")
              '("gimages"     . "https://google.com/images?q=%s")
              '("gmap"        . "https://maps.google.com/maps?q=%s")
              '("duckduckgo"  . "https://duckduckgo.com/?q=%s")
              '("wolfram"     . "https://wolframalpha.com/input/?i=%s"))

    (+org-define-basic-link "org" org-directory)
    (+org-define-basic-link "emacs" +emacs-dir)
    (+org-define-basic-link "modules" +emacs-modules-dir)

    ;; Allow inline image previews of http(s)? urls or data uris.
    ;; `+org-http-image-data-fn' will respect
    ;; `org-display-remote-inline-images'.
    (setq org-display-remote-inline-images 'download) ; TRAMP urls
    (org-link-set-parameters "http"  :image-data-fun #'+org-http-image-data-fn)
    (org-link-set-parameters "https" :image-data-fun #'+org-http-image-data-fn)
    (org-link-set-parameters "img"   :image-data-fun #'+org-inline-image-data-fn))


;;;;;; export

  (defun +org--hook-init-export ()
    (setq org-export-with-smart-quotes t
          org-html-validation-link nil)

    (when (featurep! :lang markdown)
      (add-to-list 'org-export-backends 'md))

    (defadvice! +org--advice-dont-trigger-save-hooks-on-export (orig-fn &rest args)
      "`org-export-to-file' triggers save hooks, which may
inadvertantly change the exported output (i.e. formatters)."
      :around '(org-export-to-file org-babel-tangle)
      (let (before-save-hook after-save-hook)
        (apply orig-fn args)))

    (defadvice! +org--advice-fix-async-export (orig-fn &rest args)
      "TODO"
      :around '(org-export-to-file org-export-as)
      (let ((old-async-init-file org-export-async-init-file)
            (org-export-async-init-file (make-temp-file "emacs-org-async-export")))
        (with-temp-file org-export-async-init-file
          (prin1 `(progn (setq org-export-async-debug
                               ,(or org-export-async-debug
                                    debug-on-error)
                               load-path ',load-path)
                         (unwind-protect
                             (load ,(or old-async-init-file user-init-file)
                                   nil t)
                           (delete-file load-file-name)))
                 (current-buffer)))
        (apply orig-fn args))))


;;;;;; habit

  (defun +org--hook-init-habit ()
    "TODO"
    (defhook! +org-habit--hook-resize-graph ()
      "Right align and resize the consistency graphs based on
`+org-habit-graph-window-ratio'"
      'org-agenda-mode-hook
      (require 'org-habit)
      (let* ((total-days (float (+ org-habit-preceding-days org-habit-following-days)))
             (preceding-days-ratio (/ org-habit-preceding-days total-days))
             (graph-width (floor (* (window-width) +org-habit-graph-window-ratio)))
             (preceding-days (floor (* graph-width preceding-days-ratio)))
             (following-days (- graph-width preceding-days))
             (graph-column (- (window-width) (+ preceding-days following-days)))
             (graph-column-adjusted (if (> graph-column +org-habit-min-width)
                                        (- graph-column +org-habit-graph-padding)
                                      nil)))
        (setq-local org-habit-preceding-days preceding-days)
        (setq-local org-habit-following-days following-days)
        (setq-local org-habit-graph-column graph-column-adjusted))))

;;;;;; hacks

  (defun +org--hook-init-hacks ()
    "Getting org to behave."
    ;; Don't open separate windows
    (setf (alist-get 'file org-link-frame-setup) #'find-file)
    ;; Open directory links in Emacs
    (add-to-list 'org-file-apps '(directory . emacs))
    (add-to-list 'org-file-apps '(remote . emacs))

    (defadvice! +org--advice-show-parents (&optional arg)
      "Show all headlines in the buffer, like a table of contents.
With numerical argument N, show content up to level N."
      :override #'org-content
      (interactive "p")
      (org-show-all '(headings drawers))
      (save-excursion
        (goto-char (point-max))
        (let ((regexp (if (and (wholenump arg) (> arg 0))
                          (format "^\\*\\{%d,%d\\} " (1- arg) arg)
                        "^\\*+ "))
              (last (point)))
          (while (re-search-backward regexp nil t)
            (when (or (not (wholenump arg))
                      (= (org-current-level) arg))
              (org-flag-region (line-end-position) last t 'outline))
            (setq last (line-end-position 0))))))


    (defadvice! +org--advice-respect-org-auto-align-tags (&rest _)
      "Some uses of `org-fix-tags-on-the-fly' occur without a check
on `org-auto-align-tags', such as in `org-self-insert-command'
and `org-delete-backward-char'."
      :before-while #'org-fix-tags-on-the-fly
      org-auto-align-tags)

    (defadvice! +org--advice-recenter-after-follow-link (&rest _args)
      "Recenter after following a link, but only internal or file
links."
      :after '(org-footnote-action
               org-follow-timestamp-link
               org-link-open-as-file
               org-link-search)
      (when (get-buffer-window)
        (recenter)))

    (defadvice! +org--advice-strip-properties-from-outline (orig-fn &rest args)
      "Fix variable height faces in eldoc breadcrumbs."
      :around #'org-format-outline-path
      (let ((org-level-faces
             (cl-loop for face in org-level-faces
                      collect `(:foreground ,(face-foreground face nil t)
                                :weight bold))))
        (apply orig-fn args)))

    (defun +org--hook-restart-mode ()
      "Restart `org-mode', but only once."
      (remove-hook '+emacs-switch-buffer-hook #'+org--hook-restart-mode
                   'local)
      (delq! (current-buffer) org-agenda-new-buffers)
      (let ((file buffer-file-name)
            (inhibit-redisplay t))
        (kill-buffer)
        (find-file file)))

    (defhook! +org--hook-exclude-agenda-buffers-from-workspace ()
      "Prevent temporary agenda buffers being associated with
current workspace."
      'org-agenda-finalize-hook
      (when (and org-agenda-new-buffers
                 (bound-and-true-p persp-mode)
                 (not org-agenda-sticky))
        (let (persp-autokill-buffer-on-remove)
          (persp-remove-buffer org-agenda-new-buffers
                               (get-current-persp)
                               nil))))

    (defhook! +org--hook-defer-mode-in-agenda-buffers ()
      "Org agenda opens temporary agenda incomplete org-mode buffers.
These are great for extracting agenda information from, but what
if the user tries to visit one of these buffers? Then we remove
it from the to-be-cleaned queue and restart `org-mode' so they
can grow up to be full-fledged org-mode buffers."
      'org-agenda-finalize-hook
      (dolist (buffer org-agenda-new-buffers)
        (when (buffer-live-p buffer)
          (with-current-buffer buffer
            (add-hook '+emacs-switch-buffer-hook #'+org--hook-restart-mode
                      nil 'local)))))

    (defvar recentf-exclude)
    (defadvice! +org--advice-exclude-agenda-buffers-from-recentf (orig-fn file)
      "Prevent temporarily opened agenda buffers from polluting
recentf."
      :around #'org-get-agenda-file-buffer
      (let ((recentf-exclude (list (lambda (_file) t)))
            (+emacs-inhibit-large-file-detection t)
            org-startup-indented
            org-startup-folded
            vc-handled-backends
            org-mode-hook
            find-file-hook)
        (funcall orig-fn file))))

;;;;;; keybinds

  (defun +org--hook-init-keybinds ()
    "Configure keybinds in `org-mode'."
    (add-hook '+emacs-escape-hook #'+org--hook-remove-occur-highlights)

    ;; C-a & C-e act like `+emacs/backward-to-bol-or-indent' and
    ;; `+emacs/forward-to-last-non-comment-or-eol', but with more org awareness.
    (setq org-special-ctrl-a/e t)

    (setq org-M-RET-may-split-line nil
          ;; insert new headings after current subtree rather than inside it
          org-insert-heading-respect-content t)

    (add-hook '+emacs-delete-backward-functions
              #'+org--hook-delete-backward-char-and-realign-table-maybe)

    (map!
     :map org-mode-map
     "C-c C-S-l"  #'+org/remove-link
     "C-c C-i"    #'org-toggle-inline-images
     ;; textmate-esque newline insertion
     "S-RET"      #'+org/shift-return
     "C-RET"      #'+org/insert-item-below
     "C-S-RET"    #'+org/insert-item-above
     "C-M-RET"    #'org-insert-subheading
     [C-return]   #'+org/insert-item-below
     [C-S-return] #'+org/insert-item-above
     [C-M-return] #'org-insert-subheading
     ;; Org-aware C-a/C-e
     [remap +emacs/backward-to-bol-or-indent]          #'org-beginning-of-line
     [remap +emacs/forward-to-last-non-comment-or-eol] #'org-end-of-line
     :localleader
     "'" #'org-edit-special
     "," #'org-switchb
     "*" #'org-ctrl-c-star
     "-" #'org-ctrl-c-minus
     "." #'org-goto
     (:when (featurep! :completion ivy)
      "." #'counsel-org-goto
      "/" #'counsel-org-goto-all)
     (:when (featurep! :completion helm)
      "." #'helm-org-in-buffer-headings
      "/" #'helm-org-agenda-files-headings)
     "A" #'org-archive-subtree
     "e" #'org-export-dispatch
     "f" #'org-footnote-action
     "h" #'org-toggle-heading
     "I" #'org-toggle-item
     "n" #'org-store-link
     "o" #'org-set-property
     "q" #'org-set-tags-command
     "t" #'org-todo
     "T" #'org-todo-list
     "x" #'org-toggle-checkbox
     (:prefix ("a" . "attachments")
      "a" #'+org/attach-file
      "f" #'+org/find-file-in-attachments
      "g" #'+org/open-gallery-from-attachments
      "l" #'+org/attach-file-and-insert-link
      "s" #'+org/attach-sync
      (:when (featurep! :org org +dragndrop)
       "c" #'org-download-screenshot
       "p" #'org-download-clipboard
       "P" #'org-download-yank))
     (:prefix ("b" . "babel")
      (:when (featurep! :ui transient-state)
       "." #'+emacs-org-babel-transient-state/body)
      "b" #'org-babel-execute-buffer
      "s" #'org-babel-execute-subtree
      "t" #'org-babel-tangle
      "f" #'org-babel-tangle-file)
     (:prefix ("B" . "tables")
      "-" #'org-table-insert-hline
      "a" #'org-table-align
      "b" #'org-table-blank-field
      "c" #'org-table-create-or-convert-from-region
      "e" #'org-table-edit-field
      "f" #'org-table-edit-formulas
      "h" #'org-table-field-info
      "s" #'org-table-sort-lines
      "r" #'org-table-recalculate
      "R" #'org-table-recalculate-buffer-tables
      (:prefix ("d" . "delete")
       "c" #'org-table-delete-column
       "r" #'org-table-kill-row)
      (:prefix ("i" . "insert")
       "c" #'org-table-insert-column
       "h" #'org-table-insert-hline
       "r" #'org-table-insert-row
       "H" #'org-table-hline-and-move)
      (:prefix ("t" . "toggle")
       "f" #'org-table-toggle-formula-debugger
       "o" #'org-table-toggle-coordinate-overlays))
     (:prefix ("c" . "clock")
      "c" #'org-clock-in
      "C" #'org-clock-out
      "d" #'org-clock-mark-default-task
      "e" #'org-clock-modify-effort-estimate
      "E" #'org-set-effort
      "l" #'org-clock-in-last
      "g" #'org-clock-goto
      :desc "org-clock-goto 'select" "G" (cmd! (org-clock-goto 'select))
      "r" #'org-clock-report
      "x" #'org-clock-cancel
      "=" #'org-clock-timestamps-up
      "-" #'org-clock-timestamps-down)
     (:prefix ("d" . "date/deadline")
      "d" #'org-deadline
      "s" #'org-schedule
      "t" #'org-time-stamp
      "T" #'org-time-stamp-inactive)
     (:prefix ("g" . "goto")
      "g" #'org-goto
      (:when (featurep! :completion ivy)
       "g" #'counsel-org-goto
       "G" #'counsel-org-goto-all)
      (:when (featurep! :completion helm)
       "g" #'helm-org-in-buffer-headings
       "G" #'helm-org-agenda-files-headings)
      "c" #'org-clock-goto
      :desc "org-clock-goto 'select" "C" (cmd! (org-clock-goto 'select))
      "i" #'org-id-goto
      "r" #'org-refile-goto-last-stored
      "x" #'org-capture-goto-last-stored)
     (:prefix ("i" . "insert")
      "b" #'org-insert-structure-template
      "d" #'org-insert-drawer
      "f" #'org-footnote-action
      "h" #'org-insert-heading
      "H" #'org-insert-heading-after-current
      "i" #'org-insert-item
      "l" #'org-insert-link
      "n" #'org-add-note
      "s" #'org-insert-subheading)
     (:prefix ("l" . "links")
      "c" #'org-cliplink
      "d" #'+org/remove-link
      "i" #'org-id-store-link
      "l" #'org-insert-link
      "L" #'org-insert-all-links
      "s" #'org-store-link
      "S" #'org-insert-last-stored-link
      "t" #'org-toggle-link-display)
     (:prefix ("P" . "publish")
      "a" #'org-publish-all
      "f" #'org-publish-current-file
      "p" #'org-publish
      "P" #'org-publish-current-project
      "s" #'org-publish-sitemap)
     (:prefix ("r" . "refile")
      "." #'+org/refile-to-current-file
      "c" #'+org/refile-to-running-clock
      "l" #'+org/refile-to-last-location
      "f" #'+org/refile-to-file
      "o" #'+org/refile-to-other-window
      "O" #'+org/refile-to-other-buffer
      "v" #'+org/refile-to-visible
      "r" #'org-refile) ; to all `org-refile-targets'
     (:prefix ("s" . "tree/subtree")
      "a" #'org-toggle-archive-tag
      "b" #'org-tree-to-indirect-buffer
      "d" #'org-cut-subtree
      "h" #'org-promote-subtree
      "j" #'org-move-subtree-down
      "k" #'org-move-subtree-up
      "l" #'org-demote-subtree
      "n" #'org-narrow-to-subtree
      "r" #'org-refile
      "s" #'org-sparse-tree
      "A" #'org-archive-subtree
      "N" #'widen
      "S" #'org-sort)
     (:prefix ("p" . "priority")
      "d" #'org-priority-down
      "p" #'org-priority
      "u" #'org-priority-up)
     (:prefix ("x" . "text")
      :desc "bold" "b" (cmd! (org-emphasize ?\*))
      :desc "code"  "c" (cmd! (org-emphasize ?\~))
      :desc "italic" "i" (cmd! (org-emphasize ?\/))
      "o" #'org-open-at-point
      :desc "clear" "r" (cmd! (org-emphasize ?\ ))
      :desc "strike through" "s" (cmd! (org-emphasize ?\+))
      :desc "underline" "u" (cmd! (org-emphasize ?\_))
      :desc "verbatim" "v" (cmd! (org-emphasize ?\=))))

    (map! :after org-agenda
          :map org-agenda-mode-map
          :m "C-SPC" #'org-agenda-show-and-scroll-up
          :localleader
          "d" #'org-agenda-deadline
          (:prefix ("c" . "clock")
           "c" #'org-agenda-clock-in
           "C" #'org-agenda-clock-out
           "g" #'org-agenda-clock-goto
           "r" #'org-agenda-clockreport-mode
           "s" #'org-agenda-show-clocking-issues
           "x" #'org-agenda-clock-cancel)
          "q" #'org-agenda-set-tags
          "r" #'org-agenda-refile
          "s" #'org-agenda-schedule
          "t" #'org-agenda-todo)

    (map! :after org-agenda
          (:map org-agenda-mode-map
           (:when (featurep! :ui transient-state)
            :nm "." nil
            :nm "." #'+emacs-org-agenda-transient-state/body))
          :after evil-org
          (:map evil-org-agenda-mode-map
           (:when (featurep! :ui transient-state)
            :nm "." nil
            :nm "." #'+emacs-org-agenda-transient-state/body))))

;;;;;; popup rules

  (defun +org--hook-init-popup-rules ()
    (set-popup-rules!
      '(("^\\*Org Links" :slot -1 :vslot -1 :size 2 :ttl 0)
        ("^ ?\\*\\(?:Agenda Com\\|Calendar\\|Org Export Dispatcher\\)"
         :slot -1 :vslot -1 :size #'+popup-shrink-to-fit :ttl 0 :side right)
        ("^\\*Org \\(?:Select\\|Attach\\)" :slot -1 :vslot -2 :ttl 0 :size 0.25)
        ("^\\*Org Agenda" :ignore t)
        ;; ("^\\*Org Src"        :size 0.42  :quit nil :select t :autosave t :modeline t :ttl nil)
        ("^\\*Org Src" :ignore t)
        ("^\\*Org-Babel")
        ("^\\*Capture\\*$\\|CAPTURE-.*$" :size 0.42 :quit nil :select t :autosave ignore))))

;;;;; Extra packages

;;;;;; toc-org

  (use-package toc-org
    :hook (org-mode . toc-org-enable)
    :config
    (setq toc-org-hrefify-default "gh")

    (defadvice! +org--advice-inhibit-scrolling (orig-fn &rest args)
      "Prevent the jarring scrolling that occurs when the-ToC is
regenerated."
      :around #'toc-org-insert-toc
      (let ((p (set-marker (make-marker) (point)))
            (s (window-start)))
        (prog1 (apply orig-fn args)
          (goto-char p)
          (set-window-start nil s t)
          (set-marker p nil))))

    (when (featurep! :ui popup)
      (set-popup-rule! "\\*org-toc" :side 'right :width 0.3))
    (map!
     :localleader
     :map org-mode-map
     "TAB" #'org-toc-show))

;;;;;; org-superstar

  (use-package org-superstar
    :hook (org-mode . org-superstar-mode)
    :config
    ;; Make leading stars truly invisible, by rendering them as spaces!
    (setq org-superstar-leading-bullet ?\s
          org-superstar-leading-fallback ?\s
          org-hide-leading-stars nil)
    ;; Don't do anything special for item bullets or TODOs by default; these
    ;; slow down larger org buffers.
    (setq org-superstar-prettify-item-bullets nil
          org-superstar-special-todo-items nil
          ;; ...but configure it in case the user wants it later
          org-superstar-todo-bullet-alist
          '(("TODO" . 9744)
            ("[ ]"  . 9744)
            ("DONE" . 9745)
            ("[X]"  . 9745))))

;;;;;; org-super-agenda

  (use-package org-super-agenda
    :defer t
    :init
    (add-hook! 'org-agenda-mode-hook
               #'org-super-agenda-mode)
    (setq org-super-agenda-groups
          '((:name "Habits"
             :habit t
             :order 2)
            (:name "Today"
             :date today)
            (:name "Important"
             :priority "A")
            (:name "Work"
             :tag "work"))))

;;;;;; evil-org

  (use-package evil-org
    :hook (org-mode . evil-org-mode)
    :init
    (defvar evil-org-retain-visual-state-on-shift t)
    (defvar evil-org-special-o/O '(table-row))
    (defvar evil-org-use-additional-insert t)
    :config
    (add-hook 'evil-org-mode-hook #'evil-normalize-keymaps)
    (evil-org-set-key-theme)
    ;; Only fold the current tree, rather than recursively
    (add-hook 'org-tab-first-hook #'+org--hook-cycle-only-current-subtree 'append)
    (map! :map evil-org-mode-map
          :ni [C-return]   #'+org/insert-item-below
          :ni [C-S-return] #'+org/insert-item-above
          ;; navigate table cells (from insert-mode)
          :i "C-l" (general-predicate-dispatch nil
                     (org-at-table-p) 'org-table-next-field)
          :i "C-h" (general-predicate-dispatch nil
                     (org-at-table-p) 'org-table-previous-field)
          :i "C-k" (general-predicate-dispatch nil
                     (org-at-table-p) '+org/table-previous-row)
          :i "C-j" (general-predicate-dispatch nil
                     (org-at-table-p) 'org-table-next-row)
          ;; moving/(de|pro)moting subtress & expanding tables (prepend/append columns/rows)
          :ni "C-S-l" #'org-shiftright
          :ni "C-S-h" #'org-shiftleft
          :ni "C-S-k" #'org-shiftup
          :ni "C-S-j" #'org-shiftdown
          ;; more intuitive RET keybinds
          :i [return] (cmd! (org-return electric-indent-mode))
          :i "RET"    (cmd! (org-return electric-indent-mode))
          :i [S-return] #'+org/shift-return
          :i "S-RET"    #'+org/shift-return
          :n [return] #'+org/dwim-at-point
          :n "RET"    #'+org/dwim-at-point
          ;; more vim-esque org motion keys (not covered by evil-org-mode)
          :m "]h"  #'org-forward-heading-same-level
          :m "[h"  #'org-backward-heading-same-level
          :m "]l"  #'org-next-link
          :m "[l"  #'org-previous-link
          :m "]c"  #'org-babel-next-src-block
          :m "[c"  #'org-babel-previous-src-block
          :n "gQ"  #'org-fill-paragraph
          :n "gr"  #'org-ctrl-c-ctrl-c
          :n "gR"  #'org-babel-execute-buffer
          ;; sensible vim-esque folding keybinds
          :n "za"  #'+org/toggle-fold
          :n "zA"  #'org-shifttab
          :n "zc"  #'+org/close-fold
          :n "zC"  #'outline-hide-subtree
          :n "zm"  #'+org/hide-next-fold-level
          :n "zn"  #'org-tree-to-indirect-buffer
          :n "zo"  #'+org/open-fold
          :n "zO"  #'outline-show-subtree
          :n "zr"  #'+org/show-next-fold-level
          :n "zR"  #'outline-show-all
          :n "zi"  #'org-toggle-inline-images

          :map org-read-date-minibuffer-local-map
          "C-h"   (cmd! (org-eval-in-calendar '(calendar-backward-day 1)))
          "C-l"   (cmd! (org-eval-in-calendar '(calendar-forward-day 1)))
          "C-k"   (cmd! (org-eval-in-calendar '(calendar-backward-week 1)))
          "C-j"   (cmd! (org-eval-in-calendar '(calendar-forward-week 1)))
          "C-S-h" (cmd! (org-eval-in-calendar '(calendar-backward-month 1)))
          "C-S-l" (cmd! (org-eval-in-calendar '(calendar-forward-month 1)))
          "C-S-k" (cmd! (org-eval-in-calendar '(calendar-backward-year 1)))
          "C-S-j" (cmd! (org-eval-in-calendar '(calendar-forward-year 1)))))

  (use-package evil-org-agenda
    :hook (org-agenda-mode . evil-org-agenda-mode)
    :config
    (evil-org-agenda-set-keys)
    (evil-define-key* 'motion evil-org-agenda-mode-map
      (kbd +emacs-leader-key) nil
      "." nil))

;;;;; Bootstrap

  (use-package org
    :defer-incrementally
    calendar find-func format-spec org-macs org-compat org-faces org-entities
    org-list org-pcomplete org-src org-footnote org-macro ob org org-agenda
    org-capture
    :preface
    ;; Set these to nil now so we can detect user changes to them later (and
    ;; fall back on defaults otherwise)
    (defvar org-directory nil)
    (defvar org-id-locations-file nil)
    (defvar org-attach-id-dir nil)

    (setq org-publish-timestamp-directory (concat +emacs-cache-dir "org-timestamps/")
          org-preview-latex-image-directory (concat +emacs-cache-dir "org-latex/")
          org-persist-directory (concat +emacs-cache-dir "org-persist/")
          ;; Recognize a), A), a., A., etc -- must be set before org is loaded.
          org-list-allow-alphabetical t)

    ;; Make most of the default modules opt-in to lighten its first-time load
    ;; delay.
    (defvar org-modules
      '(;; ol-w3m
        ;; ol-bbdb
        ol-bibtex
        ;; ol-docview
        ;; ol-gnus
        ;; ol-info
        ;; ol-irc
        ;; ol-mhe
        ;; ol-rmail
        ;; ol-eww
        ))

    (add-hook! 'org-mode-hook
               #'+emacs--hook-disable-show-paren-mode
               #'+org--hook-enable-auto-reformat-tables
               #'+org--hook-enable-auto-update-cookies
               #'+org--hook-make-last-point-visible)

    (add-hook! 'org-load-hook
               #'+org--hook-init-org-directory
               #'+org--hook-init-appearance
               #'+org--hook-init-agenda
               #'+org--hook-init-attachments
               #'+org--hook-init-babel
               #'+org--hook-init-babel-lazy-loader
               #'+org--hook-init-capture-defaults
               #'+org--hook-init-capture-frame
               #'+org--hook-init-custom-links
               #'+org--hook-init-export
               #'+org--hook-init-habit
               #'+org--hook-init-hacks
               #'+org--hook-init-keybinds)

    (when (featurep! :ui popup)
      (add-hook! 'org-load-hook
                 #'+org--hook-init-popup-rules))

    (add-hook 'org-mode-local-vars-hook #'eldoc-mode)

    ;; In case the user has eagerly loaded org from their configs
    (when (and (featurep 'org)
               (not +emacs-reloading-p)
               (not byte-compile-current-file))
      (message "`org' was already loaded by the time lang/org loaded, this may cause issues")
      (run-hooks 'org-load-hook))

    :config
    ;; Save target buffer after archiving
    (setq org-archive-subtree-save-file-p t)
    ;; Don't number headings with these tags
    (setq org-num-face '(:inherit org-special-keyword :underline nil :weight bold)
          org-num-skip-tags '("noexport" "nonum"))
    ;; Prevent modifications made in invisible sections of an org document, as
    ;; unintended changes can easily go unseen otherwise.
    (setq org-catch-invisible-edits 'smart)
    ;; Global ID state means we can have ID links anywhere. This is required for
    ;; `org-brain', however.
    (setq org-id-locations-file-relative t)

    ;; HACK `org-id' doesn't check if `org-id-locations-file' exists or is
    ;;      writeable before trying to read/write to it.
    (defadvice! +org--advice-fail-gracefully (&rest _)
      "Check if `org-id-locations-file' exists or is writeable
before trying to read/write to it."
      :before-while '(org-id-locations-save org-id-locations-load)
      (file-writable-p org-id-locations-file))

    ;; company backends
    (when (featurep! :completion company)
      (set-company-backend! 'org-mode
                            :company '(company-capf)
                            :capf #'pcomplete-completions-at-point))

    (add-hook 'org-open-at-point-functions #'+emacs--hook-set-jump)
    ;; allow shift selection
    (setq org-support-shift-select t)
    ;; use latexmk for pdf export
    (setq org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))
    ;; Inline images settings
    (setq org-image-actual-width 300)
    (add-hook! 'org-babel-after-execute-hook #'org-display-inline-images)
    (add-hook! 'org-mode-hook #'org-display-inline-images)))

;;; lang

;;;; emacs-lisp

(module! (:lang emacs-lisp)

  (defvar +emacs-lisp-enable-extra-fontification t
    "If non-nil, highlight special forms, and defined functions
and variables.")

  (defvar +emacs-lisp-disable-flycheck-in-dirs
    (list +emacs-dir +emacs-private-dir)
    "List of directories to disable `emacs-lisp-checkdoc' in.
This checker tends to produce a lot of false positives in your
.emacs.d and private config, so it is mostly useless there.
However, special hacks are employed so that flycheck still does
*some* helpful linting.")

  ;; `elisp-mode' is loaded at startup. In order to lazy load its config we need
  ;; to pretend it isn't loaded
  (defer-feature! elisp-mode emacs-lisp-mode)

  (use-package elisp-mode
    :mode ("\\.Cask\\'" . emacs-lisp-mode)
    :config
    (when (featurep! :tools eval)
      (set-repl-handler! '(emacs-lisp-mode lisp-interaction-mode)
        #'+emacs-lisp/open-repl)
      (set-eval-handler! '(emacs-lisp-mode lisp-interaction-mode)
        #'+emacs-lisp-eval))
    (when (featurep! :tools lookup)
      (set-lookup-handlers! '(emacs-lisp-mode lisp-interaction-mode helpful-mode)
        :definition    #'+emacs-lisp-lookup-definition
        :documentation #'+emacs-lisp-lookup-documentation)
      (set-docsets! '(emacs-lisp-mode lisp-interaction-mode) "Emacs Lisp"))

    (when (featurep! :ui pretty-code)
      (set-pretty-symbols! 'emacs-lisp-mode :lambda "lambda"))
    (when (featurep! :editor rotate-text)
      (set-rotate-patterns! 'emacs-lisp-mode
        :symbols '(("t" "nil")
                   ("let" "let*")
                   ("when" "unless")
                   ("append" "prepend")
                   ("advice-add" "advice-remove")
                   ("defadvice!" "undefadvice!")
                   ("add-hook" "remove-hook")
                   ("add-hook!" "remove-hook!"))))

    (when (featurep! :editor cleverlispy)
      (pushnew! +cleverlispy-lispyville-modes 'emacs-lisp-mode)
      (add-hook 'emacs-lisp-mode-hook #'+cleverlispy--hook-enable))

    (setq-hook! 'emacs-lisp-mode-hook
      tab-width (or lisp-indent-offset 2)
      lisp-indent-function #'+emacs-lisp-indent-function)

    ;; variable-width indentation is superior in elisp
    (add-to-list '+emacs-detect-indentation-excluded-modes 'emacs-lisp-mode nil #'eq)

    (add-hook! 'emacs-lisp-mode-hook
               ;; Fontificiation
               #'rainbow-delimiters-mode
               #'highlight-quoted-mode
               ;; initialization
               #'+emacs-lisp--hook-extend-imenu)

    ;; Flycheck produces a *lot* of false positives in emacs configs, so disable
    ;; it when you're editing them
    (add-hook 'flycheck-mode-hook #'+emacs-lisp--hook-disable-flycheck-maybe)

    ;; Special syntax highlighting for elisp...
    (font-lock-add-keywords
     'emacs-lisp-mode
     (append `(;; custom cookies
               ("^;;;###\\(autodef\\|if\\|package\\|module\\|deactivatemodule\\)[ \n]" (1 font-lock-warning-face t)))
             ;; highlight defined, special variables & functions
             (when +emacs-lisp-enable-extra-fontification
               `((+emacs-lisp-highlight-vars-and-faces . +emacs-lisp--face)))))

    ;; Recenter window after following definition
    (advice-add #'elisp-def :after #'+emacs--advice-recenter)

    (defadvice! +emacs-lisp--advice-append-value-to-eldoc (orig-fn sym)
      "Display variable value next to documentation in eldoc."
      :around #'elisp-get-var-docstring
      (when-let (ret (funcall orig-fn sym))
        (if (boundp sym)
            (concat ret " "
                    (let* ((truncated " [...]")
                           (print-escape-newlines t)
                           (str (symbol-value sym))
                           (str (prin1-to-string str))
                           (limit (- (frame-width) (length ret) (length truncated) 1)))
                      (format (format "%%0.%ds%%s" (max limit 0))
                              (propertize str 'face 'warning)
                              (if (< (length str) limit) "" truncated))))
          ret)))

    (map!
     :localleader
     :map (emacs-lisp-mode-map lisp-interaction-mode-map)
     (:prefix ("c" . "compile")
      "c" #'emacs-lisp-byte-compile)
     (:prefix ("d" . "debug")
      "f" #'+emacs-lisp/edebug-instrument-defun-on
      "F" #'+emacs-lisp/edebug-instrument-defun-off)
     (:prefix ("e" . "eval")
      "b" #'eval-buffer
      "d" #'eval-defun
      "e" #'eval-last-sexp
      "r" #'eval-region
      "l" #'load-library)
     (:prefix ("m" . "macros")
      "e" #'macrostep-expand
      "c" #'macrostep-collapse
      "n" #'macrostep-next-macro
      "N" #'macrostep-prev-macro
      "q" #'macrostep-collapse-all)
     (:prefix ("t" . "tests")
      "q" #'ert)))

;;;;; ielm

  (use-package ielm
    :defer t
    :config
    (when (featurep! :tools lookup)
      (set-lookup-handlers! 'inferior-emacs-lisp-mode
        :definition    #'+emacs-lisp-lookup-definition
        :documentation #'+emacs-lisp-lookup-documentation))

    ;; Adapted from http://www.modernemacs.com/post/comint-highlighting/ to add
    ;; syntax highlighting to ielm REPLs.
    (setq ielm-font-lock-keywords
          (append '(("\\(^\\*\\*\\*[^*]+\\*\\*\\*\\)\\(.*$\\)"
                     (1 font-lock-comment-face)
                     (2 font-lock-constant-face)))
                  (when (require 'highlight-numbers nil t)
                    (highlight-numbers--get-regexp-for-mode 'emacs-lisp-mode))
                  (cl-loop for (matcher . match-highlights)
                           in (append lisp-el-font-lock-keywords-2
                                      lisp-cl-font-lock-keywords-2)
                           collect
                           `((lambda (limit)
                               (when ,(if (symbolp matcher)
                                          `(,matcher limit)
                                        `(re-search-forward ,matcher limit t))
                                 ;; Only highlight matches after the prompt
                                 (> (match-beginning 0) (car comint-last-prompt))
                                 ;; Make sure we're not in a comment or string
                                 (let ((state (syntax-ppss)))
                                   (not (or (nth 3 state)
                                            (nth 4 state))))))
                             ,@match-highlights)))))


;;;;; elisp-demos

  (use-package elisp-demos
    :defer t
    :init
    (advice-add 'describe-function-1 :after #'elisp-demos-advice-describe-function-1)
    (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update))

;;;;; macrostep

  (use-package macrostep
    :commands macrostep-expand
    :config
    (when (featurep! :editor evil)
      (after! macrostep
        (evil-define-key* 'normal macrostep-keymap
          [return]  #'macrostep-expand
          "e"       #'macrostep-expand
          "u"       #'macrostep-collapse
          "c"       #'macrostep-collapse

          [tab]     #'macrostep-next-macro
          "\C-n"    #'macrostep-next-macro
          "J"       #'macrostep-next-macro

          [backtab] #'macrostep-prev-macro
          "K"       #'macrostep-prev-macro
          "\C-p"    #'macrostep-prev-macro

          "q"       #'macrostep-collapse-all
          "C"       #'macrostep-collapse-all)

        ;; `evil-normalize-keymaps' seems to be required for macrostep or it
        ;; won't apply for the very first invocation
        (add-hook 'macrostep-mode-hook #'evil-normalize-keymaps)))))


;;;; sh

(module! (:lang sh)

  (defvar +sh-builtin-keywords
    '("cat" "cd" "chmod" "chown" "cp" "curl" "date" "echo" "find" "git" "grep"
      "kill" "less" "ln" "ls" "make" "mkdir" "mv" "pgrep" "pkill" "pwd" "rm"
      "sleep" "sudo" "touch")
    "A list of common shell commands to be fontified especially in `sh-mode'.")

  (use-package sh-script
    :mode ("\\.bats\\'" . sh-mode)
    :mode ("\\.zunit\\'" . sh-mode)
    :mode ("/bspwmrc\\'" . sh-mode)
    :config
    (when (featurep! :emacs electric)
      (set-electric! 'sh-mode :words '("else" "elif" "fi" "done" "then" "do" "esac" ";;")))
    (when (featurep! :editor format)
      (set-formatter! 'shfmt
        '("shfmt" "-ci"
          ("-i" "%d" (unless indent-tabs-mode tab-width))
          ("-ln" "%s" (pcase sh-shell (`bash "bash") (`mksh "mksh") (_ "posix"))))))
    (when (featurep! :tools lookup)
      (set-lookup-handlers! 'sh-mode
        :documentation #'+sh-lookup-documentation-handler)
      (set-docsets! 'sh-mode "Bash"))
    (when (featurep! :tools eval)
      (set-repl-handler! 'sh-mode #'+sh/open-repl))
    (setq sh-indent-after-continuation 'always)
    ;; 80 char fill column
    (setq-hook! 'sh-mode-hook fill-column 80)
    ;; recognize function names with dashes in them
    (add-to-list 'sh-imenu-generic-expression
                 '(sh (nil "^\\s-*function\\s-+\\([[:alpha:]_-][[:alnum:]_-]*\\)\\s-*\\(?:()\\)?" 1)
                      (nil "^\\s-*\\([[:alpha:]_-][[:alnum:]_-]*\\)\\s-*()" 1)))

    ;; `sh-set-shell' is chatty about setting up indentation rules
    (advice-add #'sh-set-shell :around #'+emacs--advice-shut-up)

    ;; 1. Fontifies variables in double quotes
    ;; 2. Fontify command substitution in double quotes
    ;; 3. Fontify built-in/common commands (see `+sh-builtin-keywords')
    (add-hook! 'sh-mode-hook
      (defun +sh-init-extra-fontification-h ()
        (font-lock-add-keywords
         nil `((+sh--match-variables-in-quotes
                (1 'font-lock-constant-face prepend)
                (2 'font-lock-variable-name-face prepend))
               (+sh--match-command-subst-in-quotes
                (1 'sh-quoted-exec prepend))
               (,(regexp-opt +sh-builtin-keywords 'symbols)
                (0 'font-lock-type-face append))))))
    ;; 4. Fontify delimiters by depth
    (add-hook 'sh-mode-hook #'rainbow-delimiters-mode)

    ;; autoclose backticks
    (sp-local-pair 'sh-mode "`" "`" :unless '(sp-point-before-word-p sp-point-before-same-p))

    (map! :localleader
          :map sh-mode-map
          "\\" 'sh-backslash-region
          (:prefix ("i" . "insert")
           "c" 'sh-case
           "i" 'sh-if
           "f" 'sh-function
           "o" 'sh-for
           "e" 'sh-indexed-loop
           "w" 'sh-while
           "r" 'sh-repeat
           "s" 'sh-select
           "u" 'sh-until
           "g" 'sh-while-getopts)))


;;;;; company-shell

  (use-package company-shell
    :when (featurep! :completion company)
    :after sh-script
    :init
    (when (featurep! :completion company)
      (add-hook! sh-mode
        (set-company-backend!
         'sh-mode
         :company '((company-shell company-shell-env company-dabbrev-code)))))
    :config
    (setq company-shell-delete-duplicates t))

  ;; PATCH `company-shell--build-fish-cache'
  ;;       Fix backend, does not find completions otherwise
  (el-patch-feature company-shell)
  (after! company-shell
    (el-patch-defun company-shell--build-fish-cache ()
      "Build the list of all fish shell completions."
      (when (executable-find "fish")
        (el-patch-remove
          (setq company-shell--fish-cache
                (-flatten (--map
                           (-> it
                               (format "fish -c \"%s\"")
                               (shell-command-to-string)
                               (split-string "\n")
                               (sort #'string-lessp))
                           '("functions -a" "builtin -n")))))
        (el-patch-add
          (setq company-shell--fish-cache
                (append
                 (-> (shell-command-to-string "fish -c \"functions -a\"")
                     (split-string "\n")
                     (sort 'string-lessp))
                 (-> "fish -c \"builtin -n\""
                     (shell-command-to-string)
                     (split-string "\n")
                     (sort #'string-lessp)))))))))


;;; config

;;;; default

(module! (:config default)

  (defvar +default-want-RET-continue-comments t
    "If non-nil, RET will continue commented lines.")

  (defvar +default-minibuffer-maps
    (append '(minibuffer-local-map
              minibuffer-local-ns-map
              minibuffer-local-completion-map
              minibuffer-local-must-match-map
              minibuffer-local-isearch-map
              read-expression-map)
            (cond ((featurep! :completion ivy)
                   '(ivy-minibuffer-map
                     ivy-switch-buffer-map))
                  ((or (featurep! :completion helm)
                       (featurep! :completion helm-base))
                   '(helm-map
                     helm-ag-map
                     helm-read-file-map))))
    "A list of all the keymaps used for the minibuffer.")

;;;;; Reasonable defaults

  (after! avy
    (setq avy-all-windows nil
          avy-all-windows-alt t
          avy-background t))

  (after! epa
    ;; With GPG 2.1+, this forces gpg-agent to use the Emacs minibuffer to
    ;; prompt for the key passphrase.
    (set 'epg-pinentry-mode 'loopback)
    ;; Default to the first secret key available in your keyring.
    (setq-default
     epa-file-encrypt-to
     (or (default-value 'epa-file-encrypt-to)
         (unless (string-empty-p user-full-name)
           (cl-loop for key in (ignore-errors (epg-list-keys (epg-make-context) user-full-name))
                    collect (epg-sub-key-id (car (epg-key-sub-key-list key)))))
         user-mail-address))
    ;; And suppress prompts if epa-file-encrypt-to has a default value (without
    ;; overwriting file-local values).
    (defadvice! +default--advice-dont-prompt-for-keys (&rest _)
      "Suppress prompts if epa-file-encrypt-to has a default
value."
      :before #'epa-file-write-region
      (unless (local-variable-p 'epa-file-encrypt-to)
        (setq-local epa-file-encrypt-to (default-value 'epa-file-encrypt-to)))))

  (after! tramp
    (setq tramp-default-method "ssh"))

;;;;; Smartparens config

  ;; You can disable :unless predicates with (sp-pair "'" nil :unless nil) And
  ;; disable :post-handlers with (sp-pair "{" nil :post-handlers nil) or
  ;; specific :post-handlers with: (sp-pair "{" nil :post-handlers '(:rem ("| "
  ;; "SPC")))
  (when (featurep! :config default +smartparens)
    (after! smartparens
      ;; Smartparens' navigation feature is neat, but does not justify how
      ;; expensive it is. It's also less useful for evil users. This may need to
      ;; be reactivated for non-evil users though. Needs more testing!
      (defhook! +emacs--hook-disable-smartparens-navigate-skip-match ()
        "Disable `sp-navigate-skip-match'."
        'after-change-major-mode-hook
        (setq sp-navigate-skip-match nil
              sp-navigate-consider-sgml-tags nil))

      ;; Autopair quotes more conservatively; if I'm next to a word/before
      ;; another quote, I likely don't want to open a new pair.
      (let ((unless-list '(sp-point-before-word-p
                           sp-point-after-word-p
                           sp-point-before-same-p)))
        (sp-pair "'"  nil :unless unless-list)
        (sp-pair "\"" nil :unless unless-list))

      ;; Expand {|} => { | }
      ;; Expand {|} => {
      ;;   |
      ;; }
      (dolist (brace '("(" "{" "["))
        (sp-pair brace nil
                 :post-handlers '(("||\n[i]" "RET") ("| " "SPC"))
                 ;; I likely don't want a new pair if adjacent to a word or
                 ;; opening brace
                 :unless '(sp-point-before-word-p sp-point-before-same-p)))

      ;; In lisps ( should open a new form if before another parenthesis
      (sp-local-pair sp-lisp-modes "(" ")" :unless '(:rem sp-point-before-same-p))

      ;; Major-mode specific fixes
      (sp-local-pair '(ruby-mode enh-ruby-mode) "{" "}"
                     :pre-handlers '(:rem sp-ruby-pre-handler)
                     :post-handlers '(:rem sp-ruby-post-handler))

      ;; Don't do square-bracket space-expansion where it doesn't make sense to
      (sp-local-pair '(emacs-lisp-mode org-mode markdown-mode gfm-mode)
                     "[" nil :post-handlers '(:rem ("| " "SPC")))

      ;; Reasonable default pairs for HTML-style comments
      (sp-local-pair (append sp--html-modes '(markdown-mode gfm-mode))
                     "<!--" "-->"
                     :unless '(sp-point-before-word-p sp-point-before-same-p)
                     :actions '(insert) :post-handlers '(("| " "SPC")))

      ;; Disable electric keys in C modes because it interferes with smartparens
      ;; and custom bindings. We'll do it ourselves (mostly).
      (after! cc-mode
        (c-toggle-electric-state -1)
        (c-toggle-auto-newline -1)
        (setq c-electric-flag nil)
        (dolist (key '("#" "{" "}" "/" "*" ";" "," ":" "(" ")" "\177"))
          (define-key c-mode-base-map key nil))

        ;; Smartparens and cc-mode both try to autoclose angle-brackets
        ;; intelligently. The result isn't very intelligent (causes redundant
        ;; characters), so just do it ourselves.
        (define-key! c++-mode-map "<" nil ">" nil)

        (defun +default-cc-sp-point-is-template-p (id action context)
          "Return t if point is in the right place for C++
angle-brackets."
          (and (sp-in-code-p id action context)
               (cond ((eq action 'insert)
                      (sp-point-after-word-p id action context))
                     ((eq action 'autoskip)
                      (/= (char-before) 32)))))

        (defun +default-cc-sp-point-after-include-p (id action context)
          "Return t if point is in an #include."
          (and (sp-in-code-p id action context)
               (save-excursion
                 (goto-char (line-beginning-position))
                 (looking-at-p "[ 	]*#include[^<]+"))))

        ;; ...and leave it to smartparens
        (sp-local-pair '(c++-mode objc-mode)
                       "<" ">"
                       :when '(+default-cc-sp-point-is-template-p
                               +default-cc-sp-point-after-include-p)
                       :post-handlers '(("| " "SPC")))

        (sp-local-pair '(c-mode c++-mode objc-mode java-mode)
                       "/*!" "*/"
                       :post-handlers '(("||\n[i]" "RET") ("[d-1]< | " "SPC"))))

      ;; Expand C-style doc comment blocks. Must be done manually because some
      ;; of these languages use specialized (and deferred) parsers, whose state
      ;; we can't access while smartparens is doing its thing.
      (defun +default-expand-asterix-doc-comment-block (&rest _ignored)
        (let ((indent (current-indentation)))
          (newline-and-indent)
          (save-excursion
            (newline)
            (insert (make-string indent 32) " */")
            (delete-char 2))))
      (sp-local-pair
       '(js2-mode typescript-mode rjsx-mode rust-mode c-mode c++-mode objc-mode
                  csharp-mode java-mode php-mode css-mode scss-mode less-css-mode
                  stylus-mode scala-mode)
       "/*" "*/"
       :actions '(insert)
       :post-handlers '(("| " "SPC")
                        ("|\n[i]*/[d-2]" "RET")
                        (+default-expand-asterix-doc-comment-block "*")))

      (after! smartparens-ml
        (sp-with-modes '(tuareg-mode fsharp-mode)
          (sp-local-pair "(*" "*)" :actions nil)
          (sp-local-pair "(*" "*"
                         :actions '(insert)
                         :post-handlers '(("| " "SPC") ("|\n[i]*)[d-2]" "RET")))))

      (after! smartparens-markdown
        (sp-with-modes '(markdown-mode gfm-mode)
          (sp-local-pair "```" "```" :post-handlers '(:add ("||\n[i]" "RET")))

          ;; The original rules for smartparens had an odd quirk: inserting two
          ;; asterixex would replace nearby quotes with asterixes. These two
          ;; rules set out to fix this.
          (sp-local-pair "**" nil :actions :rem)
          (sp-local-pair "*" "*"
                         :actions '(insert skip)
                         :unless '(:rem sp-point-at-bol-p)
                         ;; * then SPC will delete the second asterix and assume
                         ;; you wanted a bullet point. * followed by another *
                         ;; will produce an extra, assuming you wanted **|**.
                         :post-handlers '(("[d1]" "SPC") ("|*" "*"))))

        ;; This keybind allows * to skip over **.
        (map! :map markdown-mode-map
              :ig "*" (cmd! (if (looking-at-p "\\*\\* *$")
                                (forward-char 2)
                              (call-interactively 'self-insert-command)))))))

;;;;; Keybinding fixes

  ;; Highjacks backspace to delete up to nearest column multiple of `tab-width'
  ;; at a time. If you have smartparens enabled, it will also:
  ;;
  ;;  a) balance spaces inside brackets/parentheses ( | ) -> (|)
  ;;  b) close empty multiline brace blocks in one step:
  ;;     {
  ;;     |
  ;;     }
  ;;     becomes {|}
  ;;  c) refresh smartparens' :post-handlers, so SPC and RET expansions work
  ;;     even after a backspace.
  ;;  d) properly delete smartparen pairs when they are encountered, without the
  ;;     need for strict mode.
  ;;  e) do none of this when inside a string
  (advice-add #'delete-backward-char :override #'+default--advice-delete-backward-char)

  ;; HACK Makes `newline-and-indent' continue comments (and more reliably).
  ;;      Consults `+emacs-point-in-comment-functions' to detect a commented
  ;;      region and uses that mode's `comment-line-break-function' to continue
  ;;      comments. If neither exists, it will fall back to the normal behavior
  ;;      of `newline-and-indent'.
  ;;
  ;;      We use an advice here instead of a remapping because many modes define
  ;;      and remap to their own newline-and-indent commands, and tackling all
  ;;      those cases was judged to be more work than dealing with the edge
  ;;      cases on a case by case basis.
  (defadvice! +default--advice-newline-indent-and-continue-comments (&rest _)
    "A replacement for `newline-and-indent'.
Continues comments if executed from a commented line. Consults
`+emacs-point-in-comment-functions' to determine if in a
comment."
    :before-until #'newline-and-indent
    (interactive "*")
    (when (and +default-want-RET-continue-comments
               (+emacs-point-in-comment-p)
               (functionp comment-line-break-function))
      (funcall comment-line-break-function nil)
      t))

  ;; Consistently use q to quit windows
  (after! tabulated-list
    (define-key tabulated-list-mode-map "q" #'quit-window))

;;;;; Keybind schemes

  ;; Custom help keys
  (define-key! help-map
    ;; new keybinds
    "'"    #'describe-char
    "O"    #'+lookup/online
    "T"    #'+emacs/toggle-profiler
    "V"    #'set-variable
    "W"    #'+default/man-or-woman
    "C-k"  #'describe-key-briefly
    "C-l"  #'describe-language-environment
    "C-m"  #'info-emacs-manual

    ;; Unbind `help-for-help'. Conflicts with which-key's help command for the
    ;; <leader> h prefix. It's already on ? and F1 anyway.
    "C-h"  nil

    ;; replacement keybinds replaces `info-emacs-manual' b/c it's on C-m now
    "r"    nil
    "rt"   #'+emacs/reload-theme
    "rf"   #'+emacs/reload-font

    ;; make `describe-bindings' available under the b prefix which it previously
    ;; occupied. Add more binding related commands under that prefix as well
    "b"    nil
    "bb"   #'describe-bindings
    "bi"   #'which-key-show-minor-mode-keymap
    "bm"   #'which-key-show-major-mode
    "bt"   #'which-key-show-top-level
    "bf"   #'which-key-show-full-keymap
    "bk"   #'which-key-show-keymap

    ;; replaces `apropos-documentation' b/c `apropos' covers this
    "d"    nil
    "dc"   #'+emacs/goto-local-config-file
    "di"   #'+emacs/goto-local-init-file
    "dd"   #'+emacs-debug-mode
    "dl"   #'+emacs/help-module-locate
    "dm"   #'+emacs/help-module-info
    "dpc"  #'+emacs/help-package-config
    "dpd"  #'+emacs/goto-local-packages-file
    "dph"  #'+emacs/help-package-homepage

    ;; replaces `apropos-command'
    "a"    #'apropos
    "A"    #'apropos-documentation
    ;; replaces `describe-copying' b/c not useful
    "C-c"  #'describe-coding-system
    ;; replaces `Info-got-emacs-command-node' b/c redundant w/ `Info-goto-node'
    "F"    #'describe-face
    ;; replaces `view-hello-file' b/c annoying
    "h"    nil
    ;; replaces `help-with-tutorial', b/c it's less useful than `load-theme'
    "t"    #'load-theme
    ;; replaces `describe-package' b/c redundant w/ `doom/help-packages'
    "P"    #'find-library)

  (after! which-key
    (let ((prefix-re (regexp-opt (list +emacs-leader-key +emacs-leader-alt-key))))
      (cl-pushnew `((,(format "\\`\\(?:<\\(?:\\(?:f1\\|help\\)>\\)\\|C-h\\|%s h\\) d\\'" prefix-re))
                    nil . "documentation")
                  which-key-replacement-alist)
      (cl-pushnew `((,(format "\\`\\(?:<\\(?:\\(?:f1\\|help\\)>\\)\\|C-h\\|%s h\\) d p\\'" prefix-re))
                    nil . "packages")
                  which-key-replacement-alist)
      (cl-pushnew `((,(format "\\`\\(?:<\\(?:\\(?:f1\\|help\\)>\\)\\|C-h\\|%s h\\) r\\'" prefix-re))
                    nil . "reload")
                  which-key-replacement-alist)
      (cl-pushnew `((,(format "\\`\\(?:<\\(?:\\(?:f1\\|help\\)>\\)\\|C-h\\|%s h\\) b\\'" prefix-re))
                    nil . "bindings")
                  which-key-replacement-alist)))

  ;; Make M-x harder to miss
  (define-key! 'override
    "M-x" #'execute-extended-command
    "A-x" #'execute-extended-command)

  ;; A convention where C-s on popups and interactive searches will invoke ivy
  ;; for its superior filtering.
  (when-let (command (cond ((featurep! :completion ivy)
                            #'counsel-minibuffer-history)
                           ((featurep! :completion helm)
                            #'helm-minibuffer-history)))
    (define-key!
      :keymaps (append +default-minibuffer-maps
                       (when (featurep! :editor evil)
                         '(evil-ex-completion-map)))
      "C-s" command))

  ;; Smarter C-a/C-e for both Emacs and Evil. C-a will jump to indentation.
  ;; Pressing it again will send you to the true bol. Same goes for C-e, except
  ;; it will ignore comments+trailing whitespace before jumping to eol.
  (map! :gnie "C-a" #'+emacs/backward-to-bol-or-indent
        :gnie "C-e" #'+emacs/forward-to-last-non-comment-or-eol

        ;; C-<mouse-scroll-up>   = text scale increase
        ;; C-<mouse-scroll-down> = text scale decrease
        [C-down-mouse-2] (cmd! (text-scale-set 0))

        ;; Standardizes the behavior of modified RET to match the behavior of
        ;; other editors, particularly Atom, textedit, textmate, and vscode, in
        ;; which ctrl+RET will add a new "item" below the current one and
        ;; cmd+RET (Mac) / meta+RET (elsewhere) will add a new, blank line below
        ;; the current one.

        ;; auto-indent on newline by default
        :gi [remap newline] #'newline-and-indent
        ;; insert literal newline
        :i "S-RET"         #'+default/newline
        :i [S-return]      #'+default/newline
        :i "C-j"           #'+default/newline

        ;; Add new item below current (without splitting current line).
        :gi "C-RET"         #'+default/newline-below
        :gn [C-return]      #'+default/newline-below
        ;; Add new item above current (without splitting current line)
        :gi "C-S-RET"       #'+default/newline-above
        :gn [C-S-return]    #'+default/newline-above

        (:when IS-MAC
         :gn "s-RET"        #'+default/newline-below
         :gn [s-return]     #'+default/newline-below
         :gn "S-s-RET"      #'+default/newline-above
         :gn [S-s-return]   #'+default/newline-above))

;;;;; Evil

  ;; Allows the select and overwrite workflow from common text editors. Enable
  ;; `delete-selection-mode' in insert state.
  (setq shift-select-mode t)
  (add-hook 'evil-insert-state-entry-hook #'delete-selection-mode)
  (defhook! +emacs--hook-disable-delete-selection-mode ()
    "Disable `delete-selection-mode' upon leaving insert state."
    'evil-insert-state-exit-hook
    (delete-selection-mode -1))

  ;; Make SPC u SPC u [...] possible
  (map! :map universal-argument-map
        :prefix +emacs-leader-key     "u" #'universal-argument-more
        :prefix +emacs-leader-alt-key "u" #'universal-argument-more)

;;;;; Evil bindings

  ;; NOTE SPC u replaces C-u as the universal argument.

  ;; Minibuffer
  (define-key! evil-ex-completion-map
    "C-a" #'evil-beginning-of-line
    "C-e" #'evil-end-of-line
    "C-b" #'evil-backward-char
    "C-f" #'evil-forward-char
    "C-j" #'next-complete-history-element
    "C-k" #'previous-complete-history-element)

  (define-key! :keymaps +default-minibuffer-maps
               [escape] #'abort-recursive-edit
               "C-a"    #'move-beginning-of-line
               "C-e"    #'move-end-of-line
               "C-r"    #'evil-paste-from-register
               "C-u"    #'evil-delete-back-to-indentation
               "C-v"    #'yank
               "C-w"    #'+emacs/delete-backward-word
               "C-z"    (cmd! (ignore-errors (call-interactively #'undo))))

  (when (featurep! :editor evil)

    (define-key! :keymaps +default-minibuffer-maps
                 "C-j"    #'next-line
                 "C-k"    #'previous-line
                 "C-S-j"  #'scroll-up-command
                 "C-S-k"  #'scroll-down-command)
    (define-key! :states 'insert :keymaps +default-minibuffer-maps
                 "C-j"    #'next-line
                 "C-k"    #'previous-line)

    (define-key! read-expression-map
      "C-j" #'next-line-or-history-element
      "C-k" #'previous-line-or-history-element))

  (map!
   ;; Smarter RET in normal mode
   :n "RET" (general-predicate-dispatch nil
              (and (bound-and-true-p flyspell-mode)
                   (+flyspell-correction-at-point-p))
              #'flyspell-correct-word-generic)
   :m [tab] (general-predicate-dispatch nil
              (and (bound-and-true-p yas-minor-mode)
                   (evil-visual-state-p)
                   (or (eq evil-visual-selection 'line)
                       (not (memq (char-after) (list ?\( ?\[ ?\{ ?\} ?\] ?\))))))
              #'yas-insert-snippet
              (or (and (featurep! :editor fold)
                       (save-excursion (end-of-line) (invisible-p (point))))
                  (and (featurep! :editor fold)
                       (+fold--vimish-fold-p))
                  (and (featurep! :editor fold)
                       (+fold--outline-fold-p)))
              #'+fold/toggle
              (or (+emacs-lookup-key
                   [tab]
                   (list (evil-get-auxiliary-keymap (current-local-map) evil-state)
                         (current-local-map)))
                  (+emacs-lookup-key
                   (kbd "TAB")
                   (list (evil-get-auxiliary-keymap (current-local-map) evil-state)))
                  (+emacs-lookup-key (kbd "TAB") (list (current-local-map))))
              it
              (fboundp 'evil-jump-item)
              #'evil-jump-item)

   (:after help :map help-mode-map
    :n "o"       #'ace-link-help)
   (:after helpful :map helpful-mode-map
    :n "o"       #'ace-link-help)
   (:after info :map Info-mode-map
    :n "o"       #'ace-link-info)
   (:after apropos :map apropos-mode-map
    :n "o"       #'ace-link-help
    :n "TAB"     #'forward-button
    :n [tab]     #'forward-button
    :n [backtab] #'backward-button)
   (:after view :map view-mode-map
    [escape]  #'View-quit-all)
   (:after man :map Man-mode-map
    :n "q"    #'kill-current-buffer)

   ;; `evil-mc'
   (:when (featurep! :editor multiple-cursors)
    (:prefix "gz"
     :nv "d" #'evil-mc-make-and-goto-next-match
     :nv "D" #'evil-mc-make-and-goto-prev-match
     :nv "s" #'evil-mc-skip-and-goto-next-match
     :nv "S" #'evil-mc-skip-and-goto-prev-match
     :nv "c" #'evil-mc-skip-and-goto-next-cursor
     :nv "C" #'evil-mc-skip-and-goto-prev-cursor
     :nv "j" #'evil-mc-make-cursor-move-next-line
     :nv "k" #'evil-mc-make-cursor-move-prev-line
     :nv "m" #'evil-mc-make-all-cursors
     :nv "n" #'evil-mc-make-and-goto-next-cursor
     :nv "N" #'evil-mc-make-and-goto-last-cursor
     :nv "p" #'evil-mc-make-and-goto-prev-cursor
     :nv "P" #'evil-mc-make-and-goto-first-cursor
     :nv "q" #'evil-mc-undo-all-cursors
     :nv "t" #'+multiple-cursors/evil-mc-toggle-cursors
     :nv "u" #'+multiple-cursors/evil-mc-undo-cursor
     :nv "z" #'+multiple-cursors/evil-mc-toggle-cursor-here)
    (:after evil-mc
     :map evil-mc-key-map
     :nv "C-n" #'evil-mc-make-and-goto-next-cursor
     :nv "C-N" #'evil-mc-make-and-goto-last-cursor
     :nv "C-p" #'evil-mc-make-and-goto-prev-cursor
     :nv "C-P" #'evil-mc-make-and-goto-first-cursor))

   ;; misc
   :n "C-S-f"  #'toggle-frame-fullscreen
   :n "C-="    #'+emacs/reset-font-size
   ;; Buffer-local font resizing
   :n "C-+"    #'text-scale-increase
   :n "C--"    #'text-scale-decrease
   ;; Frame-local font resizing
   :n "M-C-+"  #'+emacs/increase-font-size
   :n "M-C--"  #'+emacs/decrease-font-size

   ;; Repeat
   :n "." #'repeat)

;;;;;; expand-region

  (map!
   :ie "C-+" #'er/expand-region
   :ie "C--" #'er/contract-region)

;;;;;; completion

  (map! (:when (featurep! :completion company)
         :i "C-@"   (general-predicate-dispatch nil
                      (not (minibufferp))
                      #'+company/complete)
         :i "C-." (general-predicate-dispatch nil
                    (not (minibufferp))
                    #'+company/complete)
         (:after company
          (:map company-active-map
           "C-w"     nil              ; don't interfere with `evil-delete-backward-word'
           "C-n"     #'company-select-next
           "C-p"     #'company-select-previous
           "C-j"     #'company-select-next
           "C-k"     #'company-select-previous
           "C-h"     #'company-show-doc-buffer
           "C-u"     #'company-previous-page
           "C-d"     #'company-next-page
           "C-s"     #'company-filter-candidates
           "C-S-s"   (cond ((featurep! :completion helm) #'helm-company)
                           ((featurep! :completion company) #'counsel-company))
           "C-SPC"   #'company-complete-common
           "TAB"     #'company-complete-common-or-cycle
           [tab]     #'company-complete-common-or-cycle
           [backtab] #'company-select-previous)
          (:map company-search-map              ; applies to `company-filter-map' too
           "C-n"     #'company-select-next-or-abort
           "C-p"     #'company-select-previous-or-abort
           "C-j"     #'company-select-next-or-abort
           "C-k"     #'company-select-previous-or-abort
           "C-s"     #'company-filter-candidates
           [escape]     #'company-search-abort)))

        (:when (featurep! :completion ivy)
         (:map (help-mode-map helpful-mode-map)
          :n "Q" #'ivy-resume)
         (:after ivy
          :map ivy-minibuffer-map
          "C-z"   #'+ivy-dispatching-done
          "M-o"   #'hydra-ivy/body
          "C-SPC" #'ivy-call-and-recenter       ; preview file
          "C-l"   #'ivy-alt-done
          "C-v"   #'yank)
         (:after counsel
          :map counsel-ag-map
          "C-SPC"    #'ivy-call-and-recenter    ; preview
          "C-l"      #'ivy-done
          "C-c C-e"  #'+ivy/woccur              ; search/replace on results
          [backtab]  #'+ivy/woccur              ; search/replace on results
          [C-return] #'+ivy/git-grep-other-window-action)
         (:after swiper
          :map swiper-map
          [backtab] #'+ivy/woccur))

        (:when (or (featurep! :completion helm)
                   (featurep! :completion helm-base))
         (:after helm :map helm-map
          [remap next-line]     #'helm-next-line
          [remap previous-line] #'helm-previous-line
          [left]     #'left-char
          [right]    #'right-char
          "C-S-f"    #'helm-previous-page
          "C-S-n"    #'helm-next-source
          "C-S-p"    #'helm-previous-source
          (:when (featurep! :editor evil)
           "C-S-j"    #'helm-next-source
           "C-S-k"    #'helm-previous-source
           "C-j"      #'helm-next-line
           "C-k"      #'helm-previous-line)
          "C-u"      #'helm-delete-minibuffer-contents
          "C-s"      #'helm-minibuffer-history
          ;; Swap TAB and C-z
          "TAB"      #'helm-execute-persistent-action
          [tab]      #'helm-execute-persistent-action
          "C-z"      #'helm-select-action)
         (:after helm-ag :map helm-ag-map
          "C--"      #'+helm-do-ag-decrease-context
          "C-="      #'+helm-do-ag-increase-context
          [left]     nil
          [right]    nil)
         (:after helm-files :map (helm-find-files-map helm-read-file-map)
          [C-return] #'helm-ff-run-switch-other-window
          "C-w"      #'helm-find-files-up-one-level)
         (:after helm-locate :map helm-generic-files-map
          [C-return] #'helm-ff-run-switch-other-window)
         (:after helm-buffers :map helm-buffer-map
          [C-return] #'helm-buffer-switch-other-window)
         (:after helm-occur :map helm-occur-map
          [C-return] #'helm-occur-run-goto-line-ow)
         (:after helm-grep :map helm-grep-map
          [C-return] #'helm-grep-run-other-window-action)))

;;;;;; ui

  (map!
   (:when (featurep! :ui popup)
    "C-`"   #'+popup/toggle
    "C-~"   #'+popup/raise
    "C-x p" #'+popup/other))

;;;;;; editor

  (map!
   (:when (featurep! :editor format)
    :n "gQ" #'+format:region)

   (:when (featurep! :editor rotate-text)
    :n "!" #'rotate-text)

   (:when (featurep! :editor multiple-cursors)
    :v  "R"     #'evil-multiedit-match-all
    :n  "M-d"   #'evil-multiedit-match-symbol-and-next
    :n  "M-D"   #'evil-multiedit-match-symbol-and-prev
    :v  "M-d"   #'evil-multiedit-match-and-next
    :v  "M-D"   #'evil-multiedit-match-and-prev
    :nv "C-M-d" #'evil-multiedit-restore
    (:after evil-multiedit
     (:map evil-multiedit-mode-map
      :nv "M-d" #'evil-multiedit-match-and-next
      :nv "M-D" #'evil-multiedit-match-and-prev
      [return]  #'evil-multiedit-toggle-or-restrict-region)))

   (:when (featurep! :editor snippets)
    :i  [C-tab] #'aya-expand
    :nv [C-tab] #'aya-create))


;;;;; Leader keybindings

  ;; Make "," in normal and visual mode the localleader key
  (map!
   :nv "," (general-simulate-key "SPC m"))

  (map! :leader
        :desc "Eval expression"              ":"   #'pp-eval-expression
        :desc "M-x"                          "SPC" #'execute-extended-command
        :desc "Pop up scratch buffer"        "x"   #'+emacs/open-scratch-buffer
        :desc "Org Capture"                  "X"   #'+org-capture/open-frame

        ;; C-u is used by evil
        :desc "Universal argument"           "u"   #'universal-argument
        (:when (featurep! :editor evil)
         :desc "windows"                      "w"   evil-window-map)
        :desc "help"                         "h"   help-map

        (:when (featurep! :ui popup)
         :desc "Toggle last popup"            "~"   #'+popup/toggle)
        :desc "Find file"                    "."   #'find-file

        :desc "Switch buffer"                ","   #'switch-to-buffer
        (:when (featurep! :ui workspaces)
         :desc "Switch workspace buffer"      ","   #'persp-switch-to-buffer)
        :desc "Switch buffer"                "<"   #'switch-to-buffer

        :desc "Switch to last buffer"        "`"   #'evil-switch-to-windows-last-buffer
        :desc "Resume last search"           "'"
        (cond ((featurep! :completion ivy)   #'ivy-resume)
              ((featurep! :completion helm)  #'helm-resume))

        :desc "Search for symbol in project" "*"   #'+default/search-project-for-symbol-at-point

        :desc "Find file in project"         ";"   #'projectile-find-file
        :desc "Jump to bookmark"             "RET" #'bookmark-jump
        :desc "Toggle treemacs"              "0"   #'treemacs

;;;;;; workspaces
        (:when (featurep! :ui workspaces)
         (:prefix-map ("TAB" . "workspaces")
          :desc "Display tab bar"             "TAB"     #'+workspace/display
          :desc "Switch workspace"            "."       #'+workspace/switch-to
          :desc "Switch to last workspace"    "`"       #'+workspace/other
          :desc "Delete this workspace"       "d"       #'+workspace/delete
          :desc "New workspace"               "n"       #'+workspace/new
          :desc "Load workspace from file"    "l"       #'+workspace/load
          :desc "Rename workspace"            "r"       #'+workspace/rename
          :desc "Restore last session"        "R"       #'+workspace/restore-last-session
          :desc "Save workspace to file"      "s"       #'+workspace/save
          :desc "Delete session"              "x"       #'+workspace/kill-session
          :desc "Next workspace"              "C-n"     #'+workspace/switch-right
          :desc "Previous workspace"          "C-p"     #'+workspace/switch-left
          :desc "Next workspace"              "<right>" #'+workspace/switch-right
          :desc "Previous workspace"          "<left>"  #'+workspace/switch-left
          :desc "Next workspace"              "]"       #'+workspace/switch-right
          :desc "Previous workspace"          "["       #'+workspace/switch-left
          :desc "Switch to 1st workspace"     "1"       #'+workspace/switch-to-0
          :desc "Switch to 2nd workspace"     "2"       #'+workspace/switch-to-1
          :desc "Switch to 3rd workspace"     "3"       #'+workspace/switch-to-2
          :desc "Switch to 4th workspace"     "4"       #'+workspace/switch-to-3
          :desc "Switch to 5th workspace"     "5"       #'+workspace/switch-to-4
          :desc "Switch to 6th workspace"     "6"       #'+workspace/switch-to-5
          :desc "Switch to 7th workspace"     "7"       #'+workspace/switch-to-6
          :desc "Switch to 8th workspace"     "8"       #'+workspace/switch-to-7
          :desc "Switch to 9th workspace"     "9"       #'+workspace/switch-to-8
          :desc "Switch to final workspace"   "0"       #'+workspace/switch-to-final))

;;;;;; buffers
        (:prefix-map ("b" . "buffer")
         :desc "Toggle narrowing"          "-"   #'+emacs/toggle-narrow-buffer
         :desc "Next buffer"               "]"   #'next-buffer
         :desc "Previous buffer"           "["   #'previous-buffer
         (:when (featurep! :ui workspaces)
          :desc "Switch workspace buffer" "b" #'persp-switch-to-buffer
          :desc "Switch buffer"           "B" #'switch-to-buffer)
         (:unless (featurep! :ui workspaces)
          :desc "Switch buffer"           "b" #'switch-to-buffer)
         :desc "Kill buffer"               "d"   #'kill-current-buffer
         :desc "ibuffer"                   "i"   #'ibuffer
         :desc "Kill buffer"               "k"   #'kill-current-buffer
         :desc "Kill all buffers"          "K"   #'+emacs/kill-all-buffers
         :desc "Next buffer"               "n"   #'next-buffer
         :desc "New empty buffer"          "N"   #'evil-buffer-new
         :desc "Kill other buffers"        "O"   #'+emacs/kill-other-buffers
         :desc "Previous buffer"           "p"   #'previous-buffer
         :desc "Copy clipboard to buffer"  "P"   #'+emacs/copy-clipboard-to-whole-buffer
         :desc "Revert buffer"             "r"   #'revert-buffer
         :desc "Save buffer as root"       "u"   #'+emacs/sudo-save-buffer
         :desc "Reopen killed buffer"      "U"   #'+emacs/reopen-killed-buffer
         :desc "Pop up scratch buffer"     "x"   #'+emacs/open-scratch-buffer
         :desc "Switch to scratch buffer"  "X"   #'+emacs/switch-to-scratch-buffer
         :desc "Copy buffer to clipboard"  "y"   #'+emacs/copy-whole-buffer-to-clipboard
         :desc "Bury buffer"               "z"   #'bury-buffer
         :desc "Kill buried buffers"       "Z"   #'+emacs/kill-buried-buffers)

;;;;;; code
        (:prefix-map ("c" . "code")
         (:when (featurep! :tools lsp)
          :desc "LSP Execute code action" "a" #'lsp-execute-code-action
          :desc "LSP Organize imports" "o" #'lsp-organize-imports
          (:when (featurep! :completion ivy)
           :desc "Jump to symbol in current workspace" "j"   #'lsp-ivy-workspace-symbol
           :desc "Jump to symbol in any workspace"     "J"   #'lsp-ivy-global-workspace-symbol)
          (:when (featurep! :completion helm)
           :desc "Jump to symbol in current workspace" "j"   #'helm-lsp-workspace-symbol
           :desc "Jump to symbol in any workspace"     "J"   #'helm-lsp-global-workspace-symbol)
          (:when (featurep! :ui treemacs +lsp)
           :desc "Errors list"                         "X"   #'lsp-treemacs-errors-list
           :desc "Incoming call hierarchy"             "y"   #'lsp-treemacs-call-hierarchy
           :desc "Outgoing call hierarchy"             "Y"   (cmd!! #'lsp-treemacs-call-hierarchy t)
           :desc "References tree"                     "R"   (cmd!! #'lsp-treemacs-references t)
           :desc "Symbols"                             "S"   #'lsp-treemacs-symbols)
          :desc "LSP"                                  "l"   #'+default/lsp-command-map
          :desc "LSP Rename"                           "r"   #'lsp-rename)
         :desc "Compile"                    "c" #'compile
         :desc "Recompile"                  "C" #'recompile
         :desc "Jump to definition"         "d" #'+lookup/definition
         :desc "Jump to references"         "D" #'+lookup/references
         :desc "Evaluate buffer/region"     "e" #'+eval/buffer-or-region
         :desc "Evaluate & replace region"  "E" #'+eval:replace-region
         (:when (featurep! :editor format)
          :desc "Format buffer/region"       "f"   #'+format/region-or-buffer)
         :desc "Jump to documentation"      "k" #'+lookup/documentation
         :desc "Send to repl"               "s" #'+eval/send-region-to-repl
         :desc "Delete trailing whitespace" "w" #'delete-trailing-whitespace
         :desc "Delete trailing newlines"   "W" #'+emacs/delete-trailing-newlines
         :desc "List errors"                "x" #'flycheck-list-errors)

;;;;;; files
        (:prefix-map ("f" . "file")
         (:when (featurep! :tools backups)
          (:prefix ("B" . "Backups")
           :desc "List backups"   "b" #'+backups/list-backups
           :desc "List orphans"   "o" #'+backups/list-orphaned
           :desc "Remove orphans" "r" #'+backups/remove-orphaned))
         :desc "Copy this file"              "C"   #'+emacs/copy-this-file
         :desc "Find directory"              "d"   #'dired
         :desc "Delete this file"            "D"   #'+emacs/delete-this-file
         (:prefix ("e" . "emacs config")
          :desc "Find file in emacs.d"        "e"   #'+default/find-in-emacsd
          :desc "Browse emacs.d"              "E"   #'+default/browse-emacsd
          :desc "Open .emacs.d git repo"      "g"   #'+emacs/open-in-magit
          :desc "Find file in local config"   "l"   #'+emacs/find-file-in-local-config
          :desc "Browse local config"         "L"   #'+emacs/open-local-config
          :desc "Open local config file"      "c"   #'+emacs/goto-local-config-file
          :desc "Open local init file"        "i"   #'+emacs/goto-local-init-file
          :desc "Open local package file"     "p"   #'+emacs/goto-local-packages-file
          :desc "Search in .emacs.d"          "s"   #'+emacs/search-in-emacsd
          (:prefix ("m" . "maintanance")
           :desc "Search versions"       "."   #'+emacs/search-versions
           :desc "Module info"           "l"   #'+emacs/help-module-locate
           :desc "Module info"           "i"   #'+emacs/help-module-info
           :desc "Update package"        "p"   #'+emacs/update-package
           :desc "Update all packages"   "P"   #'+emacs/update-all-packages))
         :desc "Find file"                   "f"   #'find-file
         :desc "Find file from here"         "F"   #'+default/find-file-under-here
         :desc "Toggle symlink"              "l"   #'+emacs/toggle-symlink
         :desc "Locate file"                 "L"   #'locate
         (:when (featurep! :tools pandoc)
          (:prefix ("P" . "Pandoc")
           :desc "Pandoc hydra" "p" #'+pandoc/run-pandoc))
         :desc "Recent files"                "r"   #'recentf-open-files
         :desc "Rename/move file"            "R"   #'+emacs/move-this-file
         :desc "Save file"                   "s"   #'save-buffer
         :desc "Save file as..."             "S"   #'write-file
         :desc "Sudo find file"              "u"   #'+emacs/sudo-find-file
         :desc "Sudo this file"              "U"   #'+emacs/sudo-this-file
         :desc "Yank file path"              "y"   #'+default/yank-buffer-path
         :desc "Yank file path from project" "Y"   #'+default/yank-buffer-path-relative-to-project)

;;;;;; git
        (:prefix-map ("g" . "git")
         :desc "Git revert file"             "R"   #'vc-revert
         :desc "Copy git link"               "y"   #'git-link
         :desc "Copy git link to homepage"   "Y"   #'git-link-homepage
         (:when (featurep! :ui transient-state)
          :desc "SMerge"                    "m"   #'+vc/smerge-hydra/body)
         ;; vc-gutter
         (:when (featurep! :ui vc-gutter)
          :desc "Git revert hunk"       "r" #'git-gutter:revert-hunk
          :desc "Git stage hunk"        "s" #'git-gutter:stage-hunk
          :desc "Git time machine"      "t" #'git-timemachine-toggle
          :desc "Jump to next hunk"     "]" #'git-gutter:next-hunk
          :desc "Jump to previous hunk" "[" #'git-gutter:previous-hunk)
         ;; magit
         (:when (featurep! :tools magit)
          :desc "Magit dispatch"            "/"   #'magit-dispatch
          :desc "Magit file dispatch"       "."   #'magit-file-dispatch
          :desc "Magit switch branch"       "b"   #'magit-branch-checkout
          :desc "Magit status"              "g"   #'magit-status
          :desc "Magit file delete"         "D"   #'magit-file-delete
          :desc "Magit blame"               "B"   #'magit-blame-addition
          :desc "Magit clone"               "C"   #'magit-clone
          :desc "Magit fetch"               "F"   #'magit-fetch
          :desc "Magit buffer log"          "L"   #'magit-log-buffer-file
          :desc "Git stage file"            "S"   #'magit-stage-file
          :desc "Git unstage file"          "U"   #'magit-unstage-file)
         (:prefix ("f" . "find")
          :desc "Find file"             "f" #'magit-find-file
          :desc "Find gitconfig file"   "g" #'magit-find-git-config-file
          :desc "Find commit"           "c" #'magit-show-commit)
         (:prefix ("o" . "open in browser")
          :desc "Browse region or line" "." #'+vc/git-browse-region-or-line)
         (:prefix ("l" . "list")
          :desc "List repositories"     "r" #'magit-list-repositories
          :desc "List submodules"       "s" #'magit-list-submodules)
         (:prefix ("c" . "create")
          :desc "Initialize repo"           "r"   #'magit-init
          :desc "Clone repo"                "R"   #'magit-clone
          :desc "Commit"                    "c"   #'magit-commit-create
          :desc "Fixup"                     "f"   #'magit-commit-fixup
          :desc "Branch"                    "b"   #'magit-branch-and-checkout))

;;;;;; insert
        (:prefix-map ("i" . "insert")
         :desc "Current file name"             "f"   #'+default/insert-file-path
         :desc "Current file path"             "F"   (cmd!! #'+default/insert-file-path t)
         :desc "Evil ex path"                  "p"   (cmd! (evil-ex "R!echo "))
         :desc "From evil register"            "r"   #'evil-ex-registers
         :desc "Snippet"                       "s"   #'yas-insert-snippet
         :desc "Unicode"                       "u"   #'insert-char
         :desc "From clipboard"                "y"   #'+default/yank-pop)

;;;;;; notes
        (:prefix-map ("n" . "notes")
         :desc "Search notes for symbol"      "*" #'+default/search-notes-for-symbol-at-point
         :desc "Org agenda"                   "a" #'org-agenda
         :desc "Org capture"                  "c" #'org-capture
         (:when (featurep! :ui deft)
          :desc "Open deft"                    "d" #'deft)
         :desc "Find file in notes"           "f" #'+default/find-in-notes
         :desc "Browse notes"                 "F" #'+default/browse-notes
         :desc "Org store link"               "l" #'org-store-link
         :desc "Search org agenda headlines"  "n" #'+default/org-notes-headlines
         :desc "Tags search"                  "m" #'org-tags-view
         :desc "Active org-clock"             "o" #'org-clock-goto
         :desc "Todo list"                    "t" #'org-todo-list
         :desc "Search notes"                 "s" #'+default/org-notes-search
         :desc "View search"                  "v" #'org-search-view
         :desc "Org export to clipboard"        "y" #'+org/export-to-clipboard
         :desc "Org export to clipboard as RTF" "Y" #'+org/export-to-clipboard-as-rich-text
         (:when (featurep! :org org +journal)
          (:prefix ("j" . "journal")
           :desc "New Entry"      "j" #'org-journal-new-entry
           :desc "Search Forever" "s" #'org-journal-search-forever)))

;;;;;; open
        (:prefix-map ("o" . "open")
         :desc "Org agenda"       "A"  #'org-agenda
         (:prefix ("a" . "org agenda")
          :desc "Agenda"         "a"  #'org-agenda
          :desc "Todo list"      "t"  #'org-todo-list
          :desc "Tags search"    "m"  #'org-tags-view
          :desc "View search"    "v"  #'org-search-view)
         :desc "Default browser"              "b"  #'browse-url-of-file
         :desc "Start debugger"               "d"  #'+debugger/start
         :desc "New frame"                    "f"  #'make-frame
         :desc "Select frame"                 "F"  #'select-frame-by-name
         :desc "REPL"                         "r"  #'+eval/open-repl-other-window
         :desc "REPL (same window)"           "R"  #'+eval/open-repl-same-window
         :desc "Dired"                        "-"  #'dired-jump
         (:when (featurep! :ui treemacs)
          :desc "Project sidebar"              "p"  #'+treemacs/toggle
          :desc "Find file in project sidebar" "P"  #'+treemacs/find-file)
         (:when (featurep! :term eshell)
          :desc "Toggle eshell popup"          "e"  #'+eshell/toggle
          :desc "Open eshell here"             "E"  #'+eshell/here))

;;;;;; projects
        (:prefix-map ("p" . "project")
         :desc "Browse project"               "." #'+default/browse-project
         :desc "Browse other project"         ">" #'+emacs/browse-in-other-project
         :desc "Run cmd in project root"      "!" #'projectile-run-shell-command-in-root
         :desc "Async cmd in project root"    "&" #'projectile-run-async-shell-command-in-root
         :desc "Add new project"              "a" #'projectile-add-known-project
         :desc "Switch to project buffer"     "b" #'projectile-switch-to-buffer
         :desc "Compile in project"           "c" #'projectile-compile-project
         :desc "Repeat last command"          "C" #'projectile-repeat-last-command
         :desc "Remove known project"         "d" #'projectile-remove-known-project
         :desc "Discover projects in folder"  "D" #'+default/discover-projects
         :desc "Edit project .dir-locals"     "e" #'projectile-edit-dir-locals
         :desc "Find file in project"         "f" #'projectile-find-file
         :desc "Find file in other project"   "F" #'+emacs/find-file-in-other-project
         :desc "Configure project"            "g" #'projectile-configure-project
         :desc "Invalidate project cache"     "i" #'projectile-invalidate-cache
         :desc "Kill project buffers"         "k" #'projectile-kill-buffers
         :desc "Find other file"              "o" #'projectile-find-other-file
         :desc "Switch project"               "p" #'projectile-switch-project
         :desc "Find recent project files"    "r" #'projectile-recentf
         :desc "Run project"                  "R" #'projectile-run-project
         :desc "Save project files"           "s" #'projectile-save-project-buffers
         :desc "Pop up scratch buffer"        "x" #'+emacs/open-project-scratch-buffer
         :desc "Switch to scratch buffer"     "X" #'+emacs/switch-to-project-scratch-buffer
         :desc "List project tasks"           "t" #'+emacs/project-tasks
         :desc "Test project"                 "T" #'projectile-test-project)

;;;;;; quiting for pros
        (:prefix-map ("q" . "quit & session")
         :desc "Restart emacs server"        "d" #'+default/restart-server
         :desc "Start new Emacs instance"    "e" #'+emacs/new-emacs
         :desc "Delete frame"                "f" #'delete-frame
         :desc "Clear current frame"         "F" #'+emacs/kill-all-buffers
         :desc "Kill Emacs (and daemon)"     "K" #'save-buffers-kill-emacs
         :desc "Quit Emacs"                  "q" #'save-buffers-kill-terminal
         :desc "Quit Emacs without saving"   "Q" #'evil-quit-all-with-error-code
         :desc "Quick save current session"  "s" #'+emacs/quicksave-session
         :desc "Restore last session"        "l" #'+emacs/quickload-session
         :desc "Save session to file"        "S" #'+emacs/save-session
         :desc "Restore session from file"   "L" #'+emacs/load-session
         :desc "Restart & restore Emacs"     "R" #'+emacs/restart-and-restore
         :desc "Restart Emacs"               "r" #'+emacs/restart)

;;;;;; search
        (:prefix-map ("s" . "search")
         (:when (featurep! :completion helm)
          :desc "Search buffer"                "b" #'helm-occur)
         :desc "Search buffer"                "b" #'swiper
         :desc "Search all open buffers"      "B" #'swiper-all
         :desc "Search current directory"     "d" #'+default/search-cwd
         :desc "Search other directory"       "D" #'+default/search-other-cwd
         :desc "Locate file"                  "f" #'locate
         :desc "Jump to symbol"               "i" #'imenu
         :desc "Jump to link"                 "l" #'ace-link
         :desc "Jump list"                    "j" #'evil-show-jumps
         :desc "Jump to mark"                 "m" #'evil-show-marks
         :desc "Look up online"               "o" #'+lookup/online
         :desc "Look up online (w/ prompt)"   "O" #'+lookup/online-select
         :desc "Look up in local docsets"     "k" #'+lookup/in-docsets
         :desc "Look up in all docsets"       "K" #'+lookup/in-all-docsets
         :desc "Search project"               "p" #'+default/search-project
         :desc "Search other project"         "P" #'+default/search-other-project
         :desc "Search buffer"                "s" #'swiper-isearch
         :desc "Search buffer for thing at point" "S" #'swiper-isearch-thing-at-point)

;;;;;; snippets
        (:when (featurep! :editor snippets)
         (:prefix-map ("S" . "snippets")
          ;; auto-yasnippets
          :desc "Create temporary snippet" "s" #'aya-create
          :desc "Use temporary snippet"    "e" #'aya-expand
          ;; yasnippets
          :desc "View snippet (global)"    "?" #'+snippets/find
          :desc "Edit snippet"             "c" #'+snippets/edit
          :desc "View snippet for mode"    "f" #'+snippets/find-for-current-mode
          :desc "Insert snippet"           "i" #'yas-insert-snippet
          :desc "New snippet"              "n" #'+snippets/new
          :desc "Reload snippets"          "r" #'yas-reload-all))

;;;;;; toggles
        (:prefix-map ("t" . "toggle")
         :desc "Big mode"                   "b" #'+emacs-big-font-mode
         :desc "Fill Column Indicator"      "c" #'global-display-fill-column-indicator-mode
         (:when (featurep! :checkers syntax)
          :desc "Flycheck"                   "f" #'flycheck-mode)
         :desc "Frame fullscreen"           "F" #'toggle-frame-fullscreen
         (:when (featurep! :ui golden-ratio)
          :desc "Golden ratio"               "g" #'golden-ratio-mode)
         :desc "Indent style"               "I" #'+emacs/toggle-indent-style
         :desc "Line numbers"               "l" #'+emacs/toggle-line-numbers
         (:when (featurep! :lang org +present)
          :desc "org-tree-slide fancy"       "p" #'+org-present/start-fancy
          :desc "org-tree-slide mode"        "P" #'+org-present/start)
         :desc "Read-only mode"             "r" #'read-only-mode
         (:when (featurep! :checkers spelling)
          :desc "Flyspell"                   "s" #'flyspell-mode)
         (:when (featurep! :org org +pomodoro)
          :desc "Pomodoro timer"             "t" #'org-pomodoro)
         :desc "Soft line wrapping"         "w" #'visual-line-mode
         (:when (featurep! :editor word-wrap)
          :desc "Soft line wrapping"         "w" #'+word-wrap-mode)
         (:when (featurep! :ui zen)
          :desc "Zen mode"                   "z" #'writeroom-mode))))
