;; modules/+org-ql.el -*- lexical-binding: t; -*-

(use-package org-ql
  :defer t
  :config
  (require 'helm-org))
