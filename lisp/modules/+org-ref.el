;; modules/+org-ref.el -*- lexical-binding: t; -*-

(use-package org-ref
  :commands
  (org-ref-ivy-insert-cite-link
   org-ref-bibtex-next-entry
   org-ref-bibtex-previous-entry
   org-ref-open-in-browser
   org-ref-open-bibtex-notes
   org-ref-open-bibtex-pdf
   org-ref-bibtex-hydra/body
   org-ref-bibtex-hydra/org-ref-bibtex-new-entry/body-and-exit
   org-ref-sort-bibtex-entry
   arxiv-add-bibtex-entry
   arxiv-get-pdf-add-bibtex-entry
   doi-utils-add-bibtex-entry-from-doi
   isbn-to-bibtex
   pubmed-insert-bibtex-from-pmid)
  :init
  (setq org-ref-completion-library 'org-ref-helm-bibtex)
  (map!
   (:localleader
    :map org-mode-map
    ;; insert referecences via org-ref
    "i c" #'org-ref-helm-insert-cite-link)
   (:localleader
    :map LaTeX-mode-map
    ;; insert referecences via org-ref
    "i c" #'org-ref-helm-insert-cite-link)
   (:localleader
    :map markdown-mode-map
    ;; insert referecences via org-ref
    "i c" #'org-ref-helm-insert-cite-link)))
