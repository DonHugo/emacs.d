;; -*- no-byte-compile: t; -*-
;; modules/packages.el

;;; ui
;; childframes
(when (featurep! :ui childframes)
  (package! ivy-posframe :lockfile +childframes)
  (package! helm-posframe :lockfile +childframes))
;; deft
(when (featurep! :ui deft)
  (package! deft :lockfile +deft))
;; doom-theming
(when (featurep! :ui doom-theming)
  (package! doom-themes :lockfile +doom-theming)
  (package! solaire-mode :lockfile +doom-theming))
;; golden-ratio
(when (featurep! :ui golden-ratio)
  (package! golden-ratio :lockfile +golden-ratio))
;; modeline
(when (featurep! :ui modeline)
  (package! doom-modeline :lockfile +modeline)
  (package! anzu :lockfile +modeline)
  (when (featurep! :editor evil)
    (package! evil-anzu :lockfile +modeline)))
;; unicode
(when (featurep! :ui unicode)
  (package! persistent-soft :lockfile +unicode)
  (package! unicode-fonts :lockfile +unicode))
;;zen
(when (featurep! :ui zen)
  (package! writeroom-mode :lockfile +zen)
  (package! mixed-pitch :lockfile +zen))

;;; editor
;; format
(when (featurep! :editor format)
  (package! format-all
    :lockfile 'pinned
    :pin "47d862d40a088ca089c92cd393c6dca4628f87d3"))
;; word-wrap
(when (featurep! :editor word-wrap)
  (package! adaptive-wrap :lockfile +word-wrap))

;;; checkers
;; grammar
(when (featurep! :checkers grammar)
  (package! langtool :lockfile +grammar))
;; spelling
(when (featurep! :checkers spelling)
  (package! flyspell-correct :lockfile +spelling)
  (cond ((featurep! :completion ivy)
         (package! flyspell-correct-ivy :lockfile +spelling))
        ((featurep! :completion helm)
         (package! flyspell-correct-helm :lockfile +spelling))
        ((package! flyspell-correct-popup :lockfile +spelling)))

  (package! flyspell-lazy :lockfile +spelling))
;; syntax
(when (featurep! :checkers syntax)
  (package! flycheck :lockfile +syntax)
  (package! flycheck-popup-tip :lockfile +syntax))

;;; tools
;; arch-linux
(when (featurep! :tools arch-linux)
  (package! pacfiles-mode :lockfile +arch-linux)
  (package! pkgbuild-mode :lockfile +arch-linux))
;; copy-as-format
(when (featurep! :tools copy-as-format)
  (package! copy-as-format :lockfile +copy-as-format))
;; direnv
(when (featurep! :tools direnv)
  (package! envrc :lockfile +direnv)
  (package! inheritenv :lockfile +direnv))
;; editorconfig
(when (featurep! :tools editorconfig)
  (package! editorconfig :lockfile +editorconfig))
;; lsp
(when (featurep! :tools lsp)
  (package! lsp-mode :lockfile +lsp)
  (package! lsp-ui :lockfile +lsp)
  (when (featurep! :completion ivy)
    (package! lsp-ivy :lockfile +lsp-ivy))
  (when (featurep! :completion helm)
    (package! lsp-helm :lockfile +lsp-helm)))
;; pandoc
(when (featurep! :tools pandoc)
  (when (featurep! :org org)
    (package! ox-pandoc :lockfile +pandoc-org))
  (package! pandoc-mode :lockfile +pandoc))
;; pdf
(when (featurep! :tools pdf)
  (package! pdf-tools
    :recipe (:post-build
             (let ((warning-minimum-log-level :error))
               (require 'pdf-tools)
               (unless (file-exists-p pdf-info-epdfinfo-program)
                 (require 'pdf-occur)
                 (print-group!
                  (if (or (bound-and-true-p +emacs-auto-accept)
                          (not (y-or-n-p "pdf-tools requires epdfinfo. Build it now?")))
                      (print! (start "Skipped building epdfinfo. Use `M-x pdf-tools-install' to build it later."))
                    (print! (start "Building epdfinfo for pdf-tools"))
                    (with-current-buffer (pdf-tools-install 'no-query)
                      (while compilation-in-progress
                        (sleep-for 1))
                      (when (> compilation-num-errors-found 0)
                        (print! (warn "Failed to build epdfinfo because: %s" (buffer-string))))))))))
    :lockfile +pdf)
  (package! saveplace-pdf-view :lockfile +pdf)
  ;; org
  (when (featurep! :org org)
    (package! org-pdftools :lockfile +pdf-org)))

;;; org
;; dragndrop
(when (featurep! :org org +dragndrop)
  (package! org-download :lockfile +org-dragndrop))
;; journal
(when (featurep! :org org +journal)
  (package! org-journal :lockfile +org-journal))
;; jupyter
(when (featurep! :org org +jupyter)
  (package! jupyter :lockfile +org-jupyter))
;; pomodoro
(when (featurep! :org org +pomodoro)
  (package! org-pomodoro :lockfile +org-pomodoro))
;; present
(when (featurep! :org org +present)
  (package! centered-window
    :recipe (:host github :repo "anler/centered-window-mode")
    :lockfile +org-present)
  (package! org-tree-slide :lockfile +org-present)
  (package! ox-reveal :lockfile +org-present))
;; org-ql
(when (featurep! :org org +org-ql)
  (package! org-ql :lockfile +org-ql)
  (package! helm-org :lockfile +org-ql))
;; references
(when (featurep! :org org +ref)
  (package! org-ref :lockfile +org-ref))
;; tufte
(when (featurep! :org org +tufte)
  (package! ox-gfm :lockfile +org-tufte)
  (package! ox-tufte-latex
    :recipe (:host github :repo "tsdye/tufte-org-mode"
             :fork (:host github :repo "DonHugo69/tufte-org-mode"))
    :lockfile +org-tufte))

;;; lang
;; common-lisp
(when (featurep! :lang common-lisp)
  (package! sly :lockfile +common-lisp)
  (package! sly-macrostep :lockfile +common-lisp)
  (package! sly-repl-ansi-color :lockfile +common-lisp))
;; data
(when (featurep! :lang data)
  (package! csv-mode :lockfile +data)
  (package! json-mode :lockfile +data)
  (package! nginx-mode :lockfile +data)
  (package! yaml-mode :lockfile +data))
;; ess
(when (featurep! :lang ess)
  (package! ess :lockfile +ess)
  (package! ess-R-data-view :lockfile +ess)
  (package! polymode
    :lockfile 'pinned
    :pin "54888d6c15249503e1a66da7bd7761a9eda9b075")
  (package! poly-R
    :lockfile 'pinned
    :pin "e4a39caaf48e1c2e5afab3865644267b10610537"))
;; latex
(when (featurep! :lang latex)
  (package! auctex :lockfile +latex)
  (package! latex-preview-pane :lockfile +latex)
  (when (featurep! :lang latex +latexmk)
    (package! auctex-latexmk :lockfile +latex-latexmk))
  (when (featurep! :completion company)
    (package! company-auctex :lockfile +latex-company)
    (package! company-reftex :lockfile +latex-company)
    (package! company-math :lockfile +latex-company)))
;; nix
(when (featurep! :lang nix)
  (package! nix-mode :lockfile +nix)
  (package! nix-update :lockfile +nix)

  (when (featurep! :completion company)
    (package! company-nixos-options :lockfile +nix-company))

  (when (featurep! :lang nix +helm)
    (package! helm-nixos-options :lockfile +nix-helm)))
;; markdown
(when (featurep! :lang markdown)
  (package! markdown-mode :lockfile +markdown)
  (package! markdown-toc :lockfile +markdown))
;; python
(when (featurep! :lang python)
  (package! python :lockfile +python)
  (package! anaconda-mode :lockfile +python)
  (when (featurep! :completion company)
    (package! company-anaconda :lockfile +python-company))
  (package! pyimport :lockfile +python)
  (package! pyimpsort :lockfile +python))
;; scheme
(when (featurep! :lang scheme)
  (package! geiser
    :recipe (:host gitlab :repo "jaor/geiser")
    :lockfile +scheme)
  (package! macrostep-geiser :lockfile +scheme)
  (package! flycheck-guile
    :recipe (:host github :repo "flatwhatson/flycheck-guile")
    :lockfile +scheme)
  (when (featurep! :lang scheme +racket)
    (package! racket-mode :lockfile +scheme-racket)))
;; sh
(when (featurep! :lang sh +fish)
  (package! fish-mode :lockfile +shell-fish))
