;; modules/modules.el -*- lexical-binding: t; -*-

;; Detect stale bytecode
;; If Emacs version changed, the bytecode is no longer valid and we must
;; recompile.
(eval
 `(unless (equal
           (list
            (emacs-version)
            (concat
             (file-name-sans-extension (file!)) ".el"))
           ',(eval-when-compile
               (list
                (emacs-version)
                (file!))))
    (throw 'stale-bytecode nil)))

(eval-when-compile
  (require 'el-patch))


;;
;;; input

;;;; deDE

(module! (:input deDE)

  ;; This module makes some modifications to the keybindings when using a german
  ;; keyboard

  ;; Use Ctrl-Alt as a an alternative for AltGr for keys on the right side of
  ;; the keyboard. Windows should do this automatically, thus this should not be
  ;; necesassary.

  (define-key key-translation-map (kbd "C-M-7") (kbd "\{"))
  (define-key key-translation-map (kbd "C-M-8") (kbd "\["))
  (define-key key-translation-map (kbd "C-M-9") (kbd "\]"))
  (define-key key-translation-map (kbd "C-M-0") (kbd "\}"))
  (define-key key-translation-map (kbd "C-M-ß") (kbd "\\"))
  (define-key key-translation-map (kbd "C-M-+") (kbd "\~")))


;;
;;; completion

;;;; company

(module! (:completion company +childframe)

  (use-package company-box
    :when (featurep! :completion company +childframe)
    :hook (company-mode . company-box-mode)
    :config
    (setq company-box-show-single-candidate t
          company-box-backends-colors nil
          company-box-doc-enable nil
          company-box-doc-delay 1
          company-box-icons-alist 'company-box-icons-all-the-icons
          ;; Move `company-box-icons--elisp' to the end, because it has a
          ;; catch-all clause that ruins icons from other backends in elisp
          ;; buffers.
          company-box-icons-functions
          (cons #'+company-box-icons--elisp-fn
                (delq 'company-box-icons--elisp
                      company-box-icons-functions))
          company-box-icons-all-the-icons
          (let ((all-the-icons-scale-factor 0.8))
            `((Unknown       . ,(all-the-icons-material "find_in_page"             :face 'all-the-icons-purple))
              (Text          . ,(all-the-icons-material "text_fields"              :face 'all-the-icons-green))
              (Method        . ,(all-the-icons-material "functions"                :face 'all-the-icons-red))
              (Function      . ,(all-the-icons-material "functions"                :face 'all-the-icons-red))
              (Constructor   . ,(all-the-icons-material "functions"                :face 'all-the-icons-red))
              (Field         . ,(all-the-icons-material "functions"                :face 'all-the-icons-red))
              (Variable      . ,(all-the-icons-material "adjust"                   :face 'all-the-icons-blue))
              (Class         . ,(all-the-icons-material "class"                    :face 'all-the-icons-red))
              (Interface     . ,(all-the-icons-material "settings_input_component" :face 'all-the-icons-red))
              (Module        . ,(all-the-icons-material "view_module"              :face 'all-the-icons-red))
              (Property      . ,(all-the-icons-material "settings"                 :face 'all-the-icons-red))
              (Unit          . ,(all-the-icons-material "straighten"               :face 'all-the-icons-red))
              (Value         . ,(all-the-icons-material "filter_1"                 :face 'all-the-icons-red))
              (Enum          . ,(all-the-icons-material "plus_one"                 :face 'all-the-icons-red))
              (Keyword       . ,(all-the-icons-material "filter_center_focus"      :face 'all-the-icons-red))
              (Snippet       . ,(all-the-icons-material "short_text"               :face 'all-the-icons-red))
              (Color         . ,(all-the-icons-material "color_lens"               :face 'all-the-icons-red))
              (File          . ,(all-the-icons-material "insert_drive_file"        :face 'all-the-icons-red))
              (Reference     . ,(all-the-icons-material "collections_bookmark"     :face 'all-the-icons-red))
              (Folder        . ,(all-the-icons-material "folder"                   :face 'all-the-icons-red))
              (EnumMember    . ,(all-the-icons-material "people"                   :face 'all-the-icons-red))
              (Constant      . ,(all-the-icons-material "pause_circle_filled"      :face 'all-the-icons-red))
              (Struct        . ,(all-the-icons-material "streetview"               :face 'all-the-icons-red))
              (Event         . ,(all-the-icons-material "event"                    :face 'all-the-icons-red))
              (Operator      . ,(all-the-icons-material "control_point"            :face 'all-the-icons-red))
              (TypeParameter . ,(all-the-icons-material "class"                    :face 'all-the-icons-red))
              (Template      . ,(all-the-icons-material "short_text"               :face 'all-the-icons-green))
              (ElispFunction . ,(all-the-icons-material "functions"                :face 'all-the-icons-red))
              (ElispVariable . ,(all-the-icons-material "check_circle"             :face 'all-the-icons-blue))
              (ElispFeature  . ,(all-the-icons-material "stars"                    :face 'all-the-icons-orange))
              (ElispFace     . ,(all-the-icons-material "format_paint"             :face 'all-the-icons-pink)))))

    ;; Disable tab-bar in company-box child frames
    (add-to-list 'company-box-frame-parameters '(tab-bar-lines . 0))

    ;; Don't show documentation in echo area, because company-box displays its
    ;; own in a child frame.
    (delq! 'company-echo-metadata-frontend company-frontends)

    (defun +company-box-icons--elisp-fn (candidate)
      (when (derived-mode-p 'emacs-lisp-mode)
        (let ((sym (intern candidate)))
          (cond ((fboundp sym)  'ElispFunction)
                ((boundp sym)   'ElispVariable)
                ((featurep sym) 'ElispFeature)
                ((facep sym)    'ElispFace)))))

    ;; Use `+company-box-doc/toggle' instead of `company-box-doc-manually'
    (define-key! company-active-map [remap company-box-doc-manually] #'+company-box-doc/toggle)

    (defadvice! +company--advice-remove-scrollbar (orig-fn &rest args)
      "This disables the company-box scrollbar, because:
https://github.com/sebastiencs/company-box/issues/44"
      :around #'company-box--update-scrollbar
      (letf! ((#'display-buffer-in-side-window #'ignore))
        (apply orig-fn args)))

    ;; `company-box' performs insufficient frame-live-p checks. Any command that
    ;; "cleans up the session" will break company-box.
    (defadvice! +company-box--advice-detect-deleted-frame (frame)
      "TODO"
      :filter-return #'company-box--get-frame
      (if (frame-live-p frame) frame))
    (defadvice! +company-box--advice-detect-deleted-doc-frame (_selection frame)
      "TODO"
      :before #'company-box-doc
      (and company-box-doc-enable
           (frame-local-getq company-box-doc-frame frame)
           (not (frame-live-p (frame-local-getq company-box-doc-frame frame)))
           (frame-local-setq company-box-doc-frame nil frame)))))


;;
;;; ui

;;;; childframes

(module! (:ui childframes)

  (defvar +childframe-posframe-border-width 7
    "The border width used by `ivy-posframe' and `helm-posframe'.
When 0, no border is shown.")

  (defvar +childframe-threshold 0.75
    "Frame to monitor size ratio. If the current frame's
dimensions are above this threshold, use a childframe for
display.")

  (after! ivy-posframe
    (defvar +childframe-min-height 'ivy-height
      "The minimum height of `ivy-posframe' and
`helm-posframe'.")

    (defvar +childframe-height nil
      "The height of `ivy-posframe' and `helm-posframe'. For
dynamic adjustments set to nil.")

    (defvar +childframe-min-width '(round (* (frame-width) 0.62))
      "The minimum width of `ivy-posframe' and `helm-posframe'.")

    (defvar +childframe-width '(round (* (frame-width) 0.62))
      "The width of `ivy-posframe' and `helm-posframe'. For
dynamic adjustments set to nil.")

    (defhook! +childframe--hook-update-dimensions ()
      "Update `ivy-posframe' and `helm-posframe' dimensions."
      '(+emacs-switch-frame-hook
        +emacs-switch-window-hook
        +emacs-switch-buffer-hook)
      (setq ivy-posframe-min-width (eval +childframe-min-width)
            ivy-posframe-width (eval +childframe-width)
            ivy-posframe-min-height (eval +childframe-min-height)
            ivy-posframe-height (eval +childframe-height)
            helm-posframe-min-width (eval +childframe-min-width)
            helm-posframe-width (eval +childframe-width)
            helm-posframe-min-height (eval +childframe-min-height)
            helm-posframe-height (eval +childframe-height))))

  (after! (ivy-rich ivy-posframe)
    (defvar +ivy-rich-display-transformers-list-default nil)
    (defadvice! +childframe--advice-adjust-ivy-rich-transformers (orig-fn)
      "Makes some adjustments to
`ivy-rich-display-transformers-list' in order to look alright
when displayed in a childframe."
      :around #'+childframe-threshold-p
      (if (funcall orig-fn)
          (progn
            (unless +ivy-rich-display-transformers-list-default
              (setq +ivy-rich-display-transformers-list-default (copy-tree ivy-rich-display-transformers-list)))
            (ivy-rich-modify-columns
             'ivy-switch-buffer
             '((ivy-switch-buffer-transformer (:width 0.15))))
            (ivy-rich-modify-columns
             'counsel-recentf
             '((ivy-rich-candidate (:width 0.5))))
            t)
        (when +ivy-rich-display-transformers-list-default
          (setq ivy-rich-display-transformers-list +ivy-rich-display-transformers-list-default))
        nil)))


;;;;; ivy-posframe

  (use-package ivy-posframe
    :when (featurep! :completion ivy)
    :hook (ivy-mode . ivy-posframe-mode)
    :config
    (setq ivy-fixed-height-minibuffer nil
          ivy-posframe-hide-minibuffer t
          ivy-posframe-border-width +childframe-posframe-border-width)

    ;; Better visual distinction betweeen posframe and background
    (face-spec-set 'ivy-posframe-border
                   '((t (:inherit region))))

    ;; default to posframe display function
    (setf (alist-get t ivy-posframe-display-functions-alist)
          #'+ivy-display-at-frame-center-near-bottom-fn)

    ;; posframe doesn't work well with async sources (the posframe will
    ;; occasionally stop responding/redrawing), and causes violent resizing of
    ;; the posframe.
    (dolist (fn '(swiper counsel-rg counsel-grep counsel-git-grep))
      (setf (alist-get fn ivy-posframe-display-functions-alist)
            #'ivy-display-function-fallback))
    ;; Use `ivy-fixed-height-minibuffer' for the above functions
    (defadvice! +ivy-posframe--advice-use-fixed-height-fallback (orig-fn &rest args)
      "Use `ivy-fixed-height-minibuffer' for advised functions."
      :around '(swiper
                counsel-rg
                counsel-grep
                counsel-git-grep)
      (let ((ivy-fixed-height-minibuffer t))
        (apply orig-fn args))))

  ;; patches
  (when (featurep! :completion ivy)
    (el-patch-feature ivy-posframe)
    (after! ivy-posframe
      ;; Recognize custom `ivy-posframe-display-functions-alist' functions and
      ;; do not activate `ivy-posframe' in non-maximized frames
      ;; PATCH `ivy-posframe--minibuffer-setup'
      (el-patch-defun ivy-posframe--minibuffer-setup (fn &rest args)
        "Advice function of FN, `ivy--minibuffer-setup' with ARGS."
        (if (el-patch-swap (not (display-graphic-p))
                           (or (not (display-graphic-p))
                               (not (+childframe-threshold-p))))
            (el-patch-swap (apply fn args)
                           (let ((ivy-fixed-height-minibuffer t))
                             (apply fn args)))
          (let ((ivy-fixed-height-minibuffer nil))
            (apply fn args))
          (when (and ivy-posframe-hide-minibuffer
                     (posframe-workable-p)
                     ;; if display-function is not a ivy-posframe style display-function.
                     ;; do not hide minibuffer.
                     ;; The hypothesis is that all ivy-posframe style display functions
                     ;; have ivy-posframe as name prefix, need improve!
                     (string-match-p (el-patch-swap "^ivy-posframe" "^\\(\\+ivy-\\|ivy-posframe\\)") (symbol-name ivy--display-function)))
            (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
              (overlay-put ov 'window (selected-window))
              (overlay-put ov 'ivy-posframe t)
              (overlay-put ov 'face
                           (let ((bg-color (face-background 'default nil)))
                             `( :background ,bg-color :foreground ,bg-color
                                :box nil :underline nil
                                :overline nil :strike-through nil)))
              (setq-local cursor-type nil)))))

      ;; PATCH `ivy-posframe--display-function-prop'
      (el-patch-defun ivy-posframe--display-function-prop (fn &rest args)
        "Around advice of FN with ARGS."
        (if (el-patch-swap (not (display-graphic-p))
                           (or (not (display-graphic-p))
                               (not (+childframe-threshold-p))))
            (apply fn args)
          (let ((ivy-display-functions-props
                 (append ivy-display-functions-props
                         (mapcar
                          (lambda (elm)
                            `(,elm :cleanup ivy-posframe-cleanup))
                          (mapcar #'cdr ivy-posframe-display-functions-alist)))))
            (apply fn args))))

      ;; PATCH `ivy-posframe--height'
      (el-patch-defun ivy-posframe--height (fn &rest args)
        "Around advide of FN with ARGS."
        (if (el-patch-swap (not (display-graphic-p))
                           (or (not (display-graphic-p))
                               (not (+childframe-threshold-p))))
            (apply fn args)
          (let ((ivy-height-alist
                 (append ivy-posframe-height-alist ivy-height-alist)))
            (apply fn args))))

      ;; PATCH `ivy-posframe--read'
      (el-patch-defun ivy-posframe--read (fn &rest args)
        "Around advice of FN with AGS."
        (if (el-patch-swap (not (display-graphic-p))
                           (or (not (display-graphic-p))
                               (not (+childframe-threshold-p))))
            (apply fn args)
          (let ((ivy-display-functions-alist
                 (append ivy-posframe-display-functions-alist ivy-display-functions-alist)))
            (apply fn args))))))


;;;;; helm-posframe

  (defvar +helm-posframe-handler #'+childframes-poshandler-frame-center-near-bottom-fn
    "The function that determines the location of the childframe.
It should return a cons cell representing the X and Y
coordinates. See `posframe-poshandler-frame-center' as a
reference.")

  (defvar +helm-posframe-text-scale nil
    "The text-scale to use in the helm childframe. Set to nil for
no scaling. Can be negative.")

  (use-package helm-posframe
    :when (featurep! :completion helm-base)
    :after helm
    :config
    (setq helm-display-function #'+helm-posframe-display
          helm-posframe-border-width +childframe-posframe-border-width)

    ;; Better visual distinction betweeen posframe and background
    (face-spec-set 'helm-posframe-border
                   '((t (:inherit region :background nil))))

    (defadvice! +helm-posframe--advice-refresh-actions ()
      "Refresh `helm-buffer' after using `helm-select-action'."
      :after-until #'helm-select-action
      (helm-refresh))))


;;;; deft

(module! (:ui deft)

  (use-package deft
    :commands deft
    :init
    (setq deft-default-extension "org"
          ;; de-couples filename and note title:
          deft-use-filename-as-title nil
          deft-use-filter-string-for-filename t
          ;; converts the filter string into a readable file-name using kebab-case:
          deft-file-naming-rules
          '((noslash . "-")
            (nospace . "-")
            (case-fn . downcase)))
    :config
    (setq deft-recursive t
          deft-recursive-ignore-dir-regexp "\\(?:\\.\\|\\.\\.\\|\\.attach\\)$"
          deft-directory org-directory)
    (add-to-list 'deft-extensions "tex")
    (add-hook 'deft-mode-hook #'+emacs--hook-mark-buffer-as-real)
    ;; start filtering immediately
    (when (featurep! :editor evil)
      (set-evil-initial-state! 'deft-mode 'insert))
    (map! :map deft-mode-map
          :n "gr"  #'deft-refresh
          :n "C-s" #'deft-filter
          :i "C-n" #'deft-new-file
          :i "C-m" #'deft-new-file-named
          :i "C-d" #'deft-delete-file
          :i "C-r" #'deft-rename-file
          :n "r"   #'deft-rename-file
          :n "a"   #'deft-new-file
          :n "A"   #'deft-new-file-named
          :n "d"   #'deft-delete-file
          :n "D"   #'deft-archive-file
          :n "q"   #'kill-current-buffer
          :localleader
          "RET" #'deft-new-file-named
          "a"   #'deft-archive-file
          "c"   #'deft-filter-clear
          "d"   #'deft-delete-file
          "f"   #'deft-find-file
          "g"   #'deft-refresh
          "l"   #'deft-filter
          "n"   #'deft-new-file
          "r"   #'deft-rename-file
          "s"   #'deft-toggle-sort-method
          "t"   #'deft-toggle-incremental-search)))


;;;; doom-theming

(module! (:ui doom-theming)

  (use-package doom-themes
    :defer t
    :init
    (unless +emacs-theme
      (setq +emacs-theme 'doom-one))

    :config
    ;; improve integration with org-mode
    (add-hook '+emacs-load-theme-hook #'doom-themes-org-config)

    ;; treemacs config
    (when (featurep! :ui treemacs)
      (add-hook '+emacs-load-theme-hook #'doom-themes-treemacs-config)))

  (use-package solaire-mode
    :hook (+emacs-load-theme . solaire-global-mode)
    :hook (+popup-buffer-mode . turn-on-solaire-mode)))


;;;; golden-ratio

(module! (:ui golden-ratio)

  (use-package golden-ratio
    :defer t
    :config
    ;; Add `golden-ratio' to `+emacs-switch-window-hook'
    (add-hook '+emacs-switch-window-hook #'golden-ratio)
    ;; Call `balance-windows' before and after `golden-ratio-mode'
    (add-hook! 'golden-ratio-mode-hook #'balance-windows)
    ;; Inhibit `golden-ratio-mode' via the following settings. Unfortunately, it does require quite
    ;; a few exceptions
    ;; `golden-ratio-exclude-modes'
    (dolist (m '("bs-mode"
                 "calc-mode"
                 "calendar-mode"
                 "ediff-mode"
                 "dired-mode"
                 "gud-mode"
                 "gdb-locals-mode"
                 "gdb-registers-mode"
                 "gdb-breakpoints-mode"
                 "gdb-threads-mode"
                 "gdb-frames-mode"
                 "gdb-inferior-io-mode"
                 "gdb-disassembly-mode"
                 "gdb-memory-mode"
                 "help-mode"
                 "helpful-mode"
                 "org-agenda-mode"
                 "ranger-mode"
                 "rxt-help-mode"
                 "speedbar-mode"
                 "treemacs-mode"))

      (add-to-list 'golden-ratio-exclude-modes m))

    ;; `golden-ratio-exclude-buffer-regexp'
    (dolist (regex '("^\\*\\+emacs:"
                     "^\\*[hH]elm.*"
                     "\\*[tT]reemacs.*"))
      (add-to-list 'golden-ratio-exclude-buffer-regexp regex))

    ;; `golden-ratio-extra-commands'
    (dolist (f '(ace-window
                 ace-delete-window
                 ace-select-window
                 ace-swap-window
                 ace-maximize-window
                 avy-pop-mark
                 buf-move-left
                 buf-move-right
                 buf-move-up
                 buf-move-down
                 evil-avy-goto-word-or-subword-1
                 evil-avy-goto-line
                 evil-window-delete
                 evil-window-split
                 evil-window-vsplit
                 evil-window-left
                 evil-window-right
                 evil-window-up
                 evil-window-down
                 evil-window-bottom-right
                 evil-window-top-left
                 evil-window-mru
                 evil-window-next
                 evil-window-prev
                 evil-window-new
                 evil-window-vnew
                 evil-window-rotate-upwards
                 evil-window-rotate-downwards
                 evil-window-move-very-top
                 evil-window-move-far-left
                 evil-window-move-far-right
                 evil-window-move-very-bottom
                 next-multiframe-window
                 previous-multiframe-window
                 quit-window
                 winum-select-window-0-or-10
                 winum-select-window-1
                 winum-select-window-2
                 winum-select-window-3
                 winum-select-window-4
                 winum-select-window-5
                 winum-select-window-6
                 winum-select-window-7
                 winum-select-window-8
                 winum-select-window-9
                 windmove-left
                 windmove-right
                 windmove-up
                 windmove-down))
      (add-to-list 'golden-ratio-extra-commands f))

    ;; `golden-ratio-exclude-buffer-names'
    (dolist (n '("*info*"
                 "*LV*"
                 " *NeoTree*"
                 "*Messages*"
                 "*Org tags*"
                 "*Org todo*"
                 " *transient*"
                 " *which-key*"))
      (add-to-list 'golden-ratio-exclude-buffer-names n))

    ;; `golden-ratio-inhibit-functions'
    (dolist (f '((lambda () which-key--pages-obj)))
      (add-to-list 'golden-ratio-inhibit-functions f))

    ;; `ediff-mode'
    (defhook! +golden-ratio--hook-ediff-toggle-off ()
      "Disable `golden-ratio-mode' before `ediff-mode'."
      'ediff-before-setup-hook
      (when (bound-and-true-p golden-ratio-mode)
        (golden-ratio-mode 'toggle)))
    (defhook! +golden-ratio--hook-ediff-toggle-on ()
      "Restore `golden-ratio-mode' after `ediff-mode'."
      '(ediff-quit-hook ediff-suspend-hook)
      (unless (bound-and-true-p golden-ratio-mode)
        (golden-ratio-mode 'toggle)))

    ;; Modules
    ;; :editor evil
    (when (featurep! :editor evil)
      (define-key! evil-window-map
        "g" #'golden-ratio-mode))
    ;; :lang ess
    (when (featurep! :lang ess)
      (dolist (f '(ess-eval-buffer-and-go
                   ess-eval-function-and-go
                   ess-eval-line-and-go))
        (add-to-list 'golden-ratio-extra-commands f)))))


;;;; modeline

(module! (:ui modeline)

  (use-package doom-modeline
    :hook (after-init . doom-modeline-mode)
    :hook (doom-modeline-mode . size-indication-mode)
    :hook (doom-modeline-mode . column-number-mode)
    :init
    (unless after-init-time
      ;; prevent flash of unstyled modeline at startup
      (setq-default mode-line-format nil))
    ;; We display project info in the modeline ourselves
    (setq projectile-dynamic-mode-line nil)
    ;; Set these early so they don't trigger variable watchers
    (setq doom-modeline-bar-width 3
          doom-modeline-github nil
          doom-modeline-mu4e nil
          doom-modeline-persp-name t
          doom-modeline-minor-modes nil
          doom-modeline-major-mode-icon nil
          doom-modeline-buffer-file-name-style 'truncate-upto-project
          doom-modeline-modal-icon nil)
    ;; Fix modeline icons in daemon-spawned graphical frames. We have our own
    ;; mechanism for disabling all-the-icons, so we don't need doom-modeline to
    ;; do it for us. However, this may cause unwanted padding in the modeline in
    ;; daemon-spawned terminal frames. If it bothers you, you may prefer
    ;; `doom-modeline-icon' set to `nil'.
    (when (daemonp)
      (setq doom-modeline-icon t))
    :config
    (add-hook 'after-setting-font-hook #'+modeline--hook-resize-for-font)
    (add-hook '+emacs-load-theme-hook #'doom-modeline-refresh-bars)
    (add-hook '+doom-dashboard-mode-hook #'doom-modeline-set-project-modeline)

    (defhook! +modeline--hook-hide-in-non-status-buffer ()
      "Show minimal modeline in magit-status buffer, no modeline
elsewhere."
      'magit-mode-hook
      (if (eq major-mode 'magit-status-mode)
          (doom-modeline-set-vcs-modeline)
        (hide-mode-line-mode)))

    ;; Some functions modify the buffer, causing the modeline to show a false
    ;; modified state, so force them to behave.
    (defadvice! +modeline--advice-inhibit-modification-hooks (orig-fn &rest args)
      "Some functions modify the buffer, causing the modeline to
show a false modified state, so force them to behave."
      :around #'ws-butler-after-save
      (with-silent-modifications (apply orig-fn args)))

    (advice-add #'doom-modeline-redisplay :override #'ignore)

    ;; Add symlink status/toggle icon
    (defcustom +doom-modeline-buffer-symlink-icon t
      "Whether to display an icon if the buffer's visited file is
a symlink. It respects `doom-modeline-icon'."
      :type 'boolean
      :group 'doom-modeline)

    (after! doom-modeline
      (defvar-local +doom-modeline--buffer-file-symlink-icon nil)
      (defun +doom-modeline-update-buffer-file-symlink-icon (&rest _)
        "Update the buffer's symlink status in mode-line."
        (setq +doom-modeline--buffer-file-symlink-icon
              (when +doom-modeline-buffer-symlink-icon
                (ignore-errors
                  (concat
                   (cond ((or (file-symlink-p (buffer-file-name))
                              +emacs--symlink-origin)
                          (let ((icon (doom-modeline-buffer-file-state-icon
                                       "link" "🔗" "%1*" `(:inherit doom-modeline-warning
                                                           :weight ,(if doom-modeline-icon
                                                                        'normal
                                                                      'bold)))))
                            (propertize icon
                                        'help-echo "mouse-1: Toggle symlink"
                                        'mouse-face 'mode-line-highlight
                                        'local-map (let ((map (make-sparse-keymap)))
                                                     (define-key map [mode-line mouse-1]
                                                       #'+emacs/toggle-symlink)
                                                     map))))
                         (t "")))))))

      (defsubst +doom-modeline--buffer-symlink-icon ()
        "The icon of the current buffer symlink."
        (when +doom-modeline-buffer-symlink-icon
          (when-let ((icon (+doom-modeline-update-buffer-file-symlink-icon)))
            (concat
             (if (doom-modeline--active)
                 icon
               (doom-modeline-propertize-icon icon 'mode-line-inactive))
             (doom-modeline-vspc)))))

      (doom-modeline-def-segment buffer-info
        "Combined information about the current buffer, including
the current working directory, the file name, and its
state (modified, read-only or non-existent)."
        (concat
         (doom-modeline-spc)
         (doom-modeline--buffer-mode-icon)
         (doom-modeline--buffer-state-icon)
         (doom-modeline--buffer-name)
         (doom-modeline-spc)
         (+doom-modeline--buffer-symlink-icon)))))
  

  (use-package anzu
    :after-call isearch-mode)


  (use-package evil-anzu
    :when (featurep! :editor evil)
    :after-call evil-ex-start-search evil-ex-start-word-search evil-ex-search-activate-highlight
    :config (global-anzu-mode +1)))


;;;; popup

(module! (:ui popup +all)

  ;; everything starting with a "*"
  (set-popup-rules!
    '(("^\\*"  :slot 1 :vslot -1 :select t)
      ("^ \\*" :slot 1 :vslot -1 :size +popup-shrink-to-fit))))


;;;; unicode

(module! (:ui unicode)

  (defhook! +unicode--hook-init-fonts ()
    "Set up `unicode-fonts' to eventually run; accomodating the
daemon, if necessary."
    '+emacs-init-ui-hook :append
    (setq-default bidi-display-reordering t
                  +emacs-unicode-font nil)
    (if (display-graphic-p)
        (+unicode--hook-setup-fonts (selected-frame))
      (add-hook 'after-make-frame-functions #'+unicode--hook-setup-fonts))))


;;
;;;; zen

(module! (:ui zen)

  (defvar +zen-mixed-pitch-modes '(adoc-mode rst-mode markdown-mode org-mode)
    "What major-modes to enable `mixed-pitch-mode' in with
`writeroom-mode'.")

  (defvar +zen-text-scale 2
    "The text-scaling level for `writeroom-mode'.")

  (defvar +zen-window-divider-size 4
    "Pixel size of window dividers when `writeroom-mode' is
active.")

  (defvar +zen--old-window-divider-size nil)

  (after! writeroom-mode
    ;; Activate writeroom-mode in one buffer (e.g. an org buffer) and code in another.
    ;; Fullscreening/maximizing will be opt-in.
    (setq writeroom-maximize-window nil)
    (remove-hook 'writeroom-global-effects 'writeroom-set-fullscreen)

    (defhook! +zen--hook-enable-text-scaling-mode ()
      "Enable `mixed-pitch-mode' when in
`+zen-mixed-pitch-modes'."
      'writeroom-mode-hook :append
      (when (/= +zen-text-scale 0)
        (text-scale-set (if writeroom-mode +zen-text-scale 0))
        (visual-fill-column-adjust)))

    (defhook! +zen--hook-toggle-large-window-dividers ()
      "Make window dividers larger and easier to see."
      'global-writeroom-mode-hook
      (when (bound-and-true-p window-divider-mode)
        (if writeroom-mode
            (setq +zen--old-window-divider-size
                  (cons window-divider-default-bottom-width
                        window-divider-default-right-width)
                  window-divider-default-bottom-width +zen-window-divider-size
                  window-divider-default-right-width +zen-window-divider-size)
          (when +zen--old-window-divider-size
            (setq window-divider-default-bottom-width (car +zen--old-window-divider-size)
                  window-divider-default-right-width (cdr +zen--old-window-divider-size))))
        (window-divider-mode +1)))

    ;; Adjust margins when text size is changed
    (advice-add #'text-scale-adjust :after #'visual-fill-column-adjust))

  (use-package mixed-pitch
    :hook (writeroom-mode . +zen--hook-enable-mixed-pitch-mode)
    :config
    (defun +zen--hook-enable-mixed-pitch-mode ()
      "Enable `mixed-pitch-mode' when in
`+zen-mixed-pitch-modes'."
      (when (apply #'derived-mode-p +zen-mixed-pitch-modes)
        (mixed-pitch-mode (if writeroom-mode +1 -1))))

    (pushnew! mixed-pitch-fixed-pitch-faces
              'solaire-line-number-face
              'org-date
              'org-footnote
              'org-special-keyword
              'org-property-value
              'org-ref-cite-face
              'org-tag
              'org-todo-keyword-todo
              'org-todo-keyword-habt
              'org-todo-keyword-done
              'org-todo-keyword-wait
              'org-todo-keyword-kill
              'org-todo-keyword-outd
              'org-todo
              'font-lock-comment-face)))


;;
;;; editor

;;;; format

(module! (:editor format)

  (defvar +format-on-save-enabled-modes
    '(not emacs-lisp-mode    ; elisp's mechanisms are good enough
          sql-mode           ; sqlformat is currently broken
          tex-mode           ; latexindent is broken
          latex-mode
          org-msg-edit-mode) ; doesn't need a formatter
    "A list of major modes in which to reformat the buffer upon saving.
If this list begins with `not', then it negates the list. If it
is `t', it is enabled in all modes. If nil, it is disabled in all
modes, the same as if the +onsave flag wasn't used at all.
Irrelevant if you do not have the +onsave flag enabled for this
module.")

  (defvar +format-preserve-indentation t
    "If non-nil, the leading indentation is preserved when formatting the whole
buffer. This is particularly useful for partials.
Indentation is always preserved when formatting regions.")

  (defvar-local +format-with nil
    "Set this to explicitly use a certain formatter for the current buffer.")

  (defvar +format-with-lsp t
    "If non-nil, format with LSP formatter if it's available.
This can be set buffer-locally with `setq-hook!' to disable LSP
formatting in select buffers.")


;;;;; Bootstrap

  (defun +format--hook-enable-on-save-maybe ()
    "Enable formatting on save in certain major modes.
This is controlled by `+format-on-save-enabled-modes'."
    (cond ((eq major-mode 'fundamental-mode))
          ((string-prefix-p " " (buffer-name)))
          ((booleanp +format-on-save-enabled-modes)
           +format-on-save-enabled-modes)
          ((if (eq (car-safe +format-on-save-enabled-modes) 'not)
               (memq major-mode (cdr +format-on-save-enabled-modes))
             (not (memq major-mode +format-on-save-enabled-modes))))
          ((not (require 'format-all nil t)))
          ((format-all-mode +1))))

  (when (featurep! :editor format +onsave)
    (add-hook 'after-change-major-mode-hook #'+format--hook-enable-on-save-maybe))


;;;;; Hacks

  ;; Allow a specific formatter to be used by setting `+format-with', either
  ;; buffer-locally or let-bound.
  (advice-add #'format-all--probe :around #'+format--advice-probe)


  ;; 1. Enables partial reformatting (while preserving leading indentation),
  ;; 2. Applies changes via RCS patch, line by line, to protect buffer markers
  ;;    and avoid any jarring cursor+window scrolling.
  (advice-add #'format-all-buffer--with :override #'+format--advice-buffer)

  ;; `format-all-mode' raises an error when it doesn't know how to format a
  ;; buffer.
  (add-to-list 'debug-ignored-errors "^Don't know how to format ")

  (defadvice! +format--advice-all-buffer-from-hook (orig-fn &rest args)
    "Don't pop up imposing warnings about missing formatters, but
still log it in to *Messages*."
    :around #'format-all-buffer--from-hook
    (letf! (defun format-all-buffer--with (formatter mode-result)
             (when (or (eq formatter 'lsp)
                       (eq formatter 'eglot)
                       (condition-case-unless-debug e
                           (format-all--formatter-executable formatter)
                         (error
                          (message "Warning: cannot reformat buffer because %S isn't installed"
                                   (gethash formatter format-all--executable-table))
                          nil)))
               (funcall format-all-buffer--with formatter mode-result)))
      (apply orig-fn args))))


;;;; word-wrap

(module! (:editor word-wrap)

  (defvar +word-wrap-extra-indent 'double
    "The amount of extra indentation for wrapped code lines.)
When 'double, indent by twice the major-mode indentation.
When 'single, indent by the major-mode indentation.
When a positive integer, indent by this fixed amount.
When a negative integer, dedent by this fixed amount.
Otherwise no extra indentation will be used.")

  (defvar +word-wrap-disabled-modes
    '(fundamental-mode so-long-mode)
    "Major-modes where `+global-word-wrap-mode' should not enable
`+word-wrap-mode'.")

  (defvar +word-wrap-visual-modes
    '(org-mode)
    "Major-modes where `+word-wrap-mode' should not use
`adaptive-wrap-prefix-mode'.")

  (defvar +word-wrap-text-modes
    '(text-mode markdown-mode markdown-view-mode gfm-mode gfm-view-mode rst-mode
                latex-mode LaTeX-mode)
    "Major-modes where `+word-wrap-mode' should not provide extra
indentation.")

  (when (memq 'visual-line-mode text-mode-hook)
    (remove-hook 'text-mode-hook #'visual-line-mode)
    (add-hook 'text-mode-hook #'+word-wrap-mode)))


;;
;;; checkers

;;;; grammar

(module! (:checkers grammar)

  (use-package langtool
    :commands (langtool-check
               langtool-check-done
               langtool-show-message-at-point
               langtool-correct-buffer)))


;;;; spelling

(module! (:checkers spelling)

  (defvar ispell-dictionary "en_US")

  ;; `ispell'
  (after! ispell
    (setq-default ispell-dictionary "english")
    ;; Don't spellcheck org blocks
    (pushnew! ispell-skip-region-alist
              '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:")
              '("#\\+BEGIN_SRC" . "#\\+END_SRC")
              '("#\\+BEGIN_EXAMPLE" . "#\\+END_EXAMPLE"))

    ;; Enable either aspell or hunspell.
    (pcase (cond ((executable-find "aspell")   'aspell)
                 ((executable-find "hunspell") 'hunspell))
      (`aspell
       (setq ispell-program-name "aspell"
             ispell-extra-args '("--sug-mode=ultra" "--run-together" "--dont-tex-check-comments"))

       (defhook! +flyspell--hook-remove-run-together-switch-for-aspell ()
         "Remove '--run-together' switch."
         'text-mode-hook
         (setq-local ispell-extra-args (remove "--run-together" ispell-extra-args)))

       (defadvice! +flyspell--advice-setup-ispell-extra-args (orig-fun &rest args)
         "Setup ispell extra args."
         :around '(ispell-word flyspell-auto-correct-word)
         (let ((ispell-extra-args (remove "--run-together" ispell-extra-args)))
           (ispell-kill-ispell t)
           (apply orig-fun args)
           (ispell-kill-ispell t))))

      (`hunspell
       (setq ispell-program-name "hunspell"))

      (_ (message "Spell checker not found. Either install `aspell' or `hunspell'"))))

  (use-package flyspell ; built-in
    :defer t
    :preface
    ;; `flyspell' is loaded at startup. In order to lazy load its config we need to pretend it isn't
    ;; loaded.
    (defer-feature! flyspell flyspell-mode flyspell-prog-mode)
    :init
    (add-hook! '(org-mode-hook
                 markdown-mode-hook
                 TeX-mode-hook
                 rst-mode-hook
                 mu4e-compose-mode-hook
                 message-mode-hook
                 git-commit-mode-hook) #'flyspell-mode)

    :config
    (setq flyspell-issue-welcome-flag nil
          ;; Significantly speeds up flyspell, which would otherwise print messages for every word
          ;; when checking the entire buffer
          flyspell-issue-message-flag nil)

    (defhook! +flyspell--hook-inhibit-duplicate-detection-maybe ()
      "Don't mark duplicates when style/grammar linters are
present. e.g. proselint and langtool."
      'flyspell-mode-hook
      (and (or (and (bound-and-true-p flycheck-mode)
                    (executable-find "proselint"))
               (featurep 'langtool))
           (setq-local flyspell-mark-duplications-flag nil)))

    ;; Ensure mode-local predicates declared with `set-flyspell-predicate!' are used in their
    ;; respective major modes.
    (add-hook 'flyspell-mode-hook #'+flyspell--hook-init-predicate)
    (map! :map flyspell-mouse-map
          "RET"     #'flyspell-correct-at-point
          [return]  #'flyspell-correct-at-point
          [mouse-1] #'flyspell-correct-at-point))


;;;;; flyspell-correct

  (use-package flyspell-correct
    :commands flyspell-correct-previous
    :general ([remap ispell-word] #'flyspell-correct-at-point)
    :config
    (cond ((and (featurep! :completion helm)
                (require 'flyspell-correct-helm nil t)))
          ((and (featurep! :completion ivy)
                (require 'flyspell-correct-ivy nil t)))
          ((require 'flyspell-correct-popup nil t)
           (setq flyspell-popup-correct-delay 0.8)
           (define-key popup-menu-keymap [escape] #'keyboard-quit))))


;;;;; flyspell-lazy

  (use-package flyspell-lazy
    :after flyspell
    :config
    (setq flyspell-lazy-idle-seconds 1
          flyspell-lazy-window-idle-seconds 3)
    (flyspell-lazy-mode +1)))


;;;; syntax

(module! (:checkers syntax)

  (use-package flycheck
    :commands flycheck-list-errors flycheck-buffer
    :hook (+emacs-first-buffer . global-flycheck-mode)
    :config
    (setq flycheck-emacs-lisp-load-path 'inherit)
    ;; Rerunning checks on every newline is a mote excessive.
    (delq 'new-line flycheck-check-syntax-automatically)
    ;; And don't recheck on idle as often
    (setq flycheck-idle-change-delay 1.0)

    ;; For the above functionality, check syntax in a buffer that you switched to only briefly. This
    ;; allows "refreshing" the syntax check state for several buffers quickly after e.g. changing a
    ;; config file.
    (setq flycheck-buffer-switch-check-intermediate-buffers t)
    ;; Display errors a little quicker (default is 0.9s)
    (setq flycheck-display-errors-delay 0.25)

    ;; Don't commandeer input focus if the error message pops up (happens when tooltips and
    ;; childframes are disabled).
    (when (featurep! :ui popup)
      (set-popup-rules!
        '(("^\\*Flycheck error messages\\*" :select nil)
          ("^\\*Flycheck errors\\*" :size 0.25))))

    (defhook! +flycheck--hook-flycheck-buffer ()
      "Flycheck buffer on ESC in normal mode."
      '+emacs-escape-hook
      :append
      (when flycheck-mode
        (ignore-errors (flycheck-buffer))
        nil))

    (map!
     :map flycheck-error-list-mode-map
     :n "C-n" #'flycheck-error-list-next-error
     :n "C-p" #'flycheck-error-list-previous-error
     :n "j"   #'flycheck-error-list-next-error
     :n "k"   #'flycheck-error-list-previous-error
     :n "RET" #'flycheck-error-list-goto-error
     :n [return] #'flycheck-error-list-goto-error))


;;;;; flycheck-popup-tip

  (use-package flycheck-popup-tip
    :commands (flycheck-popup-tip-show-popup flycheck-popup-tip-delete-popup)
    :hook (flycheck-mode . +flycheck--hook-init-popups)
    :config
    (setq flycheck-popup-tip-error-prefix "X ")
    (after! evil
      (add-hook! '(evil-insert-state-entry-hook evil-replace-state-entry-hook)
                 #'flycheck-popup-tip-delete-popup)
      (defadvice! +flycheck--advice-disable-popup-tip-maybe (&rest _)
        "Don't display errors while in insert mode, as it can
affect the cursor's position or cause disruptive input delays."
        :before-while #'flycheck-popup-tip-show-popup
        (if evil-local-mode
            (eq evil-state 'normal)
          (not (bound-and-true-p company-backend)))))))


;;
;;; tools

;;;; arch-linux

(module! (:tools arch-linux)

  (use-package pacfiles-mode
    :commands pacfiles
    :config
    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*pacfiles" :ignore t)))

  (use-package pkgbuild-mode
    :mode ("/PKGBUILD$" . pkgbuild-mode)
    :config
    (map! :map pkgbuild-mode-map
          :localleader
          "r" 'pkgbuild-increase-release-tag
          "b" 'pkgbuild-makepkg
          "a" 'pkgbuild-tar
          "u" 'pkgbuild-browse-url
          "m" 'pkgbuild-update-sums-line
          "s" 'pkgbuild-update-srcinfo
          "e" 'pkgbuild-etags)))


;;;; copy-as-format

(module! (:tools copy-as-format)

  (map!
   :leader
   (:prefix ("ct" . "text")
    "f" #'copy-as-format
    "a" #'copy-as-format-asciidoc
    "b" #'copy-as-format-bitbucket
    "d" #'copy-as-format-disqus
    "g" #'copy-as-format-github
    "l" #'copy-as-format-gitlab
    "c" #'copy-as-format-hipchat
    "h" #'copy-as-format-html
    "j" #'copy-as-format-jira
    "m" #'copy-as-format-markdown
    "w" #'copy-as-format-mediawiki
    "o" #'copy-as-format-org-mode
    "p" #'copy-as-format-pod
    "r" #'copy-as-format-rst
    "s" #'copy-as-format-slack)))


;;;; direnv

(module! (:tools direnv)

  (use-package envrc
    :when (executable-find "direnv")
    :hook (+emacs-first-file . envrc-global-mode)
    :config
    (add-to-list '+emacs-debug-variables 'envrc-debug)

    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*envrc\\*" :quit t :ttl 0))

    (defhook! +direnv--hook-init-global-mode-earlier ()
      "A globalized minor mode triggers on
`after-change-major-mode-hook'normally, which runs after a major
mode's body and hooks. If those hooks do any initialization work
that's sensitive to environmental state set up by direnv, then
you're gonna have a bad time, so I move the trigger to
`change-major-mode-after-body-hook' instead. This runs before
said hooks (but not the body; fingers crossed that no major mode
does important env initialization there)."
      'envrc-global-mode-hook
      (let ((fn #'envrc-global-mode-enable-in-buffers))
        (if (not envrc-global-mode)
            (remove-hook 'change-major-mode-after-body-hook fn)
          (remove-hook 'after-change-major-mode-hook fn)
          (add-hook 'change-major-mode-after-body-hook fn 100))))

    (defadvice! +direnv--advice-fail-gracefully (&rest _)
      "Don't try to use direnv if the executable isn't present."
      :before-while #'envrc-mode
      (or (get 'envrc-mode 'direnv-executable)
          (put 'envrc-mode 'direnv-executable (executable-find "direnv" t)))

      ;; Ensure babel's execution environment matches the host buffer's.
      (advice-add #'org-babel-execute-src-block :around #'envrc-propagate-environment)))


  (use-package inheritenv
    :commands inheritenv-add-advice))


;;;; editorconfig

(module! (:tools editorconfig)

  ;; editorconfig cannot procure the correct settings for extension-less files.
  ;; Executable scripts with a shebang line, for example. So why not use Emacs'
  ;; major mode to drop editorconfig a hint? This is accomplished by temporarily
  ;; appending an extension to `buffer-file-name' when we talk to editorconfig.
  (defvar +editorconfig-mode-alist
    '((emacs-lisp-mode . "el")
      (js2-mode        . "js")
      (perl-mode       . "pl")
      (php-mode        . "php")
      (python-mode     . "py")
      (ruby-mode       . "rb")
      (sh-mode         . "sh"))
    "An alist mapping major modes to extensions. Used by
`+editorconfig--advice-smart-detection' to give editorconfig
filetype hints.")


  ;; Handles whitespace (tabs/spaces) settings externally. This way projects can
  ;; specify their own formatting rules.
  (use-package editorconfig
    :hook (+emacs-first-buffer . editorconfig-mode)
    :config
    (when (require 'ws-butler nil t)
      (setq editorconfig-trim-whitespaces-mode 'ws-butler-mode))

    ;; Fix #5057 archives don't need editorconfig settings, and they may
    ;; otherwise interfere with the process of opening them (office formats are
    ;; zipped XML formats).
    (add-to-list 'editorconfig-exclude-regexps
                 "\\.\\(zip\\|\\(doc\\|xls\\|ppt\\)x\\)\\'")

    (defadvice! +editorconfig--advice-smart-detection (orig-fn)
      "Retrieve the properties for the current file. If it
doesn't have an extension, try to guess one."
      :around #'editorconfig-call-editorconfig-exec
      (let ((buffer-file-name
             (if (and (not (bound-and-true-p org-src-mode))
                      (file-name-extension buffer-file-name))
                 buffer-file-name
               (format "%s%s" (buffer-file-name (buffer-base-buffer))
                       (if-let (ext (alist-get major-mode +editorconfig-mode-alist))
                           (concat "." ext)
                         "")))))
        (funcall orig-fn)))

    (add-hook! 'editorconfig-after-apply-functions
      (defun +editorconfig-disable-indent-detection-h (props)
        "Inhibit `dtrt-indent' if an explicit indent_style and
indent_size is specified by editorconfig."
        (when (or (gethash 'indent_style props)
                  (gethash 'indent_size props))
          (setq +emacs-inhibit-indent-detection 'editorconfig))))))


;;;; lsp

(module! (:tools lsp)

  (defvar +lsp-defer-shutdown 3
    "If non-nil, defer shutdown of LSP servers for this many
seconds after last workspace buffer is closed. This delay
prevents premature server shutdown when a user still intends on
working on that project after closing the last buffer, or when
programmatically killing and opening many LSP-powered buffers.")


;;;;; Common

  (defvar +lsp--default-read-process-output-max nil)
  (defvar +lsp--default-gcmh-high-cons-threshold nil)
  (defvar +lsp--optimization-init-p nil)

  (define-minor-mode +lsp-optimization-mode
    "Deploys universal GC and IPC optimizations for `lsp-mode' and
`eglot'."
    :global t
    :init-value nil
    (if (not +lsp-optimization-mode)
        (setq-default read-process-output-max +lsp--default-read-process-output-max
                      gcmh-high-cons-threshold +lsp--default-gcmh-high-cons-threshold
                      +lsp--optimization-init-p nil)
      ;; Only apply these settings once!
      (unless +lsp--optimization-init-p
        (setq +lsp--default-read-process-output-max
              ;; DEPRECATED Remove check when 26 support is dropped
              (if (boundp 'read-process-output-max)
                  (default-value 'read-process-output-max))
              +lsp--default-gcmh-high-cons-threshold
              (default-value 'gcmh-high-cons-threshold))
        ;; `read-process-output-max' is only available on recent development
        ;; builds of Emacs 27 and above.
        (setq-default read-process-output-max (* 1024 1024))
        ;; REVIEW LSP causes a lot of allocations, with or without Emacs 27+'s
        ;;        native JSON library, so we up the GC threshold to stave off
        ;;        GC-induced slowdowns/freezes. We use `gcmh' to enforce its GC
        ;;        strategy, so we modify its variables rather than
        ;;        `gc-cons-threshold' directly.
        (setq-default gcmh-high-cons-threshold (* 2 +lsp--default-gcmh-high-cons-threshold))
        (gcmh-set-high-threshold)
        (setq +lsp--optimization-init-p t))))

;;;;; lsp-mode

  (defvar +lsp-company-backends 'company-capf
    "The backends to prepend to `company-backends' in `lsp-mode' buffers.
Can be a list of backends; accepts any value `company-backends'
accepts.")

  (defvar +lsp-prompt-to-install-server t
    "If non-nil, prompt to install a server if no server is present.
If set to `quiet', suppress the install prompt and don't visibly
inform the user about it (it will be logged to *Messages*
however).")


  (use-package lsp-mode
    :commands lsp-install-server
    :init
    ;; Don't touch ~/.emacs.d
    (setq lsp-session-file (concat +emacs-cache-dir "lsp-session")
          lsp-server-install-dir (concat +emacs-etc-dir "lsp"))
    ;; Don't auto-kill LSP server after last workspace buffer is killed, because
    ;; I will do it for you, after `+lsp-defer-shutdown' seconds.
    (setq lsp-keep-workspace-alive nil)

    ;; NOTE I tweak LSP's defaults in order to make its more expensive or
    ;;      imposing features opt-in. Some servers implement these poorly and,
    ;;      in most cases, it's safer to rely on Emacs' native mechanisms (eldoc
    ;;      vs lsp-ui-doc, open in popup vs sideline, etc).

    ;; Disable features that have great potential to be slow.
    (setq lsp-enable-folding nil
          lsp-enable-text-document-color nil)
    ;; Reduce unexpected modifications to code
    (setq lsp-enable-on-type-formatting nil)
    ;; Make breadcrumbs opt-in; they're redundant with the modeline and imenu
    (setq lsp-headerline-breadcrumb-enable nil)

    ;; We bind the lsp keymap.
    (when (featurep! :config default)
      (setq lsp-keymap-prefix nil))

    :config
    (add-to-list '+emacs-debug-variables 'lsp-log-io)

    (setq lsp-intelephense-storage-path (concat +emacs-etc-dir "lsp-intelephense/")
          lsp-vetur-global-snippets-dir
          (expand-file-name
           "vetur" (or (bound-and-true-p +snippets-dir)
                       (concat +emacs-private-dir "snippets/")))
          lsp-xml-jar-file (expand-file-name "org.eclipse.lsp4xml-0.3.0-uber.jar" lsp-server-install-dir)
          lsp-groovy-server-file (expand-file-name "groovy-language-server-all.jar" lsp-server-install-dir))

    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*lsp-help" :size 0.35 :quit t :select t))
    (when (featurep! :tools lookup)
      (set-lookup-handlers! 'lsp-mode
        :definition #'+lsp-lookup-definition-handler
        :references #'+lsp-lookup-references-handler
        :documentation '(lsp-describe-thing-at-point :async t)
        :implementations '(lsp-find-implementation :async t)
        :type-definition #'lsp-find-type-definition))

    (defadvice! +lsp--advice-respect-user-defined-checkers (orig-fn &rest args)
      "Ensure user-defined `flycheck-checker' isn't overwritten by `lsp'."
      :around #'lsp-diagnostics-flycheck-enable
      (if flycheck-checker
          (let ((old-checker flycheck-checker))
            (apply orig-fn args)
            (setq-local flycheck-checker old-checker))
        (apply orig-fn args)))

    (defhook! +lsp--hook-display-guessed-project-root ()
      "Log what LSP things is the root of the current project."
      'lsp-mode-hook
      ;; Makes it easier to detect root resolution issues.
      (when-let (path (buffer-file-name (buffer-base-buffer)))
        (if-let (root (lsp--calculate-root (lsp-session) path))
            (lsp--info "Guessed project root is %s" (abbreviate-file-name root))
          (lsp--info "Could not guess project root."))))
    (add-hook 'lsp-mode-hook #'+lsp-optimization-mode)

    (when (featurep! :completion company)
      (defhook! +lsp--hook-init-company-backends ()
        "Init `company-backends' in `lsp-mode'."
        'lsp-completion-mode-hook
        (when lsp-completion-mode
          (set (make-local-variable 'company-backends)
               (cons +lsp-company-backends
                     (remove +lsp-company-backends
                             (remq 'company-capf company-backends)))))))

    (defvar +lsp--deferred-shutdown-timer nil)
    (defadvice! +lsp--advice-defer-server-shutdown (orig-fn &optional restart)
      "Defer server shutdown for a few seconds.
This gives the user a chance to open other project files before
the server is auto-killed (which is a potentially expensive
process). It also prevents the server getting expensively
restarted when reverting buffers."
      :around #'lsp--shutdown-workspace
      (if (or lsp-keep-workspace-alive
              restart
              (null +lsp-defer-shutdown)
              (= +lsp-defer-shutdown 0))
          (prog1 (funcall orig-fn restart)
            (+lsp-optimization-mode -1))
        (when (timerp +lsp--deferred-shutdown-timer)
          (cancel-timer +lsp--deferred-shutdown-timer))
        (setq +lsp--deferred-shutdown-timer
              (run-at-time
               (if (numberp +lsp-defer-shutdown) +lsp-defer-shutdown 3)
               nil (lambda (workspace)
                     (with-lsp-workspace workspace
                                         (unless (lsp--workspace-buffers workspace)
                                           (let ((lsp-restart 'ignore))
                                             (funcall orig-fn))
                                           (+lsp-optimization-mode -1))))
               lsp--cur-workspace))))

    (defadvice! +lsp--advice-dont-prompt-to-install-servers-maybe (orig-fn &rest args)
      "Maybe prompt to install lsp servers."
      :around #'lsp
      (when (buffer-file-name)
        (require 'lsp-mode)
        (lsp--require-packages)
        (if (or (lsp--filter-clients
                 (-andfn #'lsp--matching-clients?
                         #'lsp--server-binary-present?))
                (not (memq +lsp-prompt-to-install-server '(nil quiet))))
            (apply orig-fn args)
          ;; HACK `lsp--message' overrides `inhibit-message', so use `quiet!'
          (let ((+emacs-debug-p
                 (or +emacs-debug-p
                     (not (eq +lsp-prompt-to-install-server 'quiet)))))
            (+emacs--advice-shut-up #'lsp--info "No language server available for %S"
                                    major-mode))))))


  (use-package lsp-ui
    :hook (lsp-mode . lsp-ui-mode)
    :init
    (defadvice! +lsp--advice-use-hook-instead (orig-fn &rest args)
      "Change `lsp--auto-configure' to not force `lsp-ui-mode' on
us. Using a hook instead is more sensible."
      :around #'lsp--auto-configure
      (letf! ((#'lsp-ui-mode #'ignore))
        (apply orig-fn args)))
    :config
    (when (featurep! :tools lsp +peek)
      (when (featurep! :tools lookup)
        (set-lookup-handlers! 'lsp-ui-mode :async t
          :definition 'lsp-ui-peek-find-definitions
          :implementations 'lsp-ui-peek-find-implementation
          :references 'lsp-ui-peek-find-references)))

    (setq lsp-ui-peek-enable (featurep! :tools lsp +peek)
          lsp-ui-doc-max-height 8
          lsp-ui-doc-max-width 72
          lsp-ui-doc-delay 0.75
          lsp-ui-doc-show-with-mouse nil  ; don't disappear on mouseover
          lsp-ui-doc-position 'at-point
          lsp-ui-sideline-ignore-duplicate t
          ;; Don't show symbol definitions in the sideline. They are pretty
          ;; noisy, and there is a bug preventing Flycheck errors from being
          ;; shown (the errors flash briefly and then disappear).
          lsp-ui-sideline-show-hover nil
          lsp-ui-sideline-actions-icon lsp-ui-sideline-actions-icon-default)

    (map! :map lsp-ui-peek-mode-map
          "j"   #'lsp-ui-peek--select-next
          "k"   #'lsp-ui-peek--select-prev
          "C-k" #'lsp-ui-peek--select-prev-file
          "C-j" #'lsp-ui-peek--select-next-file))


  (use-package helm-lsp
    :when (featurep! :completion helm)
    :commands helm-lsp-workspace-symbol helm-lsp-global-workspace-symbol)


  (use-package lsp-ivy
    :when (featurep! :completion ivy)
    :commands lsp-ivy-workspace-symbol lsp-ivy-global-workspace-symbol))


;;;; pandoc

(module! (:tools pandoc)

  (use-package ox-pandoc
    :when (featurep! :org org)
    :when (executable-find "pandoc")
    :after ox
    :init
    (add-to-list 'org-export-backends 'pandoc)
    (setq org-pandoc-options
          '((standalone . t)
            (mathjax . t)
            (variable . "revealjs-url=https://revealjs.com"))))

  (use-package pandoc-mode
    :defer t
    :config
    (setq pandoc-data-dir (concat +emacs-cache-dir "pandoc/"))
    (add-hook 'pandoc-mode-hook 'pandoc-load-default-settings)))


;;;; pdf

(module! (:tools pdf) (load! "+tools-pdf"))


;;
;;; org

;;;; org

(after! org
  ;; Custom org modules
  (module! (:org org +dragndrop) (load! "+org-dragndrop"))
  (module! (:org org +jupyter)   (load! "+org-jupyter"))
  (module! (:org org +pomodoro)  (load! "+org-pomodoro"))
  (module! (:org org +present)   (load! "+org-present"))
  (module! (:org org +org-ql)    (load! "+org-ql"))
  (module! (:org org +ref)       (load! "+org-ref")))


;;
;;; lang

;;;; common-lisp

(module! (:lang common-lisp)
  ;; Pretend `lisp-mode' is not loaded
  (defer-feature! lisp-mode)

;;;###package lisp-mode
  (defvar inferior-lisp-program "sbcl")
  (add-hook! 'lisp-mode-hook
             #'rainbow-delimiters-mode
             #'highlight-quoted-mode)


;;;;; sly

  (use-package sly
    :hook (lisp-mode-local-vars . sly-editing-mode)
    :init
    ;; This hook was moved to `lisp-mode-local-vars', so it only affects
    ;; `lisp-mode'.
    (after! (:or emacs sly)
      (remove-hook 'lisp-mode-hook #'sly-editing-mode))

    (after! lisp-mode
      (when (featurep! :tools lookup)
        (set-lookup-handlers! 'lisp-mode
          :definition #'sly-edit-definition
          :documentation #'sly-describe-symbol))
      (when (featurep! :tools eval)
        (set-eval-handler! 'lisp-mode #'sly-eval-region)
        (set-repl-handler! 'lisp-mode #'sly-mrepl))
      (when (featurep! :editor cleverlispy)
        (pushnew! +cleverlispy-lispyville-modes 'lisp-mode)
        (add-hook 'lisp-mode-hook #'+cleverlispy--hook-enable)))

    ;; HACK Ensures that sly's contrib modules are loaded as soon as possible, but also as late as
    ;;      possible, so users have an opportunity to override `sly-contrib' in an `after!' block.
    (add-hook! '+emacs-after-init-modules-hook
      (after! sly (sly-setup)))

    :config
    (when (featurep! :ui pretty-code)
      (set-pretty-symbols! 'lisp-mode :lambda "lambda"))
    (when (featurep! :editor rotate-text)
      (set-rotate-patterns! 'lisp-mode
        :symbols '(("t" "nil"))))

    ;; Special fontification
    (font-lock-add-keywords
     'lisp-mode
     (append ;; highlight defined, special variables & functions
      `((+common-lisp-highlight-vars-and-faces . +common-lisp--face))))

    (setq sly-mrepl-history-file-name (concat +emacs-cache-dir "sly-mrepl-history")
          sly-kill-without-query-p t
          sly-net-coding-system 'utf-8-unix
          ;; Change this to `sly-flex-completions' for fuzzy completion
          sly-complete-symbol-function 'sly-simple-completions)

    (when (featurep! :ui popup)
      (set-popup-rules!
        '(("^\\*sly-mrepl"       :vslot 2 :size 0.3 :quit nil :ttl nil)
          ("^\\*sly-compilation" :vslot 3 :ttl nil)
          ("^\\*sly-traces"      :vslot 4 :ttl nil)
          ("^\\*sly-description" :vslot 5 :size 0.3 :ttl 0)
          ;; Do not display debugger or inspector buffers in a popup window. These buffers are meant
          ;; to be displayed with sufficient vertical space.
          ("^\\*sly-\\(?:db\\|inspector\\)" :ignore t))))

    (sp-with-modes '(sly-mrepl-mode)
      (sp-local-pair "'" "'" :actions nil)
      (sp-local-pair "`" "`" :actions nil))

    (defun +common-lisp--hook-cleanup-sly-maybe ()
      "Kill processes and leftover buffers when killing the last
sly buffer."
      (unless (cl-loop for buf in (delq (current-buffer) (buffer-list))
                       if (and (buffer-local-value 'sly-mode buf)
                               (get-buffer-window buf))
                       return t)
        (dolist (conn (sly--purge-connections))
          (sly-quit-lisp-internal conn 'sly-quit-sentinel t))
        (let (kill-buffer-hook kill-buffer-query-functions)
          (mapc #'kill-buffer
                (cl-loop for buf in (delq (current-buffer) (buffer-list))
                         if (buffer-local-value 'sly-mode buf)
                         collect buf)))))

    (defhook! +common-lisp--hook-init-sly ()
      "Attempt to auto-start sly when opening a lisp buffer."
      'sly-mode-hook
      (cond ((or (+emacs-temp-buffer-p (current-buffer))
                 (sly-connected-p)))
            ((executable-find (car (split-string inferior-lisp-program)))
             (let ((sly-auto-start 'always))
               (sly-auto-start)
               (add-hook 'kill-buffer-hook #'+common-lisp--hook-cleanup-sly-maybe nil t)))
            ((message "WARNING: Couldn't find `inferior-lisp-program' (%s)"
                      inferior-lisp-program))))

    (map! (:map sly-db-mode-map
           :n "gr" #'sly-db-restart-frame)
          (:map sly-inspector-mode-map
           :n "gb" #'sly-inspector-pop
           :n "gr" #'sly-inspector-reinspect
           :n "gR" #'sly-inspector-fetch-all
           :n "K"  #'sly-inspector-describe-inspectee)
          (:map sly-xref-mode-map
           :n "gr" #'sly-recompile-xref
           :n "gR" #'sly-recompile-all-xrefs)
          (:map lisp-mode-map
           :n "gb" #'sly-pop-find-definition-stack)

          (:localleader
           :map lisp-mode-map
           :desc "Sly"          "'" #'sly
           :desc "Sly (ask)"    ";" (cmd!! #'sly '-)
           :desc "Expand macro"          "m" #'macrostep-expand
           (:prefix ("c" . "compile")
            :desc "Compile file"          "c" #'sly-compile-file
            :desc "Compile/load file"     "C" #'sly-compile-and-load-file
            :desc "Compile toplevel form" "f" #'sly-compile-defun
            :desc "Load file"             "l" #'sly-load-file
            :desc "Remove notes"          "n" #'sly-remove-notes
            :desc "Compile region"        "r" #'sly-compile-region)
           (:prefix ("e" . "evaluate")
            :desc "Evaulate buffer"     "b" #'sly-eval-buffer
            :desc "Evaluate last"       "e" #'sly-eval-last-expression
            :desc "Evaluate/print last" "E" #'sly-eval-print-last-expression
            :desc "Evaluate defun"      "f" #'sly-eval-defun
            :desc "Undefine function"   "F" #'sly-undefine-function
            :desc "Evaluate region"     "r" #'sly-eval-region)
           (:prefix ("g" . "goto")
            :desc "Go back"              "b" #'sly-pop-find-definition-stack
            :desc "Go to"                "d" #'sly-edit-definition
            :desc "Go to (other window)" "D" #'sly-edit-definition-other-window
            :desc "Next note"            "n" #'sly-next-note
            :desc "Previous note"        "N" #'sly-previous-note
            :desc "Next sticker"         "s" #'sly-stickers-next-sticker
            :desc "Previous sticker"     "S" #'sly-stickers-prev-sticker)
           (:prefix ("h" . "help")
            :desc "Who calls"               "<" #'sly-who-calls
            :desc "Calls who"               ">" #'sly-calls-who
            :desc "Lookup format directive" "~" #'hyperspec-lookup-format
            :desc "Lookup reader macro"     "#" #'hyperspec-lookup-reader-macro
            :desc "Apropos"                 "a" #'sly-apropos
            :desc "Who binds"               "b" #'sly-who-binds
            :desc "Disassemble symbol"      "d" #'sly-disassemble-symbol
            :desc "Describe symbol"         "h" #'sly-describe-symbol
            :desc "HyperSpec lookup"        "H" #'sly-hyperspec-lookup
            :desc "Who macro-expands"       "m" #'sly-who-macroexpands
            :desc "Apropos package"         "p" #'sly-apropos-package
            :desc "Who references"          "r" #'sly-who-references
            :desc "Who specializes"         "s" #'sly-who-specializes
            :desc "Who sets"                "S" #'sly-who-sets)
           (:prefix ("r" . "repl")
            :desc "Clear REPL"         "c" #'sly-mrepl-clear-repl
            :desc "Quit connection"    "q" #'sly-quit-lisp
            :desc "Restart connection" "r" #'sly-restart-inferior-lisp
            :desc "Sync REPL"          "s" #'sly-mrepl-sync)
           (:prefix ("s" . "stickers")
            :desc "Toggle breaking stickers" "b" #'sly-stickers-toggle-break-on-stickers
            :desc "Clear defun stickers"     "c" #'sly-stickers-clear-defun-stickers
            :desc "Clear buffer stickers"    "C" #'sly-stickers-clear-buffer-stickers
            :desc "Fetch stickers"           "f" #'sly-stickers-fetch
            :desc "Replay stickers"          "r" #'sly-stickers-replay
            :desc "Add/remove sticker"       "s" #'sly-stickers-dwim)
           (:prefix ("t" . "trace")
            :desc "Toggle"         "t" #'sly-toggle-trace-fdefinition
            :desc "Toggle (fancy)" "T" #'sly-toggle-fancy-trace
            :desc "Untrace all"    "u" #'sly-untrace-all)))

    (when (featurep! :editor evil)
      (add-hook 'sly-mode-hook #'evil-normalize-keymaps)))


;;;;; sly-repl-ansi-color

  (use-package sly-repl-ansi-color
    :defer t
    :init
    (add-to-list 'sly-contribs 'sly-repl-ansi-color)))


;;;; data

(module! (:lang data)

  (add-to-list 'auto-mode-alist '("/sxhkdrc\\'" . conf-mode))
  (add-to-list 'auto-mode-alist '("\\.\\(?:hex\\|nes\\)\\'" . hexl-mode))

  (use-package nxml-mode
    :mode "\\.p\\(?:list\\|om\\)\\'" ; plist, pom
    :mode "\\.xs\\(?:d\\|lt\\)\\'"   ; xslt, xsd
    :mode "\\.rss\\'"
    :config
    (setq nxml-slash-auto-complete-flag t
          nxml-auto-insert-xml-declaration-flag t)
    (when (featurep! :completion company)
      (set-company-backend! 'nxml-mode :company '(company-nxml)))
    (setq-hook! 'nxml-mode-hook tab-width nxml-child-indent))


;;;;; csv-mode

;;;###package csv-mode
  (map! :after csv-mode
        :localleader
        :map csv-mode-map
        "a" #'csv-align-fields
        "u" #'csv-unalign-fields
        "s" #'csv-sort-fields
        "S" #'csv-sort-numeric-fields
        "k" #'csv-kill-fields
        "t" #'csv-transpose)


;;;;; json-mode

  (use-package json-mode
    :mode "\\.js\\(?:on\\|[hl]int\\(?:rc\\)?\\)\\'"
    :config
    (when (featurep! :emacs electric)
      (set-electric! 'json-mode :chars '(?\n ?: ?{ ?}))))


;;;;; yaml

  (use-package yaml-mode
    :mode "\\.yml\\'"
    :config
    (setq yaml-indent-offset 4)
    (add-hook! 'yaml-mode-hook (flyspell-mode -1))))


;;;; ess

(module! (:lang ess)

  (after! projectile
    (add-to-list 'projectile-project-root-files "DESCRIPTION"))

  (defvar +ess-R-remote-host nil
    "Host to connect to, given as a string.
Should be set in a local variable.")

  (defvar +ess-R-remote-session nil
    "Name of the R session to be created, given as a string.
Should be set in a local variable.")

  (defvar +ess-R-remote-cmds nil
    "Commands to be executed on the host before R is launched,
given as a list of strings. Should be set in a local
variable.")

  (use-package ess
    :commands stata SAS
    :config
    (setq ess-offset-continued 'straight
          ess-use-flymake (not (featurep! :checkers syntax))
          ess-nuke-trailing-whitespace-p t
          ess-style 'DEFAULT
          ess-history-directory (expand-file-name "ess-history/" +emacs-cache-dir))
    (unless (and (file-directory-p ess-history-directory)
                 (file-exists-p (concat ess-history-directory ".Rhistory")))
      (make-empty-file (concat ess-history-directory ".Rhistory") t))

    ;; Set fontification
    ;; ESS buffer
    (setq ess-R-font-lock-keywords
          '((ess-R-fl-keyword:keywords   . t)
            (ess-R-fl-keyword:constants  . t)
            (ess-R-fl-keyword:modifiers  . t)
            (ess-R-fl-keyword:fun-defs   . t)
            (ess-R-fl-keyword:assign-ops . t)
            (ess-R-fl-keyword:%op%       . t)
            (ess-fl-keyword:fun-calls    . t)
            (ess-fl-keyword:numbers      . t)
            (ess-fl-keyword:operators    . t)
            (ess-fl-keyword:delimiters   . t)
            (ess-fl-keyword:=            . t)
            (ess-R-fl-keyword:F&T        . t)))

    ;; iESS buffer
    (setq inferior-R-font-lock-keywords
          '((ess-S-fl-keyword:prompt      . t)
            (ess-R-fl-keyword:keywords    . t)
            (ess-R-fl-keyword:constants   . t)
            (ess-R-fl-keyword:modifiers   . t)
            (ess-R-fl-keyword:messages    . t)
            (ess-R-fl-keyword:fun-defs    . t)
            (ess-R-fl-keyword:assign-ops  . t)
            (ess-fl-keyword:matrix-labels . t)
            (ess-fl-keyword:fun-calls     . t)
            (ess-fl-keyword:numbers       . t)
            (ess-fl-keyword:operators     . t)
            (ess-fl-keyword:delimiters    . t)
            (ess-fl-keyword:=             . t)
            (ess-R-fl-keyword:F&T         . t)))

    (when (featurep! :tools lookup)
      (set-docsets! 'ess-r-mode :docsets "R")
      (set-lookup-handlers! '(ess-r-mode ess-julia-mode)
        :documentation #'ess-display-help-on-object))
    (when (featurep! :tools eval)
      (set-repl-handler! 'ess-r-mode #'+ess/open-r-repl)
      (set-repl-handler! 'ess-julia-mode #'+ess/open-julia-repl)
      (set-eval-handler! 'ess-help-mode #'ess-eval-region-and-go)
      (set-eval-handler! 'ess-r-help-mode #'ess-eval-region-and-go))

    (when (featurep! :editor evil)
      (set-evil-initial-state! 'ess-r-help-mode 'normal))

    ;; LSP
    (when (and (featurep! :tools lsp)
               (featurep! :lang ess +lsp))
      (when (featurep! :completion company)
        (setq-hook! 'ess-r-mode-local-vars-hook
          +lsp-company-backends '(:separate company-files company-capf)))
      (defhook! +ess--hook-lsp-init-maybe ()
        "Use LSP mode if the buffer is not a remote."
        'ess-r-mode-local-vars-hook
        (unless (file-remote-p default-directory)
          (lsp!))))

    ;; Use smartparens in iESS
    (add-hook! 'inferior-ess-mode-hook #'smartparens-mode)

    ;; Set company backends
    (when (featurep! :completion company)
      (set-company-backend! 'ess-r-mode :company '((:separate company-R-library
                                                    company-R-args
                                                    company-R-objects
                                                    company-files
                                                    company-dict
                                                    company-dabbrev-code
                                                    company-capf)))
      (set-company-backend! 'inferior-ess-r-mode :company '((:separate company-R-library
                                                             company-R-args
                                                             company-R-objects
                                                             company-files
                                                             company-dict
                                                             company-dabbrev-code
                                                             company-capf))))

    ;; Set insert state in iEES
    (when (featurep! :editor evil)
      (after! ess-r-mode
        (set-evil-initial-state! 'inferior-ess-mode 'insert)))

    (defhook! +ess--hook-fix-read-only-inferior-ess-mode ()
      "Fixes a bug when `comint-prompt-read-only' in non-nil.
See https://github.com/emacs-ess/ESS/issues/300"
      'inferior-ess-mode-hook
      (setq-local comint-use-prompt-regexp nil)
      (setq-local inhibit-field-text-motion nil))

    ;; Popup rules
    (when (featurep! :ui popup)
      (after! ess-r-mode
        (set-popup-rule! "^\\*R" :ignore t)))

    (when (featurep! :ui popup)
      (after! ess-help
        (set-popup-rule! "^\\*help.R.*" :slot 2 :side 'right :size 80 :height 0.4
          :select t :quit t :transient t)))

    (defhook! +ess-inferior-buffer-p (buf)
      "Returns non-nil if BUF is a `inferior-ess' buffer."
      '+emacs-real-buffer-functions
      (with-current-buffer buf (derived-mode-p 'inferior-ess-mode)))

    ;; Save history when killing the ess inferior buffer
    (defun +ess--hook-kill-proc-before-buffer ()
      (let ((proc (get-buffer-process (current-buffer))))
        (when (processp proc)
          (and (derived-mode-p 'comint-mode)
               (comint-write-input-ring))
          (delete-process proc))))
    (defun +ess--hook-comint ()
      (add-hook! '(kill-buffer-hook kill-emacs-hook) #'+ess--hook-kill-proc-before-buffer))
    (add-hook 'comint-mode-hook #'+ess--hook-comint)

    ;; Keybinds
    ;; REPL
    (map!
     :map inferior-ess-mode-map
     :i "C->" (cmd! (insert " %>% "))
     :i "M--" #'ess-cycle-assign
     :n "RET" #'+ess/goto-end-of-prompt)

    (map!
     :map ess-mode-map
     :i "M--" #'ess-cycle-assign
     :i "C->" (cmd! (insert " %>% ")))
    (map!
     (:after ess-help
      (:map ess-help-mode-map
       :n "q"  #'kill-current-buffer
       :n "Q"  #'ess-kill-buffer-and-go
       :n "K"  #'ess-display-help-on-object
       :n "go" #'ess-display-help-in-browser
       :n "gO" #'ess-display-help-apropos
       :n "gv" #'ess-display-vignettes
       :m "]]" #'ess-skip-to-next-section
       :m "[[" #'ess-skip-to-previous-section)
      (:map ess-doc-map
       "h"    #'ess-display-help-on-object
       "p"    #'ess-R-dv-pprint
       "t"    #'ess-R-dv-ctable
       [up]   #'comint-next-input
       [down] #'comint-previous-input
       [C-return] #'ess-eval-line)))

    (map!
     :map ess-mode-map
     :n [C-return] #'ess-eval-line
     :localleader
     "," #'ess-eval-region-or-function-or-paragraph-and-step
     "'" #'R
     "s" #'ess-switch-to-inferior-or-script-buffer
     "S" #'ess-switch-process
     "B" #'ess-eval-buffer-and-go
     "b" #'ess-eval-buffer
     "d" #'ess-eval-region-or-line-and-step
     "D" #'ess-eval-function-or-paragraph-and-step
     "L" #'ess-eval-line-and-go
     "l" #'ess-eval-line
     "R" #'ess-eval-region-and-go
     "r" #'ess-eval-region
     "F" #'ess-eval-function-and-go
     "f" #'ess-eval-function
     ;; predefined keymaps
     "h" #'ess-doc-map
     "x" #'ess-extra-map
     "p" #'ess-r-package-dev-map
     "v" #'ess-dev-map
     ;; noweb
     (:prefix ("c" . "noweb")
      "C" #'ess-eval-chunk-and-go
      "c" #'ess-eval-chunk
      "d" #'ess-eval-chunk-and-step
      "m" #'ess-noweb-mark-chunk
      "N" #'ess-noweb-previous-chunk
      "n" #'ess-noweb-next-chunk)))

;;;;; ess-R-data-view

  (use-package ess-R-data-view
    :commands
    (ess-R-dv-ctable
     ess-R-dv-pprint)
    :config
    (map!
     :map ess-doc-map
     "h" #'ess-display-help-on-object
     "p" #'ess-R-dv-pprint
     "t" #'ess-R-dv-ctable))


;;;;; polymode

  (use-package poly-R
    :mode ("\\.[rR]md$" . poly-markdown+r-mode)
    :config
    (when (featurep! :editor snippets)
      (set-yas-minor-mode! 'poly-markdown+r-mode))

    ;;     (defhook! +poly-r--hook-mardown-code-face ()
    ;;       "Fix unreadable highlighting of code blocks in
    ;; `poly-markdown+r-mode' buffers"
    ;;       #'poly-markdown+r-mode
    ;;       (face-remap-add-relative 'markdown-code-face '(:background nil)))

    (defvar +poly-r-template-dir (concat +file-templates-dir "Rmd-templates"))
    (setq poly-r-rmarkdown-template-dirs (list +poly-r-template-dir))))


;;;; latex

(module! (:lang latex)

  (defvar +latex-build-command (if (featurep! :lang latex +latexmk) "LatexMk" "LaTeX")
    "The default command to use with `SPC m b'")

  (defvar +latex-nofill-env '("equation"
                              "equation*"
                              "align"
                              "align*"
                              "tabular"
                              "tikzpicture")
    "List of environment names in which `auto-fill-mode' will be
inhibited.")

  (defconst +latex-indent-item-continuation-offset 'align
    "Level to indent continuation of enumeration-type environments.
i.e. This affects \\item, \\enumerate, and \\description.

Set this to `align' for:

  \\item lines aligned
         like this.

Set to `auto' for continuation lines to be offset by
`LaTeX-indent-line':

  \\item lines aligned
    like this, assuming LaTeX-indent-line == 2

Any other fixed integer will be added to `LaTeX-item-indent' and
the current indentation level.

Set this to `nil' to disable all this behavior.

You'll need to adjust `LaTeX-item-indent' to control indentation
of \\item itself.")

  (defvar +latex-viewers '(pdf-tools)
    "A list of enabled latex viewers to use, in this order. If they don't exist,
they will be ignored. Recognized viewers are skim, evince,
sumatrapdf, zathura, okular and pdf-tools. If no viewers are
found, `latex-preview-pane' is used.")


;;;;; auctex

  (add-to-list 'auto-mode-alist '("\\.tex\\'" . LaTeX-mode))

  (setq TeX-command-default +latex-build-command
        TeX-auto-save t
        TeX-parse-self t
        ;; use hidden dirs for auctex files
        TeX-auto-local ".auctex-auto"
        TeX-style-local ".auctex-style"
        TeX-source-correlate-mode t
        TeX-source-correlate-method 'synctex
        ;; don't start the emacs server when correlating sources
        TeX-source-correlate-start-server nil
        ;; automatically insert braces after sub/superscript in math mode
        TeX-electric-sub-and-superscript t)

  (after! tex
    ;; fontify common latex commands
    (load! "+lang-latex-fontification")
    ;; select viewer
    (load! "+lang-latex-viewers")
    ;; prompt for master
    (setq-default TeX-master t)
    ;; set-up chktex
    (setcar (cdr (assoc "Check" TeX-command-list)) "chktex -v6 -H %s")
    (setq-hook! 'TeX-mode-hook
      ;; tell emacs how to parse tex files
      ispell-parser 'tex
      ;; Don't auto-fill in math blocks
      fill-nobreak-predicate (cons #'texmathp fill-nobreak-predicate))
    ;; Enable rainbow mode after applying styles to the buffer
    (add-hook 'TeX-update-style-hook #'rainbow-delimiters-mode)
    ;; display output of latex commands in popup
    (when (featurep! :ui popup)
      (set-popup-rule! " output\\*$" :size 15))
    (after! smartparens-latex
      (let ((modes '(tex-mode plain-tex-mode latex-mode LaTeX-mode)))
        ;; All these excess pairs dramatically slow down typing in latex
        ;; buffers, so we remove them. Let snippets do their job.
        (dolist (open '("\\left(" "\\left[" "\\left\\{" "\\left|"
                        "\\bigl(" "\\biggl(" "\\Bigl(" "\\Biggl(" "\\bigl["
                        "\\biggl[" "\\Bigl[" "\\Biggl[" "\\bigl\\{" "\\biggl\\{"
                        "\\Bigl\\{" "\\Biggl\\{"
                        "\\lfloor" "\\lceil" "\\langle"
                        "\\lVert" "\\lvert" "`"))
          (sp-local-pair modes open nil :actions :rem))
        ;; And tweak these so that users can decide whether they want use latex
        ;; quotes or not, via `+latex-enable-plain-double-quotes'
        (sp-local-pair modes "``" nil :unless '(:add sp-in-math-p)))))

  (use-package tex-fold
    :when (featurep! :lang latex +fold)
    :hook (TeX-mode . +latex--hook-TeX-fold-buffer)
    :hook (TeX-mode . TeX-fold-mode)
    :config
    (defun +latex--hook-TeX-fold-buffer ()
      (run-with-idle-timer 0 nil 'TeX-fold-buffer))
    ;; Fold after all auctex macro insertions
    (advice-add #'TeX-insert-macro :after #'+latex--advice-fold-last-macro)
    ;; Fold after cdlatex macro insertions
    (advice-add #'cdlatex-math-symbol :after #'+latex--advice-fold-last-macro)
    (advice-add #'cdlatex-math-modify :after #'+latex--advice-fold-last-macro)
    ;; Fold after snippets
    (when (featurep! :editor snippets)
      (defhook! +latex--hook-fold-snippet-contents ()
        "Set a local after-snippet-hook to fold the snippet
contents."
        'TeX-fold-mode-hook
        (add-hook! 'yas-after-exit-snippet-hook :local
          (when (and yas-snippet-beg yas-snippet-end)
            (TeX-fold-region yas-snippet-beg yas-snippet-end)))))

    (defhook! +latex--hook-fold-set-variable-pitch ()
      "Fix folded things invariably getting fixed pitch when
using mixed-pitch. Math faces should stay fixed by the
mixed-pitch blacklist, this is mostly for \\section etc."
      'mixed-pitch-mode-hook
      (when mixed-pitch-mode
        ;; Adding to this list makes mixed-pitch clean the face remaps after us
        (add-to-list 'mixed-pitch-fixed-cookie
                     (face-remap-add-relative
                      'TeX-fold-folded-face
                      :family (face-attribute 'variable-pitch :family)
                      :height (face-attribute 'variable-pitch :height)))))

    (map! :map (tex-mode-map latex-mode-map LaTeX-mode-map TeX-fold-mode-map)
          :localleader
          (:prefix ("z" . "fold")
           "=" #'TeX-fold-math
           "b" #'TeX-fold-buffer
           "B" #'TeX-fold-clearout-buffer
           "e" #'TeX-fold-env
           "I" #'TeX-fold-clearout-item
           "m" #'TeX-fold-macro
           "p" #'TeX-fold-paragraph
           "P" #'TeX-fold-clearout-paragraph
           "r" #'TeX-fold-region
           "R" #'TeX-fold-clearout-region
           "z" #'TeX-fold-dwim)))

  ;; LaTeX-mode
  (after! latex
    (setq LaTeX-section-hook        ; Add the toc entry to the sectioning hooks.
          '(LaTeX-section-heading
            LaTeX-section-title
            LaTeX-section-toc
            LaTeX-section-section
            LaTeX-section-label)
          ;; Don't insert line-break at inline math
          LaTeX-fill-break-at-separators nil
          LaTeX-indent-level 4
          LaTeX-item-indent 0)

    (add-hook 'LaTeX-mode-hook #'+latex/auto-fill-mode)

    ;; Set company backends
    (when (featurep! :completion company)
      (set-company-backend! 'latex-mode :company '((company-math-symbols-latex company-auctex-macros company-auctex-environments))))
    ;; Provide proper indentation for LaTeX "itemize","enumerate", and
    ;; "description" environments.
    ;; See
    ;; http://emacs.stackexchange.com/questions/3083/how-to-indent-items-in-latex-auctex-itemize-environments
    (dolist (env '("itemize" "enumerate" "description"))
      (add-to-list 'LaTeX-indent-environment-list `(,env +latex-indent-item-fn)))

    (defadvice! +latex--advice-re-indent-itemize-and-enumerate (orig-fn &rest args)
      "Allow fill-paragraph in itemize/enumerate."
      :around #'LaTeX-fill-region-as-para-do
      (let ((LaTeX-indent-environment-list
             (append LaTeX-indent-environment-list
                     '(("itemize"   +latex-indent-item-fn)
                       ("enumerate" +latex-indent-item-fn)))))
        (apply orig-fn args)))

    (defadvice! +latex--advice-dont-indent-itemize-and-enumerate (orig-fn &rest args)
      "Allow fill-paragraph in itemize/enumerate."
      :around #'LaTeX-fill-region-as-paragraph
      (let ((LaTeX-indent-environment-list LaTeX-indent-environment-list))
        (delq! "itemize" LaTeX-indent-environment-list 'assoc)
        (delq! "enumerate" LaTeX-indent-environment-list 'assoc)
        (apply orig-fn args)))

    ;; Keybindings
    (map!
     :localleader
     :map (tex-mode-map latex-mode-map LaTeX-mode-map)
     "\\" #'TeX-insert-macro                   ;; C-c C-m
     "-"  #'TeX-recenter-output-buffer         ;; C-c C-l
     "%"  #'TeX-comment-or-uncomment-paragraph ;; C-c %
     ";"  #'TeX-comment-or-uncomment-region    ;; C-c ; or C-c :
     ;; TeX-command-run-all runs compile and open the viewer
     "k" #'TeX-kill-job               ;; C-c C-k
     "l" #'TeX-recenter-output-buffer ;; C-c C-l
     "m" #'TeX-insert-macro           ;; C-c C-m
     "v" #'TeX-view                   ;; C-c C-v
     ;; TeX-doc is a very slow function
     (:prefix ("h" . "help")
      "d" #'TeX-doc)
     ;; latex typography settings
     (:prefix ("x" . "text")
      "b" #'+latex/font-bold
      "c" #'+latex/font-code
      "e" #'+latex/font-emphasis
      "i" #'+latex/font-italic
      "r" #'+latex/font-clear
      "o" #'+latex/font-oblique
      "B" #'+latex/font-medium
      "r" #'+latex/font-clear
      (:prefix ("f" . "font type")
       "c" #'+latex/font-small-caps
       "f" #'+latex/font-sans-serif
       "r" #'+latex/font-serif
       "a" #'+latex/font-calligraphic
       "n" #'+latex/font-normal
       "u" #'+latex/font-upright))
     ;; build document via spacemacs function
     ;; TeX-command-run-all runs compile and open the viewer
     "a" #'TeX-command-run-all ;; C-c C-a
     "b" #'+latex/build
     ;; reftex bindings
     (:prefix ("r" . "reftex")
      "c"   #'reftex-citation
      "g"   #'reftex-grep-document
      "i"   #'reftex-index-selection-or-word
      "I"   #'reftex-display-index
      "TAB" #'reftex-index
      "l"   #'reftex-label
      "p"   #'reftex-index-phrase-selection-or-word
      "P"   #'reftex-index-visit-phrases-buffer
      "r"   #'reftex-reference
      "s"   #'reftex-search-document
      "t"   #'reftex-toc
      "T"   #'reftex-toc-recenter
      "v"   #'reftex-view-crossref)
     ;; Key bindings specific to LaTeX
     "*" #'LaTeX-mark-section      ;; C-c *
     "." #'LaTeX-mark-environment  ;; C-c .
     "c" #'LaTeX-close-environment ;; C-c ]
     "e" #'LaTeX-environment       ;; C-c C-e
     (:prefix ("i" . "insert")
      "i" #'LaTeX-insert-item ;; C-c C-j
      "s" #'LaTeX-section)    ;; C-c C-s
     (:prefix ("f" . "fill")
      "e" #'LaTeX-fill-environment ;; C-c C-q C-e
      "p" #'LaTeX-fill-paragraph   ;; C-c C-q C-p
      "r" #'LaTeX-fill-region      ;; C-c C-q C-r
      "s" #'LaTeX-fill-section)    ;; C-c C-q C-s
     (:prefix ("p" . "preview")
      "b" #'preview-buffer
      "c" #'preview-clearout
      "d" #'preview-document
      "e" #'preview-environment
      "f" #'preview-cache-preamble
      "p" #'preview-at-point
      "r" #'preview-region
      "s" #'preview-section)))


;;;;; preview

  (use-package preview
    :hook (LaTeX-mode . LaTeX-preview-setup)
    :config
    (setq-default preview-scale 1.4
                  preview-scale-function
                  (lambda () (* (/ 10.0 (preview-document-pt)) preview-scale))))


;;;;; auctex-latexmk

  (use-package auctex-latexmk
    :when (featurep! :lang latex +latexmk)
    :after latex
    :init
    ;; Pass the -pdf flag when TeX-PDF-mode is active
    (setq auctex-latexmk-inherit-TeX-PDF-mode t)
    :config
    ;; Add latexmk as a TeX target
    (auctex-latexmk-setup))


;;;;; company-auctex

  (use-package company-auctex
    :when (featurep! :completion company)
    :defer t)


;;;;; company-math

  (use-package company-math
    :when (featurep! :completion company)
    :defer t)


;;;;; company-reftex

  (use-package company-reftex
    :when (featurep! :completion company)
    :defer t
    :init
    (setq company-reftex-max-annotation-length 100))


;;;;; reftex

  (use-package reftex
    :hook (LaTeX-mode . reftex-mode)
    :config
    (when (featurep! :completion company)
      (set-company-backend! 'reftex-mode :company '(company-reftex-labels company-reftex-citations)))
    ;; Get ReTeX working with biblatex
    ;; http://tex.stackexchange.com/questions/31966/setting-up-reftex-with-biblatex-citation-commands/31992#31992
    (setq reftex-cite-format
          '((?a . "\\autocite[]{%l}")
            (?b . "\\blockcquote[]{%l}{}")
            (?c . "\\cite[]{%l}")
            (?f . "\\footcite[]{%l}")
            (?n . "\\nocite{%l}")
            (?p . "\\parencite[]{%l}")
            (?s . "\\smartcite[]{%l}")
            (?t . "\\textcite[]{%l}"))
          reftex-plug-into-AUCTeX t
          reftex-toc-split-windows-fraction 0.3)
    (when (featurep! :editor evil)
      (add-hook 'reftex-mode-hook #'evil-normalize-keymaps))
    (map! :map reftex-mode-map
          :localleader
          ";" 'reftex-toc)
    (add-hook! 'reftex-toc-mode-hook
      (reftex-toc-rescan)
      (map! :map 'local
            :e "j"   #'next-line
            :e "k"   #'previous-line
            :e "q"   #'kill-buffer-and-window
            :e "ESC" #'kill-buffer-and-window)))

  ;; set up mode for bib files
  (after! bibtex
    (setq bibtex-dialect 'biblatex
          bibtex-align-at-equal-sign t
          bibtex-text-indentation 20)
    (define-key bibtex-mode-map (kbd "C-c \\") #'bibtex-fill-entry)))


;;;; markdown

(module! (:lang markdown)

  (defvar +markdown-compile-functions
    '(+markdown-compile-marked
      +markdown-compile-pandoc
      +markdown-compile-markdown
      +markdown-compile-multimarkdown)
    "A list of commands to try when attempting to build a
markdown file with `markdown-open' or `markdown-preview',
stopping at the first one to return non-nil. Each function takes
three argument. The beginning position of the region to capture,
the end position, and the output buffer.")

  (use-package markdown-mode
    :mode ("/README\\(?:\\.\\(?:markdown\\|md\\)\\)?\\'" . gfm-mode)
    :init
    (setq markdown-enable-wiki-links t
          markdown-italic-underscore t
          markdown-asymmetric-header t
          markdown-make-gfm-checkboxes-buttons t
          markdown-gfm-additional-languages '("sh")
          markdown-hide-urls nil ; trigger with `markdown-toggle-url-hiding'
          markdown-enable-math t ; syntax highlighting for latex fragments
          markdown-gfm-uppercase-checkbox t ; for compat with org-mode
          ;; Preview/compilation defaults
          markdown-command #'+markdown-compile
          markdown-open-command
          (cond (IS-MAC "open")
                (IS-LINUX "xdg-open")))
    :config
    (when (featurep! :checkers spelling)
      (set-flyspell-predicate! '(markdown-mode gfm-mode)
        #'+markdown-flyspell-word-p))

    (setq-hook! 'markdown-mode-hook
      fill-nobreak-predicate (cons #'markdown-code-block-at-point-p
                                   fill-nobreak-predicate))

    (defadvice! +markdown--advice-disable-front-matter-fontification (&rest _)
      "HACK Prevent mis-fontification of YAML metadata blocks in
`markdown-mode' which occurs when the first line contains a colon
in it. See https://github.com/jrblevin/markdown-mode/issues/328."
      :override #'markdown-match-generic-metadata
      (ignore (goto-char (point-max))))

    (map!
     :map markdown-mode-map
     :n [tab] #'markdown-cycle
     :n "TAB" #'markdown-cycle
     :n [backtab] #'markdown-shifttab
     :n "<S-tab>" #'markdown-shifttab
     (:localleader
      ;; Movement
      "{" #'markdown-backward-paragraph
      "}" #'markdown-forward-paragraph
      ;; Completion, and Cycling
      "]" #'markdown-complete
      ;; Indentation
      ">" #'markdown-indent-region
      "<" #'markdown-outdent-region
      ;; Buffer-wide commands
      (:prefix ("c" . "command")
       "]" #'markdown-complete-buffer
       "c" #'markdown-check-refs
       "e" #'markdown-export
       "m" #'markdown-other-window
       "n" #'markdown-cleanup-list-numbers
       "o" #'markdown-open
       "p" #'markdown-preview
       "v" #'markdown-export-and-preview
       "w" #'markdown-kill-ring-save)
      ;; headings
      (:prefix ("h" . "header")
       "i" #'markdown-insert-header-dwim
       "I" #'markdown-insert-header-setext-dwim
       "1" #'markdown-insert-header-atx-1
       "2" #'markdown-insert-header-atx-2
       "3" #'markdown-insert-header-atx-3
       "4" #'markdown-insert-header-atx-4
       "5" #'markdown-insert-header-atx-5
       "6" #'markdown-insert-header-atx-6
       "!" #'markdown-insert-header-setext-1
       "@" #'markdown-insert-header-setext-2)
      ;; Insertion of common elements
      (:prefix ("i" . "insert")
       "-" #'markdown-insert-hr
       "f" #'markdown-insert-footnote
       "i" #'markdown-insert-image
       "l" #'markdown-insert-link
       "t" #'markdown-toc-generate-toc
       "w" #'markdown-insert-wiki-link
       "u" #'markdown-insert-uri)
      ;; Element removal
      "k" #'markdown-kill-thing-at-point
      ;; List editing
      (:prefix ("l" . "lists")
       "i"  #'markdown-insert-list-item)
      ;; Toggles
      (:prefix ("t" . "toggles")
       "i" #'markdown-toggle-inline-images
       "m" #'markdown-toggle-markup-hiding
       "l" #'markdown-toggle-url-hiding
       "t" #'markdown-toggle-gfm-checkbox
       "w" #'markdown-toggle-wiki-links)
      ;; region manipulation
      (:prefix ("x" . "text")
       "b" #'markdown-insert-bold
       "i" #'markdown-insert-italic
       "c" #'markdown-insert-code
       "C" #'markdown-insert-gfm-code-block
       "q" #'markdown-insert-blockquote
       "Q" #'markdown-blockquote-region
       "p" #'markdown-insert-pre
       "P" #'markdown-pre-region)
      ;; Following and Jumping
      "N"     #'markdown-next-link
      "f"     #'markdown-follow-thing-at-point
      "P"     #'markdown-previous-link
      "<RET>" #'markdown-do))))


;;;; nix

(module! (:lang nix)

  (use-package nix-mode
    :interpreter ("\\(?:cached-\\)?nix-shell" . +nix-shell-init-mode)
    :mode "\\.nix\\'"
    :config
    (when (featurep! :completion company)
      (set-company-backend! 'nix-mode :company '(company-nixos-options)))
    (when (featurep! :tools lookup)
      (set-lookup-handlers! 'nix-mode
        :documentation #'+nix/lookup-option))
    (when (featurep! :tools eval)
      (set-repl-handler! 'nix-mode #'+nix/open-repl))
    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*nixos-options-doc\\*$" :ttl 0 :quit t))

    (map! :localleader
          :map nix-mode-map
          "f" #'nix-update-fetch
          "p" #'nix-format-buffer
          "r" #'nix-repl-show
          "s" #'nix-shell
          "b" #'nix-build
          "u" #'nix-unpack
          "o" #'+nix/lookup-option))

  (use-package nix-drv-mode
    :mode "\\.drv\\'")

  (use-package nix-update
    :commands nix-update-fetch)

  (use-package nix-repl
    :commands nix-repl-show))


;;;; python

(module! (:lang python)

  (defvar +python-ipython-repl-args '("-i" "--simple-prompt" "--no-color-info")
    "CLI arguments to initialize ipython with when
`+python/open-ipython-repl' is called.")

  (defvar +python-jupyter-repl-args '("--simple-prompt")
    "CLI arguments to initialize 'jupiter console %s' with when
`+python/open-ipython-repl' is called.")

  (use-package python
    :defer t
    :mode ("[./]flake8\\'" . conf-mode)
    :mode ("/Pipfile\\'" . conf-mode)
    :init
    (setq python-environment-directory +emacs-cache-dir
          python-indent-guess-indent-offset-verbose nil)
    :config
    (when (featurep! :tools lookup)
      (set-docsets! '(python-mode inferior-python-mode) "Python 3" "NumPy" "SciPy" "Pandas"))
    (when (featurep! :tools eval)
      (set-repl-handler! 'python-mode #'+python/open-repl :persist t))
    (setq python-indent-guess-indent-offset-verbose nil)

    (when (featurep! :ui pretty-code)
      (set-pretty-symbols! 'python-mode
        ;; Functional
        :lambda "lambda"))

    ;; Stop the spam!
    (setq python-indent-guess-indent-offset-verbose nil)

    (when (and (executable-find "python3")
               (string= python-shell-interpreter "python"))
      (setq python-shell-interpreter "python3"))

    (defhook! +python--hook-use-correct-flycheck-executables ()
      "Use the correct Python executables for Flycheck."
      'python-mode-hook
      (let ((executable python-shell-interpreter))
        (save-excursion
          (goto-char (point-min))
          (save-match-data
            (when (or (looking-at "#!/usr/bin/env \\(python[^ \n]+\\)")
                      (looking-at "#!\\([^ \n]+/python[^ \n]+\\)"))
              (setq executable (substring-no-properties (match-string 1))))))
        ;; Try to compile using the appropriate version of Python for the file.
        (setq-local flycheck-python-pycompile-executable executable)
        ;; We might be running inside a virtualenv, in which case the modules won't be available.
        ;; But calling the executables directly will work.
        (setq-local flycheck-python-pylint-executable "pylint")
        (setq-local flycheck-python-flake8-executable "flake8")))

    (define-key python-mode-map (kbd "DEL") nil) ; interferes with smartparens
    (sp-local-pair 'python-mode "'" nil
                   :unless '(sp-point-before-word-p
                             sp-point-after-word-p
                             sp-point-before-same-p))

    (setq-hook! 'python-mode-hook tab-width python-indent-offset)

    (map! :localleader
          :map python-mode-map
          "'"  #'+python/open-repl
          (:prefix ("s" . "REPL")
           "b" #'python-shell-send-buffer
           "f" #'python-shell-send-defun
           "i" #'+python/open-ipython-repl
           "j" #'+python/open-jupyter-repl
           "r" #'python-shell-send-region)))


;;;;; anaconda-mode

  (use-package anaconda-mode
    :defer t
    :init
    (setq anaconda-mode-installation-directory (concat +emacs-etc-dir "anaconda/")
          anaconda-mode-eldoc-as-single-line t)
    (defhook! +python--hook-init-anaconda-mode-maybe ()
      "Enable `anaconda-mode' if `lsp-mode' isn't."
      'python-mode-local-vars-hook
      (unless (or (bound-and-true-p lsp-mode)
                  (bound-and-true-p lsp--buffer-deferred))
        (anaconda-mode +1)))
    :config
    (add-hook 'anaconda-mode-hook #'anaconda-eldoc-mode)
    ;; set company backend here... This requires anaconda-mode!
    (when (featurep! :completion company)
      (set-company-backend! 'anaconda-mode :company '(company-anaconda)))
    (when (featurep! :tools lookup)
      (set-lookup-handlers! 'anaconda-mode
        :definition #'anaconda-mode-find-definitions
        :references #'anaconda-mode-find-references
        :documentation #'anaconda-mode-show-doc))

    (when (featurep! :ui popup)
      (set-popup-rule! "^\\*anaconda-mode" :select nil))
    (defun +python--hook-auto-kill-anaconda-processes ()
      "Kill anaconda processes if this buffer is the last python buffer."
      (when (and (eq major-mode 'python-mode)
                 (not (delq (current-buffer)
                            (+emacs-buffers-in-mode 'python-mode (buffer-list)))))
        (anaconda-mode-stop)))

    (add-hook! 'python-mode-hook
      (add-hook 'kill-buffer-hook #'+python--hook-auto-kill-anaconda-processes nil t))

    (add-hook 'anaconda-mode-hook #'evil-normalize-keymaps)

    (map! :localleader
          :map anaconda-mode-map
          :prefix "g"
          "d" #'anaconda-mode-find-definitions
          "h" #'anaconda-mode-show-doc
          "a" #'anaconda-mode-find-assignments
          "f" #'anaconda-mode-find-file
          "u" #'anaconda-mode-find-references))


;;;;; pyimport

  (use-package pyimport
    :defer t
    :init
    (map! :after python
          :map python-mode-map
          :localleader
          (:prefix ("i" . "imports")
           :desc "Insert missing imports" "i" #'pyimport-insert-missing
           :desc "Remove unused imports"  "r" #'pyimport-remove-unused
           :desc "Sort imports"           "s" #'pyimpsort-buffer
           :desc "Optimize imports"       "o" #'+python/optimize-imports))))


;;;; scheme

(module! (:lang scheme)

  (use-package scheme
    :hook (scheme-mode . rainbow-delimiters-mode)
    :config
    (when (featurep! :editor cleverlispy)
      (pushnew! +cleverlispy-lispyville-modes 'scheme-mode)
      (add-hook 'scheme-mode-hook #'+cleverlispy--hook-enable))
    (advice-add #'scheme-indent-function :override #'+scheme--advice-indent-function))


  (use-package geiser
    :defer t
    :init
    (setq geiser-autodoc-identifier-format "%s → %s"
          geiser-repl-current-project-function '+emacs-project-root)
    (when (featurep! :lang scheme +racket)
      (setq auto-mode-alist
            (remove '("\\.rkt\\'" . scheme-mode) auto-mode-alist)))
    ;; built-in
    (after! scheme
      (when (featurep! :tools eval)
        (set-repl-handler! 'scheme-mode #'+scheme/open-repl)
        (set-eval-handler! 'scheme-mode #'geiser-eval-region))
      (when (featurep! :tools lookup)
        (set-lookup-handlers! '(scheme-mode geiser-repl-mode)
          :definition #'geiser-edit-symbol-at-point
          :documentation #'geiser-doc-symbol-at-point))
      (when (featurep! :ui pretty-code)
        (set-pretty-symbols! 'scheme-mode :lambda "lambda"))
      (when (featurep! :editor rotate-text)
        (set-rotate-patterns! 'scheme-mode
          :symbols '(("#t" "#f")))))
    :config
    (when (featurep! :ui popup)
      (set-popup-rules!
        '(("^\\*[gG]eiser \\(dbg\\|xref\\|messages\\)\\*$" :slot 1 :vslot -1)
          ("^\\*Geiser documentation\\*$" :slot 2 :vslot 2 :select t :size 0.35)
          ("^\\* [A-Za-z0-9_-]+ REPL \\*" :size 0.3 :quit nil :ttl nil))))

    ;; Geiser does not inherite buffer-local environment
    (when (featurep! :tools direnv)
      (inheritenv-add-advice #'geiser)
      (inheritenv-add-advice #'run-geiser))

    (map! :localleader
        (:map (scheme-mode-map geiser-repl-mode-map)
         :desc "Toggle REPL"                  "'"  #'switch-to-geiser
         :desc "Connect to external Scheme"   "\"" #'geiser-connect
         :desc "Toggle type of brackets"      "["  #'geiser-squarify
         :desc "Insert lambda"                "\\" #'geiser-insert-lambda
         :desc "Set Scheme implementation"    "s"  #'geiser-set-scheme
         :desc "Reload Geiser buffers+REPLs"  "R" #'geiser-reload
         (:prefix ("h" . "help")
          :desc "Show callers of <point>"     "<" #'geiser-xref-callers
          :desc "Show callees of <point>"     ">" #'geiser-xref-callees
          :desc "Toggle autodoc mode"         "a" #'geiser-autodoc-mode
          :desc "Show autodoc of <point>"     "s" #'geiser-autodoc-show
          :desc "Search manual for <point>"   "m" #'geiser-doc-look-up-manual
          :desc "Show docstring of <point>"   "." #'geiser-doc-symbol-at-point)
         (:prefix ("r" . "repl")
          :desc "Load file into REPL"         "f" #'geiser-load-file
          :desc "Restart REPL"                "r" #'geiser-restart-repl))
        (:map scheme-mode-map
         (:prefix ("e" . "eval")
          :desc "Eval buffer"                 "b" #'geiser-eval-buffer
          :desc "Eval buffer and go to REPL"  "B" #'geiser-eval-buffer-and-go
          :desc "Eval last sexp"              "e" #'geiser-eval-last-sexp
          :desc "Eval definition"             "d" #'geiser-eval-definition
          :desc "Eval defn. and go to REPL"   "D" #'geiser-eval-definition-and-go
          :desc "Eval region"                 "r" #'geiser-eval-region
          :desc "Eval region and go to REPL"  "R" #'geiser-eval-region-and-go)
         (:prefix ("r" . "repl")
          :desc "Load current buffer in REPL" "b" #'geiser-load-current-buffer))
        (:map geiser-repl-mode-map
         :desc "Clear REPL buffer"            "c" #'geiser-repl-clear-buffer
         :desc "Quit REPL"                    "q" #'geiser-repl-exit)))


  (use-package macrostep-geiser
    :hook (geiser-mode . macrostep-geiser-setup)
    :hook (geiser-repl-mode . macrostep-geiser-setup)
    :init
    (map! :after geiser
          :localleader
          :map (scheme-mode-map geiser-repl-mode-map)
          :desc "Expand macro by one step" "m" #'macrostep-expand
          :desc "Recursively expand macro" "M" #'macrostep-geiser-expand-all))


  (use-package flycheck-guile
    :when (featurep! :checkers syntax)
    :after geiser))


;;;; racket

(module! (:lang scheme +racket)

  (after! projectile
    (add-to-list 'projectile-project-root-files "info.rkt"))

  (use-package racket-mode
    ;; Give it precedence over `scheme-mode'
    :mode "\\.rkt\\'"
    :config
    (when (featurep! :tools eval)
      (set-repl-handler! 'racket-mode #'+racket/open-repl))

    (when (featurep! :tools lookup)
      (set-lookup-handlers! '(racket-mode racket-repl-mode)
        :definition    #'+racket-lookup-definition
        :documentation #'+racket-lookup-documentation)
      (set-docsets! 'racket-mode "Racket"))

    (when (featurep! :ui pretty-code)
      (set-pretty-symbols! 'racket-mode
        :lambda "lambda"))

    (when (featurep! :editor rotate-text)
      (set-rotate-patterns! 'racket-mode
        :symbols '(("#true" "#false")
                   ("#t" "#f"))))

    (when (featurep! :editor cleverlispy)
      (pushnew! +cleverlispy-lispyville-modes 'racket-mode)
      (add-hook 'racket-mode-hook #'+cleverlispy--hook-enable))

    (add-hook! 'racket-mode-hook
               #'rainbow-delimiters-mode
               #'highlight-quoted-mode)

    (add-hook 'racket-mode-local-vars-hook #'racket-xp-mode)
    ;; Both flycheck and racket-xp produce error popups, but racket-xp's are
    ;; higher quality so disable flycheck's:
    (when (featurep! :checkers syntax)
      (defhook! +racket--hook-disable-flycheck ()
        "TODO"
        'racket-xp-mode-hook
        (cl-pushnew 'racket flycheck-disabled-checkers)))

    (unless (or (featurep! :editor parinfer)
                (featurep! :editor lispy))
      (add-hook 'racket-mode-hook #'racket-smart-open-bracket-mode))

    (map! (:map racket-xp-mode-map
           [remap racket-doc]              #'racket-xp-documentation
           [remap racket-visit-definition] #'racket-xp-visit-definition
           [remap next-error]              #'racket-xp-next-error
           [remap previous-error]          #'racket-xp-previous-error)
          (:localleader
           :map racket-mode-map
           "a" #'racket-align
           "A" #'racket-unalign
           "f" #'racket-fold-all-tests
           "F" #'racket-unfold-all-tests
           "h" #'racket-doc
           "i" #'racket-unicode-input-method-enable
           "l" #'racket-logger
           "o" #'racket-profile
           "p" #'racket-cycle-paren-shapes
           "r" #'racket-run
           "R" #'racket-run-and-switch-to-repl
           "t" #'racket-test
           "u" #'racket-backward-up-list
           "y" #'racket-insert-lambda
           (:prefix ("m" . "macros")
            "d" #'racket-expand-definition
            "e" #'racket-expand-last-sexp
            "r" #'racket-expand-region
            "a" #'racket-expand-again)
           (:prefix ("g" . "goto")
            "b" #'racket-unvisit
            "d" #'racket-visit-definition
            "m" #'racket-visit-module
            "r" #'racket-open-require-path)
           (:prefix ("s" . "send")
            "d" #'racket-send-definition
            "e" #'racket-send-last-sexp
            "r" #'racket-send-region)
           :map racket-repl-mode-map
           "l" #'racket-logger
           "h" #'racket-repl-documentation
           "y" #'racket-insert-lambda
           "u" #'racket-backward-up-list
           (:prefix ("m" . "macros")
            "d" #'racket-expand-definition
            "e" #'racket-expand-last-sexp
            "f" #'racket-expand-file
            "r" #'racket-expand-region)
           (:prefix ("g" . "goto")
            "b" #'racket-unvisit
            "m" #'racket-visit-module
            "d" #'racket-repl-visit-definition)))))


;;;; sh

;;;;; fish-mode

(module! (:lang sh +fish)

  (use-package fish-mode
    :when (featurep! :lang sh +fish)
    :defer t
    :init
    (add-hook!
     fish-mode
     (set-company-backend!
      'fish-mode
      :company '((company-shell company-shell-env company-fish-shell company-dabbrev-code))))
    :config
    (when (featurep! :editor format)
      (set-formatter! 'fish-mode #'fish_indent))))
