;; modules/+org-present.el -*- lexical-binding: t; -*-

(defvar +org-present-text-scale 2
  "The `text-scale-amount' for `org-tree-slide-mode'.")

(after! ox
  (add-to-list 'org-export-backends 'beamer))

(use-package ox-reveal
  :after ox
  :config
  (setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js@3/"
        org-reveal-mathjax t))

;; Simple presentations, directly from org.
(use-package org-tree-slide
  :commands org-tree-slide-mode
  :config
  (setq org-tree-slide-modeline-display nil
        org-tree-slide-heading-emphasis t)

  (add-hook 'org-tree-slide-mode-after-narrow-hook #'org-display-inline-images)
  (add-hook 'org-tree-slide-mode-hook #'+org-present--hook-prettify-slide)
  (add-hook 'org-tree-slide-play-hook #'+org-present--hook-hide-blocks)

  (when (featurep! :editor evil)
    (map! :map org-tree-slide-mode-map
          :n [C-right] #'org-tree-slide-move-next-tree
          :n [C-left]  #'org-tree-slide-move-previous-tree
          :n [right] #'org-tree-slide-move-next-tree
          :n [left]  #'org-tree-slide-move-previous-tree
          :n "<f8>"  #'org-tree-slide-content
          :n "q"     #'org-tree-slide-mode)

    (add-hook 'org-tree-slide-mode-hook #'evil-normalize-keymaps)))
