;; modules/+lang-latex-viewers.el -*- lexical-binding: t; -*-

;; fall back pdf previewing to latex-preview-pane
(add-to-list 'TeX-view-program-selection '(output-pdf "preview-pane") 'append)
(add-to-list 'TeX-view-program-list '("preview-pane" latex-preview-pane-mode))

(dolist (viewer (reverse +latex-viewers))
  (pcase viewer
    (`pdf-tools
     (add-to-list 'TeX-view-program-selection '(output-pdf "PDF Tools"))
     (when IS-MAC
       ;; PDF Tools isn't in `TeX-view-program-list-builtin' on macs
       (add-to-list 'TeX-view-program-list '("PDF Tools" TeX-pdf-tools-sync-view)))
     ;; Update PDF buffers after successful LaTeX runs
     (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer))))

(after! latex-preview-pane
  (setq latex-preview-pane-multifile-mode 'auctex)

  (define-key! doc-view-mode-map
    "ESC" #'delete-window
    "q"   #'delete-window
    "k"   (cmd! (quit-window) (delete-window))))
