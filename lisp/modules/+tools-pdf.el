;; modules/+tools-pdf.el -*- lexical-binding: t; -*-

;;
;;; pdf-tools

(use-package pdf-tools
  :mode ("\\.pdf\\'" . pdf-view-mode)
  :magic ("%PDF" . pdf-view-mode)
  :init
  (after! pdf-annot
    (defun +pdf--hook-cleanup-windows ()
      "Kill left-over annotation buffers when the document is
killed."
      (when (buffer-live-p pdf-annot-list-document-buffer)
        (pdf-info-close pdf-annot-list-document-buffer))
      (when (buffer-live-p pdf-annot-list-buffer)
        (kill-buffer pdf-annot-list-buffer))
      (let ((contents-buffer (get-buffer "*Contents*")))
        (when (and contents-buffer (buffer-live-p contents-buffer))
          (kill-buffer contents-buffer))))
    (add-hook! 'pdf-view-mode-hook
      (add-hook 'kill-buffer-hook #'+pdf--hook-cleanup-windows nil t)))
  :config
  (defadvice! +pdf-tools--advice-install-epdfinfo (orig-fn &rest args)
    "Install epdfinfo after the first PDF file, if needed."
    :around #'pdf-view-mode
    (if (file-executable-p pdf-info-epdfinfo-program)
        (apply orig-fn args)
      ;; If we remain in pdf-view-mode, it'll spit out cryptic errors. This
      ;; graceful failure is better UX.
      (fundamental-mode)
      (message "Viewing PDFs in Emacs requires epdfinfo. Use `M-x pdf-tools-install' to build it")))

  (pdf-tools-install-noverify)

  (setq-default pdf-view-display-size 'fit-width
                pdf-view-use-scaling t)

  ;; PDF-tools related popups
  (when (featurep! :ui popup)
    (set-popup-rules!
      '(("^\\*Outline*" :side 'right :size 40 :select nil)
        ("^\\*Edit Annotation " :quit nil)
        ("\\(?:^\\*Contents\\|'s annots\\*$\\)" :ignore t))))

  (add-hook 'pdf-annot-list-mode-hook #'hide-mode-line-mode)

  ;; Fix #1107: flickering pdfs when evil-mode is enabled
  (setq-hook! 'pdf-view-mode-hook evil-normal-state-cursor (list nil))

  (map!
   :map pdf-view-mode-map
   :gn "q" #'kill-current-buffer
   :map pdf-annot-list-mode-map
   :n "<return>" #'pdf-annot-list-display-annotation-from-id
   :n "q" #'tablist-quit
   :localleader
   :map pdf-view-mode-map
   ;; Slicing image
   (:prefix ("s" . "slice/search")
    "m" #'pdf-view-set-slice-using-mouse
    "b" #'pdf-view-set-slice-from-bounding-box
    "r" #'pdf-view-reset-slice)
   ;; Annotations
   (:prefix ("a" . "annotations")
    "D" #'pdf-annot-delete
    "t" #'pdf-annot-attachment-dired
    "h" #'pdf-annot-add-highlight-markup-annotation
    "l" #'pdf-annot-list-annotations
    "m" #'pdf-annot-add-markup-annotation
    "o" #'pdf-annot-add-strikeout-markup-annotation
    "s" #'pdf-annot-add-squiggly-markup-annotation
    "t" #'pdf-annot-add-text-annotation
    "u" #'pdf-annot-add-underline-markup-annotation)
   ;; Fit image to window
   (:prefix ("f" . "fit")
    "w" #'pdf-view-fit-width-to-window
    "h" #'pdf-view-fit-height-to-window
    "p" #'pdf-view-fit-page-to-window)
   ;; Other
   "s s" #'pdf-occur
   "p" #'pdf-misc-print-document
   "O" #'pdf-outline
   "n" #'pdf-view-midnight-minor-mode)

  (defadvice! +pdf--advice-suppress-large-file-prompts (orig-fn size op-type filename &optional offer-raw)
    "Silence \"File *.pdf is large (X MiB), really open?\"
prompts for pdfs"
    :around #'abort-if-file-too-large
    (unless (string-match-p "\\.pdf\\'" filename)
      (funcall orig-fn size op-type filename offer-raw))))

(use-package saveplace-pdf-view
  :after pdf-view)


;;
;;; org-pdftools

(use-package org-pdftools
  :when (and (featurep! :tools pdf) (featurep! :org org))
  :commands org-pdftools-export
  :init
  (after! org
    (org-link-set-parameters "pdftools"
                             :follow #'org-pdftools-open
                             :complete #'org-pdftools-complete-link
                             :store #'org-pdftools-store-link
                             :export #'org-pdftools-export)
    (add-hook 'org-store-link-functions #'org-pdftools-store-link)))
