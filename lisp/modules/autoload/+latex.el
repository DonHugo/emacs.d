;; modules/autoload/+latex.el -*- lexical-binding: t; -*-
;;;###if (featurep! :lang latex)

;;;###autoload
(defun +latex/build ()
  (interactive)
  (progn
    (let ((TeX-save-query nil))
      (TeX-save-document (TeX-master-file)))
    (TeX-command +latex-build-command 'TeX-master-file -1)))
;; (setq build-proc (TeX-command latex-build-command 'TeX-master-file -1))
;; ;; Sometimes, TeX-command returns nil causing an error in set-process-sentinel
;; (when build-proc
;;   (set-process-sentinel build-proc 'latex//build-sentinel))))

;;;###autoload
(defun +latex--build-sentinel (process event)
  (if (string= event "finished\n")
      (TeX-view)
    (message "Errors! Check with C-`")))

;;;###autoload
(defun +latex--autofill ()
  "Check whether the pointer is currently inside one of the
  environments described in `+latex-nofill-env' and if so, inhibits
  the automatic filling of the current paragraph."
  (let ((do-auto-fill t)
        (current-environment "")
        (level 0))
    (while (and do-auto-fill (not (string= current-environment "document")))
      (setq level (1+ level)
            current-environment (LaTeX-current-environment level)
            do-auto-fill (not (member current-environment +latex-nofill-env))))
    (when do-auto-fill
      (do-auto-fill))))

;;;###autoload
(defun +latex/auto-fill-mode ()
  "Toggle auto-fill-mode using the custom auto-fill function."
  (interactive)
  (auto-fill-mode)
  (setq auto-fill-function '+latex--autofill))

;;;###autoload
(defun +latex-indent-item-fn ()
  "Indent LaTeX \"itemize\",\"enumerate\", and \"description\" environments.
\"\\item\" is indented `LaTeX-indent-level' spaces relative to the the beginning
of the environment.
See `LaTeX-indent-level-item-continuation' for the indentation strategy this
function uses."
  (save-match-data
    (let* ((re-beg "\\\\begin{")
           (re-end "\\\\end{")
           (re-env "\\(?:itemize\\|\\enumerate\\|description\\)")
           (indent (save-excursion
                     (when (looking-at (concat re-beg re-env "}"))
                       (end-of-line))
                     (LaTeX-find-matching-begin)
                     (+ LaTeX-item-indent (current-column))))
           (contin (pcase +latex-indent-item-continuation-offset
                     (`auto LaTeX-indent-level)
                     (`align 6)
                     (`nil (- LaTeX-indent-level))
                     (x x))))
      (cond ((looking-at (concat re-beg re-env "}"))
             (or (save-excursion
                   (beginning-of-line)
                   (ignore-errors
                     (LaTeX-find-matching-begin)
                     (+ (current-column)
                        LaTeX-item-indent
                        LaTeX-indent-level
                        (if (looking-at (concat re-beg re-env "}"))
                            contin
                          0))))
                 indent))
            ((looking-at (concat re-end re-env "}"))
             (save-excursion
               (beginning-of-line)
               (ignore-errors
                 (LaTeX-find-matching-begin)
                 (current-column))))
            ((looking-at "\\\\item")
             (+ LaTeX-indent-level indent))
            ((+ contin LaTeX-indent-level indent))))))

;;;###autoload
(defun +latex--advice-fold-last-macro (&rest _)
  "Advice to auto-fold LaTeX macros after functions that
typically insert macros."
  ;; A simpler approach would be to just fold the whole line, but if point was
  ;; inside a macro that would would kick it out. So instead we fold the last
  ;; macro before point, hoping its the one newly inserted.
  (TeX-fold-region (save-excursion
                     (search-backward "\\" (line-beginning-position) t)
                     (point))
                   (1+ (point))))

;; Rebindings for TeX-font
;;;###autoload
(defun +latex/font-bold () (interactive) (TeX-font nil ?\C-b))
;;;###autoload
(defun +latex/font-medium () (interactive) (TeX-font nil ?\C-m))
;;;###autoload
(defun +latex/font-code () (interactive) (TeX-font nil ?\C-t))
;;;###autoload
(defun +latex/font-emphasis () (interactive) (TeX-font nil ?\C-e))
;;;###autoload
(defun +latex/font-italic () (interactive) (TeX-font nil ?\C-i))
;;;###autoload
(defun +latex/font-clear () (interactive) (TeX-font nil ?\C-d))
;;;###autoload
(defun +latex/font-calligraphic () (interactive) (TeX-font nil ?\C-a))
;;;###autoload
(defun +latex/font-small-caps () (interactive) (TeX-font nil ?\C-c))
;;;###autoload
(defun +latex/font-sans-serif () (interactive) (TeX-font nil ?\C-f))
;;;###autoload
(defun +latex/font-normal () (interactive) (TeX-font nil ?\C-n))
;;;###autoload
(defun +latex/font-serif () (interactive) (TeX-font nil ?\C-r))
;;;###autoload
(defun +latex/font-oblique () (interactive) (TeX-font nil ?\C-s))
;;;###autoload
(defun +latex/font-upright () (interactive) (TeX-font nil ?\C-u))
