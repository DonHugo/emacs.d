;; modules/autoload/+childframes.el -*- lexical-binding: t; -*-
;;;###if (featurep! :ui childframes)

;;;###autoload
(defun +childframes-poshandler-frame-center-near-bottom-fn (info)
  "Display the child frame in the center of the frame, slightly
closer to the bottom, which is easier on the eyes on big
displays."
  (let ((parent-frame (plist-get info :parent-frame))
        (pos (posframe-poshandler-frame-center info)))
    (cons (car pos)
          (truncate (/ (frame-pixel-height parent-frame) 2)))))

;;;###autoload
(defun +childframe-threshold-p ()
  "Return t if current `frame-pixel-height' and
`frame-pixel-width' are greater or equal than
`+childframe-threshold'."
  (let (monitor-geometry)
    (dolist (element (display-monitor-attributes-list))
      (when (member (selected-frame)
                    (alist-get 'frames element))
        (setq monitor-geometry (alist-get 'geometry element))))
    (let ((monitor-width (nth 2 monitor-geometry))
          (monitor-height (nth 3 monitor-geometry)))
      (if (and (>= (frame-pixel-width) (* monitor-width +childframe-threshold))
               (>= (frame-pixel-height) (* monitor-height +childframe-threshold)))
          t
        nil))))
