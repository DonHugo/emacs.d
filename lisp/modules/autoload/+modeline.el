;; modules/autoload/+modeline.el -*- lexical-binding: t; -*-
;;;###if (featurep! :ui modeline)

(defvar +modeline--old-bar-height nil)
;;;###autoload
(defun +modeline--hook-resize-for-font ()
  "Adjust the modeline's height when the font size is changed by
`+emacs/increase-font-size' or `+emacs/decrease-font-size'.

Meant for `+emacs-change-font-size-hook'."
  (unless +modeline--old-bar-height
    (setq +modeline--old-bar-height doom-modeline-height))
  (let ((default-height +modeline--old-bar-height)
        (scale (or (frame-parameter nil 'font-scale) 0)))
    (setq doom-modeline-height
          (if (> scale 0)
              (+ default-height (* scale +emacs-font-increment))
            default-height))))
