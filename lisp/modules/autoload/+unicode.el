;; modules/+unicode.el -*- lexical-binding: t; -*-
;;;###if (featurep! :ui unicode)

;;;###autoload
(defun +unicode--hook-setup-fonts (&optional frame)
  "Initialize `unicode-fonts', if in a GUI session. If
`+emacs-unicode-font' is set, add it as preferred font for all
unicode blocks."
  (when (and frame (display-graphic-p frame))
    (with-selected-frame frame
      (require 'unicode-fonts)
      (when +emacs-unicode-font
        (let ((+emacs-unicode-font-family (plist-get (font-face-attributes +emacs-unicode-font) :family)))
          (dolist (unicode-block unicode-fonts-block-font-mapping)
            (push +emacs-unicode-font-family (cadr unicode-block)))))
      ;; NOTE will impact startup time on first run
      (unicode-fonts-setup))))
