;; modules/autoload/+org-present.el -*- lexical-binding: t; -*-
;;;###if (featurep! :org org +present)

;;
;;; Helpers

(defun +org-present--cleanup-org-tree-slides-mode ()
  (unless (cl-loop for buf in (+emacs-buffers-in-mode 'org-mode)
                   if (buffer-local-value 'org-tree-slide-mode buf)
                   return t)
    (org-tree-slide-mode -1)
    (remove-hook 'kill-buffer-hook #'+org-present--cleanup-org-tree-slides-mode
                 'local)))


;;
;;; Hooks

;;;###autoload
(defun +org-present--hook-hide-blocks ()
  "Hide org #+ constructs."
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "^[[:space:]]*\\(#\\+\\)\\(\\(?:BEGIN\\|END\\|ATTR\\|RESULTS\\)[^[:space:]]+\\).*" nil t)
      (org-flag-region (match-beginning 1)
                       (match-end 0)
                       org-tree-slide-mode
                       t))))

;;;###autoload
(defun +org-present--hook-hide-leading-stars ()
  "Hide leading stars in headings."
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "^\\(\\*+\\)" nil t)
      (org-flag-region (match-beginning 1)
                       (match-end 1)
                       org-tree-slide-mode
                       t))))

;;;###autoload
(defun +org-present--hook-detect-slide ()
  (outline-show-all)
  (if (member "title" (org-get-tags-at))
      (text-scale-set 10)
    (text-scale-set +org-present-text-scale)))

(defvar cwm-use-vertical-padding)
(defvar cwm-frame-internal-border)
(defvar cwm-left-fringe-ratio)
(defvar cwm-centered-window-width)
(defvar +org-present--last-wconf nil)
;;;###autoload
(defun +org-present--hook-prettify-slide ()
  "Set up the org window for presentation."
  (let ((arg (if org-tree-slide-mode +1 -1)))
    (if (not org-tree-slide-mode)
        (when +org-present--last-wconf
          (set-window-configuration +org-present--last-wconf))
      (setq +org-present--last-wconf (current-window-configuration))
      (+emacs/window-maximize-buffer))
    (when (fboundp 'centered-window-mode)
      (setq-local cwm-use-vertical-padding t)
      (setq-local cwm-frame-internal-border 100)
      (setq-local cwm-left-fringe-ratio -10)
      (setq-local cwm-centered-window-width 300)
      (centered-window-mode arg))
    (hide-mode-line-mode arg)
    (+org-pretty-mode arg)
    (+ligatures-mode arg)
    (cond (org-tree-slide-mode
           (set-window-fringes nil 0 0)
           (when (bound-and-true-p flyspell-mode)
             (flyspell-mode -1))
           (add-hook 'kill-buffer-hook #'+org-present--cleanup-org-tree-slides-mode
                     nil 'local)
           (text-scale-set +org-present-text-scale)
           (ignore-errors (org-latex-preview '(4))))
          (t
           (text-scale-set 0)
           (set-window-fringes nil fringe-mode fringe-mode)
           (org-clear-latex-preview)
           (org-remove-inline-images)
           (org-mode)))
    (redraw-display)))
