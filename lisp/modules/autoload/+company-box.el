;; modules/autoload/+company-box.el -*- lexical-binding: t; -*-
;;;###if (featurep! :completion company +childframe)

(defvar +company-box-doc--help-shown nil)
;;;###autoload
(defun +company-box-doc/toggle ()
  (interactive)
  (if +company-box-doc--help-shown
      (progn
        (company-box-doc--hide (or (frame-parent) (selected-frame)))
        (setq +company-box-doc--help-shown nil))
    (company-box-doc--show company-selection (or (frame-parent) (selected-frame)))
    (setq +company-box-doc--help-shown t)))
