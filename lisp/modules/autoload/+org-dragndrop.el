;; modules/autoload/+org-dragndrop.el -*- lexical-binding: t; -*-
;;;###if (featurep! :org org +dragndrop)

(defun +org-download/sreenshot ()
  "TODO"
  (interactive)
  (when (executable-find "spectacle")
    (let ((org-download-screenshot-method "spectacle -brc"))
      (shell-command-to-string org-download-screenshot-method))
    (funcall-interactively #'org-download-clipboard)))
