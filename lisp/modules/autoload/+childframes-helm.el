;; modules/autoload/+childframes-helm.el -*- lexical-binding: t; -*-
;;;###if (featurep! :ui childframes)

(defcustom helm-posframe-border-width 1
  "The border width used by helm-posframe. When 0, no border is
shown."
  :group 'helm-posframe
  :type 'number)

(defface helm-posframe
  '((t (:inherit default)))
  "Face used by the helm-posframe."
  :group 'helm-posframe)

(defface helm-posframe-border
  '((t (:inherit default :background "gray50")))
  "Face used by the helm-posframe's border."
  :group 'helm-posframe)

(defvar +helm--posframe-buffer nil)
;;;###autoload
(defun +helm-posframe-display (buffer &optional _resume)
  "TODO"
  (if (not (+childframe-threshold-p))
      (helm-default-display-buffer buffer)
    (setq helm--buffer-in-new-frame-p t)
    (setq +helm--posframe-buffer buffer)
    (require 'posframe)
    (apply #'posframe-show buffer
           :font helm-posframe-font
           :position (point)
           :poshandler +helm-posframe-handler
           :respect-header-line helm-echo-input-in-header-line
           :background-color (face-attribute 'helm-posframe :background nil t)
           :foreground-color (face-attribute 'helm-posframe :foreground nil t)
           :internal-border-width helm-posframe-border-width
           :internal-border-color (face-attribute 'helm-posframe-border :background nil t)
           :override-parameters helm-posframe-parameters
           (funcall helm-posframe-size-function))

    (unless (or (null +helm-posframe-text-scale)
                (= +helm-posframe-text-scale 0))
      (with-current-buffer buffer
        (text-scale-set +helm-posframe-text-scale)))
    ;; Do not dedicate posframe, otherwise Helm's actions won't work
    (set-window-dedicated-p (frame-selected-window
                             (buffer-local-value 'posframe--frame
                                                 (get-buffer buffer))) nil)))

;;;###autoload
(defun +helm-posframe--hook-cleanup ()
  "TODO"
  ;; Ensure focus is properly returned to the underlying window. This gives the
  ;; modeline a chance to refresh.
  (switch-to-buffer +helm--posframe-buffer t))

(add-hook 'helm-cleanup-hook #'+helm-posframe--hook-cleanup)
