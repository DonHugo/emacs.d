;; modules/autoload/+golden-ratio.el -*- lexical-binding: t; -*-
;;;###if (featurep! :ui golden-ratio)

;;;###autoload
(defun +golden-ratio-inhibit-for-buffer (bufname)
  "Disable golden-ratio if BUFNAME is the name of a visible buffer."
  (and (get-buffer bufname)
       (get-buffer-window bufname 'visible)))
