;; modules/autoload/+childframes-ivy.el -*- lexical-binding: t; -*-
;;;###if (and (featurep! :ui childframes) (featurep! :completion ivy))

;;;###autoload
(defun +ivy-display-at-frame-center-near-bottom-fn (str)
  "TODO"
  (ivy-posframe--display str #'+childframes-poshandler-frame-center-near-bottom-fn))
