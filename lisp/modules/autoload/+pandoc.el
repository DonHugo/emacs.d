;; modules/autoload/+pandoc.el -*- lexical-binding: t; -*-
;;;###if (featurep! :tools pandoc)

;;;###autoload
(defun +pandoc/run-pandoc ()
  "Check if `pandoc-mode' is loaded and run `pandoc-main-hydra'."
  (interactive)
  ;; only run pandoc-mode if not active, as it resets pandoc--local-settings
  (when (not (bound-and-true-p pandoc-mode))
    (pandoc-mode))
  (pandoc-main-hydra/body))
