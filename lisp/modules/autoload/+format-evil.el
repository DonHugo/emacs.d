;; modules/autoload/+format-evil.el -*- lexical-binding: t; -*-
;;;###if (and (featurep! :editor format) (featurep! :editor evil))

;;;###autoload (autoload '+format:region "autoload/+format-evil" nil t)
(evil-define-operator +format:region (beg end)
  "Evil ex interface to `+format/region'."
  (interactive "<r>")
  (+format/region beg end))
