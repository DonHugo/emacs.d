;; lisp/init-core-interactive.el -*- lexical-binding: t; -*-

;; Detect stale bytecode
;; If Emacs version changed, the bytecode is no longer valid and we must
;; recompile.
(eval
 `(unless (equal
           (list
            (emacs-version)
            (concat
             (file-name-sans-extension (file!)) ".el"))
           ',(eval-when-compile
               (list
                (emacs-version)
                (file!))))
    (throw 'stale-bytecode nil)))

(eval-when-compile
  (require 'el-patch))


;;
;;; el-patch

;; `el-patch' is great! We add extra `el-patch-deftype's early, so we can use
;; them.

;;Add `cl-defmacro'
(after! el-patch
  (el-patch-deftype cl-defmacro
    :classify el-patch-classify-function
    :locate el-patch-locate-function
    :declare ((doc-string 3)
              (indent defun))))


;;
;;; Keybinds

;; A centralized keybinds system, integrated with `which-key' to preview
;; available keybindings. All built into one powerful macro: `map!'. If evil is
;; never loaded, then evil bindings set with `map!' are ignored (i.e. omitted
;; entirely for performance reasons).

(defvar +emacs-leader-key "SPC"
  "The leader prefix key for Evil users.")

(defvar +emacs-leader-alt-key "M-m"
  "An alternative leader prefix key, used for Insert and Emacs
states, and for non-evil users.")

(defvar +emacs-localleader-key "SPC m"
  "The localleader prefix key, for major-mode specific
commands. ")

(defvar +emacs-localleader-alt-key "M-m m"
  "The localleader prefix key, for major-mode specific commands.
Used for Insert and Emacs states, and for non-evil users. ")

(defvar +emacs-leader-map (make-sparse-keymap)
  "An overriding keymap for <leader> keys.")


;;
;;; Keybind settings

(cond
 (IS-MAC
  (setq mac-command-modifier      'super
        ns-command-modifier       'super
        mac-option-modifier       'meta
        ns-option-modifier        'meta
        ;; Free up the right option for character composition
        mac-right-option-modifier 'none
        ns-right-option-modifier  'none))
 (IS-WINDOWS
  (setq w32-lwindow-modifier 'super
        w32-rwindow-modifier 'super)))

;; HACK Distinguish C-i from TAB.
(define-key key-translation-map [?\C-i]
  (cmd! (if (let ((keys (this-single-command-raw-keys)))
              (and keys
                   (not (cl-position 'tab    keys))
                   (not (cl-position 'kp-tab keys))
                   (display-graphic-p)
                   ;; Fall back if no <C-i> keybind can be found, otherwise
                   ;; we've broken all pre-existing C-i keybinds.
                   (let ((key
                          (+emacs-lookup-key
                           (vconcat (cl-subseq keys 0 -1) [C-i]))))
                     (not (or (numberp key) (null key))))))
            [C-i] [?\C-i])))


;;;; Universal, non-nuclear escape

(defvar +emacs-escape-hook nil
  "A hook run after C-g is pressed (or ESC in normal mode, for
evil users). Both trigger `+emacs/escape'. If any hook returns
non-nil, all hooks after it are ignored.")

(defun +emacs/escape (&optional interactive)
  "Run the `+emacs-escape-hook'."
  (interactive (list 'interactive))
  (cond ((minibuffer-window-active-p (minibuffer-window))
         ;; quit the minibuffer if open.
         (when interactive
           (setq this-command 'abort-recursive-edit))
         (abort-recursive-edit))
        ;; Run all escape hooks. If any returns non-nil, then stop there.
        ((run-hook-with-args-until-success '+emacs-escape-hook))
        ;; don't abort macros
        ((or defining-kbd-macro executing-kbd-macro) nil)
        ;; Back to the default
        ((unwind-protect (keyboard-quit)
           (when interactive
             (setq this-command 'keyboard-quit))))))

(global-set-key [remap keyboard-quit] #'+emacs/escape)

;; Allow eldoc after `+emacs/escape' or evil state change
(with-eval-after-load 'eldoc
  (eldoc-add-command '+emacs/escape))


;;;; General + leader/localleader keys

(use-package general
  :init
  ;; Convenience aliases
  (defalias 'define-key! #'general-def)
  (defalias 'unmap! #'general-unbind)
  :config
  (general-auto-unbind-keys))

;; HACK `map!' uses this instead of `define-leader-key!' because it consumes
;; 20-30% more startup time, so we reimplement it ourselves.
(defmacro +emacs--define-leader-key (&rest keys)
  (let (prefix forms wkforms)
    (while keys
      (let ((key (pop keys))
            (def (pop keys)))
        (if (keywordp key)
            (when (memq key '(:prefix :infix))
              (setq prefix def))
          (when prefix
            (setq key `(general--concat t ,prefix ,key)))
          (let* ((udef (cdr-safe (+emacs-unquote def)))
                 (bdef (if (general--extended-def-p udef)
                           (general--extract-def (general--normalize-extended-def udef))
                         def)))
            (unless (eq bdef :ignore)
              (push `(define-key +emacs-leader-map (general--kbd ,key)
                       ,bdef)
                    forms))
            (when-let (desc (cadr (memq :which-key udef)))
              (prependq!
               wkforms `((which-key-add-key-based-replacements
                           (general--concat t +emacs-leader-alt-key ,key)
                           ,desc)
                         (which-key-add-key-based-replacements
                           (general--concat t +emacs-leader-key ,key)
                           ,desc))))))))
    (macroexp-progn
     (cons `(after! which-key ,@(nreverse wkforms))
           (nreverse forms)))))

(defmacro define-leader-key! (&rest args)
  "Define <leader> keys. Uses `general-define-key' under the
hood, but does not support :states, :wk-full-keys or :keymaps."
  `(general-define-key
    :states nil
    :wk-full-keys nil
    :keymaps '+emacs-leader-map
    ,@args))

(defmacro define-localleader-key! (&rest args)
  "Define <localleader> key. Uses `general-define-key' under the
hood, but does not support :major-modes, :states, :prefix or
:non-normal-prefix."
  (if (featurep! :editor evil)
      ;; :non-normal-prefix doesn't apply to non-evil sessions (only evil's
      ;; emacs state)
      `(general-define-key
        :states '(normal visual motion emacs insert)
        :major-modes t
        :prefix +emacs-localleader-key
        :non-normal-prefix +emacs-localleader-alt-key
        ,@args)
    `(general-define-key
      :major-modes t
      :prefix +emacs-localleader-alt-key
      ,@args)))

;; PATCH `general-predicate-dispatch' Each branch binds 'it' to the return value
;;       of the predicate (anaphoric)
(el-patch-feature general)
(after! general
  (el-patch-cl-defmacro general-predicate-dispatch
    (fallback-def &rest defs
                  &key docstring
                  &allow-other-keys)
  (declare (indent 1))
  "Create a menu item that will run FALLBACK-DEF or a definition from DEFS.
DEFS consists of <predicate> <definition> pairs. Binding this menu-item to a key
will cause that key to act as the corresponding definition (a command, keymap,
etc) for the first matched predicate. If no predicate is matched FALLBACK-DEF
will be run. When FALLBACK-DEF is nil and no predicates are matched, the keymap
with the next highest precedence for the pressed key will be checked. DOCSTRING
can be specified as a description for the menu item."
  ;; remove keyword arguments from defs and sort defs into pairs
  (let ((defs (cl-loop for (key value) on defs by 'cddr
                       unless (keywordp key)
                       collect (list key value))))
    `'(menu-item
       ,(or docstring "") nil
       :filter (lambda (&optional _)
                 (el-patch-swap
                   (cond ,@(mapcar (lambda (pred-def)
                                     `(,(car pred-def) ,(cadr pred-def)))
                                   defs)
                         (t ,fallback-def))
                   (let (it)
                     (cond ,@(mapcar (lambda (pred-def)
                                       `((setq it ,(car pred-def))
                                         ,(cadr pred-def)))
                                     defs)
                           (t ,fallback-def)))))))))

;; We use a prefix commands instead of general's :prefix/:non-normal-prefix
;; properties because general is incredibly slow binding keys en mass with them
;; in conjunction with :states -- an effective doubling of startup time!
(define-prefix-command '+emacs/leader '+emacs-leader-map)
(define-key +emacs-leader-map [override-state] 'all)

;; Bind `+emacs-leader-key' and `+emacs-leader-alt-key' as late as possible to
;; give the user a chance to modify them.
(defhook! +emacs--hook-init-leader-keys ()
  "Bind `+emacs-leader-key' and `+emacs-leader-alt-key'."
  '+emacs-after-init-modules-hook
  (let ((map general-override-mode-map))
    (if (not (featurep 'evil))
        (progn
          (cond ((equal +emacs-leader-alt-key "C-c")
                 (set-keymap-parent +emacs-leader-map mode-specific-map))
                ((equal +emacs-leader-alt-key "C-x")
                 (set-keymap-parent +emacs-leader-map ctl-x-map)))
          (define-key map (kbd +emacs-leader-alt-key) '+emacs/leader))
      (evil-define-key* '(normal visual motion) map (kbd +emacs-leader-key) '+emacs/leader)
      (evil-define-key* '(emacs insert) map (kbd +emacs-leader-alt-key) '+emacs/leader))
    (general-override-mode +1)))


;;;; Packages

(use-package which-key
  :hook (+emacs-first-input . which-key-mode)
  :init
  (setq which-key-sort-order #'which-key-key-order-alpha
        which-key-sort-uppercase-first nil
        which-key-add-column-padding 1
        which-key-max-display-columns nil
        which-key-min-display-lines 6
        which-key-side-window-slot -10
        which-key-idle-delay 0)
  :config
  ;; general improvements to which-key readability
  (which-key-setup-side-window-bottom)
  (setq-hook! 'which-key-init-buffer-hook line-spacing 3)

  (which-key-add-key-based-replacements +emacs-leader-key "<leader>")
  (which-key-add-key-based-replacements +emacs-localleader-key "<localleader>")
  (which-key-add-key-based-replacements +emacs-leader-alt-key "<leader>")
  (which-key-add-key-based-replacements +emacs-localleader-alt-key "<localleader>"))


;;;; `map!' macro

(defvar +emacs-evil-state-alist
  '((?n . normal)
    (?v . visual)
    (?i . insert)
    (?e . emacs)
    (?o . operator)
    (?m . motion)
    (?r . replace)
    (?g . global))
  "A list of cons cells that map a letter to a evil state
symbol.")

(defun +emacs--map-keyword-to-states (keyword)
  "Convert a KEYWORD into a list of evil state symbols.
For example, :nvi will map to (list 'normal 'visual 'insert). See
`+emacs-evil-state-alist' to customize this."
  (cl-loop for l across (+emacs-keyword-name keyword)
           if (assq l +emacs-evil-state-alist) collect (cdr it)
           else do (error "not a valid state: %s" l)))


;; Register keywords for proper indentation (see `map!')
(put :after        'lisp-indent-function 'defun)
(put :desc         'lisp-indent-function 'defun)
(put :leader       'lisp-indent-function 'defun)
(put :localleader  'lisp-indent-function 'defun)
(put :map          'lisp-indent-function 'defun)
(put :mode         'lisp-indent-function 'defun)
(put :prefix       'lisp-indent-function 'defun)
(put :prefix-map   'lisp-indent-function 'defun)

;; specials
(defvar +emacs--map-forms nil)
(defvar +emacs--map-fn nil)
(defvar +emacs--map-batch-forms nil)
(defvar +emacs--map-state '(:dummy t))
(defvar +emacs--map-parent-state nil)
(defvar +emacs--map-evil-p nil)
(after! evil (setq +emacs--map-evil-p t))

(defun +emacs--map-process (rest)
  (let ((+emacs--map-fn +emacs--map-fn)
        +emacs--map-state
        +emacs--map-forms
        desc)
    (while rest
      (let ((key (pop rest)))
        (cond ((listp key)
               (+emacs--map-nested nil key))

              ((keywordp key)
               (pcase key
                 (:leader
                  (+emacs--map-commit)
                  (setq +emacs--map-fn '+emacs--define-leader-key))
                 (:localleader
                  (+emacs--map-commit)
                  (setq +emacs--map-fn 'define-localleader-key!))
                 (:after
                  (+emacs--map-nested (list 'after! (pop rest)) rest)
                  (setq rest nil))
                 (:desc
                  (setq desc (pop rest)))
                 (:map
                  (+emacs--map-set :keymaps `(quote ,(+emacs-enlist (pop rest)))))
                 (:mode
                  (push (cl-loop for m in (+emacs-enlist (pop rest))
                                 collect (intern (concat (symbol-name m) "-map")))
                        rest)
                  (push :map rest))
                 ((or :when :unless)
                  (+emacs--map-nested (list (intern (+emacs-keyword-name key)) (pop rest)) rest)
                  (setq rest nil))
                 (:prefix-map
                  (cl-destructuring-bind (prefix . desc)
                      (let ((arg (pop rest)))
                        (if (consp arg) arg (list arg)))
                    (let ((keymap (intern (format "+emacs-leader-%s-map" desc))))
                      (setq rest
                            (append (list :desc desc prefix keymap
                                          :prefix prefix)
                                    rest))
                      (push `(defvar ,keymap (make-sparse-keymap))
                            +emacs--map-forms))))
                 (:prefix
                  (cl-destructuring-bind (prefix . desc)
                      (let ((arg (pop rest)))
                        (if (consp arg) arg (list arg)))
                    (+emacs--map-set (if +emacs--map-fn :infix :prefix)
                                     prefix)
                    (when (stringp desc)
                      (setq rest (append (list :desc desc "" nil) rest)))))
                 (:textobj
                  (let* ((key (pop rest))
                         (inner (pop rest))
                         (outer (pop rest)))
                    (push `(map! (:map evil-inner-text-objects-map ,key ,inner)
                                 (:map evil-outer-text-objects-map ,key ,outer))
                          +emacs--map-forms)))
                 (_
                  (condition-case _
                      (+emacs--map-def (pop rest) (pop rest)
                                       (+emacs--map-keyword-to-states key)
                                       desc)
                    (error
                     (error "Not a valid `map!' property: %s" key)))
                  (setq desc nil))))

              ((+emacs--map-def key (pop rest) nil desc)
               (setq desc nil)))))

    (+emacs--map-commit)
    (macroexp-progn (nreverse (delq nil +emacs--map-forms)))))

(defun +emacs--map-append-keys (prop)
  (let ((a (plist-get +emacs--map-parent-state prop))
        (b (plist-get +emacs--map-state prop)))
    (if (and a b)
        `(general--concat t ,a ,b)
      (or a b))))

(defun +emacs--map-nested (wrapper rest)
  (+emacs--map-commit)
  (let ((+emacs--map-parent-state (+emacs--map-state)))
    (push (if wrapper
              (append wrapper (list (+emacs--map-process rest)))
            (+emacs--map-process rest))
          +emacs--map-forms)))

(defun +emacs--map-set (prop &optional value)
  (unless (equal (plist-get +emacs--map-state prop) value)
    (+emacs--map-commit))
  (setq +emacs--map-state (plist-put +emacs--map-state prop value)))

(defun +emacs--map-def (key def &optional states desc)
  (when (or (memq 'global states)
            (null states))
    (setq states (cons 'nil (delq 'global states))))
  (when desc
    (let (unquoted)
      (cond ((and (listp def)
                  (keywordp (car-safe (setq unquoted (+emacs-unquote def)))))
             (setq def (list 'quote (plist-put unquoted :which-key desc))))
            ((setq def (cons 'list
                             (if (and (equal key "")
                                      (null def))
                                 `(:ignore t :which-key ,desc)
                               (plist-put (general--normalize-extended-def def)
                                          :which-key desc))))))))
  (dolist (state states)
    (push (list key def)
          (alist-get state +emacs--map-batch-forms)))
  t)

(defun +emacs--map-commit ()
  (when +emacs--map-batch-forms
    (cl-loop with attrs = (+emacs--map-state)
             for (state . defs) in +emacs--map-batch-forms
             if (or +emacs--map-evil-p (not state))
             collect `(,(or +emacs--map-fn 'general-define-key)
                       ,@(if state `(:states ',state)) ,@attrs
                       ,@(mapcan #'identity (nreverse defs)))
             into forms
             finally do (push (macroexp-progn forms) +emacs--map-forms))
    (setq +emacs--map-batch-forms nil)))

(defun +emacs--map-state ()
  (let ((plist
         (append (list :prefix (+emacs--map-append-keys :prefix)
                       :infix  (+emacs--map-append-keys :infix)
                       :keymaps
                       (append (plist-get +emacs--map-parent-state :keymaps)
                               (plist-get +emacs--map-state :keymaps)))
                 +emacs--map-state
                 nil))
        newplist)
    (while plist
      (let ((key (pop plist))
            (val (pop plist)))
        (when (and val (not (plist-member newplist key)))
          (push val newplist)
          (push key newplist))))
    newplist))

(defmacro map! (&rest rest)
  "A convenience macro for defining keybinds, powered by `general'.
If evil isn't loaded, evil-specific bindings are ignored.

States
  :n  normal
  :v  visual
  :i  insert
  :e  emacs
  :o  operator
  :m  motion
  :r  replace
  :g global (binds the key without evil `current-global-map')
These can be combined in any order, e.g. :nvi will apply to
normal, visual and insert mode. The state resets after the
following key=>def pair. If states are omitted the keybind will
be global (no emacs state; this is different from evil's Emacs
state and will work in the absence of `evil-mode').

Properties
  :leader [...]                   an alias for (:prefix +emacs-leader-key ...)
  :localleader [...]              bind to localleader; requires a keymap
  :mode [MODE(s)] [...]           inner keybinds are applied to major MODE(s)
  :map [KEYMAP(s)] [...]          inner keybinds are applied to KEYMAP(S)
  :prefix [PREFIX] [...]          set keybind prefix for following keys. PREFIX
                                  can be a cons cell: (PREFIX . DESCRIPTION)
  :prefix-map [PREFIX] [...]      same as :prefix, but defines a prefix keymap
                                  where the following keys will be bound. DO NOT
                                  USE THIS IN YOUR PRIVATE CONFIG.
  :after [FEATURE] [...]          apply keybinds when [FEATURE] loads
  :textobj KEY INNER-FN OUTER-FN  define a text object keybind pair
  :when [CONDITION] [...]
  :unless [CONDITION] [...]

Any of the above properties may be nested, so that they only
apply to a certain group of keybinds."
  (+emacs--map-process rest))

;;; UI

;;;; Variables

(defvar +emacs-theme nil
  "A symbol representing the Emacs theme to load at startup.
Set to `nil' to load no theme at all. This variable is changed by
`load-theme'.")

(defvar +emacs-font nil
  "The default font to use.
Must be a `font-spec', a font object, an XFT font string, or an
XLFD string. This affects the `default' and `fixed-pitch' faces.
Examples:
  (setq +emacs-font (font-spec :family \"Fira Mono\" :size 12))
  (setq +emacs-font \"Terminus (TTF):pixelsize=12:antialias=off\")")

(defvar +emacs-variable-pitch-font nil
  "The font to use for variable-pitch text.
Must be a `font-spec', a font object, an XFT font string, or an
XLFD string. See `+emacs-font' for examples. An omitted font size
means to inherit `+emacs-font''s size.")

(defvar +emacs-serif-font nil
  "The default font to use for the `fixed-pitch-serif' face.
Must be a `font-spec', a font object, an XFT font string, or an
XLFD string. See `+emacs-font' for examples. An omitted font size
means to inherit `+emacs-font''s size.")

(defvar +emacs-unicode-font nil
  "Fallback font for Unicode glyphs.
Must be a `font-spec', a font object, an XFT font string, or an
XLFD string. See `+emacs-font' for examples. The defaults on
macOS and Linux are Apple Color Emoji and Symbola, respectively.
WARNING: if you specify a size for this font it will hard-lock
any usage of this font to that size. It's rarely a good idea to
do so!")

(defvar +emacs-emoji-fallback-font-families
  '("Apple Color Emoji"
    "Segoe UI Emoji"
    "Noto Color Emoji"
    "Noto Emoji")
  "A list of fallback font families to use for emojis.")

(defvar +emacs-symbol-fallback-font-families
  '("Segoe UI Symbol"
    "Apple Symbols")
  "A list of fallback font families for general symbol glyphs.")


;;;; Custom hooks

(defvar +emacs-init-ui-hook nil
  "List of hooks to run when the UI has been initialized.")

(defvar +emacs-load-theme-hook nil
  "Hook run after the theme is loaded with `load-theme' or
reloaded with `+emacs/reload-theme'.")

(defvar +emacs-switch-buffer-hook nil
  "A list of hooks run after changing the current buffer.")

(defvar +emacs-switch-window-hook nil
  "A list of hooks run after changing the focused windows.")

(defvar +emacs-switch-frame-hook nil
  "A list of hooks run after changing the focused frame.")

(defun +emacs--hook-run-switch-buffer-hooks (&optional _)
  (let ((gc-cons-threshold most-positive-fixnum)
        (inhibit-redisplay t))
    (run-hooks '+emacs-switch-buffer-hook)))

(defvar +emacs--last-frame nil)
(defun +emacs--hook-run-switch-window-or-frame-hooks (&optional _)
  (let ((gc-cons-threshold most-positive-fixnum)
        (inhibit-redisplay t))
    (unless (equal (old-selected-frame) (selected-frame))
      (run-hooks '+emacs-switch-frame-hook))
    (unless (or (minibufferp)
                (equal (old-selected-window) (minibuffer-window)))
      (run-hooks '+emacs-switch-window-hook))))

(defun +emacs--hook-protect-fallback-buffer ()
  "Don't kill the scratch buffer. Meant for
`kill-buffer-query-functions'."
  (not (eq (current-buffer) (+emacs-fallback-buffer))))

(defun +emacs--hook-highlight-non-default-indentation ()
  "Highlight whitespace that doesn't match your `indent-tabs-mode' setting.
e.g. If you indent with spaces by default, tabs will be
highlighted. If you indent with tabs, spaces at BOL are
highlighted. Does nothing if `whitespace-mode' or
'global-whitespace-mode' is already active or if the current
buffer is read-only or not file-visiting."
  (unless (or (eq major-mode 'fundamental-mode)
              (bound-and-true-p global-whitespace-mode)
              (null buffer-file-name))
    (require 'whitespace)
    (set (make-local-variable 'whitespace-style)
         (let ((style (if indent-tabs-mode '(indentation) '(tabs tab-mark))))
           (if whitespace-mode
               (cl-union style whitespace-active-style)
             style)))
    (cl-pushnew 'face whitespace-style)
    (whitespace-mode +1)))

;;;; General UX

;; Simpler confirmation prompt when killing Emacs
(setq confirm-kill-emacs #'+emacs-quit-p)

(setq uniquify-buffer-name-style 'forward
      ;; no beeping or blinking please
      ring-bell-function #'ignore
      visible-bell nil)

;; middle-click paste at point, not at click
(setq mouse-yank-at-point t)

;;;; Scrolling

(setq hscroll-margin 2
      hscroll-step 1
      scroll-conservatively 10
      scroll-margin 0
      scroll-preserve-screen-position t
      ;; Reduce cursor lag by a tiny bit by not auto-adjusting `window-vscroll'
      ;; for tall lines.
      auto-window-vscroll nil
      ;; mouse
      mouse-wheel-scroll-amount '(5 ((shift) . 2))
      mouse-wheel-progressive-speed nil)  ; don't accelerate scrolling

;; Remove hscroll-margin in shells, otherwise it causes jumpiness
(setq-hook! '(eshell-mode-hook term-mode-hook) hscroll-margin 0)


;;
;;;; Cursor

;; Don't blink the cursor, it's too distracting.
(blink-cursor-mode -1)

;; Don't blink the paren matching the one at point, it's too distracting.
(setq blink-matching-paren nil)

;; Don't stretch the cursor to fit wide characters, it is disorienting,
;; especially for tabs.
(setq x-stretch-cursor nil)


;;
;;;; Buffers

;; Make `next-buffer', `other-buffer', etc. ignore unreal buffers.
(push '(buffer-predicate . +emacs-buffer-frame-predicate) default-frame-alist)

(setq confirm-nonexistent-file-or-buffer t)

(defadvice! +emacs--advice-switch-to-fallback-buffer-maybe (&rest _)
  "Switch to `+emacs-fallback-buffer' if on last real buffer.
Advice for `kill-current-buffer'. If in a dedicated window,
delete it. If there are no real buffers left OR if all remaining
buffers are visible in other windows, switch to
`+emacs-fallback-buffer'. Otherwise, delegate to original
`kill-current-buffer'."
  :before-until #'kill-current-buffer
  (let ((buf (current-buffer)))
    (cond ((window-dedicated-p)
           (delete-window)
           t)
          ((eq buf (+emacs-fallback-buffer))
           (message "Can't kill the fallback buffer.")
           t)
          ((+emacs-real-buffer-p buf)
           (let ((visible-p (delq (selected-window) (get-buffer-window-list buf nil t))))
             (unless visible-p
               (when (and (buffer-modified-p buf)
                          (not (y-or-n-p
                                (format "Buffer %s is modified; kill anyway?"
                                        buf))))
                 (user-error "Aborted")))
             (let ((inhibit-redisplay t)
                   buffer-list-update-hook)
               (when (or ;; if there aren't more real buffers than visible
                      ;; buffers, then there are no real, non-visible buffers
                      ;; left.
                      (not (cl-set-difference (+emacs-real-buffer-list)
                                              (+emacs-visible-buffers)))
                      ;; if we end up back where we start (or previous-buffer
                      ;; returns nil), we have nowhere left to go
                      (memq (switch-to-prev-buffer nil t) (list buf 'nil)))
                 (switch-to-buffer (+emacs-fallback-buffer)))
               (unless visible-p
                 (with-current-buffer buf
                   (restore-buffer-modified-p nil))
                 (kill-buffer buf)))
             (run-hooks 'buffer-list-update-hook)
             t)))))

;;;; Fringes

;; Reduce the clutter in the fringes; we'd like to reserve that space for more
;; useful information, like git-gutter and flycheck.
(setq indicate-buffer-boundaries nil
      indicate-empty-lines nil)

;; remove continuation arrow on right fringe
(delq! 'continuation fringe-indicator-alist 'assq)

;;;; Windows/frames

;; A simple frame title
(setq frame-title-format '("%b @" (:eval (or (file-remote-p default-directory 'host) system-name)) " - Emacs")
      icon-title-format frame-title-format)

;; Don't resize emacs frames in steps
(setq frame-resize-pixelwise t
      ;; But windows
      window-resize-pixelwise nil)

;; Disable tool and scrollbars
(unless (assq 'menu-bar-lines default-frame-alist)
  (add-to-list 'default-frame-alist '(menu-bar-lines . 0))
  (add-to-list 'default-frame-alist '(tool-bar-lines . 0))
  (add-to-list 'default-frame-alist '(vertical-scroll-bars)))

;; The native border "consumes" a pixel of the fringe on righter-most splits,
;; `window-divider' does not. Available since Emacs 25.1.
(setq window-divider-default-places t
      window-divider-default-bottom-width 1
      window-divider-default-right-width 1)
(add-hook '+emacs-init-ui-hook #'window-divider-mode)

;; Prompt the user for confirmation when deleting a non-empty frame
(global-set-key [remap delete-frame] #'+emacs/delete-frame)

;; always avoid GUI
(setq use-dialog-box nil)
;; Don't display floating tooltips; display their contents in the echo-area.
(if (bound-and-true-p tooltip-mode) (tooltip-mode -1))
;; native linux tooltips are ugly
(when IS-LINUX
  (setq x-gtk-use-system-tooltips nil))

;; Favor vertical splits over horizontal ones
(setq split-width-threshold 160
      split-height-threshold nil)

;;;; Minibuffer

;; Allow for minibuffer-ception. Sometimes we need another minibuffer command
;; _while_ we're in the minibuffer.
(setq enable-recursive-minibuffers t)

;; Show current key-sequence in minibuffer, like vim does. Any feedback after
;; typing is better UX than no feedback at all.
(setq echo-keystrokes 1e-6)

;; Expand the minibuffer to fit multi-line text displayed in the echo-area. This
;; doesn't look too great with direnv, however...
(setq resize-mini-windows 'grow-only)

;; Typing yes/no is obnoxious when y/n will do
(advice-add #'yes-or-no-p :override #'y-or-n-p)

;; Try really hard to keep the cursor from getting stuck in the read-only prompt
;; portion of the minibuffer.
(setq minibuffer-prompt-properties '(read-only t intangible t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

;;;; Built-in packages

;;;;; ansi-color

;;;###package ansi-color
(setq ansi-color-for-comint-mode t)

;;;;; comint

(after! comint
  (setq comint-prompt-read-only t
        comint-buffer-maximum-size 2048)) ; double the default

;;;;; compile

(after! compile
  (setq compilation-always-kill t       ; kill compilation process before starting another
        compilation-ask-about-save nil  ; save all buffers on `compile'
        compilation-scroll-output 'first-error)
  ;; Handle ansi codes in compilation buffer
  (add-hook 'compilation-filter-hook #'+emacs--hook-apply-ansi-color-to-compilation-buffer)
  ;; Automatically truncate compilation buffers so they don't accumulate too
  ;; much data and bog down the rest of Emacs.
  (autoload 'comint-truncate-buffer "comint" nil t)
  (add-hook 'compilation-filter-hook #'comint-truncate-buffer))

;;;;; ediff

(after! ediff
  (setq ediff-diff-options "-w" ; turn off whitespace checking
        ediff-split-window-function #'split-window-horizontally
        ediff-window-setup-function #'ediff-setup-windows-plain)

  (defvar +emacs--ediff-saved-wconf nil)
  ;; Restore window config after quitting ediff
  (defhook! +emacs-ediff--hook-save-wconf ()
    "Save window config before `ediff-mode'."
    'ediff-before-setup-hook
    (setq +emacs--ediff-saved-wconf (current-window-configuration)))
  (defhook! +emacs-ediff--hook-restore-wconf ()
    "Restore window config after `ediff-mode'."
    '(ediff-quit-hook ediff-suspend-hook) :append
    (when (window-configuration-p +emacs--ediff-saved-wconf)
      (set-window-configuration +emacs--ediff-saved-wconf))))

;;;;; goto-addr

(use-package goto-addr
  :hook (text-mode . goto-address-mode)
  :hook (prog-mode . goto-address-prog-mode)
  :config
  (define-key goto-address-highlight-keymap (kbd "RET") #'goto-address-at-point))

;;;;; hl-line

(use-package hl-line
  ;; Highlights the current line
  :hook ((prog-mode text-mode conf-mode special-mode) . hl-line-mode)
  :config
  ;; Temporarily disable `hl-line' when selection is active, since it doesn't
  ;; serve much purpose when the selection is so much more visible.
  (defvar +emacs--hl-line-mode nil)

  (defhook! +emacs--hook-truly-disable-hl-line ()
    "Srsly disable `hl-line'."
    'hl-line-mode-hook
    (unless hl-line-mode
      (setq-local +emacs--hl-line-mode nil)))

  (defhook! +emacs--hook-disable-hl-line ()
    "Disable `hl-line' temporarily in evil-visual-mode."
    '(evil-visual-state-entry-hook activate-mark-hook)
    (when hl-line-mode
      (hl-line-mode -1)
      (setq-local +emacs--hl-line-mode t)))

  (defhook! +emacs--hook-enable-hl-line-maybe ()
    "Re-enable `hl-line'."
    '(evil-visual-state-exit-hook deactivate-mark-hook)
    (when +emacs--hl-line-mode
      (hl-line-mode +1))))

;;;;; winner

(use-package winner
  ;; undo/redo changes to Emacs' window layout
  :hook (+emacs-first-buffer . winner-mode)
  :preface (defvar winner-dont-bind-my-keys t) ; I'll bind keys myself
  :config
  (appendq! winner-boring-buffers
            '("*Compile-Log*" "*inferior-lisp*" "*Fuzzy Completions*"
              "*Apropos*" "*Help*" "*cvs*" "*Buffer List*" "*Ibuffer*"
              "*esh command on file*")))

;;;;; paren

(use-package paren
  ;; highlight matching delimiters
  :hook (+emacs-first-buffer . show-paren-mode)
  :config
  (setq show-paren-delay 0.1
        show-paren-highlight-openparen t
        show-paren-when-point-inside-paren t
        show-paren-when-point-in-periphery t))

;;;;; whitespace

(after! whitespace
  (setq whitespace-line-column nil
        whitespace-style
        '(face indentation tabs tab-mark spaces space-mark newline newline-mark
               trailing lines-tail)
        whitespace-display-mappings
        '((tab-mark ?\t [?› ?\t])
          (newline-mark ?\n [?¬ ?\n])
          (space-mark ?\  [?·] [?.]))))

;;;; Third party packages

;;;;; all-the-icons

(use-package all-the-icons
  :commands (all-the-icons-octicon
             all-the-icons-faicon
             all-the-icons-fileicon
             all-the-icons-wicon
             all-the-icons-material
             all-the-icons-alltheicon)
  :preface
  (setq +emacs-unicode-extra-fonts
        (list "Weather Icons"
              "github-octicons"
              "FontAwesome"
              "all-the-icons"
              "file-icons"
              "Material Icons"))
  :config
  (cond ((daemonp)
         (defadvice! +emacs--advice-disable-all-the-icons-in-tty (orig-fn &rest args)
           "Return a blank string in tty Emacs, which doesn't
support multiple fonts."
           :around '(all-the-icons-octicon
                     all-the-icons-material
                     all-the-icons-faicon
                     all-the-icons-fileicon
                     all-the-icons-wicon
                     all-the-icons-alltheicon)
           (if (display-multi-font-p)
               (apply orig-fn args)
             "")))
        ((not (display-graphic-p))
         (defadvice! +emacs--advice-disable-all-the-icons-in-tty (&rest _)
           "Return a blank string for tty users."
           :override '(all-the-icons-octicon
                       all-the-icons-material
                       all-the-icons-faicon
                       all-the-icons-fileicon
                       all-the-icons-wicon
                       all-the-icons-alltheicon)
           ""))))

;;;;; hide-mode-line

(after! hide-mode-line
  (add-hook! '(completion-list-mode-hook Man-mode-hook)
             #'hide-mode-line-mode))

;;;;; highlight-numbers

;; Many major modes do no highlighting of number literals, so we do it for them
(use-package highlight-numbers
  :hook ((prog-mode conf-mode) . highlight-numbers-mode)
  :config (setq highlight-numbers-generic-regexp "\\_<[[:digit:]]+\\(?:\\.[0-9]*\\)?\\_>"))

;;;;; image

(after! image
  (setq image-animate-loop t))

;;;;; rainbow-delimiters

;; Helps us distinguish stacked delimiter pairs, especially in parentheses-drunk
;; languages like Lisp.
(after! rainbow-delimiters
  (setq rainbow-delimiters-max-face-count 4))

;;;;; pos-tip

(after! pos-tip
  (setq pos-tip-internal-border-width 6
        pos-tip-border-width 1))

;;;; Line numbers

(setq-default display-line-numbers-width 3)

;; Show absolute line numbers for narrowed regions
(setq-default display-line-numbers-widen t)

;; line numbers in most modes
(add-hook! '(prog-mode-hook text-mode-hook conf-mode-hook)
           #'display-line-numbers-mode)

(defun +emacs--hook-enable-line-numbers ()  (display-line-numbers-mode +1))
(defun +emacs--hook-disable-line-numbers () (display-line-numbers-mode -1))

;;;; Theme & font

;; User themes should live in ~/.emacs.d/site-lisp/themes, not ~/.emacs.d
(setq custom-theme-directory (concat +emacs-private-dir "themes/"))

;; Always prioritize the user's themes above the built-in/packaged ones.
(setq custom-theme-load-path
      (cons 'custom-theme-directory
            (delq 'custom-theme-directory custom-theme-load-path)))

(defun +emacs--hook-init-fonts (&optional reload)
  "Loads `+emacs-font'."
  (cond
   (+emacs-font
    (when (or reload (daemonp))
      (set-frame-font +emacs-font t t))
    ;; Avoid `set-frame-font' at startup because it is expensive; doing extra,
    ;; unnecessary work we can avoid by setting the frame parameter directly.
    (setf (alist-get 'font default-frame-alist)
          (cond ((stringp +emacs-font) +emacs-font)
                ((fontp +emacs-font) (font-xlfd-name +emacs-font))
                ((signal 'wrong-type-argument
                         (list '(fontp stringp) +emacs-font))))))
   ((display-graphic-p)
    (setq font-use-system-font t))))

(defun +emacs--hook-init-extra-fonts (&optional reload)
  (when (fboundp 'set-fontset-font)
    (let ((fn (+emacs-rpartial #'member (font-family-list))))
      (when-let (font (cl-find-if fn +emacs-symbol-fallback-font-families))
        (set-fontset-font t 'symbol font))
      (when-let (font (cl-find-if fn +emacs-emoji-fallback-font-families))
        (set-fontset-font t 'unicode font))
      (when +emacs-unicode-font
        (set-fontset-font t 'unicode +emacs-unicode-font))))
  (apply #'custom-set-faces
         (let ((attrs '(:weight unspecified :slant unspecified :width unspecified)))
           (append (when +emacs-font
                     `((fixed-pitch ((t (:font ,+emacs-font ,@attrs))))))
                   (when +emacs-serif-font
                     `((fixed-pitch-serif ((t (:font ,+emacs-serif-font ,@attrs))))))
                   (when +emacs-variable-pitch-font
                     `((variable-pitch ((t (:font ,+emacs-variable-pitch-font ,@attrs)))))))))
  ;; Never save these settings to `custom-file'
  (dolist (sym '(fixed-pitch fixed-pitch-serif variable-pitch))
    (put sym 'saved-face nil))
  (when (or reload (daemonp))
    (set-frame-font +emacs-font t t))
  ;; Customize font logic.
  (run-hooks 'after-setting-font-hook))

(defun +emacs--hook-init-theme (&rest _)
  "Load the theme specified by `+emacs-theme' in FRAME."
  (when (and +emacs-theme (not (custom-theme-enabled-p +emacs-theme)))
    (load-theme +emacs-theme t)))

(defadvice! +emacs--advice-load-theme (orig-fn theme &optional no-confirm no-enable)
  "Record `+emacs-theme', disable old themes, and trigger
`+emacs-load-theme-hook'."
  :around #'load-theme
  ;; Run `load-theme' from an estranged buffer, where we can ensure that
  ;; buffer-local face remaps (by `mixed-pitch-mode', for instance) won't
  ;; interfere with recalculating faces in new themes.
  (with-temp-buffer
    (let ((last-themes (copy-sequence custom-enabled-themes)))
      ;; Disable previous themes so there are no conflicts. If you truly want
      ;; multiple themes enabled, then use `enable-theme' instead.
      (mapc #'disable-theme custom-enabled-themes)
      (prog1 (funcall orig-fn theme no-confirm no-enable)
        (when (and (not no-enable) (custom-theme-enabled-p theme))
          (setq +emacs-theme theme)
          (put '+emacs-theme 'previous-themes (or last-themes 'none))
          (run-hook-wrapped '+emacs-load-theme-hook #'+emacs-try-run-hook))))))


;;;; Bootstrap

(defun +emacs--hook-init-ui (&optional _)
  "Initialize user interface by applying all its advice and
hooks. These should be done as late as possible, as to
avoid/minimize prematurely triggering hooks during startup."
  (run-hook-wrapped '+emacs-init-ui-hook #'+emacs-try-run-hook)

  (add-hook 'kill-buffer-query-functions #'+emacs--hook-protect-fallback-buffer)
  (add-hook 'after-change-major-mode-hook #'+emacs--hook-highlight-non-default-indentation 'append)

  ;; Initialize `+emacs-switch-window-hook' and `+emacs-switch-frame-hook'
  (add-hook 'window-selection-change-functions #'+emacs--hook-run-switch-window-or-frame-hooks)
  ;; Initialize `+emacs-switch-buffer-hook'
  (add-hook 'window-buffer-change-functions #'+emacs--hook-run-switch-buffer-hooks)
  ;; `window-buffer-change-functions' doesn't trigger for files visited via the
  ;; server.
  (add-hook 'server-visit-hook #'+emacs--hook-run-switch-buffer-hooks)
  ;; Only execute this function once.
  (remove-hook 'window-buffer-change-functions #'+emacs--hook-init-ui))

;; Apply fonts and theme
(add-hook '+emacs-after-init-modules-hook #'+emacs--hook-init-fonts -100)
(add-hook '+emacs-load-theme-hook #'+emacs--hook-init-extra-fonts)

(add-hook (if (daemonp)
              'after-make-frame-functions
            '+emacs-init-ui-hook)
          #'+emacs--hook-init-theme -90)

;; Initialize UI as late as possible. `window-buffer-change-functions' runs
;; once, when the scratch/dashboard buffer is first displayed.
(add-hook 'window-buffer-change-functions #'+emacs--hook-init-ui -100)

;;;; Fixes/hacks

;; doesn't exist in terminal Emacs; we define it to prevent errors
(unless (fboundp 'define-fringe-bitmap)
  (fset 'define-fringe-bitmap #'ignore))

(after! whitespace
  (defun +emacs-is-childframes-p ()
    "`whitespace-mode' inundates child frames with whitespace
markers, so disable it to fix all that visual noise."
    (null (frame-parameter nil 'parent-frame)))
  (add-function :before-while whitespace-enable-predicate #'+emacs-is-childframes-p))


;;; Projects

(defvar +emacs-projectile-cache-limit 25000
  "If any project cache surpasses this many files it is purged
when quitting Emacs.")

(defvar +emacs-projectile-cache-blacklist '("~" "/tmp" "/")
  "Directories that should never be cached.")

(defvar +emacs-projectile-cache-purge-non-projects nil
  "If non-nil, non-projects are purged from the cache on
`kill-emacs-hook'.")

(defvar +emacs-projectile-fd-binary
  (or (cl-find-if #'executable-find '("fdfind" "fd"))
      "fd")
  "Name of `fd-find' executable binary")

(use-package projectile
  :commands
  (projectile-project-root
   projectile-project-name
   projectile-project-p
   projectile-locate-dominating-file
   projectile-relevant-known-projects)
  :init
  (setq projectile-enable-caching t
        projectile-cache-file (concat +emacs-cache-dir "projectile.cache")
        projectile-known-projects-file (concat +emacs-cache-dir "projectile.projects")
        projectile-globally-ignored-files '(".DS_Store" "Icon" "TAGS")
        projectile-globally-ignored-file-suffixes '(".elc" ".pyc" ".o")
        projectile-ignored-projects (list "~/" temporary-file-directory)
        projectile-ignored-project-function #'+emacs-project-ignored-p
        projectile-kill-buffers-filter 'kill-only-files
        projectile-files-cache-expire 604800 ; expire after a week
        projectile-sort-order 'recentf
        projectile-use-git-grep t
        ;; The original `projectile-default-mode-line' can be expensive over
        ;; TRAMP, so we gimp it in remote buffers.
        projectile-mode-line-function
        (lambda ()
          (if (file-remote-p default-directory)
              ""
            (projectile-default-mode-line))))

  (global-set-key [remap evil-jump-to-tag] #'projectile-find-tag)
  (global-set-key [remap find-tag]         #'projectile-find-tag)

  :config
  (add-hook 'dired-before-readin-hook #'projectile-track-known-projects-find-file-hook)
  (projectile-mode +1)

  ;; a more generic project root file
  (push ".project" projectile-project-root-files-bottom-up)
  (push (abbreviate-file-name +emacs-local-dir) projectile-globally-ignored-directories)

  ;; Per-project compilation buffers
  (setq compilation-buffer-name-function #'projectile-compilation-buffer-name
        compilation-save-buffers-predicate #'projectile-current-project-buffer-p)

  (defhook! +emacs--hook-cleanup-project-cache ()
    "Purge projectile cache entries that: a) have too many
files (see `+emacs-projectile-cache-limit'), b) represent
blacklisted directories that are too big, change too often or are
private. (see `+emacs-projectile-cache-blacklist'), c) are not
valid projectile projects."
    'kill-emacs-hook
    (when (and (bound-and-true-p projectile-projects-cache)
               projectile-enable-caching
               +emacs-interactive-p)
      (setq projectile-known-projects
            (cl-remove-if #'projectile-ignored-project-p
                          projectile-known-projects))
      (cl-loop with blacklist = (mapcar #'file-truename +emacs-projectile-cache-blacklist)
               for proot in (hash-table-keys projectile-projects-cache)
               for len = (length (gethash proot projectile-projects-cache))
               if (or (>= len +emacs-projectile-cache-limit)
                      (member (substring proot 0 -1) blacklist)
                      (and +emacs-projectile-cache-purge-non-projects
                           (not (+emacs-project-p proot)))
                      (projectile-ignored-project-p proot))
               do (remhash proot projectile-projects-cache)
               and do (remhash proot projectile-projects-cache-time)
               and do (remhash proot projectile-project-type-cache))
      (projectile-serialize-cache)))

  ;; HACK Don't rely on VCS-specific commands to generate our file lists. That's
  ;;      7 commands to maintain, versus the more generic, reliable and
  ;;      performant `fd' or `ripgrep'.
  (defadvice! +projectile--advice-only-use-generic-command (orig-fn vcs)
    "Only use `projectile-generic-command' for indexing project
files. And if it's a function, evaluate it."
    :around #'projectile-get-ext-command
    (if (and (functionp projectile-generic-command)
             (not (file-remote-p default-directory)))
        (funcall projectile-generic-command vcs)
      (let ((projectile-git-submodule-command
             (get 'projectile-git-submodule-command 'initial-value)))
        (funcall orig-fn vcs))))

  ;; `projectile-generic-command' doesn't typically support a function, but the
  ;; `+projectile--advice-only-use-generic-command' advice allows this. This way
  ;; allows projectile to adapt to remote systems (over TRAMP), rather then look
  ;; for fd/ripgrep on the remote system simply because it exists on the host.
  (put 'projectile-git-submodule-command 'initial-value projectile-git-submodule-command)
  (setq projectile-git-submodule-command nil
        projectile-indexing-method 'hybrid
        projectile-generic-command
        (lambda (_)
          ;; If fd exists, use it for git and generic projects. This is
          ;; recommended in the projectile docs.
          (cond
           ((when-let
                (bin (if (ignore-errors (file-remote-p default-directory nil t))
                         (cl-find-if (+emacs-rpartial #'executable-find t)
                                     (list "fdfind" "fd"))
                       +emacs-projectile-fd-binary))
              (concat (format "%s . -0 -H --color=never --type file --type symlink --follow --exclude .git"
                              bin)
                      (if IS-WINDOWS " --path-separator=/"))))
           ;; Otherwise, resort to ripgrep, which is also faster than find
           ((executable-find "rg" t)
            (concat "rg -0 --files --follow --color=never --hidden -g!.git"
                    (if IS-WINDOWS " --path-separator=/")))
           ("find . -type f -print0"))))

  (defadvice! +projectile--advice-default-generic-command (orig-fn &rest args)
    "If projectile can't tell what kind of project you're in, it
issues an error when using many of projectile's command, e.g.
`projectile-compile-command', `projectile-run-project',
`projectile-test-project', and `projectile-configure-project',
for instance. This suppresses the error so these commands will
still run, but prompt you for the command instead."
    :around #'projectile-default-generic-command
    (ignore-errors (apply orig-fn args))))

;;;; Project-based minor modes

(defvar +emacs-project-hook nil
  "Hook run when a project is enabled. The name of the project's
mode and its state are passed in.")

(cl-defmacro def-project-mode! (name &key
                                     modes
                                     files
                                     when
                                     match
                                     add-hooks
                                     on-load
                                     on-enter
                                     on-exit)
  "Define a project minor mode named NAME and where/how it is activated.
Project modes allow you to configure 'sub-modes' for major-modes
that are specific to a folder, project structure, framework or
whatever arbitrary context you define. These project modes can
have their own settings, keymaps, hooks, snippets, etc.

PLIST may contain any of these properties, which are all checked
to see if NAME should be activated. If they are *all* true, NAME
is activated.
  :modes MODES -- if buffers are derived from MODES (one or a list of symbols).
  :files FILES -- if project contains FILES; takes a string or a form comprised
    of nested (and ...) and/or (or ...) forms. Each path is relative to the
    project root, however, if prefixed with a '.' or '..', it is relative to the
    current buffer.
  :match REGEXP -- if file name matches REGEXP
  :when PREDICATE -- if PREDICATE returns true (can be a form or the symbol of a
    function)
  :add-hooks HOOKS -- HOOKS is a list of hooks to add this mode's hook.
  :on-load FORM -- FORM to run the first time this project mode is enabled.
  :on-enter FORM -- FORM is run each time the mode is activated.
  :on-exit FORM -- FORM is run each time the mode is disabled.
Relevant: `+emacs-project-hook'."
  (declare (indent 1))
  (let ((init-var (intern (format "%s-init" name))))
    (macroexp-progn
     (append
      (when on-load
        `((defvar ,init-var nil)))
      `((define-minor-mode ,name
          "A project minor mode generated by `def-project-mode!'."
          :init-value nil
          :lighter ""
          :keymap (make-sparse-keymap)
          (if (not ,name)
              ,on-exit
            (run-hook-with-args '+emacs-project-hook ',name ,name)
            ,(when on-load
               `(unless ,init-var
                  ,on-load
                  (setq ,init-var t)))
            ,on-enter))
        (dolist (hook ,add-hooks)
          (add-hook ',(intern (format "%s-hook" name)) hook)))
      (cond ((or files modes when)
             (cl-check-type files (or null list string))
             (let ((fn
                    `(lambda ()
                       (and (not (bound-and-true-p ,name))
                            (and buffer-file-name (not (file-remote-p buffer-file-name nil t)))
                            ,(or (null match)
                                 `(if buffer-file-name (string-match-p ,match buffer-file-name)))
                            ,(or (null files)
                                 ;; Wrap this in `eval' to prevent eager
                                 ;; expansion of `project-file-exists-p!' from
                                 ;; pulling in autoloaded files prematurely.
                                 `(eval
                                   '(project-file-exists-p!
                                     ,(if (stringp (car files)) (cons 'and files) files))))
                            ,(or when t)
                            (,name 1)))))
               (if modes
                   `((dolist (mode ,modes)
                       (let ((hook-name
                              (intern (format "+emacs--hook-enable-%s%s" ',name
                                              (if (eq mode t) "" (format "-in-%s" mode))))))
                         (fset hook-name #',fn)
                         (if (eq mode t)
                             (add-to-list 'auto-minor-mode-magic-alist (cons hook-name #',name))
                           (add-hook (intern (format "%s-hook" mode)) hook-name)))))
                 `((add-hook 'change-major-mode-after-body-hook #',fn)))))
            (match
             `((add-to-list 'auto-minor-mode-alist (cons ,match #',name)))))))))

;;; Editor

(defvar +emacs-detect-indentation-excluded-modes '(fundamental-mode so-long-mode)
  "A list of major modes in which indentation should be
automatically detected.")

(defvar-local +emacs-inhibit-indent-detection nil
  "A buffer-local flag that indicates whether `dtrt-indent'
should try to detect indentation settings or not. This should be
set by editorconfig if it successfully sets
indent_style/indent_size.")

(defvar +emacs-inhibit-large-file-detection nil
  "If non-nil, inhibit large/long file detection when opening
files.")

(defvar +emacs-large-file-p nil)
(put '+emacs-large-file-p 'permanent-local t)

(defvar +emacs-large-file-size-alist '(("." . 1.0))
  "An alist mapping regexps (like `auto-mode-alist') to filesize
thresholds. If a file is opened and discovered to be larger than
the threshold, performs emergency optimizations to prevent Emacs
from hanging, crashing or becoming unusably slow. These
thresholds are in MB, and is used by
`+emacs--advice-optimize-for-large-files'.")

(defvar +emacs-large-file-excluded-modes
  '(so-long-mode special-mode archive-mode tar-mode jka-compr
                 git-commit-mode image-mode doc-view-mode doc-view-mode-maybe
                 ebrowse-tree-mode pdf-view-mode tags-table-mode)
  "Major modes that `+emacs--advice-optimize-for-large-files'
will ignore.")

;;;; File handling

(defadvice! +emacs--advice-prepare-for-large-files (size _ filename &rest _)
  "Sets `+emacs-large-file-p' if the file is considered large.
Uses `+emacs-large-file-size-alist' to determine when a file is
too large. When `+emacs-large-file-p' is set, other plugins can
detect this and reduce their runtime costs (or disable
themselves) to ensure the buffer is as fast as possible."
  :before #'abort-if-file-too-large
  (and (numberp size)
       (null +emacs-inhibit-large-file-detection)
       (ignore-errors
         (> size
            (* 1024 1024
               (assoc-default filename +emacs-large-file-size-alist
                              #'string-match-p))))
       (setq-local +emacs-large-file-p size)))

(defhook! +emacs--hook-optimize-for-large-files ()
  "Trigger `so-long-minor-mode' if the file is large."
  'find-file-hook
  (when (and +emacs-large-file-p buffer-file-name)
    (if (or +emacs-inhibit-large-file-detection
            (memq major-mode +emacs-large-file-excluded-modes))
        (kill-local-variable '+emacs-large-file-p)
      (when (fboundp 'so-long-minor-mode) ; in case the user disabled it
        (so-long-minor-mode +1))
      (message "Large file detected! Cutting a few corners to improve performance..."))))

;; Usually we do not want to follow symlinks, as it changes `default-directory'
;; and the context we working in. Use `+emacs/toggle-symlink' to change between
;; the link and the source. However, if the symlink points towards a
;; version-controlled file, we usually do want to follow the link.
(setq vc-follow-symlinks t)
;; When we do that, we record where we are coming from, so we can still toggle
;; between the link and the source.
(defvar-local +emacs--symlink-origin nil)
(defadvice! +emacs--record-symlink-origin (orig-fan)
  "Save the origin after following a symlink."
  :around #'vc-follow-link
  (let ((origin (buffer-file-name)))
    (funcall orig-fan)
    (setq-local +emacs--symlink-origin origin)))

;; Disable the warning "X and Y are the same file". It's fine to ignore this
;; warning as it will redirect you to the existing buffer anyway.
(setq find-file-suppress-same-file-warnings t)

;; Create missing directories when we open a file that doesn't exist under a
;; directory tree that may not exist.
(defhook! +emacs-create-missing-directories-h ()
  "Automatically create missing directories when creating new
files."
  'find-file-not-found-functions
  (unless (file-remote-p buffer-file-name)
    (let ((parent-directory (file-name-directory buffer-file-name)))
      (and (not (file-directory-p parent-directory))
           (y-or-n-p (format "Directory `%s' does not exist! Create it?"
                             parent-directory))
           (progn (make-directory parent-directory 'parents)
                  t)))))

;; Backups & Autosave

(setq create-lockfiles nil
      make-backup-files nil
      ;; Some sensible defaults:
      version-control t     ; number each backup file
      backup-by-copying t   ; instead of renaming current file (clobbers links)
      delete-old-versions t ; clean up after itself
      kept-old-versions 5
      kept-new-versions 5
      backup-directory-alist (list (cons "." (concat +emacs-cache-dir "backup/")))
      tramp-backup-directory-alist backup-directory-alist)

;; Turn on auto-save, so we have a fallback in case of crashes or lost data. Use
;; `recover-file' or `recover-session' to recover them.
(setq auto-save-default t
      ;; Don't auto-disable auto-save after deleting big chunks. This defeats
      ;; the purpose of a failsafe. This adds the risk of losing the data we
      ;; just deleted, but I believe that's VCS's jurisdiction, not ours.
      auto-save-include-big-deletions t
      ;; Keep it out of `+emacs-dir' or the local directory.
      auto-save-list-file-prefix (concat +emacs-cache-dir "autosave/")
      tramp-auto-save-directory  (concat +emacs-cache-dir "tramp-autosave/")
      auto-save-file-name-transforms
      (list (list "\\`/[^/]*:\\([^/]*/\\)*\\([^/]*\\)\\'"
                  ;; Prefix tramp autosaves to prevent conflicts with local ones
                  (concat auto-save-list-file-prefix "tramp-\\2") t)
            (list ".*" auto-save-list-file-prefix t)))

(defhook! +emacs--hook-guess-mode ()
  "Guess mode when saving a file in `fundamental-mode'."
  'after-save-hook
  (when (eq major-mode 'fundamental-mode)
    (let ((buffer (or (buffer-base-buffer) (current-buffer))))
      (and (buffer-file-name buffer)
           (eq buffer (window-buffer (selected-window))) ; only visible buffers
           (set-auto-mode)))))


;;;; Formatting

;; Indentation
(setq-default indent-tabs-mode nil
              tab-width 4)
;; Only affect indentation
(setq tabify-regexp "^\t* [ \t]+")
;; Still good
(setq-default fill-column 80)
;; Word wrapping
(setq-default word-wrap t
              truncate-lines t)
(setq truncate-partial-width-windows nil)

(setq sentence-end-double-space nil
      require-final-newline t)

;; Favor hard-wrapping in text modes
(add-hook 'text-mode-hook #'auto-fill-mode)


;;;; Clipboard / kill-ring

;; Eliminate duplicates in the kill ring. That is, if you kill the same thing
;; twice, you won't have to use M-y twice to get past it to older entries in the
;; kill ring.
(setq kill-do-not-save-duplicates t)


;;;; Extra file extensions to support

(nconc
 auto-mode-alist
 '(("/LICENSE\\'" . text-mode)
   ("\\.log\\'" . text-mode)
   ("rc\\'" . conf-mode)
   ("\\.\\(?:hex\\|nes\\)\\'" . hexl-mode)))


;;;; Built-in plugins

;;;;; autorevert

(use-package autorevert
  ;; revert buffers when their files/state have changed
  :hook (focus-in . +emacs--hook-auto-revert-buffers)
  :hook (after-save . +emacs--hook-auto-revert-buffers)
  :hook (+emacs-switch-buffer . +emacs--hook-auto-revert-buffer)
  :hook (+emacs-switch-window . +emacs--hook-auto-revert-buffer)
  :config
  (setq auto-revert-verbose t           ; let us know when it happens
        auto-revert-use-notify nil
        auto-revert-stop-on-user-input nil
        ;; Only prompts for confirmation when buffer is unsaved.
        revert-without-query (list "."))
  ;; Instead of using `auto-revert-mode' or `global-auto-revert-mode', we employ
  ;; lazy auto reverting on `focus-in-hook' and `+emacs-switch-buffer-hook'.
  ;;
  ;; This is because autorevert abuses the heck out of inotify handles which can
  ;; grind Emacs to a halt if you do expensive IO (outside of Emacs) on the
  ;; files you have open (like compression). We only really need to revert
  ;; changes when we switch to a buffer or when we focus the Emacs frame.
  (defun +emacs--hook-auto-revert-buffer ()
    "Auto revert current buffer, if necessary."
    (unless (or auto-revert-mode (active-minibuffer-window))
      (let ((auto-revert-mode t))
        (auto-revert-handler))))

  (defun +emacs--hook-auto-revert-buffers ()
    "Auto revert stale buffers in visible windows, if necessary."
    (dolist (buf (+emacs-visible-buffers))
      (with-current-buffer buf
        (+emacs--hook-auto-revert-buffer)))))

;;;;; recentf

(use-package recentf
  :defer-incrementally easymenu tree-widget timer
  :hook (+emacs-first-file . recentf-mode)
  :commands recentf-open-files
  :custom (recentf-save-file (concat +emacs-cache-dir "recentf"))
  :config
  (setq recentf-auto-cleanup nil
        recentf-max-saved-items 200)

  (defun +emacs--recent-file-truename (file)
    "Resolve symlinks, strip out the /sudo:X@ prefix in local
tramp paths, and abbreviate $HOME"
    (if (or (not (file-remote-p file))
            (equal "sudo" (file-remote-p file 'method)))
        (abbreviate-file-name (file-truename (tramp-file-name-localname tfile)))
      file))

  (add-to-list 'recentf-filename-handlers #'+emacs--recent-file-truename)
  ;; Text properties inflate the size of recentf's files, and there is no
  ;; purpose in persisting them (Must be first in the list!)
  (add-to-list 'recentf-filename-handlers #'substring-no-properties)

  (defhook! +emacs--hook-recentf-touch-buffer ()
    "Bump file in recent file list when it is switched or written
to."
    '(+emacs-switch-window-hook write-file-functions)
    (when buffer-file-name
      (recentf-add-file buffer-file-name))
    ;; Return nil to call from `write-file-functions'
    nil)

  (defhook! +emacs--hook-recentf-add-dired-directory ()
    "Add dired directories to recentf file list."
    'dired-mode-hook
    (recentf-add-file default-directory))

  ;; The most sensible time to clean up your recent files list is when you quit
  ;; Emacs (unless this is a long-running daemon session).
  (setq recentf-auto-cleanup (if (daemonp) 300))
  (when +emacs-interactive-p
    (add-hook 'kill-emacs-hook #'recentf-cleanup)))

;;;;; savehist

(use-package savehist
  ;; persist variables across sessions
  :defer-incrementally custom
  :hook (+emacs-first-input . savehist-mode)
  :custom (savehist-file (concat +emacs-cache-dir "savehist"))
  :config
  (setq savehist-save-minibuffer-history t
        savehist-autosave-interval nil ; save on kill only
        savehist-additional-variables
        '(kill-ring
          register-alist
          mark-ring
          global-mark-ring
          search-ring
          regexp-search-ring))

  (defhook! +savehist--hook-unpropertize-variables ()
    "Remove text properties from `kill-ring' in the interest of
shrinking the savehist file."
    'savehist-save-hook
    (setq kill-ring
          (mapcar #'substring-no-properties
                  (cl-remove-if-not #'stringp kill-ring))
          register-alist
          (cl-loop for (reg . item) in register-alist
                   if (stringp item)
                   collect (cons reg (substring-no-properties item))
                   else collect (cons reg item))))

  (defhook! +savehist--hook-remove-unprintable-registers ()
    "Remove unwriteable registers (e.g. containing window configurations).
Otherwise, `savehist' would discard `register-alist' entirely if
we don't omit the unwritable tidbits."
    'savehist-save-hook
    ;; Save new value in the temp buffer savehist is running
    ;; `savehist-save-hook' in. We don't want to actually remove the
    ;; unserializable registers in the current session!
    (setq-local register-alist
                (cl-remove-if-not #'savehist-printable register-alist))))

;;;;; saveplace

(use-package saveplace
  ;; persistent point location in buffers
  :hook (+emacs-first-file . save-place-mode)
  :custom (save-place-file (concat +emacs-cache-dir "saveplace"))
  :config
  (defadvice! +emacs--advice-recenter-on-load-saveplace (&rest _)
    "Recenter on cursor when loading a saved place."
    :after-while #'save-place-find-file-hook
    (if buffer-file-name (ignore-errors (recenter))))

  (defadvice! +emacs--advice-inhibit-saveplace-in-long-files (orig-fn &rest args)
    "Inhibit saveplace in long files."
    :around #'save-place-to-alist
    (unless +emacs-large-file-p
      (apply orig-fn args)))

  (defadvice! +saveplace--advice-dont-prettify-saveplace-cache (orig-fn)
    "`save-place-alist-to-file' uses `pp' to prettify the contents of its cache.
`pp' can be expensive for longer lists, and there's no reason to
prettify cache files, so we replace calls to `pp' with the much
faster `prin1'."
    :around #'save-place-alist-to-file
    (letf! ((#'pp #'prin1)) (funcall orig-fn))))

;;;;; server

(use-package server
  :when (display-graphic-p)
  :after-call pre-command-hook after-find-file focus-out-hook
  :defer 1
  :init
  (when-let ((name (getenv "EMACS_SERVER_NAME")))
    (setq server-name name))
  :config
  (setq server-auth-dir (concat +emacs-dir "server/"))
  (unless (server-running-p)
    (server-start)))


(after! tramp
  (setq remote-file-name-inhibit-cache 60
        tramp-completion-reread-directory-timeout 60
        tramp-verbose 1
        vc-ignore-dir-regexp (format "%s\\|%s\\|%s"
                                     vc-ignore-dir-regexp
                                     tramp-file-name-regexp
                                     "[/\\\\]node_modules")))


;;;; Packages

;;;;; better-jumper

(use-package better-jumper
  :hook (+emacs-first-input . better-jumper-mode)
  :commands +emacs--advice-set-jump +emacs--advice-set-jump-maybe +emacs--hook-set-jump
  :init
  (global-set-key [remap evil-jump-forward]  #'better-jumper-jump-forward)
  (global-set-key [remap evil-jump-backward] #'better-jumper-jump-backward)
  (global-set-key [remap xref-pop-marker-stack] #'better-jumper-jump-backward)
  (global-set-key [remap xref-go-back] #'better-jumper-jump-backward)
  (global-set-key [remap xref-go-forward] #'better-jumper-jump-forward)
  :config
  (defun +emacs--advice-set-jump (orig-fn &rest args)
    "Set a jump point and ensure ORIG-FN doesn't set any new jump
points."
    (better-jumper-set-jump (if (markerp (car args)) (car args)))
    (let ((evil--jumps-jumping t)
          (better-jumper--jumping t))
      (apply orig-fn args)))

  (defun +emacs--advice-set-jump-maybe (orig-fn &rest args)
    "Set a jump point if ORIG-FN returns non-nil."
    (let ((origin (point-marker))
          (result
           (let* ((evil--jumps-jumping t)
                  (better-jumper--jumping t))
             (apply orig-fn args))))
      (unless result
        (with-current-buffer (marker-buffer origin)
          (better-jumper-set-jump
           (if (markerp (car args))
               (car args)
             origin))))
      result))

  (defun +emacs--hook-set-jump ()
    "Run `better-jumper-set-jump' but return nil, for
short-circuiting hooks."
    (better-jumper-set-jump)
    nil)
  (advice-add #'kill-current-buffer :around #'+emacs--advice-set-jump)
  (advice-add #'imenu :around #'+emacs--advice-set-jump))

;;;;; dtrt-indent

(use-package dtrt-indent
  ;; Automatic detection of indent settings
  :when +emacs-interactive-p
  :defer t
  :init
  (defhook! +emacs--hook-detect-indentation ()
    "Detect indentation settings."
    '(change-major-mode-after-body-hook read-only-mode-hook)
    (unless (or (not after-init-time)
                +emacs-inhibit-indent-detection
                +emacs-large-file-p
                (memq major-mode +emacs-detect-indentation-excluded-modes)
                (member (substring (buffer-name) 0 1) '(" " "*")))
      (dtrt-indent-mode +1)))

  :config
  (setq dtrt-indent-run-after-smie t)
  (setq dtrt-indent-max-lines 2000)
  ;; always keep tab-width up-to-date
  (push '(t tab-width) dtrt-indent-hook-generic-mapping-list))

;;;;; expand-region

(use-package expand-region
  :commands (er/contract-region er/mark-symbol er/mark-word)
  :config
  (defadvice! +emacs--advice-quit-expand-region (&rest _)
    "Properly abort an expand-region region."
    :before '(evil-escape +emacs/escape)
    (when (memq last-command '(er/expand-region er/contract-region))
      (er/contract-region 0))))

;;;;; helpful

;; Standard location for the Emacs source code
(setq source-directory (concat +emacs-etc-dir "src/"))

;; This is initialized to nil by `find-func' if the source is not cloned when
;; the library is loaded
(setq find-function-C-source-directory
      (expand-file-name "src" source-directory))

(defun +emacs-clone-emacs-source-maybe ()
  "Prompt user to clone Emacs source repository if needed."
  (when (and (not (file-directory-p source-directory))
             (not (get-buffer "*clone-emacs-src*"))
             (yes-or-no-p "Clone Emacs source repository? "))
    (make-directory (file-name-directory source-directory) 'parents)
    (let ((branch (concat "emacs-" emacs-version))
          (compilation-buffer-name-function
           (lambda (&rest _)
             "*clone-emacs-src*")))
      (save-current-buffer
        (compile
         (format
          "git clone -b %s --depth 1 https://github.com/emacs-mirror/emacs.git %s"
          (shell-quote-argument branch)
          (shell-quote-argument source-directory)))))))

(after! find-func
  (defadvice! +find-func--advice-clone-emacs-source (&rest _)
    "Clone Emacs source if needed to view definition."
    :before #'find-function-C-source
    (+emacs-clone-emacs-source-maybe)))

(use-package helpful
  :commands helpful--read-symbol
  :hook (helpful-mode . visual-line-mode)
  :init
  ;; Make apropos omnipotent. It's more useful this way.
  (setq apropos-do-all t)


  (global-set-key [remap describe-function] #'helpful-callable)
  (global-set-key [remap describe-command]  #'helpful-command)
  (global-set-key [remap describe-variable] #'helpful-variable)
  (global-set-key [remap describe-key]      #'helpful-key)
  (global-set-key [remap describe-symbol]   #'helpful-symbol)

  (defun +emacs--advice-use-helpful (orig-fn &rest args)
    "Force ORIG-FN to use helpful instead of the old describe-*
commands."
    (letf! ((#'describe-function #'helpful-function)
            (#'describe-variable #'helpful-variable))
      (apply orig-fn args)))

  (after! apropos
    ;; patch apropos buttons to call helpful instead of help
    (dolist (fun-bt '(apropos-function apropos-macro apropos-command))
      (button-type-put
       fun-bt 'action
       (lambda (button)
         (helpful-callable (button-get button 'apropos-symbol)))))
    (dolist (var-bt '(apropos-variable apropos-user-option))
      (button-type-put
       var-bt 'action
       (lambda (button)
         (helpful-variable (button-get button 'apropos-symbol))))))
  :config
  (defadvice! +helpful--advice-clone-emacs-source (library-name)
    "Prompt user to clone Emacs source code when looking up functions.
Otherwise, it only happens when looking up variables, for some
bizarre reason."
    :before #'helpful--library-path
    (when (member (file-name-extension library-name) '("c" "rs"))
      (+emacs-clone-emacs-source-maybe))))

(after! imenu
  (add-hook 'imenu-after-jump-hook #'recenter))

;;;;; smartparens

(use-package smartparens
  :hook (+emacs-first-buffer . smartparens-global-mode)
  :commands
  (sp-pair
   sp-local-pair
   sp-with-modes
   sp-point-in-comment
   sp-point-in-string)
  :config
  (add-to-list 'sp-lisp-modes 'sly-mrepl-mode)

  (require 'smartparens-config)

  (setq sp-highlight-pair-overlay nil
        sp-highlight-wrap-overlay nil
        sp-highlight-wrap-tag-overlay nil)
  (with-eval-after-load 'evil
    (setq sp-show-pair-from-inside t
          sp-cancel-autoskip-on-backward-movement nil))
  (setq sp-max-pair-length 4
        sp-max-prefix-length 25)

  ;; autopairing in `eval-expression' and `evil-ex'
  (defhook! +emacs--hook-init-smartparens-in-eval-expression ()
    "Enable `smartparens-mode' in the minibuffer for
`eval-expression'. This includes everything that calls
`read--expression', e.g. `edebug-eval-expression' Only enable it
if `smartparens-global-mode' is on."
    'eval-expression-minibuffer-setup-hook
    (when smartparens-global-mode (smartparens-mode +1)))

  (defhook! +emacs--hook-init-smartparens-in-minibuffer-maybe ()
    "Enable `smartparens' for non-`eval-expression' commands.
Only enable `smartparens-mode' if `smartparens-global-mode' is
on."
    'minibuffer-setup-hook
    (when (and smartparens-global-mode (memq this-command '(evil-ex)))
      (smartparens-mode +1)))

  (sp-local-pair '(minibuffer-mode minibuffer-inactive-mode) "'" nil :actions nil)
  (sp-local-pair '(minibuffer-mode minibuffer-inactive-mode) "`" nil :actions nil)

  ;; smartparens breaks evil-mode's replace state
  (defvar +emacs-buffer-smartparens-mode nil)
  (defhook! +smartparens--hook-enable-smartparens-mode-maybe ()
    "Re-enable `smartparens-mode'."
    'evil-replace-state-exit-hook
    (when +emacs-buffer-smartparens-mode
      (turn-on-smartparens-mode)
      (kill-local-variable '+emacs-buffer-smartparens-mode)))
  (defhook! +smartparens--hook-disable-smartparens-mode-maybe ()
    "Disable `smartparens-mode'."
    'evil-replace-state-entry-hook
    (when smartparens-mode
      (setq-local +emacs-buffer-smartparens-mode t)
      (turn-off-smartparens-mode))))

;;;;; so-long

(use-package so-long
  :hook (+emacs-first-file . global-so-long-mode)
  :config
  ;; reduce false positives w/ larger threshold
  (setq so-long-threshold 400)
  (delq! 'font-lock-mode so-long-minor-modes)
  (delq! 'display-line-numbers-mode so-long-minor-modes)
  (delq! 'buffer-read-only so-long-variable-overrides 'assq)
  (add-to-list 'so-long-variable-overrides '(font-lock-maximum-decoration . 1))
  ;; ...and insist that save-place not operate in large/long files
  (add-to-list 'so-long-variable-overrides '(save-place-alist . nil))
  (appendq! so-long-minor-modes
            '(flycheck-mode
              eldoc-mode
              smartparens-mode
              highlight-numbers-mode
              better-jumper-local-mode
              ws-butler-mode
              auto-composition-mode
              undo-tree-mode
              highlight-indent-guides-mode
              hl-fill-column-mode))
  (defun +emacs-buffer-has-long-lines-p ()
    ;; HACK Fix #2183: `so-long-detected-long-line-p' tries to parse comment
    ;;      syntax, but in some buffers comment state isn't initialized, leading
    ;;      to a wrong-type-argument: stringp error.
    (unless (bound-and-true-p visual-line-mode)
      (let ((so-long-skip-leading-comments
             (bound-and-true-p comment-use-syntax)))
        (so-long-detected-long-line-p))))
  (setq so-long-predicate #'+emacs-buffer-has-long-lines-p))

;;;;; undo-tree

(use-package undo-tree
  ;; Branching & persistent undo
  :hook (+emacs-first-buffer . global-undo-tree-mode)
  :custom undo-tree-history-directory-alist `(("." . ,(concat +emacs-cache-dir "undo-tree-hist/")))
  :config
  (setq undo-tree-visualizer-diff t
        undo-tree-auto-save-history t
        undo-tree-enable-undo-in-region t
        ;; Increase undo-limits by a factor of ten to avoid emacs prematurely
        ;; truncating the undo history and corrupting the tree. See
        ;; https://github.com/syl20bnr/spacemacs/issues/12110
        undo-limit 800000
        undo-strong-limit 12000000
        undo-outer-limit 128000000)

  ;; Compress undo-tree history files with zstd, if available. File size isn't
  ;; the (only) concern here: the file IO barrier is slow for Emacs to cross;
  ;; reading a tiny file and piping it in-memory through zstd is *slightly*
  ;; faster than Emacs reading the entire undo-tree file from the get go (on
  ;; SSDs). Whether or not that's true in practice, we still enjoy zstd's ~80%
  ;; file savings (these files add up over time and zstd is so incredibly fast).
  (when (executable-find "zstd")
    (defadvice! +undo-tree--advice-make-history-save-file-name (file)
      "Compress `undo-tree' history save file."
      :filter-return #'undo-tree-make-history-save-file-name
      (concat file ".zst")))

  ;; Strip text properties from undo-tree data to stave off bloat. File size
  ;; isn't the concern here; undo cache files bloat easily, which can cause
  ;; freezing, crashes, GC-induced stuttering or delays when opening files.
  (defadvice! +undo-tree---advice-strip-text-properties (&rest _)
    "Strip text properties from `undo-tree' save file."
    :before #'undo-list-transfer-to-tree
    (dolist (item buffer-undo-list)
      (and (consp item)
           (stringp (car item))
           (setcar item (substring-no-properties (car item))))))

  ;; Undo-tree is too chatty about saving its history files. This doesn't
  ;; totally suppress it logging to *Messages*, it only stops it from appearing
  ;; in the echo-area.
  (advice-add #'undo-tree-save-history :around #'+emacs--advice-shut-up))

;;;;; ws-butler

(use-package ws-butler
  ;; a less intrusive `delete-trailing-whitespaces' on save
  :hook (+emacs-first-buffer . ws-butler-global-mode)
  :config
  (setq ws-butler-global-exempt-modes
        (append ws-butler-global-exempt-modes
                '(special-mode comint-mode term-mode eshell-mode))))
