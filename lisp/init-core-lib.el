;; lisp/init-core-lib.el -*- lexical-binding: t; -*-

;; Detect stale bytecode
;; If Emacs version changed, the bytecode is no longer valid and we must
;; recompile.
(eval
 `(unless (equal
           (list
            (emacs-version)
            (concat (file-name-sans-extension
                     (cond ((bound-and-true-p byte-compile-current-file))
                           (load-file-name)
                           ((stringp (car-safe current-load-list))
                            (car current-load-list))
                           (buffer-file-name)
                           ((error "Cannot get this file-path"))))
                    ".el"))
           ',(eval-when-compile
               (list
                (emacs-version)
                (cond ((bound-and-true-p byte-compile-current-file))
                      (load-file-name)
                      ((stringp (car-safe current-load-list))
                       (car current-load-list))
                      (buffer-file-name)
                      ((error "Cannot get this file-path"))))))
    (throw 'stale-bytecode nil)))


;;
;;; Helpers

(defun +emacs--resolve-hook-forms (hooks)
  "Converts a list of modes into a list of hook symbols.
If a mode is quoted, it is left as is. If the entire HOOKS list
is quoted, the list is returned as-is."
  (declare (pure t) (side-effect-free t))
  (let ((hook-list (+emacs-enlist (+emacs-unquote hooks))))
    (if (eq (car-safe hooks) 'quote)
        hook-list
      (cl-loop for hook in hook-list
               if (eq (car-safe hook) 'quote)
               collect (cadr hook)
               else collect (intern (format "%s-hook" (symbol-name hook)))))))

(defun +emacs--setq-hook-fns (hooks rest &optional singles)
  (unless (or singles (= 0 (% (length rest) 2)))
    (signal 'wrong-number-of-arguments (list #'evenp (length rest))))
  (cl-loop with vars = (let ((args rest)
                             vars)
                         (while args
                           (push (if singles
                                     (list (pop args))
                                   (cons (pop args) (pop args)))
                                 vars))
                         (nreverse vars))
           for hook in (+emacs--resolve-hook-forms hooks)
           for mode = (string-remove-suffix "-hook" (symbol-name hook))
           append
           (cl-loop for (var . val) in vars
                    collect
                    (list var val hook
                          (intern (format "+emacs--hook-setq-%s-for-%s"
                                          var mode))))))


;;
;;; Public library

(defun +emacs-unquote (exp)
  "Return EXP unquoted."
  (declare (pure t) (side-effect-free t))
  (while (memq (car-safe exp) '(quote function))
    (setq exp (cadr exp)))
  exp)

(defun +emacs-enlist (exp)
  "Return EXP wrapped in a list, or as-is if already a list."
  (declare (pure t) (side-effect-free t))
  (if (proper-list-p exp) exp (list exp)))

(defun +emacs-keyword-intern (str)
  "Converts STR (a string) into a keyword (`keywordp')."
  (declare (pure t) (side-effect-free t))
  (cl-check-type str string)
  (intern (concat ":" str)))

(defun +emacs-keyword-name (keyword)
  "Returns the string name of KEYWORD (`keywordp') minus the
leading colon."
  (declare (pure t) (side-effect-free t))
  (cl-check-type keyword keyword)
  (substring (symbol-name keyword) 1))

(defmacro +emacs-log (format-string &rest args)
  "Log to *Messages* if `+emacs-debug-p' is on.
Does not interrupt the minibuffer if it is in use, but still logs
to *Messages*. Accepts the same arguments as `message'."
  `(when +emacs-debug-p
     (let ((inhibit-message (active-minibuffer-window)))
       (message
        ,(concat (propertize "EMACSLOG " 'face 'font-lock-comment-face)
                 (when (bound-and-true-p +emacs--current-module)
                   (propertize
                    (format "[%s/%s] "
                            (+emacs-keyword-name (car +emacs--current-module))
                            (cdr +emacs--current-module))
                    'face 'warning))
                 format-string)
        ,@args))))

(defalias '+emacs-partial #'apply-partially)

(defun +emacs-rpartial (fn &rest args)
  "Return a function that is a partial application of FUN to right-hand ARGS.

ARGS is a list of the last N arguments to pass to FUN. The result
is a new function which does the same as FUN, except that the
last N arguments are fixed at the values with which this function
was called."
  (lambda (&rest pre-args)
    (apply fn (append pre-args args))))

(defun +emacs-lookup-key (keys &rest keymaps)
  "Like `lookup-key', but search active keymaps if KEYMAP is
omitted."
  (if keymaps
      (cl-some (+emacs-rpartial #'lookup-key keys) keymaps)
    (cl-loop for keymap
             in (append (cl-loop for alist in emulation-mode-map-alists
                                 if (boundp alist)
                                 append (mapcar #'cdr (symbol-value alist)))
                        (list (current-local-map))
                        (mapcar #'cdr minor-mode-overriding-map-alist)
                        (mapcar #'cdr minor-mode-alist)
                        (list (current-global-map)))
             if (keymapp keymap)
             if (lookup-key keymap keys)
             return it)))


;;
;;; Convenience

(defun dir! ()
  "Returns the directory of the emacs lisp file this macro is
called from."
  (when-let (path (file!))
    (directory-file-name (file-name-directory path))))

(defun file! ()
  "Return the emacs lisp file this macro is called from."
  (cond ((bound-and-true-p byte-compile-current-file))
        (load-file-name)
        ((stringp (car-safe current-load-list))
         (car current-load-list))
        (buffer-file-name)
        ((error "Cannot get this file-path"))))

(defmacro letf! (bindings &rest body)
  "Temporarily rebind function, macros, and advice in BODY.
Intended as syntax sugar for `cl-letf', `cl-labels',
`cl-macrolet', and temporary advice. BINDINGS is either:
  A list of, or a single, `defun', `defun*', `defmacro', or
  `defadvice' forms.
  A list of (PLACE VALUE) bindings as `cl-letf*' would accept.
TYPE is one of:
  `defun' (uses `cl-letf')
  `defun*' (uses `cl-labels'; allows recursive references)
  `defmacro' (uses `cl-macrolet')
  `defadvice' (uses `defadvice!' before BODY, then `undefadvice!'
  after)
NAME, ARGLIST, and BODY are the same as `defun', `defun*',
`defmacro', and `defadvice!', respectively.

\(fn ((TYPE NAME ARGLIST &rest BODY) ...) BODY...)"
  (declare (indent defun))
  (setq body (macroexp-progn body))
  (when (memq (car bindings) '(defun defun* defmacro defadvice))
    (setq bindings (list bindings)))
  (dolist (binding (reverse bindings) body)
    (let ((type (car binding))
          (rest (cdr binding)))
      (setq
       body (pcase type
              (`defmacro `(cl-macrolet ((,@rest)) ,body))
              (`defadvice `(progn (defadvice! ,@rest)
                                  (unwind-protect ,body (undefadvice! ,@rest))))
              ((or `defun `defun*)
               `(cl-letf ((,(car rest) (symbol-function #',(car rest))))
                  (ignore ,(car rest))
                  ,(if (eq type 'defun*)
                       `(cl-labels ((,@rest)) ,body)
                     `(cl-letf (((symbol-function #',(car rest))
                                 (fn! ,(cadr rest) ,@(cddr rest))))
                        ,body))))              (_
               (when (eq (car-safe type) 'function)
                 (setq type (list 'symbol-function type)))
               (list 'cl-letf (list (cons type rest)) body)))))))

(defmacro quiet! (&rest forms)
  "Run FORMS without generating any output.
This silences calls to `message', `load-file', `write-region' and
anything that writes to `standard-output'."
  `(if +emacs-debug-p
       (progn ,@forms)
     ,(if +emacs-interactive-p
          `(let ((inhibit-message t)
                 (save-silently t))
             (prog1 ,@forms (message "")))
        `(letf! ((standard-output (lambda (&rest _)))
                 (defun message (&rest _))
                 (defun load (file &optional noerror nomessage nosuffix must-suffix)
                   (funcall load file noerror t nosuffix must-suffix))
                 (defun write-region (start end filename &optional append visit lockname mustbenew)
                   (unless visit (setq visit 'no-message))
                   (funcall write-region start end filename append visit lockname mustbenew)))
           ,@forms))))


;;
;;; Eval/compilation

(defmacro protect-macros! (&rest body)
  "Eval BODY, protecting macros from incorrect expansion. This
macro should be used in the following situation: Some form is
being evaluated, and this form contains as a sub-form some code
that will not be evaluated immediately, but will be evaluated
later. The code uses a macro that is not defined at the time the
top-level form is evaluated, but will be defined by time the
sub-form's code is evaluated. This macro handles its arguments in
some way other than evaluating them directly. And finally, one of
the arguments of this macro could be interpreted itself as a
macro invocation, and expanding the invocation would break the
evaluation of the outer macro. You might think this situation is
such an edge case that it would never happen, but you'd be wrong,
unfortunately. In such a situation, you must wrap at least the
outer macro in this form, but can wrap at any higher level up to
the top-level form."
  (declare (indent 0))
  `(eval '(progn ,@body)))

(defmacro if-compiletime! (cond then else)
  "Like `if', but COND is checked at compile/expansion time,
allowing BODY to be omitted entirely when the elisp is
byte-compiled. The macro expands directly to either THEN or ELSE,
and the other branch is not compiled. This can be helpful to deal
with code that uses functions only defined in a specific Emacs
version."
  (declare (indent 2))
  (if (eval cond)
      then
    else))

(defmacro when-compiletime! (cond &rest body)
  "Like `when', but COND is checked at compile/expansion time.
BODY is only compiled if COND evaluates to non-nil. See
`if-compiletime!'."
  (declare (indent 1))
  (when (eval cond)
    (macroexp-progn body)))

(defmacro unless-compiletime! (cond &rest body)
  "Like `unless', but COND is checked at compile/expansion time.
BODY is only compiled if COND evaluates to non-nil. See
`if-compiletime!'."
  (declare (indent 1))
  (unless (eval cond)
    (macroexp-progn body)))


;;
;;; Closure factories

(defmacro fn! (arglist &rest body)
  "Returns (cl-function (lambda ARGLIST BODY...)).
The closure is wrapped in `cl-function', meaning ARGLIST will
accept anything `cl-defun' will. Implicitly adds
`&allow-other-keys' if `&key' is present in ARGLIST."
  (declare (indent defun) (doc-string 1) (pure t) (side-effect-free t))
  `(cl-function
    (lambda
      ,(letf! (defun* allow-other-keys (args)
                (mapcar
                 (lambda (arg)
                   (cond ((nlistp (cdr-safe arg)) arg)
                         ((listp arg) (allow-other-keys arg))
                         (arg)))
                 (if (and (memq '&key args)
                          (not (memq '&allow-other-keys args)))
                     (if (memq '&aux args)
                         (let (newargs arg)
                           (while args
                             (setq arg (pop args))
                             (when (eq arg '&aux)
                               (push '&allow-other-keys newargs))
                             (push arg newargs))
                           (nreverse newargs))
                       (append args (list '&allow-other-keys)))
                   args)))
         (allow-other-keys arglist))
      ,@body)))

(defmacro cmd! (&rest body)
  "Expands to (lambda () (interactive) ,@body).
A factory for quickly producing interaction commands,
particularly for keybinds or aliases."
  (declare (doc-string 1) (pure t) (side-effect-free t))
  `(lambda (&rest _) (interactive) ,@body))

(defmacro cmd!! (command &rest args)
  "Expands to a closure that interactively calls COMMAND with ARGS.
A factory for quickly producing interactive, prefixed commands
for keybinds or aliases."
  (declare (doc-string 1) (pure t) (side-effect-free t))
  `(lambda (&rest _) (interactive)
     (funcall-interactively ,command ,@args)))


;;
;;; Mutation

(defmacro appendq! (sym &rest lists)
  "Append LISTS to SYM in place."
  `(setq ,sym (append ,sym ,@lists)))

(defmacro setq! (&rest settings)
  "A stripped-down `customize-set-variable' with the syntax of `setq'.
This can be used as a drop-in replacement for `setq'.
Particularly when you know a variable has a custom setter (a :set
property in its `defcustom' declaration). This triggers setters.
`setq' does not."
  (macroexp-progn
   (cl-loop for (var val) on settings by 'cddr
            collect `(funcall (or (get ',var 'custom-set) #'set)
                              ',var ,val))))

(defmacro delq! (elt list &optional fetcher)
  "`delq' ELT from LIST in-place.
If FETCHER is a function, ELT is used as the key in LIST (an
alist)."
  `(setq ,list
         (delq ,(if fetcher
                    `(funcall ,fetcher ,elt ,list)
                  elt)
               ,list)))

(defmacro pushnew! (place &rest values)
  "Push VALUES sequentially into PLACE, if they aren't already present.
This is a variadic `cl-pushnew'."
  (let ((var (make-symbol "result")))
    `(dolist (,var (list ,@values) (with-no-warnings ,place))
       (cl-pushnew ,var ,place :test #'equal))))

(defmacro prependq! (sym &rest lists)
  "Prepend LISTS to SYM in place."
  `(setq ,sym (append ,@lists ,sym)))


;;
;;; Loading

(defmacro add-load-path! (&rest dirs)
  "Add DIRS to `load-path', relative to the current file.
The current file is the file from which `add-to-load-path!' is
used."
  `(let ((default-directory ,(dir!))
         file-name-handler-alist)
     (dolist (dir (list ,@dirs))
       (cl-pushnew (expand-file-name dir) load-path))))

(defmacro after! (package &rest body)
  "Evaluate BODY after PACKAGE have loaded.
PACKAGE is a symbol or list of them. These are package names, not
modes, functions or variables. It can be:

- An unquoted package symbol (the name of a package)
    (after! helm BODY...)

- An unquoted list of package symbols (i.e. BODY is evaluated
  once both magit and git-gutter have loaded)
    (after! (magit git-gutter) BODY...)

- An unquoted, nested list of compound package lists, using any
  combination of
  :or/:any and :and/:all
    (after! (:or package-a package-b ...)  BODY...)
    (after! (:and package-a package-b ...) BODY...)
    (after! (:and package-a (:or package-b package-c) ...) BODY...)
  Without :or/:any/:and/:all, :and/:all are implied.

This is a wrapper around `eval-after-load' that:

1. Suppresses warnings for disabled packages at compile-time

2. No-ops for package that are disabled by the user (via
`package!')

3. Supports compound package statements (see below)

4. Prevents eager expansion pulling in autoloaded macros all at
once"
  (declare (indent defun) (debug t))
  (if (symbolp package)
      (list (if (or (not (bound-and-true-p byte-compile-current-file))
                    (require package nil 'noerror))
                #'progn
              #'with-no-warnings)
            (let ((body (macroexp-progn body)))
              `(if (featurep ',package)
                   ,body
                 ;; We intentionally avoid `with-eval-after-load' to prevent
                 ;; eager macro expansion from pulling (or failing to pull) in
                 ;; autoloaded macros/packages.
                 (eval-after-load ',package ',body))))
    (let ((p (car package)))
      (cond ((memq p '(:or :any))
             (macroexp-progn
              (cl-loop for next in (cdr package)
                       collect `(after! ,next ,@body))))
            ((memq p '(:and :all))
             (dolist (next (reverse (cdr package)) (car body))
               (setq body `((after! ,next ,@body)))))
            (`(after! (:and ,@package) ,@body))))))

(defun +emacs--handle-load-error (e target path)
  (let* ((source (file-name-sans-extension target))
         (err (cond ((not (featurep 'init-core))
                     (cons 'error (file-name-directory path)))
                    ((file-in-directory-p source +emacs-core-dir)
                     (cons '+emacs-error +emacs-core-dir))
                    ((file-in-directory-p source +emacs-private-dir)
                     (cons '+emacs-private-error +emacs-private-dir))
                    ((cons '+emacs-module-error +emacs-dir)))))
    (signal (car err)
            (list (file-relative-name
                   (concat source ".el")
                   (cdr err))
                  e))))

(defmacro load! (filename &optional path noerror)
  "Load a file relative to the current executing
file (`load-file-name'). FILENAME is either a file path string or
a form that should evaluate to such a string at run time. PATH is
where to look for the file (a string representing a directory
path). If omitted, the lookup is relative to either
`load-file-name', `byte-compile-current-file' or
`buffer-file-name' (checked in that order). If NOERROR is
non-nil, don't throw an error if the file doesn't exist."
  (let* ((path (or path
                   (dir!)
                   (error "Could not detect path to look for '%s' in"
                          filename)))
         (file (if path
                   `(expand-file-name ,filename ,path)
                 filename)))
    `(condition-case-unless-debug e
         (let (file-name-handler-alist)
           (load ,file ,noerror 'nomessage))
       (+emacs-error (signal (car e) (cdr e)))
       (error (+emacs--handle-load-error e ,file ,path)))))

(defmacro defer-until! (condition &rest body)
  "Run BODY when CONDITION is true (checks on
`after-load-functions'). Meant to serve as a predicated
alternative to `after!'."
  (declare (indent defun) (debug t))
  `(if ,condition
       (progn ,@body)
     ,(let ((fn (intern (format "+emacs--delay-form-%s-h" (sxhash (cons condition body))))))
        `(progn
           (fset ',fn (lambda (&rest args)
                        (when ,(or condition t)
                          (remove-hook 'after-load-functions #',fn)
                          (unintern ',fn nil)
                          (ignore args)
                          ,@body)))
           (put ',fn 'permanent-local-hook t)
           (add-hook 'after-load-functions #',fn)))))

(defmacro defer-feature! (feature &rest fns)
  "Pretend FEATURE hasn't been loaded yet, until FEATURE-hook or FN runs.
Some packages (like `elisp-mode' and `lisp-mode') are loaded
immediately at startup, which will prematurely trigger
`after!' (and `with-eval-after-load') blocks. To get around this
we make Emacs believe FEATURE hasn't been loaded yet, then wait
until FEATURE-hook (or MODE-hook, if FN is provided) is triggered
to reverse this and trigger `after!' blocks at a more reasonable
time."
  (let ((advice-fn (intern (format "+emacs--advice-defer-feature-%s" feature))))
    `(progn
       (delq! ',feature features)
       (defadvice! ,advice-fn (&rest _)
         ,(format "This advice defers `%S'." feature)
         :before ',fns
         ;; Some plugins (like yasnippet) will invoke a fn early to parse code,
         ;; which would prematurely trigger this. In those cases, well behaved
         ;; plugins will use `delay-mode-hooks', which we can check for:
         (unless delay-mode-hooks
           ;; ...Otherwise, announce to the world this package has been loaded,
           ;; so `after!' handlers can react.
           (provide ',feature)
           (dolist (fn ',fns)
             (advice-remove fn #',advice-fn)))))))


;;
;;; Hooks

(defvar +emacs--transient-counter 0)
(defmacro add-transient-hook! (hook-or-function &rest forms)
  "Attaches a self-removing function to HOOK-OR-FUNCTION.

FORMS are evaluated once when that function/hook is first
invoked, then never again.

HOOK-OR-FUNCTION can be a quoted hook or a sharp-quoted
function (which will be advised)."
  (declare (indent 1))
  (let ((append (if (eq (car forms) :after) (pop forms)))
        (fn (intern (format "+emacs--hook-transient-%s" (cl-incf +emacs--transient-counter)))))
    `(let ((sym ,hook-or-function))
       (defun ,fn (&rest _)
         ,(format "Transient hook for %S" (+emacs-unquote hook-or-function))
         ,@forms
         (let ((sym ,hook-or-function))
           (cond ((functionp sym) (advice-remove sym #',fn))
                 ((symbolp sym)   (remove-hook sym #',fn))))
         (unintern ',fn nil))
       (cond ((functionp sym)
              (advice-add ,hook-or-function ,(if append :after :before) #',fn))
             ((symbolp sym)
              (put ',fn 'permanent-local-hook t)
              (add-hook sym #',fn ,append))))))

(defmacro add-hook! (hooks &rest rest)
  "A convenience macro for adding N functions to M hooks.

If N and M = 1, there's no benefit to using this macro over
`add-hook'. This macro accepts, in order:

  1. The hook(s) to be added to: either an unquoted mode, an
     unquoted list of modes, a quoted hook variable or a quoted
     list of hook variables. If unquoted, '-hook' will be
     appended to each symbol.

  2. Optional properties :local, :append, and/or :depth [N],
     which will make the hook buffer-local or append to the list
     of hooks (respectively).

  3. The function(s) to be added: this can be one function, a
     list thereof, a list of `defun's, or body forms (implicitly
     wrapped in a closure).

\(fn HOOKS [:append :local] FUNCTIONS)"
  (declare (indent (lambda (indent-point state)
                     (goto-char indent-point)
                     (when (looking-at-p "\\s-*(")
                       (lisp-indent-defform state indent-point))))
           (debug t))
  (let* ((hook-forms (+emacs--resolve-hook-forms hooks))
         (func-forms ())
         (defn-forms ())
         append-p local-p remove-p depth)
    (while (keywordp (car rest))
      (pcase (pop rest)
        (:append (setq append-p t))
        (:depth  (setq depth (pop rest)))
        (:local  (setq local-p t))
        (:remove (setq remove-p t))))
    (while rest
      (let* ((next (pop rest))
             (first (car-safe next)))
        (push (cond ((memq first '(function nil))
                     next)
                    ((eq first 'quote)
                     (let ((quoted (cadr next)))
                       (if (atom quoted)
                           next
                         (when (cdr quoted)
                           (setq rest (cons (list first (cdr quoted)) rest)))
                         (list first (car quoted)))))
                    ((memq first '(defun cl-defun))
                     (push next defn-forms)
                     (list 'function (cadr next)))
                    ((prog1 `(lambda (&rest _) ,@(cons next rest))
                       (setq rest nil))))
              func-forms)))
    `(progn
       ,@defn-forms
       (dolist (hook (nreverse ',hook-forms))
         (dolist (func (list ,@func-forms))
           ,(if remove-p
                `(remove-hook hook func ,local-p)
              `(add-hook hook func ,(or depth append-p) ,local-p)))))))

(defmacro remove-hook! (hooks &rest rest)
  "A convenience macro for removing N functions from M hooks.
Takes the same arguments as `add-hook!'. If N and M = 1, there's
no benefit to using this macro over `remove-hook'.

\(fn HOOKS [:append :local] FUNCTIONS)"
  (declare (indent defun) (debug t))
  `(add-hook! ,hooks :remove ,@rest))

(defmacro setq-hook! (hooks &rest var-vals)
  "Sets buffer-local variables on HOOKS.

\(fn HOOKS &rest [SYM VAL]...)"
  (declare (indent 1))
  (macroexp-progn
   (cl-loop for (var val hook fn) in (+emacs--setq-hook-fns hooks var-vals)
            collect `(defun ,fn (&rest _)
                       ,(format "%s = %s" var (pp-to-string val))
                       (setq-local ,var ,val))
            collect `(remove-hook ',hook #',fn) ; ensure set order
            collect `(add-hook ',hook #',fn))))

(defmacro unsetq-hook! (hooks &rest vars)
  "Unbind setq hooks on HOOKS for VARS.

\(fn HOOKS &rest [SYM VAL]...)"
  (declare (indent 1))
  (macroexp-progn
   (cl-loop for (_var _val hook fn)
            in (+emacs--setq-hook-fns hooks vars 'singles)
            collect `(remove-hook ',hook #',fn))))


;;
;;; Definers

(defmacro defadvice! (name arglist docstring where places &rest body)
  "Define an advice called NAME and add it to a function.
ARGLIST is as in `defun'. WHERE is a keyword as passed to
`advice-add', and PLACE is the function to which to add the
advice, like in `advice-add'. DOCSTRING and BODY are as in
`defun'.
\(fn SYMBOL ARGLIST &optional DOCSTRING &rest [WHERE PLACES...] BODY\)"
  (declare (indent defun)
           (doc-string 3))
  `(progn
     (defun ,name ,arglist
       ,(let ((article (if (string-match-p "^:[aeiou]" (symbol-name where))
                           "an"
                         "a")))
          (format "%s\n\nThis is %s `%S' advice for `%S'."
                  docstring article where
                  (if (and (listp places)
                           (memq (car places) ''function))
                      (cadr places)
                    places)))
       ,@body)
     (dolist (target (+emacs-enlist ,places))
       (if (eq ,where :remove)
           (advice-remove target #',name)
         (advice-add target ,where #',name)))))

(defmacro undefadvice! (symbol _arglist &optional docstring &rest body)
  "Undefine an advice called SYMBOL.
This has the same signature as `defadvice!' an exists as an easy
undefiner when testing advice (when combined with `rotate-text').
\(fn SYMBOL ARGLIST &optional DOCSTRING &rest [WHERE PLACES...]
BODY\)"
  (declare (doc-string 3) (indent defun))
  (let (where-alist)
    (unless (stringp docstring)
      (push docstring body))
    (while (keywordp (car body))
      (push `(cons ,(pop body) (+emacs-enlist ,(pop body)))
            where-alist))
    `(dolist (targets (list ,@(nreverse where-alist)))
       (dolist (target (cdr targets))
         (advice-remove target #',symbol)))))

(defmacro defhook! (name arglist docstring hook &rest body)
  "Define a function called NAME and add it to a hook. ARGLIST is
as in `defun'. HOOK is the same as in `add-hook!',e.g. either an
unquoted mode, an unquoted list of modes, a quoted hook variable
or a quoted list of hook variables. If unquoted, '-hook' will be
appended to each symbol. DOCSTRING and BODY are as in `defun'."
  (declare (indent defun)
           (doc-string 3))
  (let (append-p local-p)
    (while (keywordp (car body))
      (pcase (pop body)
        (:append (setq append-p :append))
        (:local (setq local-p :local))))
    `(progn
       (defun ,name ,arglist
         ,(format "%s\n\nThis function is for use in `%S'."
                  docstring (+emacs-unquote hook))
         ,@body)
       ,@(if (or append-p local-p)
             (cond ((and append-p local-p) `((add-hook! ,hook ,append-p ,local-p ',name)))
                   (append-p `((add-hook! ,hook ,append-p ',name)))
                   (local-p `((add-hook! ,hook ,local-p ',name))))
           `((add-hook! ,hook ',name))))))

(provide 'init-core-lib)
