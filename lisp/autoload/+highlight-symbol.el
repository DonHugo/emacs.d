;; lisp/autoload/+highlight-symbol.el -*- lexical-binding: t; -*-
;;;###if (featurep! :ui highlight-symbol)

;; This hydra is based on Spacemacs Navigation layer which uses
;; `auto-highlight-symbol-mode'. However, this implementation also works for
;; selected regions and relies on `evil-visualstar' and `evil-multiedit'.

(defun +highlight-symbol/goto-last-searched-ahs-symbol ()
  "Go to the last known occurrence of the last symbol searched
with `auto-highlight-symbol'."
  (interactive)
  (if (bound-and-true-p +highlight-symbol-last-ahs-highlight-p)
      (progn (goto-char (nth 1 +highlight-symbol-last-ahs-highlight-p))
             (+highlight-symbol-ahs-setup)
             (+emacs-symbols-highlight-transient-state/body))
    (message "No previously searched for symbol found")))

(defun +highlight-symbol-integrate-evil-search (forward)
  ;; `isearch-string' is the last searched item. Next time "n" is hit we will
  ;; use this.
  (let* ((symbol (evil-find-thing forward 'symbol))
         (regexp (concat "\\<" symbol "\\>")))
    (setq isearch-string regexp
          isearch-regexp regexp
          evil-ex-search-pattern (evil-ex-make-search-pattern regexp)))
  ;; Next time "n" is hit, go the correct direction.
  (setq isearch-forward forward)
  (setq evil-ex-search-direction (if forward 'forward 'backward))
  ;; `auto-highlight-symbol' does a case sensitive search. We could set this,
  ;; but it would break the user's current sensitivity settings. We could save
  ;; the setting, then next time the user starts a search we could restore the
  ;; setting.
  ;; (setq case-fold-search nil)
  ;; Place the search term into the search rings.
  (isearch-update-ring isearch-string t)
  (evil-push-search-history isearch-string forward)
  ;; Use this search term for empty pattern "%s//replacement/". Append case
  ;; sensitivity
  (setq evil-ex-last-was-search nil
        evil-ex-substitute-pattern `(,(concat isearch-string "\\C")
                                     nil (0 0))))

(defun +highlight-symbol-ahs-setup ()
  "Remember the `auto-highlight-symbol-mode' state,
before highlighting a symbol."
  (unless (bound-and-true-p auto-highlight-symbol-mode)
    (setq-local +highlight-symbol-ahs-was-disabled t))
  (auto-highlight-symbol-mode)
  (ahs-highlight-now))

(defun +highlight-symbol/enter-ahs-forward ()
  "Go to the next occurrence of symbol under point with
 `auto-highlight-symbol'"
  (interactive)
  (setq +highlight-symbol--ahs-searching-forward t)
  (+highlight-symbol/quick-ahs-forward))

(defun +highlight-symbol/enter-ahs-backward ()
  "Go to the previous occurrence of symbol under point with
 `auto-highlight-symbol'"
  (interactive)
  (setq +highlight-symbol--ahs-searching-forward nil)
  (+highlight-symbol/quick-ahs-forward))

(defun +highlight-symbol/quick-ahs-forward ()
  "Go to the Next occurrence of symbol under point with
 `auto-highlight-symbol'"
  (interactive)
  (+highlight-symbol-quick-ahs-move t))

(defun +highlight-symbol/quick-ahs-backward ()
  "Go to the previous occurrence of symbol under point with
 `auto-highlight-symbol'"
  (interactive)
  (+highlight-symbol-quick-ahs-move nil))

(defun +highlight-symbol-quick-ahs-move (forward)
  "Go to the next occurrence of symbol under point with
 `auto-highlight-symbol'"
  (better-jumper-set-jump)
  (+highlight-symbol-ahs-setup)
  (if (eq forward +highlight-symbol--ahs-searching-forward)
      (progn
        (+highlight-symbol-integrate-evil-search t)
        (ahs-forward))
    (progn
      (+highlight-symbol-integrate-evil-search nil)
      (ahs-backward)))
  (+emacs-symbols-highlight-transient-state/body))

(defun +highlight-symbol/symbol-highlight ()
  "Highlight the symbol under point with `auto-highlight-symbol'."
  (interactive)
  (+highlight-symbol-integrate-evil-search t)
  (+highlight-symbol-remember-last-ahs-highlight)
  (+highlight-symbol-ahs-setup)
  (+emacs-symbols-highlight-transient-state/body))

(defun +highlight-symbol-remember-last-ahs-highlight ()
  (setq +highlight-symbol-last-ahs-highlight-p (ahs-highlight-p)))

(defvar-local +highlight-symbol-ahs-was-disabled t
  "This is used to disable `auto-highlight-symbol-mode',
when the Symbol Highlight Transient State is closed. If ahs mode
was disabled before a symbol was highlighted.")

(defun +highlight-symbol-ahs-was-disabled-in-ahs-ts-exit-window-p ()
  (let ((prev-win (selected-window)))
    (select-window +highlight-symbol-ahs-ts-exit-window)
    (prog1 +highlight-symbol-ahs-was-disabled
      (select-window prev-win))))

(defvar +highlight-symbol-ahs-ts-exit-window nil
  "Remember the selected window when the Symbol Highlight
Transient State is closed. This is used to disable
`auto-highlight-symbol-mode', in the window where the Symbol
Highlight Transient State was closed, when the TS was closed by
opening a prompt. For example:

 SPC SPC (or M-x)       ;; +highlight-symbol/helm-M-x-fuzzy-matching
 SPC ?                  ;; helm-descbinds
 M-:                    ;; eval-expression
 :                      ;; evil-ex

`auto-highlight-symbol-mode' is only disabled if it was disabled
before a symbol was highlighted.")

(defun +highlight-symbol-ahs-ts-on-exit ()
  (setq +highlight-symbol-ahs-ts-exit-window (selected-window))
  ;; Restore user search direction state as ahs has exitted in a state good for
  ;; <C-s>, but not for 'n' and 'N'"
  (setq isearch-forward +highlight-symbol--ahs-searching-forward)
  (+highlight-symbol-disable-symbol-highlight-after-ahs-ts-exit))

(defun +highlight-symbol-disable-symbol-highlight-after-ahs-ts-exit ()
  "Disable `auto-highlight-symbol-mode', when the Symbol
Highlight Transient State buffer isn't found. This occurs when
the TS wasn't restarted. It is restarted when navigating to the
next or previous symbol. ahs mode is only disabled if it was
disabled before a symbol was highlighted."
  (run-with-idle-timer
   0 nil
   (lambda ()
     (unless (string= (+highlight-symbol//transient-state-buffer-title)
                      "Symbol Highlight")
       (cond ((and (+highlight-symbol-prompt-opened-from-ahs-ts-p)
                   (+highlight-symbol-ahs-was-disabled-in-ahs-ts-exit-window-p))
              (+highlight-symbol-disable-ahs-mode-in-ahs-ts-exit-window))
             (+highlight-symbol-ahs-was-disabled
              (+highlight-symbol-disable-symbol-highlight)))))))

(defun +highlight-symbol-prompt-opened-from-ahs-ts-p ()
  "Was a prompt opened (for example: M-x),
from the Symbol Highlight Transient State?"
  (not (eq +highlight-symbol-ahs-ts-exit-window (selected-window))))

(defun +highlight-symbol-disable-ahs-mode-in-ahs-ts-exit-window ()
  "Disable `auto-highlight-symbol-mode',
in the window where the Symbol Highlight Transient State was
closed."
  (let ((prev-win (selected-window)))
    (select-window +highlight-symbol-ahs-ts-exit-window)
    (+highlight-symbol-disable-symbol-highlight)
    (setq +highlight-symbol-ahs-ts-exit-window nil)
    (select-window prev-win)))

(defun +highlight-symbol-disable-symbol-highlight ()
  (auto-highlight-symbol-mode -1)
  (setq-local +highlight-symbol-ahs-was-disabled nil))

(defun +highlight-symbol//transient-state-buffer-title ()
  (let ((transient-state-buffer-name " *LV*"))
    (when (get-buffer transient-state-buffer-name)
      (with-current-buffer transient-state-buffer-name
        (buffer-substring-no-properties
         (point-min)
         (string-match "Transient State" (buffer-string)))))))

(defun +highlight-symbol-symbol-highlight-reset-range ()
  "Reset the range for `auto-highlight-symbol'."
  (interactive)
  (ahs-change-range ahs-default-range))

(defun +highlight-symbol-symbol-highlight-doc ()
  (let* ((i 0)
         (overlay-list (ahs-overlay-list-window))
         (overlay-count (length overlay-list))
         (overlay (format "%s" (nth i overlay-list)))
         (current-overlay (format "%s" (ahs-current-overlay-window)))
         (st (ahs-stat))
         (plighter (ahs-current-plugin-prop 'lighter))
         (plugin (format "%s"
                         (cond ((string= plighter "HS")  "Display")
                               ((string= plighter "HSA") "Buffer")
                               ((string= plighter "HSD") "Function"))))
         (face (cond ((string= plighter "HS")  ahs-plugin-default-face)
                     ((string= plighter "HSA") ahs-plugin-whole-buffer-face)
                     ((string= plighter "HSD") ahs-plugin-bod-face))))
    (while (not (string= overlay current-overlay))
      (setq i (1+ i))
      (setq overlay (format "%s" (nth i overlay-list))))
    (let* ((x/y (format "[%s/%s]" (- overlay-count i) overlay-count))
           (hidden (if (< 0 (- overlay-count (nth 4 st))) "*" "")))
      (concat
       (propertize (format " %s " plugin) 'face face)
       (propertize (format " %s%s " x/y hidden) 'face `(:inherit font-lock-keyword-face :weight bold))))))

;;; Commands

(defun +highlight-symbol/edit (&optional with-ahs)
  (interactive)
  (if with-ahs
      (progn
        (let ((evil-multiedit-use-symbols t))
          (evil-multiedit-match-all)
          (iedit-restrict-region (ahs-current-plugin-prop 'start)
                                 (ahs-current-plugin-prop 'end))
          (auto-highlight-symbol-mode -1)))
    (progn
      (evil-visual-select evil-ex-search-match-beg evil-ex-search-match-end 'char -1)
      (evil-multiedit-match-all)
      (+evil--hook-disable-highlights))))

(defun +highlight-symbol/swiper-region-or-symbol (&optional with-ahs with-search)
  "Run `swiper' with the selected region or the symbol around
point as the initial input. If WITH-AHS or WITH-SEARCH are t, use
selection from `auto-highlight-symbol-mode' or `evil-ex-search'."
  (interactive)
  (let ((input (cond (with-ahs (when (bound-and-true-p auto-highlight-symbol-mode)
                                 (auto-highlight-symbol-mode -1))
                               (thing-at-point 'symbol t))
                     (with-search (buffer-substring-no-properties evil-ex-search-match-beg evil-ex-search-match-end))
                     ((region-active-p) (buffer-substring-no-properties (region-beginning) (region-end)))
                     (t (thing-at-point 'symbol t)))))
    (deactivate-mark)
    (swiper input)))

(defun +highlight-symbol/swiper-all-region-or-symbol (&optional with-ahs with-search)
  "Run `swiper-all' with the selected region or the symbol around
point as the initial input. If WITH-AHS or WITH-SEARCH are t, use
selection from `auto-highlight-symbol-mode' or `evil-ex-search'."
  (interactive)
  (let ((input (cond (with-ahs (when (bound-and-true-p auto-highlight-symbol-mode)
                                 (auto-highlight-symbol-mode -1))
                               (thing-at-point 'symbol t))
                     (with-search (buffer-substring-no-properties evil-ex-search-match-beg evil-ex-search-match-end))
                     ((region-active-p) (buffer-substring-no-properties (region-beginning) (region-end)))
                     (t (thing-at-point 'symbol t)))))
    (deactivate-mark)
    (swiper-all input)))

(defun +highlight-symbol/file-search (&optional with-ahs with-search)
  "Run `+ivy-file-search' with the selected region or the symbol around
point as the initial input. If WITH-AHS or WITH-SEARCH are t, use
selection from `auto-highlight-symbol-mode' or `evil-ex-search'."
  (interactive)
  (let ((input (cond (with-ahs (when (bound-and-true-p auto-highlight-symbol-mode)
                                 (auto-highlight-symbol-mode -1))
                               (thing-at-point 'symbol t))
                     (with-search (buffer-substring-no-properties evil-ex-search-match-beg evil-ex-search-match-end))
                     ((region-active-p) (buffer-substring-no-properties (region-beginning) (region-end)))
                     (t (thing-at-point 'symbol t))))
        (initial-directory (read-directory-name "Start from directory: ")))
    (deactivate-mark)
    (+ivy-file-search nil :query (rxt-quote-pcre input) :in initial-directory :recursive t)))

(defun +highlight-symbol/project-search (&optional with-ahs with-search)
  "Run `+ivy/project-search' with the selected region or the symbol
around point as the initial input."
  (interactive)
  (let ((input (cond (with-ahs (when (bound-and-true-p auto-highlight-symbol-mode)
                                 (auto-highlight-symbol-mode -1))
                               (thing-at-point 'symbol t))
                     (with-search (buffer-substring-no-properties evil-ex-search-match-beg evil-ex-search-match-end))
                     ((region-active-p) (buffer-substring-no-properties (region-beginning) (region-end)))
                     (t (thing-at-point 'symbol t)))))
    (deactivate-mark)
    (+ivy/project-search nil (rxt-quote-pcre input))))

(defun +evil-cycle-search-case ()
  "Cycle between `evil-ex-search-case' values."
  (interactive)
  (let* ((case-values '(smart insensitive sensitive))
         (index-before
          (if (symbol-value 'evil-ex-search-case)
              (cl-position (symbol-value 'evil-ex-search-case) case-values :test 'equal)
            0))
         (index-after (if (= index-before (- (length case-values) 1))
                          0
                        (+ index-before 1)))
         (next-value (nth index-after case-values)))
    (setq-local evil-ex-search-case next-value)))

;;;###autoload
(defun +highlight-symbol/start (beg end)
  (interactive "r")
  (if (evil-visual-state-p)
      (progn
        (evil-visualstar/begin-search-forward beg end)
        (+emacs-region-highlight-transient-state/body)
        (+emacs-region-highlight-transient-state/evil-ex-search-previous))
    (+highlight-symbol/enter-ahs-forward)
    (+emacs-symbols-highlight-transient-state/+highlight-symbol/quick-ahs-backward)))

;;; Hydras

(defun +highlight-symbol-match-hint ()
  (require 'evil-anzu)
  (let ((here anzu--current-position)
        (total anzu--total-matched))
    (propertize
     (format "[%s/%s]" here total)
     'face `(:inherit font-lock-keyword-face :weight bold))))

(defun +highlight-symbol-case-hint ()
  (propertize
   (format "%s" (symbol-value 'evil-ex-search-case))
   'face `(:inherit font-lock-variable-name-face :weight bold)))

;;;###autoload (autoload '+emacs-region-highlight-transient-state/body "autoload/+highlight-symbol" nil t)
(define-transient-state! region-highlight
  :title "Region highlight transient state"
  :doc "
 Current matches: %s(+highlight-symbol-match-hint)   Current case: %s(+highlight-symbol-case-hint)

 Navigation^^^^             Edit^^              Search^^
 ──────────^^^^───────────  ────^^────────────  ──────^^──────────
 [_n_]:^^   next match      [_e_]: edit         [_s_]: buffer
 [_p_/_N_]: previous match  [_c_]: change case  [_b_]: all buffers
 ^^^^                       ^^                  [_f_]: files
 ^^^^                       ^^                  [_/_]: project
 ^^[_z_]: recenter          [_q_]: quit"
  :foreign-keys warn
  :bindings
  ;; Navigation
  ("n" evil-ex-search-next)
  ("N" evil-ex-search-previous)
  ("p" evil-ex-search-previous)
  ;; Edit
  ("e" +highlight-symbol/edit :exit t)
  ("c" (progn
         (evil-visual-select evil-ex-search-match-beg evil-ex-search-match-end 'char)
         (+evil-cycle-search-case)
         (+highlight-symbol/start evil-ex-search-match-beg evil-ex-search-match-end)) :exit t)
  ;; Search
  ("s" (+highlight-symbol/swiper-region-or-symbol nil t) :exit t)
  ("b" (+highlight-symbol/swiper-all-region-or-symbol nil t) :exit t)
  ("f" (+highlight-symbol/file-search nil t) :exit t)
  ("/" (+highlight-symbol/project-search nil t) :exit t)
  ;; Other
  ("z" recenter)
  ("q" (+evil--hook-disable-highlights) :exit t)
  ("<escape>" (+evil--hook-disable-highlights) :exit t))

;;;###autoload (autoload '+emacs-symbols-highlight-transient-state/body "autoload/+highlight-symbol" nil t)
(define-transient-state! symbols-highlight
  :title "Symbol highlight transient state"
  :doc "
 Range & matches: %s(+highlight-symbol-symbol-highlight-doc)

 Navigation^^^^                       Edit^^              Search^^
 ──────────^^^^───────────            ────^^────────────  ──────^^──────────
 [_n_]:^^   next match                [_e_]: edit         [_s_]: buffer
 [_p_/_N_]: previous match            [_r_]: range        [_b_]: all buffers
 [_d_/_D_]: next/previous definition  [_R_]: Reset        [_f_]: files
 ^^^^                                 ^^                  [_/_]: project
 ^^[_z_]: recenter                    [_q_]: quit"
  ;; :on-exit (+highlight-symbol-ahs-ts-on-exit)
  :foreign-keys warn
  :bindings
  ;; Navigation
  ("n" +highlight-symbol/quick-ahs-forward)
  ("N" +highlight-symbol/quick-ahs-backward)
  ("p" +highlight-symbol/quick-ahs-backward)
  ("d" ahs-forward-definition)
  ("D" ahs-backward-definition)
  ;; Edit
  ("e" (+highlight-symbol/edit t) :exit t)
  ("r" ahs-change-range)
  ("R" ahs-back-to-start)
  ;; Search
  ("s" (+highlight-symbol/swiper-region-or-symbol t) :exit t)
  ("b" (+highlight-symbol/swiper-all-region-or-symbol t) :exit t)
  ("f" (+highlight-symbol/file-search t) :exit t)
  ("/" (+highlight-symbol/project-search t) :exit t)
  ;; Other
  ("z" (progn (recenter-top-bottom)
              (+highlight-symbol/symbol-highlight)))
  ("q" (quiet! (auto-highlight-symbol-mode -1)) :exit t)
  ("<escape>" (quiet! (auto-highlight-symbol-mode -1)) :exit t))
