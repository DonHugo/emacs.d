;; lisp/autoload/+lookup-evil.el -*- lexical-binding: t; -*-
;;;###if (and (featurep! :tools lookup) (featurep! :editor evil))

;;;###autoload (autoload '+lookup:online "autoload/+lookup-evil" nil t)
(evil-define-command +lookup:online (query &optional bang)
  "Look up QUERY online. Will prompt for search engine the first
time, then reuse it on consecutive uses of this command. If BANG,
always prompt for search engine."
  (interactive "<a><!>")
  (+lookup/online query (+lookup--online-provider bang 'evil-ex)))

;;;###autoload (autoload '+lookup:dash "autoload/+lookup-evil" nil t)
(evil-define-command +lookup:dash (query &optional bang)
  "Look up QUERY in your dash docsets. If BANG, prompt to select
a docset (and install it if necessary)."
  (interactive "<a><!>")
  (let (selected)
    (when bang
      (setq selected (replace-regexp-in-string
                      "_" " "
                      (completing-read "Select docset: " (dash-docs-official-docsets))))
      (unless (dash-docs-docset-path selected)
        (dash-docs-install-docset selected)))
    (+lookup/in-docsets query)))
