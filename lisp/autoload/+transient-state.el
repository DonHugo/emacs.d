;; lisp/autoload/+transient-state.el -*- lexical-binding: t; -*-
;;;###if (featurep! :ui transient-state)

;; Forked from https://github.com/syl20bnr/spacemacs/blob/develop/core/core-transient-state.el

(defface +emacs-transient-state-toggle-on-face
  '((t (:inherit 'font-lock-keyword-face)))
  "Face used to render titles for transient states.")

(defface +emacs-transient-state-toggle-off-face
  '((t (:inherit 'font-lock-comment-face)))
  "Face used to render titles for transient states.")

;;;###autoload
(defun +emacs-transient-state-toggle (name status)
  "Create a dynamic hint that look like a radio button with given NAME.
Radio is considered on when STATUS is non-nil, otherwise off."
  (s-concat name " " (if status
                         (propertize "(*)" 'face '+emacs-transient-state-toggle-on-face)
                       (propertize "( )" 'face '+emacs-transient-state-toggle-off-face))))

(defvar +emacs-show-transient-state-title t
  "If non nil show the titles of transient states.")

(defvar +emacs-show-transient-state-color-guide t
  "If non nil show the color guide hint for transient state keys.")

(defun +emacs--transient-state-func-name (name)
  "Return the name of the transient state function."
  (intern (format "+emacs-%S-transient-state" name)))

(defun +emacs--transient-state-props-var-name (name)
  "Return the name of the variable use to store the transient state properties."
  (intern (format "+emacs--%S-transient-state-props" name)))

(defun +emacs--transient-state-body-func-name (name)
  "Return the name of the transient state function."
  (intern (format "+emacs-%S-transient-state/body" name)))

(defun +emacs--transient-state-heads-name (name)
  "Return the name of the transient state heads variable which
holds the key bindings."
  (intern (format "+emacs-%S-transient-state/heads" name)))

(defun +emacs--transient-state-make-doc (transient-state docstring &optional body)
  "Use `hydra' internal function to format and apply DOCSTRING."
  (let ((heads (+emacs--transient-state-heads-name transient-state)))
    (setq body (if body body '(nil nil :hint nil :foreign-keys nil)))
    (eval
     (hydra--format nil body docstring (symbol-value heads)))))

(defmacro +emacs--transient-state-format-hint (name var hint)
  "Format HINT and store the result in VAR for transient state NAME."
  (declare (indent defun))
  `(let* ((props-var ,(+emacs--transient-state-props-var-name
                       name))
          (prop-hint (cadr (assq 'hint props-var)))
          (prop-columns (cadr (assq 'columns props-var)))
          (prop-foreign-keys (cadr (assq 'foreign-keys props-var)))
          (prop-entry-sexp (cadr (assq 'entry-sexp props-var)))
          (prop-exit-sexp (cadr (assq 'exit-sexp props-var))))
     (setq ,var (+emacs--transient-state-make-doc
                 ',name
                 ,hint
                 `(nil
                   nil
                   :hint ,prop-hint
                   :columns ,prop-columns
                   :foreign-keys ,prop-foreign-keys
                   :body-pre ,prop-entry-sexp
                   :before-exit ,prop-exit-sexp)))))

;;;###autoload
(defmacro define-transient-state! (name &rest props)
  "Define a transient state called NAME.
NAME is a symbol.
Available PROPS:
`:on-enter SEXP'
    Evaluate SEXP when the transient state is switched on.
`:on-exit SEXP'
    Evaluate SEXP when leaving the transient state.
`:doc STRING or SEXP'
    A docstring supported by `defhydra'.
`:additional-docs cons cells (VARIABLE . STRING)'
    Additional docstrings to format and store in the corresponding VARIABLE.
    This can be used to dynamically change the docstring.
`:title STRING'
    Provide a title in the header of the transient state
`:columns INTEGER'
    Automatically generate :doc with this many number of columns.
`:hint BOOLEAN'
    Whether to display hints. Default is nil.
`:hint-is-doc BOOLEAN'
    Whether the hints act as a documentation, the only effect of this value is
    to change where the hints are displayed. If non-nil the hints are displayed
    on the same line as the `:title', otherwise they are displayed below it.
    Default is nil.
`:dynamic-hint SEXP'
    An sexp evaluating to a string for dynamic hinting.
    When provided `:hint' has no effect. Default is nil.
`:foreign-keys SYMBOL'
    What to do when keys not bound in the transient state are entered. This
    can be nil (default), which means to exit the transient state, warn,
    which means to not exit but warn the user that the key is not part
    of the transient state, or run, which means to try to run the key binding
    without exiting.
`:bindings EXPRESSIONS'
    One or several EXPRESSIONS with the form
    (STRING1 SYMBOL1 DOCSTRING
                     :exit SYMBOL)
    where:
    - STRING1 is a key to be bound to the function or key map SYMBOL1.
    - DOCSTRING is a STRING or an SEXP that evaluates to a string
    - :exit SYMBOL or SEXP, if non nil then pressing this key will
      leave the transient state (default is nil).
      Important note: due to inner working of transient-maps in Emacs
      the `:exit' keyword is evaluate *before* the actual execution
      of the bound command.
All properties supported by `spacemacs//create-key-binding-form' can be
used."
  (declare (indent 1))
  (let* ((func (+emacs--transient-state-func-name name))
         (props-var (+emacs--transient-state-props-var-name name))
         (body-func (+emacs--transient-state-body-func-name name))
         (bindings (+emacs-mplist-get-values props :bindings))
         (doc (or (plist-get props :doc) "\n"))
         (title (concat "\n " (plist-get props :title)))
         (hint-var (intern (format "%s/hint" func)))
         (columns (plist-get props :columns))
         (entry-sexp (plist-get props :on-enter))
         (exit-sexp (plist-get props :on-exit))
         (hint (plist-get props :hint))
         (hint-doc-p (plist-get props :hint-is-doc))
         (dyn-hint (plist-get props :dynamic-hint))
         (additional-docs (+emacs-mplist-get-values props :additional-docs))
         (foreign-keys (plist-get props :foreign-keys)))
    `(progn
       (defvar ,props-var nil
         ,(format (concat "Association list containing a copy of some "
                          "properties of the transient state %S. Those "
                          "properties are used in macro "
                          "`+emacs--transient-state-format-hint'.") name))
       (add-to-list ',props-var '(hint ,hint))
       (add-to-list ',props-var '(columns ,columns))
       (add-to-list ',props-var '(foreign-keys ,foreign-keys))
       (add-to-list ',props-var '(entry-sexp ,entry-sexp))
       (add-to-list ',props-var '(exit-sexp ,exit-sexp))
       ,(append `(defhydra ,func (nil nil
                                      :hint ,hint
                                      :columns ,columns
                                      :foreign-keys ,foreign-keys
                                      :body-pre ,entry-sexp
                                      :before-exit ,exit-sexp)
                   ,doc)
                `,bindings)
       (when ,title
             (let ((guide (concat "\n [" (propertize "KEY" 'face 'hydra-face-blue)
                                  "] exits state  ["
                                  (if ',foreign-keys
                                      (propertize "KEY" 'face 'hydra-face-pink)
                                    (propertize "KEY" 'face 'hydra-face-red))
                                  "] will not exit")))
               (add-face-text-property 0 (length guide) '(:slant italic :height 0.9) t guide)
               (setq ,hint-var
                     (list 'concat
                           (when +emacs-show-transient-state-title
                             (concat
                              ,title
                              (if ,hint-doc-p " " "\n"))) ,hint-var
                              ',dyn-hint
                              (when +emacs-show-transient-state-color-guide
                                (concat "\n" guide)))))))))


;;
;;; Transient states

;; General transient states which do not fit to specific modules

;;;; Buffers

;;;###autoload (autoload '+emacs-buffers-transient-state/body "autoload/+transient-state" nil t)
(define-transient-state! buffers
  :title "Buffers transient state\n"
  :doc "
 Navigation^^                    Kill & revert^^              New^^                    Open^^                           Commands^^
 ──────────^^──────────────────  ─────────────^^────────────  ───^^──────────────────  ────^^─────────────────────────  ────────^^─────────────────────
 [_]_]: next buffer              [_k_]: kill buffer           [_N_]: new empty buffer  [_x_]: pop up scratch buffer     [_-_]: toggle narrowing
 [_[_]: previous buffer          [_K_]: kill all buffers      ^^                       [_X_]: switch to scratch buffer  [_y_]: copy buffer to clipboard
 [_b_]: switch workspace buffer  [_O_]: kill other buffers    ^^                       ^^                               [_P_]: copy clipboard to buffer
 [_B_]: switch buffer            [_u_]: reopen killed buffer  ^^                       ^^                               [_z_]: bury buffer
 ^^                              [_r_]: revert buffer         ^^                       ^^                               ^^
 [_q_]: Quit"
  :bindings
  ;;Navigation
  ("]" next-buffer)
  ("[" previous-buffer)
  ("b" persp-switch-to-buffer :exit t)
  ("B" switch-to-buffer :exit t)
  ;; Kill & revert
  ("k" kill-current-buffer)
  ("K" +emacs/kill-all-buffers)
  ("O" +emacs/kill-other-buffers)
  ("u" +emacs/reopen-killed-buffer)
  ("r" revert-buffer)
  ;; New
  ("N" evil-buffer-new)
  ;; Open
  ("x" +emacs/open-scratch-buffer :exit t)
  ("X" +emacs/switch-to-scratch-buffer :exit t)
  ;; Commands
  ("-" +emacs/toggle-narrow-buffer)
  ("y" +emacs/copy-whole-buffer-to-clipboard :exit t)
  ("P" +emacs/copy-clipboard-to-whole-buffer :exit t)
  ("z" bury-buffer)
  ;; Quit
  ("q" nil :exit t))


;;;; Text zoom

;;;###autoload (autoload '+emacs-text-zoom-transient-state/body "autoload/+transient-state" nil t)
(define-transient-state! text-zoom
  :title "Text Zoom transient state\n"
  :doc "
 ^^─────────────  ^^─────────────  ^^──────────
 [_j_]: zoom in   [_k_]: zoom out  [_0_]: reset

 [_q_]: Quit"
  :bindings
  ("j" +emacs/increase-font-size)
  ("k" +emacs/decrease-font-size)
  ("0" +emacs/reset-font-size)
  ("q" nil :exit t))


;;;; Window navigation

;;;###autoload (autoload '+emacs-windows-transient-state/body "autoload/+transient-state" nil t)
(define-transient-state! windows
  :title "Window navigation transient state\n"
  :doc "
 Split^^            Switch^^      Swap^^               Resize^^                        Delete^^      Move^^
 ─────^^──────────  ──────^^────  ────^^─────────────  ──────^^──────────────────────  ──────^^────  ────^^──────
 [_v_]: vertical    [_h_]: left   [_H_]: swap left     [_C-h_]: splitter left          [_c_]: close  [_z_]: up
 [_s_]: horizontal  [_j_]: down   [_J_]: swap down     [_C-j_]: splitter down          [_o_]: other  [_a_]: down
 ^^                 [_k_]: up     [_K_]: swap up       [_C-k_]: splitter up            ^^            [_i_]: imenu
 ^^                 [_l_]: right  [_L_]: swap right    [_C-l_]: splitter right         ^^            ^^
 ^^                 ^^            [_S_]: swap windows  [_b_]:   balance                ^^            ^^
 ^^                 ^^            ^^                   [_M_]:   maximize               ^^            ^^
 ^^                 ^^            ^^                   [_S_]:   maximize horizontally  ^^            ^^
 ^^                 ^^            ^^                   [_V_]:   maximize vertically    ^^            ^^
 [_q_]: Quit"
  :bindings
  ;; Split
  ("v" evil-window-vsplit)
  ("s" evil-window-split)
  ;; Switch
  ("h" evil-window-left)
  ("j" evil-window-down)
  ("k" evil-window-up)
  ("l" evil-window-right)
  ;; Swap
  ("H" +evil/window-move-left)
  ("J" +evil/window-move-down)
  ("K" +evil/window-move-up)
  ("L" +evil/window-move-right)
  ("S" ace-swap-window)
  ;; Resize
  ("C-h" hydra-move-splitter-left)
  ("C-j" hydra-move-splitter-down)
  ("C-k" hydra-move-splitter-up)
  ("C-l" hydra-move-splitter-right)
  ("b" balance-windows)
  ("M" +emacs/window-maximize-buffer)
  ("S" +emacs/window-maximize-horizontally)
  ("V" +emacs/window-maximize-vertically)
  ;; Delete
  ("c" +workspace/close-window-or-workspace)
  ("o" delete-other-windows)
  ;; Move
  ("z" scroll-up-line)
  ("a" scroll-down-line)
  ("i" imenu-list-smart-toggle :exit t)
  ;; Quit
  ("q" nil :exit t))
