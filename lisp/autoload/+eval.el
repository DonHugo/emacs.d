;; lisp/autoload/+eval.el -*- lexical-binding: t; -*-
;;;###if (featurep! :tools eval)

;;;###autoload
(defun +eval-display-results-in-popup (output &optional _source-buffer)
  "Display OUTPUT in a popup buffer."
  (let ((output-buffer (get-buffer-create "*+emacs eval*"))
        (origin (selected-window)))
    (with-current-buffer output-buffer
      (setq-local scroll-margin 0)
      (erase-buffer)
      (insert output)
      (goto-char (point-min))
      (if (fboundp '+word-wrap-mode)
          (+word-wrap-mode +1)
        (visual-line-mode +1)))
    (when-let (win (display-buffer output-buffer))
      (fit-window-to-buffer
       win (/ (frame-height) 2)
       nil (/ (frame-width) 2)))
    (select-window origin)
    output-buffer))

;;;###autoload
(defun +eval-display-results (output &optional source-buffer)
  "Display OUTPUT in an overlay or a popup buffer."
  (funcall (if (or current-prefix-arg
                   (with-temp-buffer
                     (insert output)
                     (or (>= (string-width
                              (buffer-substring (point-min)
                                                (save-excursion
                                                  (goto-char (point-min))
                                                  (line-end-position))))
                             (window-width))))
                   (not nil))
               #'+eval-display-results-in-popup)
           output source-buffer)
  output)


;;
;;; Commands

(defvar quickrun-option-cmdkey)
;;;###autoload
(defun +eval/buffer ()
  "Evaluate the whole buffer."
  (interactive)
  (let ((quickrun-option-cmdkey (bound-and-true-p quickrun-option-cmdkey)))
    (if (or (assq major-mode +eval-runners)
            (and (fboundp '+eval--ensure-in-repl-buffer)
                 (ignore-errors
                   (get-buffer-window (or (+eval--ensure-in-repl-buffer)
                                          t))))
            (and (require 'quickrun nil t)
                 (equal (setq
                         quickrun-option-cmdkey
                         (quickrun--command-key
                          (buffer-file-name (buffer-base-buffer))))
                        "emacs")
                 (alist-get 'emacs-lisp-mode +eval-runners)))
        (+eval/region (point-min) (point-max))
      (quickrun))))

;;;###autoload
(defun +eval/region (beg end)
  "Evaluate a region between BEG and END and display the output."
  (interactive "r")
  (let ((load-file-name buffer-file-name)
        (load-true-file-name
         (or buffer-file-truename
             (if buffer-file-name
                 (file-truename buffer-file-name)))))
    (cond ((and (fboundp '+eval--ensure-in-repl-buffer)
                (ignore-errors
                  (get-buffer-window (or (+eval--ensure-in-repl-buffer)
                                         t))))
           (+eval/send-region-to-repl beg end))
          ((let ((runner
                  (or (alist-get major-mode +eval-runners)
                      (and (require 'quickrun nil t)
                           (equal (setq
                                   lang (quickrun--command-key
                                         (buffer-file-name (buffer-base-buffer))))
                                  "emacs")
                           (alist-get 'emacs-lisp-mode +eval-runners))))
                 lang)
             (if runner
                 (funcall runner beg end)
               (let ((quickrun-option-cmdkey lang))
                 (quickrun-region beg end))))))))

;;;###autoload
(defun +eval/line-or-region ()
  "Evaluate the current line or selected region."
  (interactive)
  (if (use-region-p)
      (call-interactively #'+eval/region)
    (+eval/region (line-beginning-position) (line-end-position))))

;;;###autoload
(defun +eval/buffer-or-region ()
  "Evaluate the whole buffer."
  (interactive)
  (call-interactively
   (if (use-region-p)
       #'+eval/region
     #'+eval/buffer)))

;;;###autoload
(defun +eval/region-and-replace (beg end)
  "Evaluation a region between BEG and END, and replace it with the result."
  (interactive "r")
  (cond ((eq major-mode 'emacs-lisp-mode)
         (kill-region beg end)
         (condition-case nil
             (prin1 (eval (read (current-kill 0)))
                    (current-buffer))
           (error (message "Invalid expression")
                  (insert (current-kill 0)))))
        ((quickrun-replace-region beg end))))
