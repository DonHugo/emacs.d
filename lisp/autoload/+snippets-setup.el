;; lisp/autoload/+snippets-setup.el -*- lexical-binding: t; -*-
;;;###if (featurep! :editor snippets)

;;;###autoload
(defvar +emacs-snippets-dir (expand-file-name "snippets/" +emacs-dir)
  "Directory where `yasnippet' will search for snippets.")

(defvar +emacs-snippets-enable-short-helpers nil
  "If non-nil, defines convenience aliases for +emacs-snippets' api.
+ `!%!' = (+emacs-snippets-format \"%n%s%n\")
+ `!%' = (+emacs-snippets-format \"%n%s\")
+ `%$' = (+emacs-snippets-format \"%e\")
+ `%expand' = `+emacs-snippets-expand'
+ `%format' = `+emacs-snippets-format'
+ `%without-trigger' = `+emacs-snippets-without-trigger'")

;;;###autoload
(defun +emacs-snippets-remove-compiled-snippets ()
  "Delete all .yas-compiled-snippets.el files."
  (interactive)
  (let ((default-directory +emacs-snippets-dir))
    (dolist (file (file-expand-wildcards "*/.yas-compiled-snippets.el"))
      (delete-file file)
      (message "Deleting %s" file)))
  (let ((default-directory +emacs-snippets-dir))
    (dolist (file (file-expand-wildcards "*/*/.yas-compiled-snippets.el"))
      (delete-file file)
      (message "Deleting %s" file))))

;;;###autoload
(defun +emacs-snippets-initialize ()
  "Add `+snippets-dir' to `yas-snippet-dirs', replacing the default
yasnippet directory."
  (setq yas-wrap-around-region nil)
  (add-to-list 'yas-snippet-dirs '+emacs-snippets-dir)
  (yas-load-directory +emacs-snippets-dir t))

;;;###autoload
(eval-after-load 'yasnippet
  (lambda () (+emacs-snippets-initialize)))
