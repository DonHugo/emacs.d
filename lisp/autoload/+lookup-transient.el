;; lisp/autoload/+lookup-transient.el -*- lexical-binding: t; -*-
;;;###if (and (featurep! :tools lookup) (featurep! :ui transient-state))

;;;###autoload (autoload '+emacs-lookup-transient-state/body "autoload/+lookup-transient" nil t)
(define-transient-state! lookup
  :title "Lookup transient state\n"
  :doc "
 Documentation^^              Definition^^              References^^              Docsets^^              Online^^
 ─────────────^^────────────  ──────────^^────────────  ──────────^^────────────  ───────^^────────────  ──────^^────────────
 [_d_]: Lookup documentation  [_D_]: Lookup definition  [_r_]: Lookup references  [_f_]: Lookup docsets  [_o_]: Lookup online

 [_q_]: Quit"
  :bindings
  ;; Documentation
  ("d" +lookup/documentation :exit t)
  ;; Definition
  ("D" +lookup/definition :exit t)
  ;; References
  ("r" +lookup/references :exit t)
  ;; Docsets
  ("f" +lookup/in-docsets :exit t)
  ;; Online
  ("o" +lookup/online :exit t)
  ;; Quit
  ("q" nil :exit t))
