;; lisp/autoload/+org-capture.el -*- lexical-binding: t; -*-
;;;###if (featurep! :org org)

(defvar org-capture-initial)

;;
;;; External frame

(defvar +org-capture-fn #'org-capture
  "Command to use to initiate org-capture.")

;;;###autoload
(defvar +org-capture-frame-parameters
  `((name . "org-capture")
    (width . 120)
    (height . 25)
    (transient . t)
    ,(when (and IS-LINUX (not (getenv "DISPLAY")))
       `(display . ":0"))
    ,(if IS-MAC '(menu-bar-lines . 1)))
  "TODO")

;;;###autoload
(defun +org-capture--hook-cleanup-frame ()
  "Closes the org-capture frame once done adding an entry."
  (when (and (+org-capture-frame-p)
             (not org-capture-is-refiling))
    (delete-frame nil t)))

;;;###autoload
(defun +org-capture-frame-p (&rest _)
  "Return t if the current frame is an org-capture frame opened
by `+org-capture/open-frame'."
  (and (equal (alist-get 'name +org-capture-frame-parameters)
              (frame-parameter nil 'name))
       (frame-parameter nil 'transient)))

;;;###autoload
(defun +org-capture/open-frame (&optional initial-input key)
  "Opens the org-capture window in a floating frame that cleans
itself up once you're done. This can be called from an external
shell script."
  (interactive)
  (when (and initial-input (string-empty-p initial-input))
    (setq initial-input nil))
  (when (and key (string-empty-p key))
    (setq key nil))
  (let* ((frame-title-format "")
         (frame (if (+org-capture-frame-p)
                    (selected-frame)
                  (make-frame +org-capture-frame-parameters))))
    (select-frame-set-input-focus frame)  ; fix MacOS not focusing new frames
    (with-selected-frame frame
      (require 'org-capture)
      (condition-case ex
          (letf! ((#'pop-to-buffer #'switch-to-buffer))
            (switch-to-buffer (+emacs-fallback-buffer))
            (let ((org-capture-initial initial-input)
                  org-capture-entry)
              ;; For whatever reason we need to echo the template, otherwise
              ;; invocation from CLI with a predefined key and input does not
              ;; work....
              (message "%s" (assoc key org-capture-templates))
              (when (and key (not (string-empty-p key)))
                (setq org-capture-entry (org-capture-select-template key)))
              (funcall +org-capture-fn)))
        ('error
         (message "org-capture: %s" (error-message-string ex))
         (delete-frame frame))))))

;;;###autoload
(defun +org-capture-available-keys ()
  "TODO"
  (string-join (mapcar #'car org-capture-templates) ""))


;;
;;; Capture targets

;;;###autoload
(defun +org-capture-todo-file ()
  "Expand `+org-capture-todo-file' from `org-directory'.
If it is an absolute path return `+org-capture-todo-file'
verbatim."
  (expand-file-name +org-capture-todo-file org-directory))

;;;###autoload
(defun +org-capture-notes-file ()
  "Expand `+org-capture-notes-file' from `org-directory'.
If it is an absolute path return `+org-capture-todo-file'
verbatim."
  (expand-file-name +org-capture-notes-file org-directory))

(defun +org-capture--choose-project-directory (&optional directory)
  "Return a directory chosen by the user. `default-directory' is
set to DIRECTORY if given.."
  (let* ((counsel--find-file-predicate #'file-directory-p)
         (default-directory (unless directory default-directory))
         (selected-directory
          (ivy-read "Choose project directory: "
                    #'read-file-name-internal
                    :matcher #'counsel--find-file-matcher)))
    selected-directory))

(defun +org--capture-local-root (path)
  "Return the root directory of PATH. Allows a manual selection,
the project directory of the current buffer and already known
projects."
  (condition-case ex
      (let* ((filename (file-name-nondirectory path))
             (current-project-directory
              (or (locate-dominating-file (file-truename default-directory)
                                          filename)
                  (+emacs-project-root)))
             (completion-cmd (cond ((featurep! :completion ivy)
                                    #'ivy-completing-read)
                                   (t (user-error "Enable ivy"))))
             (directory
              (funcall completion-cmd "Choose project directory: "
                       (cl-delete-duplicates
                        (append (list "Select")
                                (when current-project-directory
                                  (list current-project-directory))
                                (when projectile-known-projects
                                  projectile-known-projects))
                        :test #'equal)
                       nil t)))
        (cond ((not directory) (user-error "Couldn't detect a project"))
              ((string= directory "Select")
               (setq directory (+org-capture--choose-project-directory))))
        (expand-file-name filename directory))
    ('quit
     (user-error "Aborted"))))

;;;###autoload
(defun +org-capture-project-todo-file ()
  "Find the nearest `+org-capture-todo-file' in a parent
directory, otherwise, opens a blank one at the project root.
Throws an error if not in a project."
  ;; HACK `+file-templates' are usually disabled during `org-capture', for good reason. However,
  ;;      here we allow them only during capturing a project-local todo item
  (add-transient-hook! 'find-file-hook
    (and buffer-file-name
         (not buffer-read-only)
         (bobp) (eobp)
         (not (member (substring (buffer-name) 0 1) '("*" " ")))
         (not (file-exists-p buffer-file-name))
         (when-let (rule (cl-find-if #'+file-template-p +file-templates-alist))
           (apply #'+file-templates--expand rule))))
  (+org--capture-local-root +org-capture-todo-file))

;;;###autoload
(defun +org-capture-project-notes-file ()
  "Find the nearest `+org-capture-notes-file' in a parent
directory, otherwise, opens a blank one at the project root.
Throws an error if not in a project."
  (+org--capture-local-root +org-capture-notes-file))

;;;###autoload
(defun +org-capture-project-changelog-file ()
  "Find the nearest `+org-capture-changelog-file' in a parent
directory,otherwise, opens a blank one at the project root.
Throws an error if not in a project."
  (+org--capture-local-root +org-capture-changelog-file))

(defun +org--capture-ensure-heading (headings &optional initial-level)
  (if (not headings)
      (widen)
    (let ((initial-level (or initial-level 1)))
      (if (and (re-search-forward (format org-complex-heading-regexp-format
                                          (regexp-quote (car headings)))
                                  nil t)
               (= (org-current-level) initial-level))
          (progn
            (beginning-of-line)
            (org-narrow-to-subtree))
        (goto-char (point-max))
        (unless (and (bolp) (eolp)) (insert "\n"))
        (insert (make-string initial-level ?*)
                " " (car headings) "\n")
        (beginning-of-line 0))
      (+org--capture-ensure-heading (cdr headings) (1+ initial-level)))))

(defun +org--capture-central-file (file project)
  (let ((file (expand-file-name file org-directory)))
    (set-buffer (org-capture-target-buffer file))
    (org-capture-put-target-region-and-position)
    (widen)
    (goto-char (point-min))
    ;; Find or create the project headling
    (+org--capture-ensure-heading
     (append (org-capture-get :parents)
             (list project (org-capture-get :heading))))))

(autoload '+emacs--org-headings (expand-file-name "help" (concat +emacs-core-dir "core-autoload/")))
(defun +org--capture-central-project-find-name (file)
  "Return name of the project. Allows manual input, the current
buffer's project and already present projects in the file."
  (condition-case ex
      (let ((file (expand-file-name file org-directory))
            (current-project-directory (if (string= (projectile-project-name) "-")
                                           nil
                                         (projectile-project-name)))
            (completion-cmd (cond ((featurep! :completion ivy)
                                   #'ivy-completing-read)
                                  (t (user-error "Enable ivy")))))
        (string-trim (funcall completion-cmd "Choose project name: "
                              (append (when current-project-directory
                                        (list current-project-directory))
                                      (when (file-exists-p file)
                                        (+emacs--org-headings file 1))))))
    ('quit (user-error "Aborted"))))

;;;###autoload
(defun +org-capture-central-project-todo-file ()
  "TODO"
  (+org--capture-central-file
   +org-capture-projects-file
   (+org--capture-central-project-find-name +org-capture-projects-file)))

;;;###autoload
(defun +org-capture-central-project-notes-file ()
  "TODO"
  (+org--capture-central-file
   +org-capture-projects-file
   (+org--capture-central-project-find-name +org-capture-projects-file)))

;;;###autoload
(defun +org-capture-central-project-changelog-file ()
  "TODO"
  (+org--capture-central-file
   +org-capture-projects-file
   (+org--capture-central-project-find-name +org-capture-projects-file)))
