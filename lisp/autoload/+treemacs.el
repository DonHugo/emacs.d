;; modules/+treemacs.el -*- lexical-binding: t; -*-
;;;###if (featurep! :ui treemacs)

;;;###autoload
(defun +treemacs--backup-projects ()
  "Backup `treemacs-persist-file'."
  (-if-let (ws (treemacs--select-workspace-by-name
                (treemacs-workspace->name (treemacs-current-workspace))))
      (setf (treemacs-current-workspace) ws)
    (treemacs--find-workspace))
  (treemacs--consolidate-projects)
  (treemacs--persist)
  (when (file-exists-p treemacs-persist-file)
    (let ((backup-file (concat treemacs-persist-file ".bk")))
      (copy-file treemacs-persist-file backup-file 'overwrite))))

;;;###autoload
(defun +treemacs--restore-projects ()
  "Restore `treemacs-persist-file'."
  ;; If a backup of `treemacs-persist-file' exists, restore it
  (let ((backup-file (concat treemacs-persist-file ".bk")))
    (when (file-exists-p backup-file)
      (rename-file backup-file treemacs-persist-file 'overwrite)))
  (treemacs--restore)
  ;; No idea why, but necessary
  (-if-let (ws (treemacs--select-workspace-by-name
                (treemacs-workspace->name (treemacs-current-workspace))))
      (setf (treemacs-current-workspace) ws)
    (treemacs--find-workspace))
  (treemacs--consolidate-projects))

;;;###autoload
(defun +treemacs/toggle ()
  "Initialize or toggle treemacs.
Ensures that only the current project is present and all other
projects have been removed. Use `treemacs' command for old
functionality."
  (interactive)
  (require 'treemacs)
  (pcase (treemacs-current-visibility)
    (`visible (delete-window (treemacs-get-local-window)))
    (_ (if (+emacs-project-p)
           (treemacs-display-current-project-exclusively)
         (treemacs)))))
