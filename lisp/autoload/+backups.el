;; lisp/autoload/+backups.el -*- lexical-binding: t; -*-
;;;###if (featurep! :tools backups)

;; This file makes Emacs' backup system a bit more useful. It implements a new
;; major mode `+backups-mode' and `+backups/list-backups' which lists all
;; available local backups for an open file. These backups can viewed, diffed
;; and if desired, used to revert the file. This implementation is based on
;; https://github.com/chadbraunduin/backups-mode, but slimmed down and adjusted
;; to my liking.

(eval-when-compile (require 'cl-macs))
(eval-and-compile (require 'diff))


;;
;;; Library

(cl-defun +backups--list-backups-from-file (original-file &key data)
  ;; Create new named buffer
  (let ((buf (format "*Backups: %s*" (buffer-name (get-file-buffer original-file)))))
    (get-buffer-create buf)
    (pop-to-buffer buf)
    (erase-buffer)
    ;; Switch to +backups-mode
    (+backups-mode)
    ;; Remove write protection
    (read-only-mode -1)
    ;; Collect data
    (unless (assq :original-file data)
      (push `(:original-file . ,original-file) data))
    (push `(:backups . ,(+backups-get-sorted-backups original-file +backups-files-function)) data)
    (push `(:backups-buffer . ,(current-buffer)) data)
    ;; Assign data
    (setq +backups-mode-data-alist data)
    ;; Assign first diff index
    (setq +backups-first-diff-index nil)
    ;; Collect and assign related backup files
    (let (value)
      (dolist (element (cdr (assq :backups data)) value)
        (setq value (cons (nth 2 element) value)))
      (setq +backups-assoc-files-alist (butlast value))))
  ;; Insert buffer contents
  (insert (format "%s\n" original-file))
  (insert
   (apply 'concat (mapcar
                   (lambda (file)
                     (let* ((version (+backups-get-version file))
                            (version (if version (number-to-string version) "current"))
                            (last-modified-date (or (+backups-get-last-modified-date file) (concat "unknown" "\t"))))
                       (format "  %-6s\t%s"
                               (propertize version 'face 'font-lock-keyword-face)
                               last-modified-date)))
                   (+backups-get-backups data))))
  ;; Move the cursor to the top
  (goto-char 1)
  (forward-line)
  (set-buffer-modified-p nil)
  ;; Enable write protection
  (read-only-mode 1))


;;;; Helpers

;; Listing
(defvar +backups-files-function '+backups-backup-files
  "Function to return backup files.")
(defvar +backups-last-modified-date-command-function '+backups-nix-last-modified-date-command
  "Platform specific way of getting last modified date.")
(defvar +backups-unknown-last-modified-date "date:"
  "Platform specific output for unknown last modified date.")

(defun +backups-file-sort-p (file-name1 file-name2)
  (let ((version1 (+backups-make-version-number file-name1))
        (version2 (+backups-make-version-number file-name2)))
    (> version1 version2)))

(defun +backups-get-sorted-backups (original-file +backups-files-function)
  "Return sorted backup file list."
  (mapcar '+backups-make-file
          (cons original-file (sort
                               (funcall +backups-files-function original-file)
                               '+backups-file-sort-p))))

;; Getters
(defun +backups-get-backups (data)
  (cdr (assq :backups data)))

(defun +backups-get-file-name (file)
  (nth 2 file))

(defun +backups-get-last-modified-date (file)
  (nth 1 file))

(defun +backups-get-version (file)
  (nth 0 file))

(defun +backups-make-version-number (file-name)
  (let ((try-version-index (string-match "~[0-9]+~$" file-name)))
    (when try-version-index
      (+backups-full-version-number file-name try-version-index))))

(defun +backups-full-version-number (file-name start &optional number-str)
  (let* ((number-str (or number-str ""))
         (number (string-to-number number-str)))
    (if (< start (length file-name))
        (let ((current-char (substring file-name (+ 1 start) (+ 2 start))))
          (cond ((equal current-char "0") (+backups-full-version-number file-name (+ 1 start) (concat number-str current-char)))
                ((equal (string-to-number current-char) 0) number)
                (t (+backups-full-version-number file-name (+ 1 start) (concat number-str current-char)))))
      number)))

(defun +backups-make-last-modified-date (file-name)
  (let ((last-modified-date (shell-command-to-string
                             (funcall +backups-last-modified-date-command-function file-name))))
    (when (not (equal (car (split-string last-modified-date)) +backups-unknown-last-modified-date))
      last-modified-date)))

(defun +backups-make-file (file-name)
  (list
   (+backups-make-version-number file-name)
   (+backups-make-last-modified-date file-name)
   file-name))

(defun +backups-nix-last-modified-date-command (file-name) (concat "date -r " file-name " +\"%x %r\""))

(defun +backups-backup-files (original-file)
  (let* ((backup-file (file-name-sans-versions
                       (make-backup-file-name (expand-file-name original-file))))
         (backup-directory (file-name-directory backup-file)))
    (mapcar
     (lambda (f) (concat backup-directory f))
     (file-name-all-completions
      (file-name-nondirectory backup-file)
      backup-directory))))

;; Diff

(defun +backups-get-file-name-from-index (index)
  (+backups-get-file-name (nth index (+backups-get-backups +backups-mode-data-alist))))

(defun +backups-get-index-number (line-number)
  (- line-number 2))

(defun +backups-get-line-number (index)
  (+ index 2))

(defun +backups-get-original-file (data)
  (cdr (assq :original-file data)))


;;
;;; Functions

(defun +backups-cleanup-and-close ()
  "Cleanup open buffers and close backup session."
  (mapc (lambda (buffer)
          (when (buffer-live-p (get-file-buffer buffer))
            (kill-buffer (get-file-buffer buffer))))
        +backups-assoc-files-alist)
  (kill-buffer-and-window))

(defun +backups-diff ()
  "Diff two versions of the file."
  (interactive)
  ;; Remove write protection
  (read-only-mode -1)
  (let* ((line-number (line-number-at-pos))
         (index (+backups-get-index-number line-number))
         (orig-buffer-name (buffer-name (get-file-buffer (+backups-get-original-file +backups-mode-data-alist)))))
    (if (and (>= index 0) (< index (length (+backups-get-backups +backups-mode-data-alist))))
        (progn
          (cond ((eq +backups-first-diff-index index)
                 (beginning-of-line)
                 (delete-char 1)
                 (insert " ")
                 (setq +backups-first-diff-index nil)
                 (beginning-of-line))
                (+backups-first-diff-index
                 (goto-line (+backups-get-line-number +backups-first-diff-index))
                 (delete-char 1)
                 (insert " ")
                 (goto-line line-number)
                 (progn
                   (when (and
                          (zerop +backups-first-diff-index)
                          (get-buffer orig-buffer-name)
                          (buffer-modified-p (get-buffer orig-buffer-name)))
                     (let ((backups-mode-buffer-name (buffer-name)))
                       (switch-to-buffer orig-buffer-name)
                       (save-buffer)
                       (switch-to-buffer backups-mode-buffer-name)))
                   (let ((first-file-name (+backups-get-file-name-from-index +backups-first-diff-index))
                         (second-file-name (+backups-get-file-name-from-index index)))
                     (setq +backups-first-diff-index nil)
                     (set-buffer-modified-p nil)
                     (ediff first-file-name second-file-name))))
                (t
                 (setq +backups-first-diff-index index)
                 (beginning-of-line)
                 (insert "d")
                 (delete-char 1)
                 (forward-line)))
          (set-buffer-modified-p nil))
      (error "No file selected.")))
  ;; Enable write protection
  (read-only-mode 1))

(defun +backups-revert ()
  "Revert current file to chosen backup."
  (interactive)
  (let ((index (+backups-get-index-number (line-number-at-pos))))
    (cond ((zerop index)
           (error "Cannot revert current file."))
          ((and (> index 0) (< index (length (+backups-get-backups +backups-mode-data-alist))))
           (+backups-revert-backup-from-file (+backups-get-original-file +backups-mode-data-alist)
                                             (+backups-get-file-name-from-index index)
                                             +backups-mode-data-alist))
          (t (error "No file selected.")))))

(defun +backups-revert-backup-from-file (orig-file-name backup-file-name data)
  (let* ((temp-backup-file-name (concat backup-file-name "#temp#"))
         (orig-buffer-name (buffer-name (get-file-buffer orig-file-name))))
    ;; Ask before reverting
    (when (y-or-n-p "Do you really want to revert this file permanently?")
      ;; Create temporary copy
      (copy-file backup-file-name temp-backup-file-name t)
      ;; Save current file
      (when orig-buffer-name
        (with-current-buffer orig-buffer-name
          (save-buffer)))
      ;; Copy temporary copy over original file
      (copy-file temp-backup-file-name orig-file-name t)
      ;; Remove temporary copy
      (delete-file temp-backup-file-name))))

(defun +backups-view ()
  "View a single backup file."
  (interactive)
  (let ((index (+backups-get-index-number (line-number-at-pos))))
    (cond ((zerop index)
           (error "Cannot view current file."))
          ((and (> index 0) (< index (length (+backups-get-backups +backups-mode-data-alist))))
           (let ((file-name (+backups-get-file-name-from-index index))
                 (buf (format "*Backup-View: %s*" (+backups-get-file-name-from-index index))))
             (find-file-read-only-other-window file-name)
             (+backups-view-mode)))
          (t (error "No file selected.")))))


;;
;;; Modes

;; Major mode
(define-derived-mode +backups-mode special-mode
  "Backups"
  "Major mode for viewing, inspecting and reverting of a buffer's
  backup files."
  :syntax-table nil
  :abbrev-table nil
  (buffer-disable-undo)
  (setq-local header-line-format (concat "<return> view backup, <d> + <d> diff, <R> revert, <q> quit"))
  (defvar-local +backups-mode-data-alist nil
    "Stores information about the opened file and its available backups.")
  (defvar-local +backups-first-diff-index nil)
  (defvar-local +backups-assoc-files-alist nil
    "Stores filenames of associated backup files."))

(define-key! +backups-mode-map
  "d"   #'+backups-diff
  "RET" #'+backups-view
  "R"   #'+backups-revert
  "q"   (cmd! (+backups-cleanup-and-close)))


;; Minor mode
(define-minor-mode +backups-view-mode
  "Minor mode for viewing a single backup file."
  :init-value nil
  :lighter " Backup-file"
  :keymap (let ((map (make-sparse-keymap)))
            map)
  (setq header-line-format "Press <q> to exit buffer.")
  (defun +backups-close-view-buffer ()
    (interactive)
    (kill-buffer-and-window)
    (set-window-configuration +backups--saved-wconf))
  (define-key! +backups-view-mode-map
    "q" #'+backups-close-view-buffer)
  (evil-emacs-state))


(after! evil
  (set-evil-initial-state! '(+backups-mode +backups-view-mode)
    'emacs)
  (setq-hook! '(+backups-mode-hook +backups-view-mode-hook)
    evil-emacs-state-cursor '(box +evil-default-cursor)))


;;
;;; Commands

;;;###autoload
(defun +backups/list-backups ()
  "Lists all saved backups for the current file."
  (interactive)
  (let ((original-file (buffer-file-name)))
    (if original-file
        (+backups--list-backups-from-file original-file)
      (error "No backups for a non-file visiting buffer."))))

;;;###autoload
(defun +backups/list-orphaned ()
  "List orphaned backup files."
  (interactive)
  (let ((cmd (expand-file-name "scripts/show-orphaned.sh" user-emacs-directory))
        (buf (format "*Backups: Orphans*")))
    (get-buffer-create buf)
    (pop-to-buffer buf)
    (erase-buffer)
    (dolist (orphan (split-string (cdr (+emacs-call-process cmd))))
      (insert (format "%s\n" orphan)))
    (+backups-view-mode +1)))


;;;###autoload
(defun +backups/remove-orphaned ()
  "Remove orphaned backup files."
  (interactive)
  (let ((cmd (expand-file-name "scripts/show-orphaned.sh" user-emacs-directory)))
    (dolist (file (split-string (cdr (+emacs-call-process cmd))))
      (delete-file file)
      (message "Deleting %s" file))))


;;
;;; Popup rules and window configuration

(defvar +backups--saved-wconf nil)

(defhook! +backups--hook-save-wconf ()
  "Save window config during backup mode."
  '+backups-mode-hook
  (setq +backups--saved-wconf (current-window-configuration)))

(set-popup-rule! "^\\*Backups: " :side 'bottom :height 0.4 :select t :quit nil :ttl nil)
(set-popup-rule! "^\\*Backup-View: " :side 'right :width 0.4 :select t :quit t)
