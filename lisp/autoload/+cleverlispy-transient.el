;; lisp/autoload/+cleverlispy-transient.el -*- lexical-binding: t; -*-
;;;###if (and (featurep! :editor cleverlispy) (featurep! :ui transient-state))

(defvar +cleverlispy-ts-full-hint-toggle nil
  "Display cleverlispy transient state documentation.")

(defun +cleverlispy-ts-toggle-hint ()
  "Toggle the full hint docstring for the cleverlispy transient state."
  (interactive)
  (setq +cleverlispy-ts-full-hint-toggle
        (not +cleverlispy-ts-full-hint-toggle)))

(defun +cleverlispy-ts-hint ()
  "Return a condensed/full hint for the window transient state"
  (concat
   " "
   (if +cleverlispy-ts-full-hint-toggle
       +cleverlispy--ts-full-hint
     (concat "[" (propertize "?" 'face 'hydra-face-red) "] help"
             +cleverlispy--ts-minified-hint))))

(add-transient-hook! #'+emacs-cleverlispy-transient-state/body
  (+emacs--transient-state-format-hint cleverlispy
    +cleverlispy--ts-minified-hint "\n
 Move: _H_ _L_ _M-h_ _M-l_ _(_/_[_/_{_ _)_/_]_/_}_ Edit: _<_ _>_ _M-J_")

  (+emacs--transient-state-format-hint cleverlispy
    +cleverlispy--ts-full-hint
    "\n[_?_] toggle help\n
 Move^^                     Editing^^                Editing^^                       Editing^^
 ────^^───────────────────  ───────^^──────────────  ───────^^─────────────────────  ───────^^────────────────────
 [_H_]:   backward sexp     [_>_]:   slurp           [_M-a_]: insert at end of list  [_M-o_]: open below
 [_L_]:   forward sexp      [_<_]:   barf            [_M-i_]: insert at beg of list  [_M-O_]: open above
 [_M-h_]: beginning defun   [_M-t_]: transpose sexp  [_M-w_]: copy paste form        [_M-v_]: convolute sexp
 [_M-l_]: end defun         [_M-k_]: drag backward   [_M-y_]: yank sexp              [_M-(_]: wrap next round
 [_(_]:   backward up sexp  [_M-j_]: drag forward    [_M-d_]: delete sexp            [_M-)_]: wrap previous round
 [_)_]:   up sexp           [_M-J_]: join sexp       [_M-c_]: change sexp            [_M-[_]: wrap next square
 [_[_]:   previous opening  [_M-s_]: splice sexp     [_M-Y_]: yank enclosing         [_M-]_]: wrap previous square
 [_]_]:   next closing      [_M-S_]: split sexp      [_M-D_]: delete enclosing       [_M-{_]: wrap next curly
 [_{_]:   next opening      [_M-R_]: raise form      [_M-C_]: change enclosing       [_M-}_]: wrap previous curly
 [_}_]:   previous closing  [_M-r_]: raise sexp      [_M-q_]: indent defun           ^^
 ^^                         ^^                       ^^                              ^^
 [_u_/_C-r_]: undo/redo [_1_.._9_]: digit argument [_q_]: quit"))

;;;###autoload (autoload '+emacs-cleverlispy-transient-state/body "autoload/+cleverlispy-transient" nil t)
(define-transient-state! cleverlispy
  :title "Cleverlispy"
  :foreign-keys run
  :hint-is-doc t
  :dynamic-hint (+cleverlispy-ts-hint)
  :on-enter (unless (featurep 'evil-cleverparens)
              (evil-cleverparens-mode +1))
  :bindings
  ("?" +cleverlispy-ts-toggle-hint)
  ;; Move
  ("H" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville-backward-sexp))
             (t (call-interactively #'evil-cp-backward-sexp))))
  ("L" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville-forward-sexp))
             (t (call-interactively #'evil-cp-forward-sexp))))
  ("M-h" (cond ((bound-and-true-p lispyville-mode)
                (call-interactively #'lispyville-beginning-of-defun))
             (t (call-interactively #'evil-cp-beginning-of-defun))))
  ("M-l" (cond ((bound-and-true-p lispyville-mode)
                (call-interactively #'lispyville-end-of-defun))
             (t (call-interactively #'evil-cp-end-of-defun))))
  ("(" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville-backward-up-list))
             (t (call-interactively #'evil-cp-backward-up-sexp))))
  (")" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville-up-list))
             (t (call-interactively #'evil-cp-up-sexp))))
  ("[" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville-previous-opening))
             (t (call-interactively #'evil-cp-previous-opening))))
  ("]" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville-next-closing))
             (t (call-interactively #'evil-cp-next-closing))))
  ("{" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville-next-opening))
             (t (call-interactively #'evil-cp-next-opening))))
  ("}" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville-previous-closing))
             (t (call-interactively #'evil-cp-previous-closing))))
  ;; Slurp/Barf
  (">" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville->))
             (t (call-interactively #'evil-cp->))))
  ("<" (cond ((bound-and-true-p lispyville-mode)
              (call-interactively #'lispyville-<))
             (t (call-interactively #'evil-cp-<))))
  ;; Additonal bindings
  ("M-t" (call-interactively #'sp-transpose-sexp))
  ("M-k" (cond ((bound-and-true-p lispyville-mode)
                (call-interactively #'lispyville-drag-backward))
               (t (call-interactively #'evil-cp-drag-backward))))
  ("M-j" (cond ((bound-and-true-p lispyville-mode)
                (call-interactively #'lispyville-drag-forward))
               (t (call-interactively #'evil-cp-drag-forward))))
  ("M-J" (call-interactively #'sp-join-sexp))
  ("M-s" (call-interactively #'sp-splice-sexp))
  ("M-S" (call-interactively #'sp-split-sexp))
  ("M-R" (cond ((bound-and-true-p lispyville-mode)
                (call-interactively #'lispyville-raise-list))
               (t (call-interactively #'evil-cp-raise-form))))
  ("M-r" (call-interactively #'sp-raise-sexp))
  ("M-a" (cond ((bound-and-true-p lispyville-mode)
                (call-interactively #'lispyville-insert-at-end-of-list))
               (t (call-interactively #'evil-cp-insert-at-end-of-form))))
  ("M-i" (cond ((bound-and-true-p lispyville-mode)
                (call-interactively #'lispyville-insert-at-beginning-of-list))
               (t (call-interactively #'evil-cp-insert-at-beginning-of-form))))
  ("M-w" (call-interactively #'evil-cp-copy-paste-form))
  ("M-y" (call-interactively #'evil-cp-yank-sexp))
  ("M-d" (call-interactively #'evil-cp-delete-sexp))
  ("M-c" (call-interactively #'evil-cp-change-sexp))
  ("M-Y" (call-interactively #'evil-cp-yank-enclosing))
  ("M-D" (call-interactively #'evil-cp-delete-enclosing))
  ("M-C" (call-interactively #'evil-cp-change-enclosing))
  ("M-q" (call-interactively #'sp-indent-defun))
  ("M-o" (cond ((bound-and-true-p lispyville-mode)
                (call-interactively #'lispyville-open-below-list))
               (t (call-interactively #'evil-cp-open-below-form))))
  ("M-O" (cond ((bound-and-true-p lispyville-mode)
                (call-interactively #'lispyville-open-above-list))
               (t (call-interactively #'evil-cp-open-above-form))))
  ("M-v" (call-interactively #'sp-convolute-sexp))
  ("M-(" (call-interactively #'evil-cp-wrap-next-round))
  ("M-)" (call-interactively #'evil-cp-wrap-previous-round))
  ("M-[" (call-interactively #'evil-cp-wrap-next-square))
  ("M-]" (call-interactively #'evil-cp-wrap-previous-square))
  ("M-{" (call-interactively #'evil-cp-wrap-next-curly))
  ("M-}" (call-interactively #'evil-cp-wrap-previous-curly))
  ;; Digit argument
  ("1" digit-argument)
  ("2" digit-argument)
  ("3" digit-argument)
  ("4" digit-argument)
  ("5" digit-argument)
  ("6" digit-argument)
  ("7" digit-argument)
  ("8" digit-argument)
  ("9" digit-argument)
  ("u" (call-interactively #'undo-tree-undo))
  ("C-r" (call-interactively #'undo-tree-redo))
  ;; Quit
  ("q" nil :exit t))
