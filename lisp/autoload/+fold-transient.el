;; lisp/autoload/+fold-transient.el -*- lexical-binding: t; -*-
;;;###if (and (featurep! :editor fold) (featurep! :ui transient-state))

;;;###autoload (autoload '+emacs-fold-transient-state/body "autoload/+fold-transient" nil t)
(define-transient-state! fold
  :title "Fold transient state\n"
  :doc "
 Movement^^^^         Actions^^
 ────────^^^^───────  ───────^^─────────
 [_n_/_j_]: next      [_TAB_]: toggle
 [_p_/_k_]: previous  [_o_]:   open
 ^^^^                 [_c_]:   close
 ^^^^                 [_O_]:   open all
 ^^^^                 [_C_]:   close all
 [_q_]: quit"
  :bindings
  ;; Movement
  ("n" #'+fold/next)
  ("j" #'+fold/next)
  ("p" #'+fold/previous)
  ("k" #'+fold/previous)
  ;; Actions
  ("TAB" #'+fold/toggle)
  ("<tab>" #'+fold/toggle)
  ("o" #'+fold/open)
  ("c" #'+fold/close)
  ("O" #'+fold/open-all)
  ("C" #'+fold/close-all)

  ("q" nil :exit t))
