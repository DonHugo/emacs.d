;; lisp/autoload/+vc-evil.el -*- lexical-binding: t; -*-
;;;###if (and (featurep! :emacs vc) (featurep! :editor evil))

;;;###autoload (autoload '+vc:git-browse "autoload/+vc-evil" nil t)
(evil-define-command +vc:git-browse (bang)
  "Ex interface to `+vc/git-browse-region-or-line'."
  (interactive "<!>")
  (+vc/git-browse-region-or-line bang))
