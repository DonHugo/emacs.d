;; lisp/autoload/+org-transient.el -*- lexical-binding: t; -*-
;;;###if (and (featurep! :org org) (featurep! :ui transient-state))

;;
;;; `org-babel'

;;;###autoload (autoload '+emacs-org-babel-transient-state/body "autoload/+org-transient" nil t)
(define-transient-state! org-babel
  :title "Org babel transient state\n"
  :doc "
 ^^^^───────────────────────  ^^──────────────────────
 [_j_/_n_]: next              [_'_]: edit src block
 [_k_/_N_]: previous          [_e_]: execute src block
 [_g_]:^^   goto named block  ^^
 [_z_]:^^   recenter          ^^

 [_q_]: quit"
  :bindings
  ("j" #'org-babel-next-src-block)
  ("n" #'org-babel-next-src-block)
  ("k" #'org-babel-previous-src-block)
  ("N" #'org-babel-next-src-block)
  ("g" #'org-babel-goto-named-src-block)
  ("z" #'recenter-top-bottom)

  ("'" #'org-edit-special :exit t)
  ("e" #'org-babel-execute-maybe :exit t)

  ("q" #'nil :exit t))


;;
;;; `org-agenda'

;;;###autoload (autoload '+emacs-org-agenda-transient-state/body "autoload/+org-transient" nil t)
(define-transient-state! org-agenda
  :title "Org-agenda transient state\n"
  :doc "
 Headline^^            Visit entry^^               Filter^^                    Date^^                   View^^             Clock^^         Other^^
 ────────^^──────────  ───────────^^─────────────  ──────^^──────────────────  ────^^─────────────────  ────^^───────────  ─────^^───────  ─────^^───────────
 [_ht_]: set status    [_SPC_]: in other window    [_ft_]: by tag              [_ds_]: schedule         [_vd_]: day        [_cI_]: in      [_gr_]: reload
 [_hk_]: kill          [_TAB_]: go to location     [_fr_]: refine by tag       [_dS_]: un-schedule      [_vw_]: week       [_cO_]: out     [_._]: go to today
 [_hr_]: refile        [_RET_]: del other windows  [_fc_]: by category         [_dd_]: set deadline     [_vt_]: fortnight  [_cq_]: cancel  [_gd_]: go to date
 [_hA_]: archive       [_o_]: link                 [_fh_]: by top headline     [_dD_]: remove deadline  [_vm_]: month      [_cj_]: jump    ^^
 [_h:_]: set tags      ^^                          [_fx_]: by regexp           [_dt_]: timestamp        [_vy_]: year       ^^              ^^
 [_hp_]: set priority  ^^                          [_fd_]: delete all filters  [_+_]: do later          [_vn_]: next span  ^^              ^^
 ^^                    ^^                          ^^                          [_-_]: do earlier        [_vp_]: prev span  ^^              ^^
 ^^                    ^^                          ^^                          ^^                       [_vr_]: reset      ^^              ^^
 [_t_]: toggles
 [_q_]: exit
"
  :on-enter (setq which-key-inhibit t)
  :on-exit (setq which-key-inhibit nil)
  :foreign-keys run
  :bindings
  ;; Headline
  ("ht" org-agenda-todo)
  ("hk" org-agenda-kill)
  ("hr" org-agenda-refile)
  ("hA" org-agenda-archive-default)
  ("h:" org-agenda-set-tags)
  ("hp" org-agenda-priority)
  ;; Visit entry
  ("SPC" org-agenda-show-and-scroll-up)
  ("TAB" org-agenda-goto :exit t)
  ("<tab>" org-agenda-goto :exit t)
  ("RET" org-agenda-switch-to :exit t)
  ("<return>" org-agenda-switch-to :exit t)
  ("o"   link-hint-open-link :exit t)
  ;; Filter
  ("ft" org-agenda-filter-by-tag)
  ("fr" org-agenda-filter-by-tag-refine)
  ("fc" org-agenda-filter-by-category)
  ("fh" org-agenda-filter-by-top-headline)
  ("fx" org-agenda-filter-by-regexp)
  ("fd" org-agenda-filter-remove-all)
  ;; Date
  ("ds" org-agenda-schedule)
  ("dS" (let ((current-prefix-arg '(4)))
          (call-interactively 'org-agenda-schedule)))
  ("dd" org-agenda-deadline)
  ("dD" (let ((current-prefix-arg '(4)))
          (call-interactively 'org-agenda-deadline)))
  ("dt" org-agenda-date-prompt)
  ("+" org-agenda-do-date-later)
  ("-" org-agenda-do-date-earlier)
  ;; Toggles
  ("t" +emacs-org-agenda-toggles-transient-state/body :exit t)
  ;; View
  ("vd" org-agenda-day-view)
  ("vw" org-agenda-week-view)
  ("vt" org-agenda-fortnight-view)
  ("vm" org-agenda-month-view)
  ("vy" org-agenda-year-view)
  ("vn" org-agenda-later)
  ("vp" org-agenda-earlier)
  ("vr" org-agenda-reset-view)
  ;; Clock
  ("cI" org-agenda-clock-in :exit t)
  ("cO" org-agenda-clock-out)
  ("cq" org-agenda-clock-cancel)
  ("cj" org-agenda-clock-goto :exit t)
  ;; Other
  ("gr" org-agenda-redo)
  ("." org-agenda-goto-today)
  ("gd" org-agenda-goto-date)
  ("q" nil "exit" :exit t))


(define-transient-state! org-agenda-toggles
  :title "Org-agenda toggles\n"
  :doc "
 ^^─────────────────
 [_h_]: %s(+emacs-transient-state-toggle \"habits\" (default-value 'org-habit-show-habits))
 [_f_]: %s(+emacs-transient-state-toggle \"follow\" (bound-and-true-p org-agenda-follow-mode))
 [_l_]: %s(+emacs-transient-state-toggle \"log\" (default-value 'org-agenda-show-log))
 [_a_]: %s(+emacs-transient-state-toggle \"archive\" (bound-and-true-p org-agenda-archives-mode))
 [_r_]: %s(+emacs-transient-state-toggle \"clock report\" (bound-and-true-p org-agenda-clockreport-mode))
 [_i_]: clock issues
 [_d_]: %s(+emacs-transient-state-toggle \"diaries\" (default-value 'org-agenda-include-diary))
 ^^
 [_q_] Quit
"
  :on-enter (setq which-key-inhibit t)
  :on-exit (setq which-key-inhibit nil)
  :foreign-keys run
  :bindings
  ("h" org-habit-toggle-display-in-agenda)
  ("f" org-agenda-follow-mode)
  ("l" org-agenda-log-mode)
  ("a" org-agenda-archives-mode)
  ("r" org-agenda-clockreport-mode)
  ("i" org-agenda-show-clocking-issues)
  ("d" org-agenda-toggle-diary)

  ("q" nil :exit t))
