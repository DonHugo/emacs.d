;; lisp/autoload/+snippets-settings.el -*- lexical-binding: t; -*-
;;;###if (featurep! :editor snippets)

;;;###autoload
(defun set-yas-minor-mode! (modes)
  "Register minor MODES (one mode symbol or a list of them) with yasnippet so it
can have its own snippets category, if the folder exists."
  (dolist (mode (+emacs-enlist modes))
    (let ((fn (intern (format "+snippets--register-%s-hook" mode))))
      (fset fn (lambda () (yas-activate-extra-mode mode)))
      (add-hook (intern (format "%s-hook" mode)) fn))))
