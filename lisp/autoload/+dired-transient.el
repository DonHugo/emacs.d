;; lisp/autoload/+dired-transient.el -*- lexical-binding: t; -*-
;;;###if (and (featurep! :emacs dired) (featurep! :ui transient-state))

;;;###autoload (autoload '+emacs-dired-transient-state/body "autoload/+dired-transient" nil t)
(define-transient-state! dired
  :title "Dired transient state\n"
  :doc "
 Movement^^               Actions^^                    Marking & Action^^   Modifying^^
 ────────^^─────────────  ───────^^──────────────────  ────────────────^^  ──────────^^─────────────────────
 [_n_]: Next line         [_d_]: Flag for del          [_m_]: Mark         [_i_]: Insert subdir
 [_p_]: Previous line     [_x_]: Delete flagged        [_u_]: Unmark       [_k_]: Remove marked from listing
 [_>_]: Next dirline      [_f_]: Visit file            [_U_]: Unmark all   [_g_]: Refresh
 [_<_]: Previous dirline  [_v_]: View file             [_c_]: Actions      [_s_]: Toggle sort
 [_-_]: Dir up            [_o_]: Visit file /o window  [_*_]: Marks        [_$_]: Hide subdir
 ^^                       [_+_]: New dir               [_t_]: Invert       [_M-$_]: Hide all subdir
 ^^                       [_=_]: Diff                  [_%_]: Regex        [_._]: Toggle hidden
 [_q_]: Quit"
  :foreign-keys run
  :bindings
  ;; Movement
  ("n" #'dired-next-line)
  ("p" #'dired-previous-line)
  (">" #'dired-next-dirline)
  ("<" #'dired-prev-dirline)
  ("-" #'dired-up-directory)
  ;; Actions
  ("d" #'dired-flag-file-deletion)
  ("x" #'dired-do-flagged-delete)
  ("f" #'dired-find-file)
  ("v" #'dired-view-file)
  ("o" #'dired-find-file-other-window)
  ("+" #'dired-create-directory)
  ("=" #'dired-diff)
  ;; Marking & Action
  ("m" #'dired-mark)
  ("u" #'dired-unmark)
  ("U" #'dired-unmark-all-files)
  ("c" #'+emacs-dired-mark-action-transient-state/body :exit t)
  ("*" #'+emacs-dired-mark-transient-state/body :exit t)
  ("t" #'dired-toggle-marks)
  ("%" #'+emacs-dired-regex-transient-state/body :exit t)
  ;; Modifying
  ("i" #'dired-maybe-insert-subdir)
  ("k" #'dired-do-kill-lines)
  ("g" #'revert-buffer)
  ("s" #'dired-sort-toggle-or-edit)
  ("$" #'dired-hide-subdir)
  ("M-$" #'dired-hide-all)
  ("." #'+dired/toggle-hidden-files)
  ;; Quit
  ("q" nil :exit t))

(define-transient-state! dired-mark-action
  :title "Dired marked actions transient state\n"
  :doc "
 ^^───────────  ^^─────────────────  ^^────────────────
 [_C_]: Copy    [_P_]: Print         [_S_]: Symlink
 [_R_]: Rename  [_l_]: Downcase      [_Y_]: Relsymlink
 [_O_]: chown   [_u_]: Upcase        [_H_]: Hardlink
 [_G_]: Chgrp   [_D_]: Delete        [_B_]: Bytecompile
 [_M_]: chmod   [_Z_]: Un-/Compress  ^^

 [_q_]: back"
  :foreign-keys run
  :bindings
  ("C" #'dired-do-copy)
  ("R" #'dired-do-rename)
  ("O" #'dired-do-chown)
  ("G" #'dired-do-chgrp)
  ("M" #'dired-do-chmod)

  ("P" #'dired-do-print )
  ("l" #'dired-downcase)
  ("u" #'dired-upcase)
  ("D" #'dired-do-delete)
  ("Z" #'dired-do-compress)

  ("S" #'dired-do-symlink)
  ("Y" #'dired-do-relsymlink)
  ("H" #'dired-do-hardlink)
  ("B" #'dired-do-byte-compile)

  ("q" #'+emacs-dired-transient-state/body :exit t))

(define-transient-state! dired-mark
  :title "Dired mark transient state\n"
  :doc "
 ^^────────────────  ^^────────────────────
 [_._]: Extension    [_s_]: Files in subdir
 [_/_]: Directories  [_%_]: Regex
 [_@_]: Symlinks     [_c_]: Change marks
 [_*_]: Executables  [_(_]: Sexp

 [_q_]: back"
  :foreign-keys run
  :bindings
  ("." #'dired-mark-extension)
  ("/" #'dired-mark-directories)
  ("@" #'dired-mark-symlinks)
  ("*" #'dired-mark-executables)

  ("s" #'dired-mark-subdir-files)
  ("%" #'dired-mark-files-regexp)
  ("c" #'dired-change-marks)
  ("(" #'dired-mark-sexp)

  ("q" #'+emacs-dired-transient-state/body :exit t))

(define-transient-state! dired-regex
  :title "Dired regex transient state\n"
  :doc "
 ^^────────────────────  ^^───────────────────────────
 [_m_]: Mark regexp      [_S_]: Symlink regexp
 [_C_]: Copy regexp      [_Y_]: Relative symlink regex
 [_R_]: Rename regexp    [_d_]: Mark deletion
 [_H_]: Hardlink regexp  ^^

 [_q_]: back"
  :foreign-keys run
  :bindings
  ("m" #'dired-mark-files-regexp)
  ("C" #'dired-do-copy-regexp)
  ("R" #'dired-do-rename-regexp)
  ("H" #'dired-do-hardlink-regexp)

  ("S" #'dired-do-symlink-regexp)
  ("Y" #'dired-do-relsymlink-regexp)
  ("d" #'dired-flag-files-regexp)

  ("q" #'+emacs-dired-transient-state/body :exit t))
