;; lisp/autoload/+pretty-code.el -*- lexical-binding: t; -*-
;;;###if (featurep! :ui pretty-code)

;;;###autoload
(defun set-pretty-symbols! (modes &rest plist)
  "Associates string patterns with icons in certain major-modes.

MODES is a major mode symbol or a list of them. PLIST is a
property list whose keys must match keys in
`+pretty-code-extra-symbols', and whose values are strings
representing the text to be replaced with that symbol. If the car
of PLIST is nil, then unset any pretty symbols previously defined
for MODES.

This function accepts one special property:

:alist ALIST Appends ALIST to `prettify-symbols-alist' literally,
without mapping text to `+pretty-code-extra-symbols'.

For example, the rule for emacs-lisp-mode is very simple:

  (set-pretty-symbols! 'emacs-lisp-mode
    :lambda \"lambda\")

This will replace any instances of \"lambda\" in emacs-lisp-mode
with the symbol assicated with :lambda in
`+pretty-code-extra-symbols'.

Pretty symbols can be unset for emacs-lisp-mode with:

  (set-pretty-symbols! 'emacs-lisp-mode nil)"
  (declare (indent defun))
  (if (null (car-safe plist))
      (dolist (mode (+emacs-enlist modes))
        (delq! mode +pretty-code-extra-alist 'assq))
    (let (results)
      (while plist
        (let ((key (pop plist)))
          (if (eq key :alist)
              (prependq! results (pop plist))
            (when-let (char (plist-get +pretty-code-extra-symbols key))
              (push (cons (pop plist) char) results)))))
      (dolist (mode (+emacs-enlist modes))
        (setf (alist-get mode +pretty-code-extra-alist)
              (if-let (old-results (alist-get mode +pretty-code-extra-alist))
                  (dolist (cell results old-results)
                    (setf (alist-get (car cell) old-results) (cdr cell)))
                results))))))


;;
;;; Ligatures

(defvar +ligature--composition-table (make-char-table nil))
;;;###autoload
(define-minor-mode +ligatures-mode "Enables typographic ligatures" nil nil nil
  (if +ligatures-mode
      (when (and (or (featurep 'ns)
                     (string-match-p "HARFBUZZ" system-configuration-features))
                 (featurep 'composite))
        (dolist (char-regexp +pretty-code-composition-alist)
          (set-char-table-range
           +ligature--composition-table
           (car char-regexp) `([,(concat "." (cdr char-regexp)) 0 font-shape-gstring])))
        (set-char-table-parent +ligature--composition-table composition-function-table)
        (when (+pretty-code--enable-p +pretty-code-in-modes)
          (setq-local composition-function-table +ligature--composition-table)))
    (setq-local composition-function-table (default-value 'composition-function-table))))
