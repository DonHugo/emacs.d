;; lisp/autoload/+evil-advice.el -*- lexical-binding: t; -*-
;;;###if (featurep! :editor evil)

;;;###autoload
(defun +evil--advice-escape (&rest _)
  "Call `+emacs/escape' if `evil-force-normal-state' is called interactively."
  (when (called-interactively-p 'any)
    (call-interactively #'+emacs/escape)))

;;;###autoload
(defun +evil--advice-resolve-vim-path (file-name)
  "Take a path and resolve any vim-like filename modifiers in it. This adds
support for most vim file modifiers, as well as:
  %:P   Resolves to `+emacs-project-root'.
See http://vimdoc.sourceforge.net/htmldoc/cmdline.html#filename-modifiers for
more information on modifiers."
  (let (case-fold-search)
    (with-temp-buffer
      (save-excursion (insert file-name))
      (while (re-search-forward "\\(^\\|[^\\\\]\\)\\(\\([%#]\\)\\(:\\([PphtreS~.]\\|g?s\\)\\)*\\)" nil t)
        (catch 'continue
          (unless buffer-file-name
            (replace-match (match-string 1) t t nil 2)
            (throw 'continue t))
          (let ((beg (match-beginning 2))
                (end (match-end 3))
                (path (pcase (match-string 3)
                        ("%" (file-relative-name buffer-file-name))
                        ("#" (and (other-buffer)
                                  (buffer-file-name (other-buffer)))))))
            (save-match-data
              (goto-char beg)
              (while (re-search-forward ":\\([PphtreS~.]\\|g?s\\)" (+ (point) 3) t)
                (let* ((modifier (match-string 1))
                       (global (string-prefix-p "gs" modifier)))
                  (when global
                    (setq modifier (substring modifier 1)))
                  (setq end (match-end 1)
                        path
                        (or (when path
                              (pcase (substring modifier 0 1)
                                ("p" (expand-file-name path))
                                ("~" (concat "~/" (file-relative-name path "~")))
                                ("." (file-relative-name path default-directory))
                                ("t" (file-name-nondirectory (directory-file-name path)))
                                ("r" (file-name-sans-extension path))
                                ("e" (file-name-extension path))
                                ("S" (shell-quote-argument path))
                                ("h"
                                 (let ((parent (file-name-directory (expand-file-name path))))
                                   (unless (file-equal-p path parent)
                                     (if (file-name-absolute-p path)
                                         (directory-file-name parent)
                                       (file-relative-name parent)))))
                                ("s"
                                 (if (featurep 'evil)
                                     (when-let (args (evil-delimited-arguments (substring modifier 1) 2))
                                       (let ((pattern (evil-transform-vim-style-regexp (car args)))
                                             (replace (cadr args)))
                                         (replace-regexp-in-string
                                          (if global pattern (concat "\\(" pattern "\\).*\\'"))
                                          (evil-transform-vim-style-regexp replace) path t t
                                          (unless global 1))))
                                   path))
                                ("P"
                                 (let ((project-root (+emacs-project-root (file-name-directory (expand-file-name path)))))
                                   (unless project-root
                                     (user-error "Not in a project"))
                                   (abbreviate-file-name project-root)))))
                            ""))
                  ;; strip trailing slash, if applicable
                  (or (string-empty-p path)
                      (not (equal (substring path -1) "/"))
                      (setq path (substring path 0 -1))))))
            (replace-match path t t nil 2))))
      (replace-regexp-in-string "\\\\\\([#%]\\)" "\\1" (buffer-string) t))))

;;;###autoload (autoload '+evil--advice-window-split "autoload/+evil-advice" nil t)
(evil-define-command +evil--advice-window-split (&optional count file)
  "Same as `evil-window-split', but correctly updates the window history."
  :repeat nil
  (interactive "P<f>")
  ;; HACK This ping-ponging between the destination and source windows is to
  ;;      update the window focus history, so that, if you close either split
  ;;      afterwards you won't be sent to some random window.
  (let ((origwin (selected-window))
        window-selection-change-functions)
    (select-window (split-window origwin count 'below))
    (unless evil-split-window-below
      (select-window origwin)))
  (run-hooks 'window-selection-change-functions)
  (recenter)
  (when (and (not count) evil-auto-balance-windows)
    (balance-windows (window-parent)))
  (if file (evil-edit file)))

;;;###autoload (autoload '+evil--advice-window-vsplit "autoload/+evil-advice" nil t)
(evil-define-command +evil--advice-window-vsplit (&optional count file)
  "Same as `evil-window-split', but correctly updates the window history."
  :repeat nil
  (interactive "P<f>")
  ;; HACK This ping-ponging between the destination and source windows is to
  ;;      update the window focus history, so that, if you close either split
  ;;      afterwards you won't be sent to some random window.
  (let ((origwin (selected-window))
        window-selection-change-functions)
    (select-window (split-window origwin count 'right))
    (unless evil-vsplit-window-right
      (select-window origwin)))
  (run-hooks 'window-selection-change-functions)
  (recenter)
  (when (and (not count) evil-auto-balance-windows)
    (balance-windows (window-parent)))
  (if file (evil-edit file)))

;;;###autoload (autoload '+evil--advice-join "autoload/+evil-advice" nil nil)
(defun +evil--advice-join (orig-fn beg end)
  "Join the selected lines.
This advice improves on `evil-join' by removing comment
delimiters when joining commented lines, without
`fill-region-as-paragraph'. Adapted from
https://github.com/emacs-evil/evil/issues/606"
  (if-let* (((not (= (line-end-position) (point-max))))
            (cend (save-excursion (goto-char end) (line-end-position)))
            (cbeg (save-excursion
                    (goto-char beg)
                    (and (+emacs-point-in-comment-p
                          (save-excursion
                            (goto-char (line-beginning-position 2))
                            (skip-syntax-forward " \t")
                            (point)))
                         (or (comment-search-backward (line-beginning-position) t)
                             (comment-search-forward  (line-end-position) t)
                             (and (+emacs-point-in-comment-p beg)
                                  (stringp comment-continue)
                                  (or (search-forward comment-continue (line-end-position) t)
                                      beg)))))))
      (let* ((count (count-lines beg end))
             (count (if (> count 1) (1- count) count))
             (fixup-mark (make-marker)))
        (uncomment-region (line-beginning-position 2)
                          (save-excursion
                            (goto-char cend)
                            (line-end-position 0)))
        (unwind-protect
            (dotimes (_ count)
              (join-line 1)
              (save-match-data
                (when (or (and comment-continue
                               (not (string-empty-p comment-continue))
                               (looking-at (concat "\\(\\s-*" (regexp-quote comment-continue) "\\) ")))
                          (and comment-start-skip
                               (not (string-empty-p comment-start-skip))
                               (looking-at (concat "\\(\\s-*" comment-start-skip "\\)"))))
                  (replace-match "" t nil nil 1)
                  (just-one-space))))
          (set-marker fixup-mark nil)))
    ;; But revert to the default we're not in a comment, where
    ;; `fill-region-as-paragraph' is too greedy.
    (funcall orig-fn beg end)))

;;;###autoload
(defun +evil--hook-fix-dabbrev-in-minibuffer ()
  "Make `try-expand-dabbrev' from `hippie-expand' work in
minibuffer. See `he-dabbrev-beg', so we need to redefine syntax
for '/'."
  (set-syntax-table (let* ((table (make-syntax-table)))
                      (modify-syntax-entry ?/ "." table)
                      table)))
