;; lisp/init-cli.el -*- lexical-binding: t; no-byte-compile: t; -*-

(defvar +emacs-auto-accept (getenv "YES")
  "If non-nil, Emacs will auto-accept any confirmation prompts
during batch commands.")

(defvar +emacs-auto-discard (getenv "FORCE")
  "If non-nil, discard all local changes while updating.")


;;
;;; Bootstrap

(require 'seq)
;; Eagerly load these libraries
(mapc (+emacs-rpartial #'load nil (not +emacs-debug-p) 'nosuffix)
      (append (file-expand-wildcards (concat +emacs-core-dir "core-autoload/*.el"))
              (file-expand-wildcards (concat +emacs-core-dir "cli/*.el"))))

;; Create all our core directories to quell file errors.
(mapc (+emacs-rpartial #'make-directory 'parents)
      (list +emacs-local-dir
            +emacs-etc-dir
            +emacs-cache-dir))

;; Ensure package management is ready
(require 'init-packages)

;; Don't generate superfluous files when writing temp buffers
(setq make-backup-files nil)
;; Stop user configuration from interfering with package management
(setq enable-dir-local-variables nil)

(provide 'init-cli)
