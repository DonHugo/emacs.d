;; lisp/init-packages.el -*- lexical-binding: t; -*-

(defvar +emacs-init-packages-p nil
  "If non-nil, the package management system has been
initialized.")

(defvar +emacs-core-packages '(straight use-package)
  "A list of packages that must be installed (and will be
auto-installed if missing) and shouldn't be deleted.")


;;
;;; package.el

;; Ensure that, if we do need package.el, it is configured correctly. You really
;; shouldn't be using it, but it may be convenient for quick package testing.
(setq package-enable-at-startup nil
      package-user-dir (concat +emacs-local-dir "elpa/")
      package-gnupghome-dir (expand-file-name "gpg" package-user-dir)
      ;; I omit Marmalade because its packages are manually submitted rather
      ;; than pulled, so packages are often out of date with upstream.
      package-archives
      (let ((proto (if gnutls-verify-error "https" "http")))
        (list (cons "gnu"   (concat proto "://elpa.gnu.org/packages/"))
              (cons "melpa" (concat proto "://melpa.org/packages/"))
              (cons "org"   (concat proto "://orgmode.org/elpa/")))))

(advice-add #'package--ensure-init-file :override #'ignore)

(defadvice! +emacs--advice-package-inhibit-custom-file (&optional value)
  "Don't save `package-selected-packages' to `custom-file'."
  :override #'package--save-selected-packages
  (if value (setq package-selected-packages value)))

;; Refresh package.el the first time you call `package-install'
(add-transient-hook! 'package-install (package-refresh-contents))


;;
;;; straight.el

(setq straight-repository-branch "develop"
      ;; Since byte-code is rarely compatible across different versions of
      ;; Emacs, it's best we build them in separate directories, per emacs
      ;; version.
      ;; straight-build-dir (format "build-%s" emacs-version)
      ;; We already do this, and better.
      straight-cache-autoloads nil
      ;; Disabling this makes 'make refresh' instant (once everything set up),
      ;; which is much nicer UX than the several seconds modification checks.
      straight-check-for-modifications nil
      ;; We handle package.el ourselves (and a little more comprehensively)
      straight-enable-package-integration nil
      ;; Use shallow clones
      ;; straight-vc-git-default-clone-depth 1
      ;; Tell straight.el about the profiles we are going to be using.
      straight-profiles
      '(;; Packages registered by the packages.el files in `+emacs-core-dir'
        (+emacs . "+emacs.el")
        ;; Packages registered in the packages.el file in `+emacs-private-dir'
        (+emacs-local . "+emacs-local.el")
        ;; Packages registered interactively.
        (nil . "default.el")
        ;; Packages which are pinned to a specific commit.
        (pinned . "pinned.el")))

(with-eval-after-load 'straight
  ;; `let-alist' is built into Emacs 26 and onwards
  (add-to-list 'straight-built-in-pseudo-packages 'let-alist))


;;;; Straight hacks

;; Use only the first profile a package was associated with. This way, all core
;; packages stay in one lockfile and the modules can have their own ones
(defun +straight--use-first-profile ()
  (dolist (package (hash-table-keys straight--profile-cache))
    (puthash package (list (car (nreverse
                                 (gethash package straight--profile-cache))))
             straight--profile-cache)))

(defvar +straight--auto-options
  '(("has diverged from"
     . "^Reset [^ ]+ to branch")
    ("but recipe specifies a URL of"
     . "Delete remote \"[^\"]+\", re-create it with correct URL")
    ("has a merge conflict:"
     . "^Abort merge$")
    ("has a dirty worktree:"
     . "^Discard changes$")
    ("^In repository \"[^\"]+\", [^ ]+ (on branch \"main\") is ahead of default branch \"master\""
     . "^Checkout branch \"master\"")
    ("^In repository \"[^\"]+\", [^ ]+ (on branch \"[^\"]+\") is ahead of default branch \"[^\"]+\""
     . "^Checkout branch \"")
    ("^In repository "
     . "^Reset branch \\|^Delete remote [^,]+, re-create it with correct URL"))
  "A list of regexps, mapped to regexps.
Their CAR is tested against the prompt, and CDR is tested against
the presented option, and is used by `straight-vc-git--popup-raw'
to select which option to recommend.")

;; HACK Remove dired & magit options from prompt, since they're inaccessible in
;;      noninteractive sessions.
(when noninteractive
  (advice-add #'straight-vc-git--popup-raw :override #'straight--popup-raw))

(defadvice! +straight--advice-fallback-to-y-or-n-prompt (orig-fn &optional prompt)
  "HACK Replace GUI popup prompts (which hang indefinitely in tty
Emacs) with simple prompts."
  :around #'straight-are-you-sure
  (or (bound-and-true-p +emacs-auto-accept)
      (if +emacs-interactive-p
          (funcall orig-fn prompt)
        (y-or-n-p (format! "%s" (or prompt ""))))))

(defun +straight--recommended-option-p (prompt option)
  (cl-loop for (prompt-re . opt-re) in +straight--auto-options
           if (string-match-p prompt-re prompt)
           return (string-match-p opt-re option)))

(defadvice! +straight--advice-fallback-to-tty-prompt (orig-fn prompt actions)
  "Modifies straight to prompt on the terminal when in
noninteractive sessions."
  :around #'straight--popup-raw
  (if +emacs-interactive-p
      (funcall orig-fn prompt actions)
    (let ((+straight--auto-options +straight--auto-options))
      ;; We can't intercept C-g, so no point displaying any options for this key
      ;; when C-c is the proper way to abort batch Emacs.
      (delq! "C-g" actions 'assoc)
      ;; HACK These are associated with opening dired or magit, which isn't
      ;;      possible in tty Emacs, so...
      (delq! "e" actions 'assoc)
      (delq! "g" actions 'assoc)
      (if +emacs-auto-discard
          (cl-loop with +emacs-auto-accept = t
                   for (_key desc func) in actions
                   when desc
                   when (+straight--recommended-option-p prompt desc)
                   return (funcall func))
        (print! (start "%s") (red prompt))
        (print-group!
         (terpri)
         (let (recommended options)
           (print-group!
            (print! " 1) Abort")
            (cl-loop for (_key desc func) in actions
                     when desc
                     do (push func options)
                     and do
                     (print! "%2s) %s" (1+ (length options))
                             (if (+straight--recommended-option-p prompt desc)
                                 (progn
                                   (setq +straight--auto-options nil
                                         recommended (length options))
                                   (green (concat desc " (Choose this if unsure)")))
                               desc))))
           (terpri)
           (let* ((options
                   (cons (lambda ()
                           (let ((+emacs-format-indent 0))
                             (terpri)
                             (print! (warn "Aborted")))
                           (kill-emacs 1))
                         (nreverse options)))
                  (prompt
                   (format! "How to proceed? (%s%s) "
                            (mapconcat #'number-to-string
                                       (number-sequence 1 (length options))
                                       ", ")
                            (if (not recommended) ""
                              (format "; don't know? Pick %d" (1+ recommended)))))
                  answer fn)
             (while (null (nth (setq answer (1- (read-number prompt)))
                               options))
               (print! (warn "%s is not a valid answer, try again.")
                       answer))
             (funcall (nth answer options)))))))))

(defadvice! +straight--advice-respect-print-indent (args)
  "Indent straight progress messages to respect
`+emacs-format-indent', so we don't have to pass whitespace to
`straight-use-package's fourth argument everywhere we use it (and
internally)."
  :filter-args #'straight-use-package
  (cl-destructuring-bind
      (melpa-style-recipe &optional no-clone no-build cause interactive)
      args
    (list melpa-style-recipe no-clone no-build
          (if (and (not cause)
                   (boundp '+emacs-format-indent)
                   (> +emacs-format-indent 0))
              (make-string (1- (or +emacs-format-indent 1)) 32)
            cause)
          interactive)))


;;
;;; Bootstrap

(defun +emacs-initialize-packages (&optional force-p)
  "Ensures that the package system and straight.el are initialized.
If FORCE-P is non-nil, do it anyway. Use this before any of
straight's API to ensure all the necessary package metadata is
initialized and available for them."
  (unless +emacs-init-packages-p
    (setq force-p t))
  (when (or force-p (not (bound-and-true-p package--initialized)))
    (+emacs-log "Initializing package.el")
    (require 'package)
    (package-initialize))
  (when (or force-p (not (fboundp 'straight--reset-caches)))
    (+emacs-log "Initializing straight")
    (setq +emacs-init-packages-p t)
    (setq straight-current-profile '+emacs)
    (+emacs-bootstrap-straight)
    (mapc #'straight-use-package +emacs-core-packages)
    (+emacs-log "Initializing packages")
    ;; Core packages
    (load! "packages.el")
    (setq straight-current-profile nil)
    ;; Module packages
    (load! "packages.el" +emacs-modules-dir t)
    (setq straight-current-profile '+emacs-local)
    ;; Private/local packages
    (load! "packages.el" (concat +emacs-private-dir "modules/") t)
    (load! "packages.el" +emacs-private-dir t)
    (setq straight-current-profile nil)
    (+straight--use-first-profile)))

(defun +emacs-bootstrap-straight ()
  "Ensure `straight' is installed and was compiled with this
version of Emacs."
  (defvar bootstrap-version)
  (let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5)
        ;; Get commit which is stored in the lockfile
        (commit (cdr (assoc
                      "straight.el"
                      (with-temp-buffer
                        (insert-file-contents-literally
                         (+emacs-path user-emacs-directory
                                      "straight/versions/"
                                      (concat
                                       (symbol-name straight-current-profile)
                                       ".el")))
                        (read (current-buffer)))))))
    (or (require 'straight nil t)
        (file-readable-p bootstrap-file)
        ;; Download install.el from the specified commit
        (with-current-buffer
            (url-retrieve-synchronously
             (format
              (concat "https://raw.githubusercontent.com/"
                      "raxod502/straight.el/%s/install.el")
              commit)
             'silent 'inhibit-cookies)
          ;; If we don't change the url here, straight.el will download an older
          ;; version which has a different `straight--build-cache-version' and
          ;; this will lead to a whole rebuild of all packages, even if not
          ;; necessary.
          (goto-char (point-min))
          (while (re-search-forward "raxod502/straight.el/install/%s/straight.el" nil t)
            (replace-match (format "raxod502/straight.el/%s/straight.el"
                                   commit)))
          (goto-char (point-max))
          (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage)))

(autoload 'straight--convert-recipe "straight" nil t)
(defun +emacs-package-recipe-repo (package)
  "Resolve and return PACKAGE's (symbol) local-repo property."
  (if-let* ((recipe (straight--convert-recipe package))
            (repo (straight--with-plist recipe (local-repo) local-repo)))
      repo
    (symbol-name package)))

(cl-defmacro package! (name &rest args &key built-in recipe ignore lockfile pin)
  "A wrapper around `straight-register-package' which allows to
specifiy a lockfile and profile. NAME is the package name.

:recipe RECIPE A straight.el recipe

:lockfile LOCKFILE|BOOL|'ignore Which profile and lockfile to
use. Will create LOCKFILE entry in `straight-profiles' and let
bind `straight-current-profile' to LOCKFILE. If nil or t,default
profile will be used. If set to 'ignore, no profile will be used
and the package will not be tracked."
  (declare (indent defun))
  (when built-in
    (when (and (not ignore)
               (equal built-in '(quote prefer)))
      (setq built-in (locate-library (symbol-name name) nil +emacs--initial-load-path)))
    (plist-put! args :ignore built-in)
    (plist-delete! args :built-in)
    (setq ignore built-in))
  (unless ignore
    `(progn
       (unless (or (equal ',lockfile '(quote ignore))
                   (equal ',lockfile '(quote pinned))
                   (booleanp ',lockfile))
         (pushnew! straight-profiles (cons ',lockfile (format "%s.el" ',lockfile))))
       (let ((straight-current-profile ',(cond ((not lockfile) '+emacs)
                                               ((equal lockfile '(quote ignore)) 'ignore)
                                               ((equal lockfile '(quote pinned)) 'pinned)
                                               ((not (booleanp lockfile)) lockfile)
                                               (t '+emacs))))
         ,(if recipe
              `(straight-register-package '(,name ,@recipe))
            `(straight-register-package ',name))
         (let ((pkg-name (symbol-name ',name)))
           (add-to-list 'load-path (directory-file-name (straight--build-dir pkg-name)))
           (straight--load-package-autoloads pkg-name))
         ,(when (and (equal lockfile '(quote pinned))
                     pin)
            `(add-to-list 'straight-x-pinned-packages
                          '(,(+emacs-package-recipe-repo name) . ,pin)))))))

(provide 'init-packages)
