;; lisp/init-core.el -*- lexical-binding: t; -*-

;; Detect stale bytecode
;; If Emacs version changed, the bytecode is no longer valid and we must
;; recompile.
(eval
 `(unless (equal
           (list
            (emacs-version)
            (concat (file-name-sans-extension
                     (cond ((bound-and-true-p byte-compile-current-file))
                           (load-file-name)
                           ((stringp (car-safe current-load-list))
                            (car current-load-list))
                           (buffer-file-name)
                           ((error "Cannot get this file-path"))))
                    ".el"))
           ',(eval-when-compile
               (list
                (emacs-version)
                (cond ((bound-and-true-p byte-compile-current-file))
                      (load-file-name)
                      ((stringp (car-safe current-load-list))
                       (car current-load-list))
                      (buffer-file-name)
                      ((error "Cannot get this file-path"))))))
    (throw 'stale-bytecode nil)))


;;
;;; Initialize

(defconst +emacs-core-loaded t
  "Non-nil if the core has been loaded.")

(defvar +emacs-init-p nil
  "Non-nil if Emacs has been initialized.")

(defvar +emacs-init-time nil
  "The time it took, in seconds, for Emacs to initialize.")

(defvar +emacs-debug-p (or (getenv-internal "DEBUG") init-file-debug)
  "If non-nil, Emacs will log more.
Use `+emacs-debug-mode' to toggle it. The --debug-init flag and
setting the DEBUG envvar will enable this at startup.")

(defconst +emacs-interactive-p (not noninteractive)
  "If non-nil, Emacs is in interactive mode.")

(defconst EMACS28+   (> emacs-major-version 27))
(defconst IS-MAC     (eq system-type 'darwin))
(defconst IS-LINUX   (eq system-type 'gnu/linux))
(defconst IS-WINDOWS (memq system-type '(cygwin windows-nt ms-dos)))
(defconst IS-BSD     (or IS-MAC (eq system-type 'berkeley-unix)))

;; This is consulted on every `require', `load' and various path/io functions.
;; You get a minor speed up by nooping this.
(unless noninteractive
  (defvar +emacs--initial-file-name-handler-alist file-name-handler-alist)

  (setq file-name-handler-alist nil)

  ;; Restore `file-name-handler-alist', because it is needed for handling
  ;; encrypted or compressed files, among other things.
  (defun +emacs--hook-reset-file-handler-alist ()
    (setq file-name-handler-alist +emacs--initial-file-name-handler-alist))
  (add-hook 'emacs-startup-hook #'+emacs--hook-reset-file-handler-alist))

;; Unix tools look for HOME, but this is normally not defined on Windows.
(when (and IS-WINDOWS (null (getenv-internal "HOME")))
  (setenv "HOME" (getenv "USERPROFILE"))
  (setq abbreviated-home-dir nil))

;; UTF-8 as the default coding system
(when (fboundp 'set-charset-priority)
  (set-charset-priority 'unicode))
(prefer-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
;; Except for the clipboard on Windows, where its contents could be in an
;; encoding that's wider than utf-8, so we let Emacs/the OS decide what encoding
;; to use.
(unless IS-WINDOWS
  (setq selection-coding-system 'utf-8))


;;
;;; Custom error types

(define-error '+emacs-error "Error in Emacs core")
(define-error '+emacs-hook-error "Error in a startup hook" '+emacs-error)
(define-error '+emacs-autoload-error "Error in an autoloads file" '+emacs-error)
(define-error '+emacs-module-error "Error in a module" '+emacs-error)
(define-error '+emacs-private-error "Error in private config" '+emacs-error)
(define-error '+emacs-package-error "Error with packages" '+emacs-error)


;;
;;; Directories

(defconst +emacs-dir (eval-when-compile (file-truename user-emacs-directory))
  "The path to the currently loaded .emacs.d directory. Must end
with a slash.")

(defconst +emacs-core-dir (concat +emacs-dir "lisp/")
  "The root directory of Emacs's core files. Must end with a
slash.")

(defconst +emacs-modules-dir (concat +emacs-core-dir "modules/")
  "The root directory for Emacs's modules. Must end with a
slash.")

(defconst +emacs-local-dir
  (let ((localdir (getenv "EMACSLOCALDIR")))
    (if localdir
        (expand-file-name (file-name-as-directory localdir))
      (concat +emacs-dir ".local/")))
  "Root directory for local storage.
Use this as a storage location for this system's installation of
Emacs Emacs. These files should not be shared across systems. By
default, it is used by `+emacs-etc-dir' and `+emacs-cache-dir'.
Must end with a slash.")

(defconst +emacs-etc-dir (concat +emacs-local-dir "etc/")
  "Directory for non-volatile local storage.
Use this for files that don't change much, like server binaries,
external dependencies or long-term shared data. Must end with a
slash.")

(defconst +emacs-cache-dir (concat +emacs-local-dir "cache/")
  "Directory for volatile local storage.
Use this for files that change often, like cache files. Must end
with a slash.")

(defconst +emacs-private-dir
  (let ((dir (getenv "EMACSDIR")))
    (if dir
        (expand-file-name (file-name-as-directory dir))
      (concat +emacs-dir "site-lisp/")))
  "Where your private configuration is placed.
Defaults to ~/.emacs.d/site-lisp or the value of the EMACSDIR
envvar; whichever is found first. Must end in a slash.")

(defconst +emacs-autoloads-file
  (concat +emacs-local-dir "autoloads.el")
  "Where `+emacs-autoloads-reload' stores its autoloads. This
file is responsible for informing Emacs where to find all
autoloaded functions.")

(defconst +emacs-env-file (concat +emacs-local-dir "env")
  "The location of your envvar file, generated by `+emacs/refresh'.
This file contains environment variables scraped from your shell
environment, which is loaded at startup (if it exists). This is
helpful if Emacs can't \(easily) be launched from the correct
shell session (particularly for MacOS users).")


;;
;;; Custom hooks

(defvar +emacs-first-input-hook nil
  "Transient hooks run before the first user input.")
(put '+emacs-first-input-hook 'permanent-local t)

(defvar +emacs-first-file-hook nil
  "Transient hooks run before the first interactively opened
file.")
(put '+emacs-first-input-hook 'permanent-local t)

(defvar +emacs-first-buffer-hook nil
  "Transient hooks run before the first interactively opened
buffer.")
(put '+emacs-first-input-hook 'permanent-local t)


;;
;;; Essentials

;; Ensure `+emacs-core-dir' is in `load-path'
(add-to-list 'load-path +emacs-core-dir)

;; Load the bare necessities
(require 'cl-lib)
(require 'subr-x)
(require 'init-core-lib)


;;
;;; Quiet startup

;; Disable warnings from legacy advice system.
(setq ad-redefinition-action 'accept)

;; Reduce debug output, well, unless we've asked for it.
(setq debug-on-error +emacs-debug-p
      jka-compr-verbose +emacs-debug-p)

;; Get rid of "For information about GNU Emacs..." message at startup, unless
;; we're in a daemon session where it'll say "Starting Emacs daemon.".
(unless (daemonp)
  (advice-add #'display-startup-echo-area-message :override #'ignore))

;; Display the bare minimum at startup.
(setq inhibit-startup-message t
      inhibit-startup-echo-area-message user-login-name
      inhibit-default-init t
      initial-major-mode 'fundamental-mode
      initial-scratch-message nil)


;;
;;; Don't litter

(setq async-byte-compile-log-file  (concat +emacs-etc-dir "async-bytecomp.log")
      custom-file                  (concat +emacs-private-dir "custom.el")
      desktop-dirname              (concat +emacs-etc-dir "desktop")
      desktop-base-file-name       "autosave"
      desktop-base-lock-name       "autosave-lock"
      pcache-directory             (concat +emacs-cache-dir "pcache/")
      request-storage-directory    (concat +emacs-cache-dir "request")
      shared-game-score-directory  (concat +emacs-etc-dir "shared-game-score/"))

(defadvice! +emacs--advice-write-to-etc-dir (orig-fn &rest args)
  "Resolve Emacs storage directory to `+emacs-etc-dir', to keep
local files from polluting `+emacs-dir'."
  :around #'locate-user-emacs-file
  (let ((user-emacs-directory +emacs-etc-dir))
    (apply orig-fn args)))

(defadvice! +emacs--advice-save-enabled-commands-to-custom-file (orig-fn &rest args)
  "When enabling a disabled command, the `put' call is written to
~/.emacs.d/init.el, which causes issues, so write it to the
user's custom.el instead."
  :around #'en/disable-command
  (let ((user-init-file custom-file))
    (apply orig-fn args)))


;;
;;; Optimizations

;; Don't make a second case-insensitive pass over `auto-mode-alist'. If it has
;; to, it's our (the user's) failure.
(setq auto-mode-case-fold nil)

;; Disable bidirectional text scanning for a modest performance boost.
(setq-default bidi-display-reordering 'left-to-right
              bidi-paragraph-direction 'left-to-right)

;; Disabling the BPA makes redisplay faster, but might produce incorrect display
;; reordering of bidirectional text.
(setq bidi-inhibit-bpa t)

;; Reduce rendering/line scan work for Emacs by not rendering cursors or regions
;; in non-focused windows.
(setq-default cursor-in-non-selected-windows nil)
(setq highlight-nonselected-windows nil)

;; More performant rapid scrolling over unfontified regions. May cause brief
;; spells of inaccurate fontification immediately after scrolling.
(setq fast-but-imprecise-scrolling t)

;; Don't ping things that look like domain names.
(setq ffap-machine-p-known 'reject)

;; Resizing the Emacs frame can be a terribly expensive part of changing the
;; font. By inhibiting this, we halve startup times, particularly when we use
;; fonts that are larger than the system default (which would resize the frame).
(setq frame-inhibit-implied-resize t)

;; Adopt a sneaky garbage collection strategy via `gcmh-mode'.
(setq gcmh-idle-delay 'auto
      gcmh-auto-idle-delay-factor 10
      ;; 16 mb
      gcmh-high-cons-threshold (* 16 1024 1024)
      gcmh-verbose +emacs-debug-p)

;; Emacs "updates" its ui more often than it needs to, so we slow it down
;; slightly, from 0.5s
(setq idle-update-delay 1.0)

;; Font compacting can be terribly expensive, especially for rendering icon
;; fonts on Windows. Whether it has a noteable affect on Linux and Mac hasn't
;; been determined.
(setq inhibit-compacting-font-caches t)

;; Increase how much is read from processes in a single chunk (default is 4kb)
;; to 64kb.
(setq read-process-output-max (* 64 1024))

;; This inhibits fontification while receiving input, which should help a little
;; with scrolling performance.
(setq redisplay-skip-fontification-on-input t)

;; Performance on Windows is considerably worse than elsewhere. We'll need
;; everything we can get.
(when IS-WINDOWS
  ;; Reduce the workload when doing file IO
  (setq w32-get-true-file-attributes nil
        ;; Faster IPC
        w32-pipe-read-delay 0
        ;; Read more at a time (was 4K)
        w32-pipe-buffer-size (* 64 1024)))

;; Remove command line options that aren't relevant to our current OS; that
;; means less to process at startup.
(unless IS-MAC   (setq command-line-ns-option-alist nil))
(unless IS-LINUX (setq command-line-x-option-alist nil))

;; HACK `tty-run-terminal-initialization' is *tremendously* slow for some
;;      reason. Disabling it completely could have many side-effects, so we
;;      defer it until later, at which time it (somehow) runs very quickly.
(unless (daemonp)
  (advice-add #'tty-run-terminal-initialization :override #'ignore)
  (defhook! +emacs--hook-init-tty ()
    "Init tty."
    'window-setup-hook
    (advice-remove #'tty-run-terminal-initialization #'ignore)
    (tty-run-terminal-initialization (selected-frame) nil t)))


;;
;;; Security

;; Emacs is a huge security vulnerability, what with all the dependencies it
;; pulls in from all corners of the globe. Let's at least try to be more
;; discerning.
(setq gnutls-verify-error (not (getenv "INSECURE"))
      gnutls-algorithm-priority
      (when (boundp 'libgnutls-version)
        (concat "SECURE128:+SECURE192:-VERS-ALL"
                (if (and (not IS-WINDOWS)
                         (>= libgnutls-version 30605))
                    ":+VERS-TLS1.3")
                ":+VERS-TLS1.2"))
      ;; `gnutls-min-prime-bits' is set based on recommendations from
      ;; https://www.keylength.com/en/4/
      gnutls-min-prime-bits 3072
      tls-checktrust gnutls-verify-error
      ;; Emacs is built with `gnutls' by default, so `tls-program' would not be
      ;; used in that case. Otherwiese, people have reasons to not go with
      ;; `gnutls', we use `openssl' instead. For more details, see
      ;; https://redd.it/8sykl1
      tls-program '("openssl s_client -connect %h:%p -CAfile %t -nbio -no_ssl3 -no_tls1 -no_tls1_1 -ign_eof"
                    "gnutls-cli -p %p --dh-bits=3072 --ocsp --x509cafile=%t \
--strict-tofu --priority='SECURE192:+SECURE128:-VERS-ALL:+VERS-TLS1.2:+VERS-TLS1.3' %h"
                    ;; compatibility fallbacks
                    "gnutls-cli -p %p %h"))

;; Emacs stores authinfo in HOME and in plaintext. This file usually stores
;; usernames, passwords, and other such treasures for the aspiring malicious
;; third party.
(setq auth-sources (list (expand-file-name "authinfo.gpg" +emacs-etc-dir)
                         "~/.authinfo.gpg"))


;;
;;; MODE-local-vars-hook

;; File+dir local variables are initialized after the major mode and its hooks
;; have run. If you want hook functions to be aware of these customizations, add
;; them to MODE-local-vars-hook instead.
(defvar +emacs-inhibit-local-var-hooks nil)

(defun +emacs--hook-run-local-var-hooks ()
  "Run MODE-local-vars-hook after local variables are
initialized."
  (unless +emacs-inhibit-local-var-hooks
    (setq-local +emacs-inhibit-local-var-hooks t)
    (run-hook-wrapped (intern-soft (format "%s-local-vars-hook" major-mode))
                      #'+emacs-try-run-hook)))

;; If `enable-local-variables' is diabled, then `hack-local-variables-hook' is
;; never triggered, so we trigger it at the end of
;; `after-change-major-mode-hook':
(defun +emacs--hook-run-local-var-hooks-maybe ()
  "Run `+emacs--hook-run-local-var-hooks' if
`enable-local-variables' is disabled."
  (unless enable-local-variables
    (+emacs--hook-run-local-var-hooks)))


;;
;;; Incremental lazy-loading

(defvar +emacs-incremental-packages '(t)
  "A list of packages to load incrementally after startup. Any
large packages here may cause noticable pauses, so it's
recommended you break them up into sub-packages. For example,
`org' is comprised of many packages, and can be broken up into:

(+emacs-load-packages-incrementally
   '(calendar find-func format-spec org-macs org-compat
     org-faces org-entities org-list org-pcomplete org-src
     org-footnote org-macro ob org org-clock org-agenda
     org-capture))

This is already done by the lang/org module, however. If you want
to disable incremental loading altogether, either remove
`+emacs--hook-load-packages-incrementally' from
`emacs-startup-hook' or set `+emacs-incremental-first-idle-timer'
to nil.")

(defvar +emacs-incremental-first-idle-timer 2.0
  "How long (in idle seconds) until incremental loading starts.
Set this to nil to disable incremental loading.")

(defvar +emacs-incremental-idle-timer 1.5
  "How long (in idle seconds) in between incrementally loading
packages.")

(defvar +emacs-incremental-load-immediately (daemonp)
  "If non-nil, load all incrementally deferred packages
immediately at startup.")

(defun +emacs-load-packages-incrementally (packages &optional now)
  "Registers PACKAGES to be loaded incrementally.
If NOW is non-nil, load PACKAGES incrementally, in
`+emacs-incremental-idle-timer' intervals."
  (if (not now)
      (appendq! +emacs-incremental-packages packages)
    (while packages
      (let ((req (pop packages)))
        (unless (featurep req)
          (+emacs-log "Incrementally loading %s" req)
          (condition-case e
              (or (while-no-input
                    ;; If `default-directory' is a directory that doesn't exist
                    ;; or is unreadable, Emacs throws up file-missing errors, so
                    ;; we set it to a directory we know exists and is readable.
                    (let ((default-directory +emacs-dir)
                          (gc-cons-threshold most-positive-fixnum)
                          file-name-handler-alist)
                      (require req nil t))
                    t)
                  (push req packages))
            ((error debug)
             (message "Failed to load '%s' package incrementally, because: %s"
                      req e)))
          (if (not packages)
              (+emacs-log "Finished incremental loading")
            (run-with-idle-timer +emacs-incremental-idle-timer
                                 nil #'+emacs-load-packages-incrementally
                                 packages t)
            (setq packages nil)))))))

(defun +emacs--hook-load-packages-incrementally ()
  "Begin incrementally loading packages in `+emacs-incremental-packages'.
If this is a daemon session, load them all immediately instead."
  (if +emacs-incremental-load-immediately
      (mapc #'require (cdr +emacs-incremental-packages))
    (when (numberp +emacs-incremental-first-idle-timer)
      (run-with-idle-timer +emacs-incremental-first-idle-timer
                           nil #'+emacs-load-packages-incrementally
                           (cdr +emacs-incremental-packages) t))))


;;
;;; Bootstrap helpers

(defun +emacs--hook-display-benchmark (&optional return-p)
  "Display a benchmark, showing number of packages and modules,
and how quickly they were loaded at startup. If RETURN-P, return
the message as a string instead of displaying it."
  (funcall (if return-p #'format #'message)
           "Emacs loaded %d packages across %d modules in %.03fs"
           (- (length load-path) (length +emacs--initial-load-path))
           (if +emacs-modules (hash-table-count +emacs-modules) 0)
           (or +emacs-init-time
               (setq +emacs-init-time
                     (float-time (time-subtract (current-time) before-init-time))))))

(defun +emacs-load-envvars-file (file &optional noerror)
  "Read and set envvars from FILE.
If NOERROR is non-nil, don't throw an error if the file doesn't
exist or is unreadable. Returns the names of envvars that were
changed."
  (if (null (file-exists-p file))
      (unless noerror
        (signal 'file-error (list "No envvar file exists" file)))
    (with-temp-buffer
      (insert-file-contents file)
      (when-let (env (read (current-buffer)))
        (let ((tz (getenv-internal "TZ")))
          (setq-default
           process-environment
           (append env (default-value 'process-environment))
           exec-path
           (append (split-string (getenv "PATH") path-separator t)
                   (list exec-directory))
           shell-file-name
           (or (getenv "SHELL")
               (default-value 'shell-file-name)))
          (when-let (newtz (getenv-internal "TZ"))
            (unless (equal tz newtz)
              (set-time-zone-rule newtz))))
        env))))

(defun +emacs-try-run-hook (hook)
  "Run HOOK (a hook function), but handle errors better, to make
debugging issues easier. Meant to be used with
`run-hook-wrapped'."
  (+emacs-log "Running +emacs hook: %s" hook)
  (condition-case e
      (funcall hook)
    ((debug error)
     (signal '+emacs-hook-error (list hook e))))
  ;; return nil so `run-hook-wrapped' won't short circuit
  nil)

(defun +emacs-run-hook-on (hook-var trigger-hooks)
  "Configure HOOK-VAR to be invoked exactly once when any of the
TRIGGER-HOOKS are invoked *after* Emacs has initialized (to
reduce false positives). Once HOOK-VAR is triggered, it is reset
to nil.
HOOK-VAR is a quoted hook.
TRIGGER-HOOK is a list of quoted hooks and/or sharp-quoted
functions."
  (dolist (hook trigger-hooks)
    (let ((fn (intern (format "%s--init-on-%s" hook-var (if (string-match "^+[aA-zZ]" (symbol-name hook))
                                                            (string-remove-prefix "+" (symbol-name hook))
                                                          (symbol-name hook))))))

      (fset
       fn (lambda (&rest _)
            ;; Only trigger this after Emacs has initialized.
            (when (and after-init-time
                       (or (daemonp)
                           ;; In some cases, hooks may be lexically unset to
                           ;; inhibit them during expensive batch operations on
                           ;; buffers (such as when processing buffers
                           ;; internally). In these cases we should assume this
                           ;; hook wasn't invoked interactively.
                           (and (boundp hook)
                                (symbol-value hook))))
              (run-hook-wrapped hook-var #'+emacs-try-run-hook)
              (set hook-var nil))))
      (cond ((daemonp)
             ;; In a daemon session we don't need all these lazy loading
             ;; shenanigans. Just load everything immediately.
             (add-hook 'after-init-hook fn 'append))
            ((eq hook 'find-file-hook)
             ;; Advise `after-find-file' instead of using `find-file-hook'
             ;; because the latter is triggered too late (after the file has
             ;; opened and modes are all set up).
             (advice-add 'after-find-file :before fn '((depth . -101))))
            ((add-hook hook fn -101)))
      fn)))


;;
;;; Bootstrapper

(defvar +emacs--initial-exec-path exec-path)
(defvar +emacs--initial-load-path load-path)
(defvar +emacs--initial-process-environment process-environment)

(defun +emacs-initialize (&optional force-p)
  "Bootstrap Emacs config, if it hasn't already (or if FORCE-P is
non-nil)."
  (when (or force-p (not +emacs-init-p))
    (setq +emacs-init-p t)

    (+emacs-log "Initializing Emacs config")

    ;; Reset as much state as possible, so `+emacs-initialize' can be treated
    ;; like a reset function. Particularly useful for reloading the config.
    (setq-default exec-path +emacs--initial-exec-path
                  load-path +emacs--initial-load-path
                  process-environment +emacs--initial-process-environment)

    ;; A lot of information is cached in `+emacs-autoloads-file'. Compiling them
    ;; into one place is a big reduction in startup time.

    (condition-case e
        ;; Avoid `file-name-sans-extension' for premature optimization reasons.
        ;; `string-remove-suffix' is cheaper because it performs no file sanity
        ;; checks; just plain ol' string manipulation.
        (load (string-remove-suffix ".el" +emacs-autoloads-file)
              nil 'nomessage)
      (file-missing
       ;; If the autoloads file fails to load then the user forgot to sync!
       (if (locate-file +emacs-autoloads-file load-path)
           ;; Something inside the autoloads file is triggering this error;
           ;; forward it to the caller!
           (signal '+emacs-autoload-error e)
         (signal '+emacs-error
                 (list "Emacs is in an incomplete state"
                       "run 'make sync' on the command line to repair it")))))

    ;; Load shell environment, optionally generated from 'make env'. No need to
    ;; do so if we're in terminal Emacs, where Emacs correctly inherits your
    ;; shell environment.
    (if (or (display-graphic-p)
            (daemonp))
        (+emacs-load-envvars-file +emacs-env-file 'noerror))

    ;; In case we want to use package.el or straight via M-x
    (autoload '+emacs-initialize-packages "init-packages")
    (autoload 'straight-x-fetch-all "straight-x" nil t)
    (autoload 'straight-x-pull-all "straight-x" nil t)
    (autoload 'straight-x-freeze-versions "straight-x" nil t)
    (with-eval-after-load 'package
      (require 'init-packages))
    (with-eval-after-load 'straight
      (require 'init-packages)
      (+emacs-initialize-packages))
    (with-eval-after-load 'straight-x
      (require 'init-packages)
      (+emacs-initialize-packages))

    ;; Bootstrap GC manager
    (add-hook '+emacs-first-buffer-hook #'gcmh-mode)

    ;; Bootstrap interactive session
    (add-hook 'after-change-major-mode-hook #'+emacs--hook-run-local-var-hooks-maybe 100)
    (add-hook 'hack-local-variables-hook #'+emacs--hook-run-local-var-hooks)
    (add-hook 'emacs-startup-hook #'+emacs--hook-load-packages-incrementally)
    (add-hook 'window-setup-hook #'+emacs--hook-display-benchmark)
    (+emacs-run-hook-on '+emacs-first-buffer-hook '(find-file-hook +emacs-switch-buffer-hook))
    (+emacs-run-hook-on '+emacs-first-file-hook '(find-file-hook dired-initial-position-hook))
    (+emacs-run-hook-on '+emacs-first-input-hook '(pre-command-hook))

    (when +emacs-debug-p (+emacs-debug-mode +1))

    (+emacs-initialize-modules force-p))

  +emacs-init-p)


;;
;;; Modules

(defvar +emacs-init-modules-p nil
  "Non-nil if `+emacs-initialize-modules' has run.")

(defvar +emacs-modules (make-hash-table :test 'equal)
  "A hash table of enabled modules. Set by
`+emacs-initialize-modules'.")

(defvar +emacs-modules-dirs
  (list (expand-file-name "modules/" +emacs-private-dir)
        +emacs-modules-dir)
  "A list of module root directories. Order determines
priority.")


;;;; Custom hooks

(defvar +emacs-before-init-modules-hook nil
  "A list of hooks to run before modules' files are loaded, but
after their package files are loaded.")

(defvar +emacs-init-modules-hook nil
  "A list of hooks to run after modules' config.el files have
loaded, but before the private module.")

(defvaralias '+emacs-after-init-modules-hook 'after-init-hook)

(defvar +emacs--current-module nil)
(defvar +emacs--current-flags nil)


;;;; Bootstrap

(defun +emacs-read-modules (&optional force-p)
  "Load modules defined in the init.el in `+emacs-core-dir' and
`+emacs-private-dir'."
  (when force-p
    (setq +emacs-modules (make-hash-table :test 'equal)))
  ;; Load core modules
  (+emacs-log "Reading core modules")
  (load! "init" +emacs-core-dir)
  ;; Load variable modules
  (+emacs-log "Reading user modules")
  (load! "init" +emacs-private-dir t)
  ;; Deactivate modules
  (+emacs-log "Looking for modules to deactivate")
  (+emacs-module-deactivate-cookies (concat +emacs-private-dir "modules/modules.el")))

(defun +emacs-initialize-modules (&optional force-p no-config-p)
  "Loads the modules' files in `+emacs-modules-dir' and in
`+emacs-private-dir'. Will noop if used more than once, unless
FORCE-P is non-nil."
  (when (or force-p (not +emacs-init-modules-p))
    (setq +emacs-init-modules-p t)
    (unless no-config-p
      (+emacs-log "Initializing interactive config")
      (load! "init-core-interactive" +emacs-core-dir))
    (run-hook-wrapped '+emacs-before-init-modules-hook #'+emacs-try-run-hook)
    (unless no-config-p
      (+emacs-log "Initializing core modules")
      (load! "init-core-modules" +emacs-core-dir)
      (+emacs-log "Initializing modules")
      (load! "modules" +emacs-modules-dir t)
      (+emacs-log "Initializing user modules")
      (load! "modules/modules" +emacs-private-dir t)
      (run-hook-wrapped '+emacs-init-modules-hook #'+emacs-try-run-hook)
      (+emacs-log "Initializing user config")
      (load! "config" +emacs-private-dir t)
      (load custom-file 'noerror (not +emacs-debug-p)))))


;;;; Module API

(defun +emacs-module-p (category module &optional flag)
  "Returns t if CATEGORY MODULE is enabled (ie. present in
`+emacs-modules')."
  (declare (pure t) (side-effect-free t))
  (when-let (plist (gethash (cons category module) +emacs-modules))
    (or (null flag)
        (and (memq flag (plist-get plist :flags))
             t))))

(defun +emacs-module-get (category module &optional property)
  "Returns the plist for CATEGORY MODULE. Gets PROPERTY,
specifically, if set."
  (declare (pure t) (side-effect-free t))
  (when-let (plist (gethash (cons category module) +emacs-modules))
    (if property
        (plist-get plist property)
      plist)))

(defun +emacs-module-put (category module &rest plist)
  "Set a PROPERTY for CATEGORY MODULE to VALUE. PLIST should be
additional pairs of PROPERTY and VALUEs.

\(fn CATEGORY MODULE PROPERTY VALUE &rest [PROPERTY VALUE [...]])"
  (puthash (cons category module)
           (if-let (old-plist (+emacs-module-get category module))
               (if (null plist)
                   old-plist
                 (when (cl-oddp (length plist))
                   (signal 'wrong-number-of-arguments (list (length plist))))
                 (while plist
                   (plist-put old-plist (pop plist) (pop plist)))
                 old-plist)
             plist)
           +emacs-modules))

(defun +emacs-module-set (category module &rest plist)
  "Enables a module by adding it to `+emacs-modules'.
CATEGORY is a keyword, module is a symbol, PLIST is a plist that
accepts the following properties:

  :flags [SYMBOL LIST]  list of enabled category flags
  :path  [STRING]       path to category root directory

Example:
  (+emacs-module-set :lang 'haskell :flags '(+intero))"
  (puthash (cons category module)
           plist
           +emacs-modules))

(defun +emacs-module-deactivate-cookies (file)
  "Evaluate the ;;;###deactivatemodule cookies in FILE. The
specified module gets deleted from `+emacs-modules'.

Example: ;;;###deactivatemodule (:completion company)."
  (when (file-exists-p file)
    (with-temp-buffer
      (insert-file-contents file)
      (while (re-search-forward (format "^;;;###%s " (regexp-quote "deactivatemodule"))
                                nil t)
        (let ((load-file-name file))
          (remhash (cons (car (sexp-at-point)) (cadr (sexp-at-point))) +emacs-modules))))))


;;;; Use-package modifications

(eval-and-compile
  (autoload 'use-package "use-package-core" nil nil t)

  (setq use-package-compute-statistics +emacs-debug-p
        use-package-verbose +emacs-debug-p
        use-package-minimum-reported-time (if +emacs-debug-p 0 0.1)
        use-package-expand-minimally +emacs-interactive-p))

(defvar +emacs--deferred-packages-alist '(t))

(with-eval-after-load 'use-package-core
  ;; Macros are already fontified, no need for this
  (font-lock-remove-keywords 'emacs-lisp-mode use-package-font-lock-keywords)

  ;; We define :minor and :magic-minor from the `auto-minor-mode' package here
  ;; so we don't have to load `auto-minor-mode' so early.
  (dolist (keyword '(:minor :magic-minor))
    (setq use-package-keywords
          (use-package-list-insert keyword use-package-keywords :commands)))

  (defalias 'use-package-normalize/:minor #'use-package-normalize-mode)
  (defun use-package-handler/:minor (name _ arg rest state)
    (use-package-handle-mode name 'auto-minor-mode-alist arg rest state))

  (defalias 'use-package-normalize/:magic-minor #'use-package-normalize-mode)
  (defun use-package-handler/:magic-minor (name _ arg rest state)
    (use-package-handle-mode name 'auto-minor-mode-magic-alist arg rest state))

  ;; Adds two keywords to `use-package' to expand its lazy-loading capabilities:
  ;;
  ;; :after-call SYMBOL|LIST
  ;; Takes a symbol or list of symbols representing functions or hook variables.
  ;; The first time any of these functions or hooks are executed, the package is
  ;; loaded.
  ;;
  ;; :defer-incrementally SYMBOL|LIST|t
  ;; Takes a symbol or list of symbols representing packages that will be loaded
  ;; incrementally at startup before this one. This is helpful for large
  ;; packages like magit or org, which load a lot of dependencies on first load.
  ;; This lets you load them piece-meal during idle periods, so that when you
  ;; finally do need the package, it'll load quicker.
  (dolist (keyword '(:defer-incrementally :after-call))
    (push keyword use-package-deferring-keywords)
    (setq use-package-keywords
          (use-package-list-insert keyword use-package-keywords :after)))

  (defalias 'use-package-normalize/:defer-incrementally #'use-package-normalize-symlist)
  (defun use-package-handler/:defer-incrementally (name _keyword targets rest state)
    (use-package-concat
     `((+emacs-load-packages-incrementally
        ',(if (equal targets '(t))
              (list name)
            (append targets (list name)))))
     (use-package-process-keywords name rest state)))

  (defalias 'use-package-normalize/:after-call #'use-package-normalize-symlist)
  (defun use-package-handler/:after-call (name _keyword hooks rest state)
    (if (plist-get state :demand)
        (use-package-process-keywords name rest state)
      (let ((fn (make-symbol (format "+emacs--hook-after-call-%s" name))))
        (use-package-concat
         `((fset ',fn
                 (lambda (&rest _)
                   (+emacs-log "Loading deferred package %s from %s" ',name ',fn)
                   (condition-case e
                       ;; If `default-directory' is a directory that doesn't
                       ;; exist or is unreadable, Emacs throws up file-missing
                       ;; errors, so we set it to a directory we know exists and
                       ;; is readable.
                       (let ((default-directory +emacs-dir))
                         (require ',name))
                     ((debug error)
                      (message "Failed to load deferred package %s: %s" ',name e)))
                   (when-let (deferral-list (assq ',name +emacs--deferred-packages-alist))
                     (dolist (hook (cdr deferral-list))
                       (advice-remove hook #',fn)
                       (remove-hook hook #',fn))
                     (delq! deferral-list +emacs--deferred-packages-alist)
                     (unintern ',fn nil)))))
         (let (forms)
           (dolist (hook hooks forms)
             (push (if (string-match-p "-\\(?:functions\\|hook\\)$" (symbol-name hook))
                       `(add-hook ',hook #',fn)
                     `(advice-add #',hook :before #',fn))
                   forms)))
         `((unless (assq ',name +emacs--deferred-packages-alist)
             (push '(,name) +emacs--deferred-packages-alist))
           (nconc (assq ',name +emacs--deferred-packages-alist)
                  '(,@hooks)))
         (use-package-process-keywords name rest state))))))


;;
;;;; Macros

(defmacro modules! (&rest modules)
  "Generates `+emacs-modules', which defines which modules are
active and supposed to be loaded."
  `(let ((modules
          ,@(if (keywordp (car modules))
                (list (list 'quote modules))
              modules)))
     (unless +emacs-modules
       (setq +emacs-modules
             (make-hash-table :test 'equal
                              :size (if modules (length modules) 150)
                              :rehash-threshold 1.0)))
     (let (category m)
       (while modules
         (setq m (pop modules))
         (cond ((keywordp m)
                (setq category m))
               ((let* ((module (if (listp m) (car m) m))
                       (flags  (if (listp m) (cdr m))))
                  (+emacs-module-set category module :flags flags)))))
       +emacs-modules)))

(defmacro module! (module &rest body)
  "Little wrapper which helps to define a module.
MODULE is an unquoted list of the form (:CATEGORY MODULE
+FLAG). BODY contains the rest of the definition."
  (declare (indent 1))
  (let ((c (car module))
        (m (car (cdr module)))
        (f (cdr (cdr module)))
        (inhibit-redisplay t))
    (when (eval `(featurep! ,c ,m ,@f))
      (setq +emacs--current-module `(,c . ,m))
      (setq +emacs--current-flags f)
      `(progn
         ,@body
         (setq +emacs--current-module nil)
         (setq +emacs--current-flags nil)))))

(defmacro featurep! (category &optional module flag)
  "Returns t if CATEGORY MODULE is enabled.
If FLAG is provided, returns t if CATEGORY MODULE has FLAG
enabled. Handles special case when this function is called within
a `package!' or `when-compiletime!' macro for defining
modules. (featurep! :config default)"
  (and (cond (flag (memq flag (+emacs-module-get category module :flags)))
             (module (+emacs-module-p category module))
             (+emacs--current-flags (memq category +emacs--current-flags))
             (memq category (+emacs-module-get (car module) (cdr module) :flags)))
       t))

(provide 'init-core)
