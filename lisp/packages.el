;; -*- no-byte-compile: t; -*-
;; lisp/packages.el

;;; org-mode

;; This is done early, so other packages will not require an outdated org
;; version
(package! org
  :recipe (:host github
           ;; REVIEW I intentionally avoid git.savannah.gnu.org because of SSL
           ;;   issues (see #5655), uptime issues, download time, and lack of
           ;;   shallow clone support.
           :repo "emacs-straight/org-mode")
  :lockfile 'pinned
  :pin "73875939a8b5545ac53a86ec467239f510d14de8")
(package! org-contrib
  :lockfile 'pinned
  :pin "56a3bbbd486c567234e63c95c954c5e2ed77f8e7")

;;; core

;; core.el
(package! auto-minor-mode)
(package! gcmh)

;; core-ui.el
(package! all-the-icons)
(package! hide-mode-line)
(package! highlight-numbers)
(package! rainbow-delimiters)
(package! restart-emacs)

;; core-editor.el
(package! better-jumper)
(package! dtrt-indent)
(package! expand-region)
(package! helpful)
(package! pcre2el)
(package! smartparens)
(package! undo-tree)
(package! ws-butler
  :recipe (:host github :repo "hlissner/ws-butler"))

;; core-modules.el
(package! org-ql)
(package! el-patch)

;; core-packages.el

;; core-projects.el
(package! projectile)

;; core-keybinds.el
(package! general)
(package! which-key)

;;; core-modules

;;;; completion
;; company
(when (featurep! :completion company)
  (package! company :lockfile +company)
  (package! company-dict :lockfile +company)
  (package! company-box :lockfile +company))
;; ivy
(when (featurep! :completion ivy)
  (package! ivy :lockfile +ivy)
  (package! counsel :lockfile +ivy)
  (package! counsel-projectile :lockfile +ivy)
  (package! swiper :lockfile +ivy)
  (package! ivy-rich :lockfile +ivy)
  (package! ivy-hydra :lockfile +ivy)
  (package! amx :lockfile +ivy)
  (package! wgrep :lockfile +ivy))

;;;; ui
;; hl-todo
(when (featurep! :ui hl-todo)
  (package! hl-todo :lockfile +hl-todo))
;; evil-goggles
(when (featurep! :ui evil-goggles)
  (package! evil-goggles :lockfile +evil-goggles))
;; highlight-symbol
(when (featurep! :ui highlight-symbol)
  (package! auto-highlight-symbol :lockfile +highlight-symbol))
;; transient-state
(when (featurep! :ui transient-state)
  (package! hydra :lockfile +transient-state))
;; treemacs
(when (featurep! :ui treemacs)
  (package! treemacs :lockfile +treemacs)
  (when (featurep! :editor evil)
    (package! treemacs-evil :lockfile +treemacs))
  (package! treemacs-projectile :lockfile +treemacs)
  (when (featurep! :tools magit)
    (package! treemacs-magit :lockfile +treemacs))
  (when (featurep! :ui workspaces)
    (package! treemacs-persp :lockfile +treemacs)))
;; vc-gutter
(when (featurep! :ui vc-gutter)
  (package! git-gutter :lockfile +vc-gutter)
  (package! git-gutter-fringe :lockfile +vc-gutter))
;; window-select
(when (featurep! :ui window-select)
  (package! ace-window :lockfile +window-select))
;; workspaces
(when (featurep! :ui workspaces)
  (package! persp-mode :lockfile +workspaces))

;;;; editor
;; evil
(when (featurep! :editor evil)
  (package! evil :lockfile +evil)
  (package! evil-nerd-commenter :lockfile +evil)
  (package! evil-lion :lockfile +evil)
  (package! evil-snipe :lockfile +evil)
  (package! evil-surround :lockfile +evil)
  (package! evil-traces :lockfile +evil)
  (package! evil-visualstar :lockfile +evil)
  ;; evil-collection
  (package! evil-collection :lockfile +evil))
;;file-templates
(when (featurep! :editor file-templates)
  (package! yasnippet :lockfile +file-templates))
;; fold
(when (featurep! :editor fold)
  (when (featurep! :editor evil)
    (package! evil-vimish-fold :lockfile +fold-evil))
  (package! outshine :lockfile +fold)
  (when (featurep! :completion ivy)
    (package! dash-functional :lockfile +fold)
    (package! outline-ivy
      :recipe (:host github :repo "DonHugo69/outline-ivy-mirror")
      :lockfile +fold)))
;; multiple-cursors
(when (featurep! :editor multiple-cursors)
  (when (featurep! :editor evil)
    (package! evil-mc :lockfile +multiple-cursors-evil)
    (package! evil-multiedit :lockfile +multiple-cursors-evil)))
;; rotate-text
(when (featurep! :editor rotate-text)
  (package! rotate-text
    :recipe (:host github :repo "debug-ito/rotate-text.el")
    :lockfile +rotate-text))
;; snippets
(when (featurep! :editor snippets)
  (package! yasnippet :lockfile +snippets)
  (package! auto-yasnippet :lockfile +snippets))
;; structural-editing
(when (and (featurep! :editor cleverlispy)
           (featurep! :editor evil))
  ;; lispy
  (package! lispyville :lockfile +cleverlispy)
  ;; evil-cleverparens
  (package! evil-cleverparens
    :recipe (:host github :repo "luxbock/evil-cleverparens"
             :fork (:host github :repo "DonHugo69/evil-cleverparens"))
    :lockfile +cleverlispy))

;;;; emacs
;; dired
(when (featurep! :emacs dired)
  (package! diredfl :lockfile +dired)
  (package! dired-git-info :lockfile +dired)
  (package! diff-hl :lockfile +dired)
  (package! dired-rsync :lockfile +dired)
  (package! fd-dired :lockfile +dired))
;; ibuffer
(when (featurep! :emacs ibuffer)
  (package! ibuffer-projectile :lockfile +ibuffer)
  (package! ibuffer-vc :lockfile +ibuffer))
;; imenu
(when (featurep! :emacs imenu)
  (package! imenu-list :lockfile +imenu))
;; vc
(when (featurep! :emacs vc)
  (package! git-commit :lockfile +vc)
  (package! git-link :lockfile +vc)
  (package! git-modes :lockfile +vc)
  (package! git-timemachine :lockfile +vc))

;;;; term
;; eshell
(when (featurep! :term eshell)
  (package! esh-autosuggest :lockfile +eshell)
  (package! eshell-up :lockfile +eshell)
  (package! shrink-path :lockfile +eshell)
  (package! eshell-z :lockfile +eshell)
  (package! esh-help :lockfile +eshell)
  (package! eshell-did-you-mean :lockfile +eshell)
  (package! eshell-syntax-highlighting :lockfile +eshell)
  (package! bash-completion :lockfile +eshell-company)
  (package! fish-completion :lockfile +eshell-company))

;;;; tools
;; eval
(when (featurep! :tools eval)
  (package! quickrun :lockfile +eval))
;; lookup
(when (featurep! :tools lookup)
  (package! dumb-jump :lockfile +lookup)
  (when (featurep! :completion ivy)
    (package! ivy-xref :lockfile +lookup-ivy))
  (when (featurep! :completion helm)
    (package! helm-xref :lockfile +lookup-helm))
  (package! request :lockfile +lookup)
  (package! dash-docs :lockfile +lookup)
  (when (featurep! :completion ivy)
    (package! counsel-dash :lockfile +lookup-ivy))
  (when (featurep! :completion helm)
    (package! helm-dash :lockfile +lookup-helm)))
;; magit
(when (featurep! :tools magit)
  (package! magit :lockfile +magit)
  (package! magit-todos :lockfile +magit))

;;;; org
(package! avy :lockfile +org)
(package! htmlize :lockfile +org)
(package! org-superstar :lockfile +org)
(package! ox-clip :lockfile +org)
(package! toc-org :lockfile +org)
(when (featurep! :editor evil)
  (package! evil-org
    :recipe (:host github :repo "hlissner/evil-org-mode")
    :lockfile +org-evil))
;; agenda
(package! org-super-agenda :lockfile +org)
;; babel
(package! ob-async :lockfile +org)

;;;; lang
;; sh
(when (featurep! :lang sh)
  (when (featurep! :completion company)
    (package! company-shell :lockfile +shell-company)))
;; emacs-lisp
(when (featurep! :lang emacs-lisp)
  (package! highlight-quoted :lockfile +emacs-lisp)
  (package! macrostep :lockfile +emacs-lisp)
  (package! elisp-def :lockfile +emacs-lisp)
  (package! elisp-demos :lockfile +emacs-lisp))

;;;; config
;; default
(when (featurep! :config default)
  (package! avy :lockfile +config-default)
  (package! ace-link :lockfile +config-default))
