;; lisp/init.el -*- lexical-binding: t; -*-

(modules!

 :completion
 company
 ivy

 :ui
 dashboard
 hl-todo
 evil-goggles
 highlight-symbol
 popup
 pretty-code
 transient-state
 treemacs
 vc-gutter
 window-select
 workspaces

 :editor
 cleverlispy
 evil
 file-templates
 fold
 multiple-cursors
 rotate-text
 snippets

 :emacs
 dired
 electric
 ibuffer
 imenu
 vc

 :term
 eshell

 :tools
 backups
 eval
 lookup
 magit

 :org
 org

 :lang
 emacs-lisp
 sh

 :config
 (default
   +smartparens))
