;; lisp/core-autoload/debug.el -*- lexical-binding: t; -*-

;;
;;; Profiling

(defvar +emacs--profiler nil)
;;;###autoload
(defun +emacs/toggle-profiler ()
  "Toggle the Emacs profiler. Run it again to see the profiling report."
  (interactive)
  (if (not +emacs--profiler)
      (profiler-start 'cpu+mem)
    (profiler-report)
    (profiler-stop))
  (setq +emacs--profiler (not +emacs--profiler)))


;;
;;; Debug

;;;###autoload
(defvar +emacs-debug-variables
  '(async-debug
    debug-on-error
    +emacs-debug-p
    garbage-collection-messages
    gcmh-verbose
    init-file-debug
    jka-compr-verbose
    url-debug
    use-package-verbose
    (message-log-max . 16384))
  "A list of variable to toggle on `+emacs-debug-mode'.
Each entry can be a variable symbol or a cons cell whose CAR is the variable
symbol and CDR is the value to set it to when `+emacs-debug-mode' is activated.")

(defvar +emacs--debug-vars-undefined nil)

(defun +emacs--hook-watch-debug-vars (&rest _)
  (when-let (bound-vars (cl-remove-if-not #'boundp +emacs--debug-vars-undefined))
    (+emacs-log "New variables available: %s" bound-vars)
    (let ((message-log-max nil))
      (+emacs-debug-mode -1)
      (+emacs-debug-mode +1))))

;;;###autoload
(define-minor-mode +emacs-debug-mode
  "Toggle `debug-on-error' and `+emacs-debug-p' for verbose logging."
  :init-value nil
  :global t
  (let ((enabled +emacs-debug-mode))
    (setq +emacs--debug-vars-undefined nil)
    (dolist (var +emacs-debug-variables)
      (cond ((listp var)
             (cl-destructuring-bind (var . val) var
               (if (boundp var)
                   (set-default
                    var (if (not enabled)
                            (prog1 (get var 'initial-value)
                              (put 'x 'initial-value nil))
                          (put var 'initial-value (symbol-value var))
                          val))
                 (add-to-list 'doom--debug-vars-undefined var))))
            ((if (boundp var)
                 (set-default var enabled)
               (add-to-list '+emacs--debug-vars-undefined var)))))
    (when (called-interactively-p 'any)
      (when (fboundp 'explain-pause-mode)
        (explain-pause-mode (if enabled +1 -1))))
    ;; Watch for changes in `+emacs-debug-variables', or when packages load (and potentially define
    ;; one of `+emacs-debug-variables'), in case some of them aren't defined when
    ;; `+emacs-debug-mode' is first loaded.
    (cond (enabled
           (add-variable-watcher '+emacs-debug-variables #'+emacs--hook-watch-debug-vars)
           (add-hook 'after-load-functions #'+emacs--hook-watch-debug-vars))
          (t
           (remove-variable-watcher '+emacs-debug-variables #'+emacs--hook-watch-debug-vars)
           (remove-hook 'after-load-functions #'+emacs--hook-watch-debug-vars)))
    (message "Debug mode %s" (if enabled "on" "off"))))


;;
;;; Hooks

;;;###autoload
(defun +emacs--hook-run-all-startup-hooks ()
  "Run all startup Emacs hooks. Meant to be executed after starting Emacs with
-q or -Q, for example:
  emacs -Q -l init.el -f +emacs--hook-run-all-startup-hooks"
  (setq after-init-time (current-time))
  (let ((inhibit-startup-hooks nil))
    (mapc (lambda (hook)
            (run-hook-wrapped hook #'+emacs-try-run-hook))
          '(after-init-hook
            delayed-warnings-hook
            emacs-startup-hook
            tty-setup-hook
            window-setup-hook))))
