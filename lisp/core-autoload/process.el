;; lisp/core-autoload/process.el -*- lexical-binding: t; -*-

;;;###autoload
(defun +emacs-call-process (command &rest args)
  "Execute COMMAND with ARGS synchronously.
Returns (STATUS . OUTPUT) when it is done, where STATUS is the
returned error code of the process and OUTPUT is its stdout
output."
  (with-temp-buffer
    (cons (or (apply #'call-process command nil t nil args)
              -1)
          (string-trim (buffer-string)))))
