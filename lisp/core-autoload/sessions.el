;; lisp/autoloads/sessions.el -*- lexical-binding: t; -*-

(defvar desktop-base-file-name)
(defvar desktop-dirname)
(defvar desktop-restore-eager)
(defvar desktop-file-modtime)

;;; Helpers

;;;###autoload
(defun +emacs-session-file (&optional name)
  "TODO"
  (cond ((require 'persp-mode nil t)
         (expand-file-name (or name persp-auto-save-fname) persp-save-dir))
        ((require 'desktop nil t)
         (if name
             (expand-file-name name (file-name-directory (desktop-full-file-name)))
           (desktop-full-file-name)))
        ((error "No session backend available"))))

;;;###autoload
(defun +emacs-save-session (&optional file)
  "TODO"
  (setq file (expand-file-name (or file (+emacs-session-file))))
  (cond ((require 'persp-mode nil t)
         (unless persp-mode (persp-mode +1))
         (setq persp-auto-save-opt 0)
         (persp-save-state-to-file file))
        ((and (require 'frameset nil t)
              (require 'restart-emacs nil t))
         (let ((frameset-filter-alist (append '((client . restart-emacs--record-tty-file))
                                              frameset-filter-alist))
               (desktop-base-file-name (file-name-nondirectory file))
               (desktop-dirname (file-name-directory file))
               (desktop-restore-eager t)
               desktop-file-modtime)
           (make-directory desktop-dirname t)
           ;; Prevents confirmation prompts
           (let ((desktop-file-modtime (nth 5 (file-attributes (desktop-full-file-name)))))
             (desktop-save desktop-dirname t))))
        ((error "No session backend to save session with"))))

;;;###autoload
(defun +emacs-load-session (&optional file)
  "TODO"
  (setq file (expand-file-name (or file (+emacs-session-file))))
  (message "Attempting to load %s" file)
  (cond ((require 'persp-mode nil t)
         (unless persp-mode
           (persp-mode +1))
         (let ((allowed (persp-list-persp-names-in-file file)))
           (cl-loop for name being the hash-keys of *persp-hash*
                    unless (member name allowed)
                    do (persp-kill name))
           (persp-load-state-from-file file)))
        ((and (require 'frameset nil t)
              (require 'restart-emacs nil t))
         (restart-emacs--restore-frames-using-desktop file))
        ((error "No session backend to load session with"))))


;;
;;; Commands

;;;###autoload
(defun +emacs/quickload-session ()
  "TODO"
  (interactive)
  (message "Restoring session...")
  (+emacs-load-session)
  (message "Session restored. Welcome back."))

;;;###autoload
(defun +emacs/quicksave-session ()
  "TODO"
  (interactive)
  (message "Saving session")
  (+emacs-save-session)
  (message "Saving session...DONE"))

;;;###autoload
(defun +emacs/load-session (file)
  "TODO"
  (interactive
   (let ((session-file (+emacs-session-file)))
     (list (or (read-file-name "Session to restore: "
                               (file-name-directory session-file)
                               nil t
                               (file-name-nondirectory session-file))
               (user-error "No session selected. Aborting")))))
  (unless file
    (error "No session file selected"))
  (message "Loading '%s' session" file)
  (+emacs-load-session file)
  (message "Session restored. Welcome back."))


;;;###autoload
  (defun +emacs/save-session (file)
    "TODO"
    (interactive
     (let ((session-file (+emacs-session-file)))
       (list (or (read-file-name "Save session to: "
                                 (file-name-directory session-file)
                                 nil nil
                                 (file-name-nondirectory session-file))
                 (user-error "No session selected. Aborting")))))
    (unless file
      (error "No session file selected"))
    (message "Saving '%s' session" file)
    (+emacs-save-session file))

;;;###autoload
(defalias '+emacs/restart #'restart-emacs)

;;;###autoload
(defalias '+emacs/new-emacs #'restart-emacs-start-new-emacs)

;;;###autoload
(defun +emacs/restart-and-restore (&optional debug)
  "TODO"
  (interactive "P")
  (setq +emacs-autosave-session nil)
  (+emacs/quicksave-session)
  (save-some-buffers nil t)
  (letf! ((#'save-buffers-kill-emacs #'kill-emacs)
          (confirm-kill-emacs))
    (restart-emacs
     (append (if debug (list "--debug-init"))
             (when (boundp 'chemacs-current-emacs-profile)
               (list "--with-profile" chemacs-current-emacs-profile))
             (list "--eval" "(add-hook 'window-setup-hook #'+emacs-load-session 100)")))))
