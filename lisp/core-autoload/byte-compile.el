;; lisp/core-autoload/byte-compile.el -*- lexical-binding: t; -*-

(defun +emacs--byte-compile-ignore-file-p (path)
  (let ((filename (file-name-nondirectory path)))
    (or (string-prefix-p "." filename)
        (string-prefix-p "test-" filename)
        (string-suffix-p ".example.el" filename)
        (string-prefix-p "+" filename)
        (string-prefix-p "flycheck_" filename)
        (not (equal (file-name-extension path) "el"))
        (member filename (list "packages.el" "init-cli.el" "init-packages.el")))))

;;;###autoload
(defun +emacs-config-byte-compile (&optional report-progress)
  "Byte-compile configuration. For interactive usage.
REPORT-PROGRESS non-nil (or interactively) means to print more
messages."
  (interactive (list 'report-progress))
  (cl-block nil
    (unless (cl-loop for file
                     in (+emacs-files-in (list +emacs-core-dir +emacs-modules-dir)
                               :match "\\.el$"
                               :filter #'+emacs--byte-compile-ignore-file-p
                               :depth 0)
                     for elc-file = (concat file "c")
                     if (file-newer-than-file-p file elc-file)
                     return t)
      (when report-progress
        (print! (success "Byte-compiled configuration already up to date")))
      (cl-return))
    (when report-progress
      (print! (start "Byte-compiling configuration...")))

    (let* ((evil-collection-mode-list nil)
           (default-directory +emacs-dir)
           (buf (get-buffer-create " *emacs*"))
           (+emacs-format-backend 'ansi)
           (ignore-window-parameters t)
           (noninteractive t))
      (with-current-buffer buf
        (erase-buffer)
        (make-process
         :name "+emacs-config-byte-compile"
         :buffer " *emacs*"
         :command '("make" "compile")
         :noquery t
         :sentinel
         (lambda (proc _event)
           (unless (process-live-p proc)
             (with-current-buffer (process-buffer proc)
               (require 'ansi-color)
               (ansi-color-apply-on-region (point-min) (point-max))
               (if (= 0 (process-exit-status proc))
                   (progn
                     (message
                      (if report-progress
                          "Byte-compiling updated configuration...done"
                        "Byte-compiled updated configuration")))
                 (message "Failed to byte-compile"))))))))))
