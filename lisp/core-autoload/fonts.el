;; lisp/core-autoload/fonts.el -*- lexical-binding: t; -*-

;;;###autoload
(defvar +emacs-font-increment 2
  "How many steps to increase the font size each time
`+emacs/increase-font-size' or `+emacs/decrease-font-size' are
invoked.")

;;;###autoload
(defvar +emacs-big-font nil
  "The font to use for `+emacs-big-font-mode'.
If nil, `+emacs-font' will be used, scaled up by
`+emacs-big-font-increment'. See `+emacs-font' for details on
acceptable values for this variable.")

;;;###autoload
(defvar +emacs-big-font-increment 4
  "How many steps to increase the font size (with `+emacs-font'
as the base) when `+emacs-big-font-mode' is enabled and
`+emacs-big-font' is nil.")


;;
;;; Library

;;;###autoload
(defun +emacs-normalize-font (font)
  "Return FONT as a normalized font spec.
The font will be normalized (i.e. :weight, :slant, and :width
will set to 'normal if not specified) before it is converted.
FONT can be a `font-spec', a font object, an XFT font string, or
an XLFD font string."
  (cl-check-type font (or font string vector))
  (when (and (stringp font)
             (string-prefix-p "-" font))
    (setq font (x-decompose-font-name font)))
  (let* ((font
          (cond ((stringp font)
                 (dolist (prop '("weight" "slant" "width") (aref (font-info font) 0))
                   (unless (string-match-p (format ":%s=" prop) font)
                     (setq font (concat font ":" prop "=normal")))))
                ((fontp font)
                 (dolist (prop '(:weight :slant :width) (font-xlfd-name font))
                   (unless (font-get font prop)
                     (font-put font prop 'normal))))
                ((vectorp font)
                 (dolist (i '(1 2 3) (x-compose-font-name font))
                   (unless (aref font i)
                     (aset font i "normal"))))))
         (font (x-resolve-font-name font))
         (font (font-spec :name font)))
    (unless (font-get font :size)
      (font-put font :size
                (font-get (font-spec :name (face-font 'default))
                          :size)))
    font))


;;;###autoload
(defun +emacs-adjust-font-size (increment &optional fixed-size-p font-alist)
  "Increase size of font in FRAME by INCREMENT.
If FIXED-SIZE-P is non-nil, treat INCREMENT as a font size,
rather than a scaling factor. FONT-ALIST is an alist give
temporary values to certain font variables, like `+emacs-font' or
`+emacs-variable-pitch-font'. e.g.

  `((+emacs-font . ,(font-spec :family \"Sans Serif\" :size 12)))

Doesn't work in terminal Emacs."
  (unless (display-multi-font-p)
    (user-error "Cannot resize fonts in terminal Emacs"))
  (condition-case-unless-debug e
      (let (changed)
        (dolist (sym '((+emacs-font . default)
                       (+emacs-serif-font . fixed-pitch-serif)
                       (+emacs-variable-pitch-font . variable-pitch))
                     (when changed
                       (+emacs--hook-init-fonts 'reload)
                       t))
          (cl-destructuring-bind (var . face) sym
            (if (null increment)
                (when (get var 'initial-value)
                  (set var (get var 'initial-value))
                  (put var 'initial-value nil)
                  (setq changed t))
              (let* ((original-font (or (symbol-value var)
                                        (face-font face t)
                                        (with-temp-buffer (face-font face))))
                     (font (+emacs-normalize-font original-font))
                     (dfont
                      (or (if-let* ((remap-font (alist-get var font-alist))
                                    (remap-xlfd (+emacs-normalize-font remap-font)))
                              remap-xlfd
                            (purecopy font))
                          (error "Could not decompose %s font" var))))
                (let* ((step      (if fixed-size-p 0 (* increment +emacs-font-increment)))
                       (orig-size (font-get font :size))
                       (new-size  (if fixed-size-p increment (+ orig-size step))))
                  (cond ((<= new-size 0)
                         (error "`%s' font is too small to be resized (%d)" var new-size))
                        ((= orig-size new-size)
                         (user-error "Could not resize `%s' for some reason" var))
                        ((setq changed t)
                         (unless (get var 'initial-value)
                           (put var 'initial-value original-font))
                         (font-put dfont :size new-size)
                         (set var dfont)))))))))
    (error
     (ignore-errors (+emacs-adjust-font-size nil))
     (signal (car e) (cdr e)))))


;;
;;; Commands

;;;###autoload
(defun +emacs/reload-font ()
  "Reload your fonts, if they're set.
See `+emacs--hook-init-fonts'."
  (interactive)
  (+emacs--hook-init-fonts 'reload))

;;;###autoload
(defun +emacs/increase-font-size (count &optional increment)
  "Enlargens the font size across the current and child frames."
  (interactive "p")
  (+emacs-adjust-font-size (* count (or increment +emacs-font-increment))))

;;;###autoload
(defun +emacs/decrease-font-size (count &optional increment)
  "Shrinks the font size across the current and child frames."
  (interactive "p")
  (+emacs-adjust-font-size (* (- count) (or increment +emacs-font-increment))))

;;;###autoload
(defun +emacs/reset-font-size ()
  "Reset font size and `text-scale'.

Assuming it has been adjusted via `+emacs/increase-font-size' and
`+emacs/decrease-font-size', or `text-scale-*' commands."
  (interactive)
  (let (success)
    (when (and (boundp 'text-scale-mode-amount)
               (/= text-scale-mode-amount 0))
      (text-scale-set 0)
      (setq success t))
    (cond (+emacs-big-font-mode
           (message "Disabling `+emacs-big-font-mode'")
           (+emacs-big-font-mode -1)
           (setq success t))
          ((+emacs-adjust-font-size nil)
           (setq success t)))
    (unless success
      (user-error "The font hasn't been resized"))))

;;;###autoload
(define-minor-mode +emacs-big-font-mode
  "A global mode that resizes the font, for streams,
screen-sharing and presentations. Uses `+emacs-big-font' if its
set, otherwise uses `+emacs-font' (falling back to your system
font). Also resizees `+emacs-variable-pitch-font' and
`+emacs-serif-font'."
  :init-value nil
  :lighter " BIG"
  :global t
  (if +emacs-big-font
      ;; Use `+emacs-big-font' in lieu of `+emacs-font'
      (+emacs-adjust-font-size
       (when +emacs-big-font-mode
         (font-get (+emacs-normalize-font +emacs-big-font) :size))
       t `((+emacs-font . ,+emacs-big-font)))
    ;; Resize the current font
    (+emacs-adjust-font-size (if +emacs-big-font-mode +emacs-big-font-increment))))
