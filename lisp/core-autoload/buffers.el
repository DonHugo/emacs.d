;; lisp/autoloads/buffers.el -*- lexical-binding: t; -*-

;;;###autoload
(defvar +emacs-real-buffer-functions
  '(+emacs-dired-buffer-p)
  "A list of predicate functions run to determine if a buffer is real, unlike
`+emacs-unreal-buffer-functions'. They are passed one argument: the buffer to be
tested.
Should any of its function returns non-nil, the rest of the functions are
ignored and the buffer is considered real.
See `+emacs-real-buffer-p' for more information.")

;;;###autoload
(defvar +emacs-unreal-buffer-functions
  '(minibufferp +emacs-special-buffer-p +emacs-non-file-visiting-buffer-p)
  "A list of predicate functions run to determine if a buffer is *not* real,
unlike `+emacs-real-buffer-functions'. They are passed one argument: the buffer to
be tested.
Should any of these functions return non-nil, the rest of the functions are
ignored and the buffer is considered unreal.
See `+emacs-real-buffer-p' for more information.")

;;;###autoload
(defvar-local +emacs-real-buffer-p nil
  "If non-nil, this buffer should be considered real no matter what. See
`+emacs-real-buffer-p' for more information.")

;;;###autoload
(defvar +emacs-fallback-buffer-name "*scratch*"
  "The name of the buffer to fall back to if no other buffers exist (will create
it if it doesn't exist).")

;;; Functions

;;;###autoload
(defun +emacs-buffer-frame-predicate (buf)
  "To be used as the default frame buffer-predicate parameter. Returns nil if
BUF should be skipped over by functions like `next-buffer' and `other-buffer'."
  (or (+emacs-real-buffer-p buf)
      (eq buf (+emacs-fallback-buffer))))

;;;###autoload
(defun +emacs-fallback-buffer ()
  "Returns the fallback buffer, creating it if necessary. By default this is the
scratch buffer. See `+emacs-fallback-buffer-name' to change this."
  (let (buffer-list-update-hook)
    (get-buffer-create +emacs-fallback-buffer-name)))

;;;###autoload
(defalias '+emacs-buffer-list #'buffer-list)

;;;###autoload
(defun +emacs-project-buffer-list (&optional project)
  "Return a list of buffers belonging to the specified PROJECT.
If PROJECT is nil, default to the current project."
  (let ((buffers (+emacs-buffer-list)))
    (if-let* ((project-root
               (if project (expand-file-name project)
                 (+emacs-project-root))))
        (cl-loop for buf in buffers
                 if (projectile-project-buffer-p buf project-root)
                 collect buf)
      buffers)))

;;;###autoload
(defun +emacs-open-projects ()
  "Return a list of projects with open buffers."
  (cl-loop with projects = (make-hash-table :test 'equal :size 8)
           for buffer in (+emacs-buffer-list)
           if (buffer-live-p buffer)
           if (+emacs-real-buffer-p buffer)
           if (with-current-buffer buffer (+emacs-project-root))
           do (puthash (abbreviate-file-name it) t projects)
           finally return (hash-table-keys projects)))

;;;###autoload
(defun +emacs-dired-buffer-p (buf)
  "Returns non-nil if BUF is a dired buffer."
  (with-current-buffer buf (derived-mode-p 'dired-mode)))

;;;###autoload
(defun +emacs-special-buffer-p (buf)
  "Returns non-nil if BUF's name starts and ends with an *."
  (equal (substring (buffer-name buf) 0 1) "*"))

;;;###autoload
(defun +emacs-temp-buffer-p (buf)
  "Returns non-nil if BUF is temporary."
  (equal (substring (buffer-name buf) 0 1) " "))

;;;###autoload
(defun +emacs-non-file-visiting-buffer-p (buf)
  "Returns non-nil if BUF does not have a value for `buffer-file-name'."
  (not (buffer-file-name buf)))

;;;###autoload
(defun +emacs-real-buffer-list (&optional buffer-list)
  "Return a list of buffers that satify `+emacs-real-buffer-p'."
  (cl-remove-if-not #'+emacs-real-buffer-p (or buffer-list (+emacs-buffer-list))))

;;;###autoload
(defun +emacs-real-buffer-p (buffer-or-name)
  "Returns t if BUFFER-OR-NAME is a 'real' buffer.
A real buffer is a useful buffer; a first class citizen. Real
ones should get special treatment, because we will be spending
most of our time in them. Unreal ones should be low-profile and
easy to cast aside, so we can focus on real ones.

The exact criteria for a real buffer is:

1. A non-nil value for the buffer-local value of the
`+emacs-real-buffer-p' variable OR

2. Any function in `+emacs-real-buffer-functions' returns non-nil

OR

3. None of the functions in `+emacs-unreal-buffer-functions' must
return non-nil.

If BUFFER-OR-NAME is omitted or nil, the current buffer is tested."
  (or (bufferp buffer-or-name)
      (stringp buffer-or-name)
      (signal 'wrong-type-argument (list '(bufferp stringp) buffer-or-name)))
  (when-let (buf (get-buffer buffer-or-name))
    (when-let (basebuf (buffer-base-buffer buf))
      (setq buf basebuf))
    (and (buffer-live-p buf)
         (not (+emacs-temp-buffer-p buf))
         (or (buffer-local-value '+emacs-real-buffer-p buf)
             (run-hook-with-args-until-success '+emacs-real-buffer-functions buf)
             (not (run-hook-with-args-until-success '+emacs-unreal-buffer-functions buf))))))

;;;###autoload
(defun +emacs-unreal-buffer-p (buffer-or-name)
  "Return t if BUFFER-OR-NAME is an 'unreal' buffer.
See `+emacs-real-buffer-p' for details on what that means."
  (not (+emacs-real-buffer-p buffer-or-name)))

;;;###autoload
(defun +emacs-buffers-in-mode (modes &optional buffer-list derived-p)
  "Return a list of buffers whose `major-mode' is `eq' to MODE(S).
If DERIVED-P, test with `derived-mode-p', otherwise use `eq'."
  (let ((modes (+emacs-enlist modes)))
    (cl-remove-if-not (if derived-p
                          (lambda (buf)
                            (with-current-buffer buf
                              (apply #'derived-mode-p modes)))
                        (lambda (buf)
                          (memq (buffer-local-value 'major-mode buf) modes)))
                      (or buffer-list (+emacs-buffer-list)))))

;;;###autoload
(defun +emacs-visible-windows (&optional window-list)
  "Return a list of the visible, non-popup (dedicated) windows."
  (cl-loop for window in (or window-list (window-list))
           when (or (window-parameter window 'visible)
                    (not (window-dedicated-p window)))
           collect window))

;;;###autoload
(defun +emacs-visible-buffers (&optional buffer-list)
  "Return a list of visible buffers (i.e. not buried)."
    (let ((buffers (delete-dups (mapcar #'window-buffer (window-list)))))
    (if buffer-list
        (cl-delete-if (lambda (b) (memq b buffer-list))
                      buffers)
      (delete-dups buffers))))

;;;###autoload
(defun +emacs-buried-buffers (&optional buffer-list)
  "Get a list of buffers that are buried."
  (cl-remove-if #'get-buffer-window (or buffer-list (+emacs-buffer-list))))

;;;###autoload
(defun +emacs-matching-buffers (pattern &optional buffer-list)
  "Get a list of all buffers that match the regex PATTERN."
  (cl-loop for buf in (or buffer-list (+emacs-buffer-list))
           when (string-match-p pattern (buffer-name buf))
           collect buf))

;;;###autoload
(defun +emacs-set-buffer-real (buffer flag)
  "Forcibly mark BUFFER as FLAG (non-nil = real)."
  (with-current-buffer buffer
    (setq +emacs-real-buffer-p flag)))

;;;###autoload
(defun +emacs-kill-buffer-and-windows (buffer)
  "Kill the buffer and delete all the windows it's displayed in."
  (dolist (window (get-buffer-window-list buffer))
    (unless (one-window-p t)
      (delete-window window)))
  (kill-buffer buffer))

;;;###autoload
(defun +emacs-fixup-windows (windows)
  "Ensure that each of WINDOWS is showing a real buffer or the fallback buffer."
  (dolist (window windows)
    (with-selected-window window
      (when (+emacs-unreal-buffer-p (window-buffer))
        (previous-buffer)
        (when (+emacs-unreal-buffer-p (window-buffer))
          (switch-to-buffer (+emacs-fallback-buffer)))))))

;;;###autoload
(defun +emacs-kill-buffer-fixup-windows (buffer)
  "Kill the BUFFER and ensure all the windows it was displayed in have switched
to a real buffer or the fallback buffer."
  (let ((windows (get-buffer-window-list buffer)))
    (kill-buffer buffer)
    (+emacs-fixup-windows (cl-remove-if-not #'window-live-p windows))))

;;;###autoload
(defun +emacs-kill-buffers-fixup-windows (buffers)
  "Kill the BUFFERS and ensure all the windows they were displayed in have
switched to a real buffer or the fallback buffer."
  (let ((seen-windows (make-hash-table :test 'eq :size 8)))
    (dolist (buffer buffers)
      (let ((windows (get-buffer-window-list buffer)))
        (kill-buffer buffer)
        (dolist (window (cl-remove-if-not #'window-live-p windows))
          (puthash window t seen-windows))))
    (+emacs-fixup-windows (hash-table-keys seen-windows))))

;;;###autoload
(defun +emacs-kill-matching-buffers (pattern &optional buffer-list)
  "Kill all buffers (in current workspace OR in BUFFER-LIST) that match the
regex PATTERN. Returns the number of killed buffers."
  (let ((buffers (+emacs-matching-buffers pattern buffer-list)))
    (dolist (buf buffers (length buffers))
      (kill-buffer buf))))

;;; Hooks

;;;###autoload
(defun +emacs--hook-mark-buffer-as-real ()
  "Hook function that marks the current buffer as real."
  (+emacs-set-buffer-real (current-buffer) t))

;;; Interactive commands

;;;###autoload
(defun +emacs/kill-this-buffer-in-all-windows (buffer &optional dont-save)
  "Kill BUFFER globally and ensure all windows previously showing this buffer
have switched to a real buffer or the fallback buffer.
If DONT-SAVE, don't prompt to save modified buffers (discarding their changes)."
  (interactive
   (list (current-buffer) current-prefix-arg))
  (cl-assert (bufferp buffer) t)
  (when (and (buffer-modified-p buffer) dont-save)
    (with-current-buffer buffer
      (set-buffer-modified-p nil)))
  (+emacs-kill-buffer-fixup-windows buffer))

(defun +emacs--message-or-count (interactive message count)
  (if interactive
      (message message count)
    count))

;;;###autoload
(defun +emacs/kill-all-buffers (&optional buffer-list interactive)
  "Kill all buffers and closes their windows.
If the prefix arg is passed, doesn't close windows and only kill buffers that
belong to the current project."
  (interactive
   (list (if current-prefix-arg
             (+emacs-project-buffer-list)
           (+emacs-buffer-list))
         t))
  (if (null buffer-list)
      (message "No buffers to kill")
    (save-some-buffers)
    (delete-other-windows)
    (when (memq (current-buffer) buffer-list)
      (switch-to-buffer (+emacs-fallback-buffer)))
    (mapc #'kill-buffer buffer-list)
    (+emacs--message-or-count
     interactive "Killed %d buffers"
     (- (length buffer-list)
        (length (cl-remove-if-not #'buffer-live-p buffer-list))))))

;;;###autoload
(defun +emacs/kill-other-buffers (&optional buffer-list interactive)
  "Kill all other buffers (besides the current one).
If the prefix arg is passed, kill only buffers that belong to the current
project."
  (interactive
   (list (delq (current-buffer)
               (if current-prefix-arg
                   (+emacs-project-buffer-list)
                 (+emacs-buffer-list)))
         t))
  (mapc #'+emacs-kill-buffer-and-windows buffer-list)
  (+emacs--message-or-count
   interactive "Killed %d other buffers"
   (- (length buffer-list)
      (length (cl-remove-if-not #'buffer-live-p buffer-list)))))

;;;###autoload
(defun +emacs/kill-matching-buffers (pattern &optional buffer-list interactive)
  "Kill buffers that match PATTERN in BUFFER-LIST.
If the prefix arg is passed, only kill matching buffers in the current project."
  (interactive
   (list (read-regexp "Buffer pattern: ")
         (if current-prefix-arg
             (+emacs-project-buffer-list)
           (+emacs-buffer-list))
         t))
  (+emacs-kill-matching-buffers pattern buffer-list)
  (+emacs--message-or-count
   interactive "Killed %d buried buffers"
   (- (length buffer-list)
      (length (cl-remove-if-not #'buffer-live-p buffer-list)))))

;;;###autoload
(defun +emacs/kill-buried-buffers (&optional buffer-list interactive)
  "Kill buffers that are buried.
If PROJECT-P (universal argument), only kill buried buffers belonging to the
current project."
  (interactive
   (list (+emacs-buried-buffers
          (if current-prefix-arg (+emacs-project-buffer-list)))
         t))
  (mapc #'kill-buffer buffer-list)
  (when interactive
    (message "Killed %s buried buffers"
             (- (length buffer-list)
                (length (cl-remove-if-not #'buffer-live-p buffer-list))))))

;;;###autoload
(defun +emacs/kill-project-buffers (project &optional interactive)
  "Kill buffers for the specified PROJECT."
  (interactive
   (list (if-let (open-projects (+emacs-open-projects))
             (completing-read
              "Kill buffers for project: " open-projects
              nil t nil nil
              (if-let* ((project-root (+emacs-project-root))
                        (project-root (abbreviate-file-name project-root))
                        ((member project-root open-projects)))
                  project-root))
           (message "No projects are open!")
           nil)
         t))
  (when project
    (let ((buffer-list (+emacs-project-buffer-list project)))
      (+emacs-kill-buffers-fixup-windows buffer-list)
      (+emacs--message-or-count
       interactive "Killed %d project buffers"
       (- (length buffer-list)
          (length (cl-remove-if-not #'buffer-live-p buffer-list)))))))

;;; Spacemacs

;;;###autoload
(defvar +emacs--killed-buffer-list nil
  "List of recently killed buffers.")

;;;###autoload
(defun +emacs-add-buffer-to-killed-list ()
  "If buffer is associated with a file name, add that file
to the `killed-buffer-list' when killing the buffer."
  (when buffer-file-name
    (push buffer-file-name +emacs--killed-buffer-list)))

;;; Interactive commands

;;;###autoload
(defun +emacs/copy-clipboard-to-whole-buffer ()
  "Copy clipboard and replace buffer"
  (interactive)
  (delete-region (point-min) (point-max))
  (clipboard-yank)
  (deactivate-mark))

;;;###autoload
(defun +emacs/copy-whole-buffer-to-clipboard ()
  "Copy entire buffer to clipboard"
  (interactive)
  (clipboard-kill-ring-save (point-min) (point-max)))

;;;###autoload
(defun +emacs/reopen-killed-buffer ()
  "Reopen the most recently killed file buffer, if one exists."
  (interactive)
  (when +emacs--killed-buffer-list
    (find-file (pop +emacs--killed-buffer-list))))

;;;###autoload
(defun +emacs/safe-erase-buffer ()
  "Prompt before erasing the content of the file."
  (interactive)
  (if (y-or-n-p (format "Erase content of buffer %s ? " (current-buffer)))
      (erase-buffer)))

;;;###autoload
(defun +emacs/switch-to-help-buffer ()
  "Open or select the `*Help*' buffer, if it exists."
  (interactive)
  (if (get-buffer "*Help*")
      (switch-to-buffer (help-buffer))
    (message "No previous Help buffer found")))

;;;###autoload
(defun +emacs/switch-to-messages-buffer (&optional arg)
  "Switch to the `*Messages*' buffer.
if prefix argument ARG is given, switch to it in an other, possibly new window."
  (interactive "P")
  (with-current-buffer (messages-buffer)
    (goto-char (point-max))
    (if arg
        (switch-to-buffer-other-window (current-buffer))
      (switch-to-buffer (current-buffer)))))
