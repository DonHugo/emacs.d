;; lisp/core-autoloads/packages.el -*- lexical-binding: t; -*-

(mapc (+emacs-rpartial #'load nil (not +emacs-debug-p) 'nosuffix)
      (file-expand-wildcards (concat +emacs-core-dir "cli/*.el")))

;;;###autoload
(defun +emacs/search-versions ()
  (interactive)
  (let ((default-directory (concat +emacs-dir "straight/versions")))
    (funcall-interactively #'+default/search-cwd)))

;;;###autoload
(defun +emacs/update-package ()
  "Updates a single package."
  (interactive)
  (print! (start "Updating package (this may take a while)..."))
  (call-interactively #'straight-pull-package)
  (print! (start "Exit and run 'make referesh'!")))

;;;###autoload
(defun +emacs/update-all-packages ()
  "Updates all packages and forces a rebuild of all packages."
  (interactive)
  (require 'init-cli)
  (let* ((evil-collection-mode-list nil)
         (default-directory +emacs-dir)
         (buf (get-buffer-create " *emacs*"))
         (+emacs-format-backend 'ansi)
         (ignore-window-parameters t)
         (noninteractive t)
         (+emacs-interactive-p nil)
         (standard-output
          (lambda (char)
            (with-current-buffer buf
              (insert char)
              (when (memq char '(?\n ?\r))
                (require 'ansi-color)
                (ansi-color-apply-on-region (line-beginning-position -1) (line-end-position))
                (redisplay))))))
    (with-current-buffer (switch-to-buffer buf)
      (erase-buffer)

      (print! (start "Updating packages (this may take a while)..."))
      (straight-x-pull-all)
      (+emacs-cli-packages-build t)))
  (print! (start "Exit and run 'make referesh'!")))
