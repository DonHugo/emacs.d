;; lisp/autoloads/scratch.el -*- lexical-binding: t; -*-

(defvar +emacs-scratch-default-file "__default"
  "The default file name for a project-less scratch buffer.
Will be saved in `+emacs-scratch-dir'.")

(defvar +emacs-scratch-dir (concat +emacs-etc-dir "scratch")
  "Where to save persistent scratch buffers.")

(defvar +emacs-scratch-initial-major-mode t
  "What major mode to start fresh scratch buffers in.
Scratch buffers preserve their last major mode, however, so this only affects
the first, fresh scratch buffer you create. This accepts:
  t           Inherits the major mode of the last buffer you had selected.
  nil         Uses `fundamental-mode'
  MAJOR-MODE  Any major mode symbol")

(defvar +emacs-scratch-buffers nil
  "A list of active scratch buffers.")

(defvar +emacs-scratch-current-project nil
  "The name of the project associated with the current scratch buffer.")
(put '+emacs-scratch-current-project 'permanent-local t)

(defvar +emacs-scratch-buffer-hook ()
  "The hooks to run after a scratch buffer is created.")


(defun +emacs--load-persistent-scratch-buffer (project-name)
  (setq-local +emacs-scratch-current-project
              (or project-name
                  +emacs-scratch-default-file))
  (let ((smart-scratch-file
         (expand-file-name (concat +emacs-scratch-current-project ".el")
                           +emacs-scratch-dir)))
    (make-directory +emacs-scratch-dir t)
    (when (file-readable-p smart-scratch-file)
      (message "Reading %s" smart-scratch-file)
      (cl-destructuring-bind (content point mode)
          (with-temp-buffer
            (save-excursion (insert-file-contents smart-scratch-file))
            (read (current-buffer)))
        (erase-buffer)
        (funcall mode)
        (insert content)
        (goto-char point)
        t))))

;;;###autoload
(defun +emacs-scratch-buffer (&optional dont-restore-p mode directory project-name)
  "Return a scratchpad buffer in major MODE."
  (let* ((buffer-name (if project-name
                          (format "*+emacs:scratch (%s)*" project-name)
                        "*+emacs:scratch*"))
         (buffer (get-buffer buffer-name)))
    (with-current-buffer
        (or buffer (get-buffer-create buffer-name))
      (setq default-directory directory)
      (setq-local so-long--inhibited t)
      (if dont-restore-p
          (erase-buffer)
        (unless buffer
          (+emacs--load-persistent-scratch-buffer project-name)
          (when (and (eq major-mode 'fundamental-mode)
                     (functionp mode))
            (funcall mode))))
      (cl-pushnew (current-buffer) +emacs-scratch-buffers)
      (add-transient-hook! '+emacs-switch-buffer-hook (+emacs--hook-persist-scratch-buffers))
      (add-transient-hook! '+emacs-switch-window-hook (+emacs--hook-persist-scratch-buffers))
      (add-hook 'kill-buffer-hook #'+emacs--hook-persist-scratch-buffer nil 'local)
      (add-hook '+emacs-scratch-buffer-hook #'+emacs--hook-mark-buffer-as-real)
      (run-hooks '+emacs-scratch-buffer-hook)
      (current-buffer))))


;;; Persistent scratch buffer

;;;###autoload
(defun +emacs--hook-persist-scratch-buffer ()
  "Save the current buffer to `+emacs-scratch-dir'."
  (let ((content (buffer-substring-no-properties (point-min) (point-max)))
        (point (point))
        (mode major-mode))
    (with-temp-file
        (expand-file-name (concat (or +emacs-scratch-current-project
                                      +emacs-scratch-default-file)
                                  ".el")
                          +emacs-scratch-dir)
      (prin1 (list content
                   point
                   mode)
             (current-buffer)))))

;;;###autoload
(defun +emacs--hook-persist-scratch-buffers ()
  "Save all scratch buffers to `+emacs-scratch-dir'."
  (setq +emacs-scratch-buffers
        (cl-delete-if-not #'buffer-live-p +emacs-scratch-buffers))
  (dolist (buffer +emacs-scratch-buffers)
    (with-current-buffer buffer
      (+emacs--hook-persist-scratch-buffer))))

;;;###autoload
(defun +emacs--hook-persist-scratch-buffers-after-switch ()
  "Kill scratch buffers when they are no longer visible, saving them to disk."
  (unless (cl-some #'get-buffer-window +emacs-scratch-buffers)
    (mapc #'kill-buffer +emacs-scratch-buffers)
    (remove-hook '+emacs-switch-buffer-hook #'+emacs--hook-persist-scratch-buffers-after-switch)))

;;;###autoload
(unless noninteractive
  (add-hook 'kill-emacs-hook #'+emacs--hook-persist-scratch-buffers))


;;; Commands

(defvar projectile-enable-caching)
;;;###autoload
(defun +emacs/open-scratch-buffer (&optional arg project-p same-window-p)
  "Pop up a persistent scratch buffer.
If passed the prefix ARG, do not restore the last scratch buffer.
If PROJECT-P is non-nil, open a persistent scratch buffer associated with the
  current project."
  (interactive "P")
  (let (projectile-enable-caching)
    (funcall
     (if same-window-p
         #'switch-to-buffer
       #'pop-to-buffer)
     (+emacs-scratch-buffer
      arg
      (cond ((eq +emacs-scratch-initial-major-mode t)
             (unless (or buffer-read-only
                         (derived-mode-p 'special-mode)
                         (string-match-p "^ ?\\*" (buffer-name)))
               major-mode))
            ((null +emacs-scratch-initial-major-mode)
             nil)
            ((symbolp +emacs-scratch-initial-major-mode)
             +emacs-scratch-initial-major-mode))
      default-directory
      (when project-p
        (+emacs-project-name))))))

;;;###autoload
(defun +emacs/switch-to-scratch-buffer (&optional arg project-p)
  "Like `+emacs/open-scratch-buffer', but switches to it in the current window.
If passed the prefix ARG, do not restore the last scratch buffer."
  (interactive "P")
  (+emacs/open-scratch-buffer arg project-p 'same-window))

;;;###autoload
(defun +emacs/open-project-scratch-buffer (&optional arg same-window-p)
  "Opens the (persistent) project scratch buffer in a popup.
If passed the prefix ARG, do not restore the last scratch buffer."
  (interactive "P")
  (+emacs/open-scratch-buffer arg 'project same-window-p))

;;;###autoload
(defun +emacs/switch-to-project-scratch-buffer (&optional arg)
  "Like `+emacs/open-project-scratch-buffer', but switches to it in the current
window.
If passed the prefix ARG, do not restore the last scratch buffer."
  (interactive "P")
  (+emacs/open-project-scratch-buffer arg 'same-window))

;;;###autoload
(defun +emacs/revert-scratch-buffer ()
  "Revert scratch buffer to last persistent state."
  (interactive)
  (unless (string-match-p "^\\*+emacs:scratch" (buffer-name))
    (user-error "Not in a scratch buffer"))
  (when (+emacs--load-persistent-scratch-buffer +emacs-scratch-current-project)
    (message "Reloaded scratch buffer")))

;;;###autoload
(defun +emacs/delete-persistent-scratch-file (&optional arg)
  "Deletes a scratch buffer file in `+emacs-scratch-dir'.
If prefix ARG, delete all persistent scratches."
  (interactive)
  (if arg
      (progn
        (delete-directory +emacs-scratch-dir t)
        (message "Cleared %S" (abbreviate-file-name +emacs-scratch-dir)))
    (make-directory +emacs-scratch-dir t)
    (let ((file (read-file-name "Delete scratch file > " +emacs-scratch-dir "scratch")))
      (if (not (file-exists-p file))
          (message "%S does not exist" (abbreviate-file-name file))
        (delete-file file)
        (message "Successfully deleted %S" (abbreviate-file-name file))))))
