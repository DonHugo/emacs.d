;; lisp/autoload/store.el -*- lexical-binding: t; -*-

;; This little library abstracts the process of writing arbitrary elisp values to a 2-tiered file
;; store (in `+emacs-store-dir'/`+emacs-store-location').

(defvar +emacs-store-dir (concat +emacs-etc-dir "store/")
  "Directory to look for and store data accessed through this
API.")

(defvar +emacs-store-persist-alist ()
  "An alist of alists, containing lists of variables for the
store library to persist across Emacs sessions.")

(defvar +emacs-store-location "default"
  "The default location for cache files. This symbol is
translated into a file name under `pcache-directory' (by default
a subdirectory under `+emacs-store-dir'). One file may contain
multiple cache entries.")

(defvar +emacs--store-table (make-hash-table :test 'equal))

(defun +emacs--hook-save-persistent-store ()
  "Hook to persist storage when Emacs is killed."
  (let (locations)
    ;; Persist `+emacs-store-persist-alist'
    (dolist (alist (butlast +emacs-store-persist-alist 1))
      (cl-loop with location = (car alist)
               for var in (cdr alist)
               do (+emacs-store-put var (symbol-value var) nil location 'noflush)
               and do (cl-pushnew location locations :test #'equal)))
    ;; Clean up expired entries,
    (dolist (location (+emacs-files-in +emacs-store-dir :relative-to +emacs-store-dir))
      (maphash (lambda (key val)
                 (when (+emacs--store-expired-p key val)
                   (cl-pushnew location locations :test #'equal)
                   (+emacs--store-rem key location 'noflush)))
               (+emacs--store-init location)))
    (mapc #'+emacs--store-flush locations)))
(add-hook 'kill-emacs-hook #'+emacs--hook-save-persistent-store)


;;
;;; Library

;;;###autoload
(defun +emacs-store-persist (location variables)
  "Persist VARIABLES (list of symbols) in LOCATION (symbol).
This populates these variables with cached values, if one exists,
and saves them to file when Emacs quits. This cannot persist
buffer-local variables."
  (cl-check-type location string)
  (dolist (var variables)
    (when (+emacs-store-member-p var location)
      (set var (+emacs-store-get var location))))
  (setf (alist-get location +emacs-store-persist-alist)
        (append variables (alist-get location +emacs-store-persist-alist))))

;;;###autoload
(defun +emacs-store-desist (location &optional variables)
  "Unregisters VARIABLES (list of symbols) in LOCATION (symbol).
Variables to persist are recorded in
`+emacs-store-persist-alist'. Does not affect the actual
variables themselves or their values."
  (cl-check-type location string)
  (if variables
      (setf (alist-get location +emacs-store-persist-alist)
            (cl-set-difference (cdr (assq location +emacs-store-persist-alist))
                               variables))
    (delq! location +emacs-store-persist-alist 'assoc)))

(defun +emacs--store-init (&optional location)
  (cl-check-type location (or null string))
  (let ((location (or location +emacs-store-location)))
    (or (gethash location +emacs--store-table)
        (let* ((file-name-handler-alist nil)
               (location-path (expand-file-name location +emacs-store-dir)))
          (if (file-exists-p location-path)
              (puthash location
                       (with-temp-buffer
                         (set-buffer-multibyte nil)
                         (setq buffer-file-coding-system 'binary)
                         (insert-file-contents-literally location-path)
                         (read (current-buffer)))
                       +emacs--store-table)
            (puthash location (make-hash-table :test 'equal)
                     +emacs--store-table))))))

(defun +emacs--store-expired-p (key data)
  (let ((ttl (car data)))
    (cond ((functionp ttl)
           (not (funcall ttl key data)))
          ((consp ttl)
           (time-less-p ttl (current-time))))))

(defun +emacs--store-flush (location)
  "Write `+emacs--store-table' to `+emacs-store-dir'."
  (let ((file-name-handler-alist nil)
        (coding-system-for-write 'binary)
        (write-region-annotate-functions nil)
        (write-region-post-annotation-function nil))
    (let* ((location (or location +emacs-store-location))
           (data (+emacs--store-init location)))
      (make-directory +emacs-store-dir 'parents)
      (with-temp-file (expand-file-name location +emacs-store-dir)
        (prin1 data (current-buffer)))
      data)))


;;;###autoload
(defun +emacs-store-get (key &optional location default-value noflush)
  "Retrieve KEY from LOCATION (defaults to `+emacs-store-location').
If it doesn't exist or has expired, DEFAULT_VALUE is returned."
  (let ((data (gethash key (+emacs--store-init location) default-value)))
    (if (not (or (eq data default-value)
                 (+emacs--store-expired-p key data)))
        (cdr data)
      (+emacs-store-rem key location noflush)
      default-value)))

;;;###autoload
(defun +emacs-store-put (key value &optional ttl location noflush)
  "Set KEY to VALUE in the store at LOCATION.
KEY can be any lisp object that is comparable with `equal'. TTL
is the duration (in seconds) after which this cache entry
expires; if nil, no cache expiration. LOCATION is the super-key
to store this cache item under. It defaults to
`+emacs-store-location'."
  (cl-check-type ttl (or null integer function))
  (puthash key (cons (if (integerp ttl)
                         (time-add (current-time) ttl)
                       ttl)
                     value)
           (+emacs--store-init location))
  (unless noflush
    (+emacs--store-flush location)))

;;;###autoload
(defun +emacs-store-rem (key &optional location noflush)
  "Clear a cache LOCATION (defaults to `+emacs-store-location')."
  (remhash key (+emacs--store-init location))
  (unless noflush
    (+emacs--store-flush location)))

;;;###autoload
(defun +emacs-store-member-p (key &optional location)
  "Return t if KEY in LOCATION exists.
LOCATION defaults to `+emacs-store-location'."
  (let ((nil-value (format "--nilvalue%s--" (current-time))))
    (not (equal (+emacs-store-get key location nil-value)
                nil-value))))

;;;###autoload
(defun +emacs-store-clear (&optional location)
  "Clear the store at LOCATION (defaults to
`+emacs-store-location')."
  (let* ((location (or location +emacs-store-location))
         (path (expand-file-name location +emacs-store-dir)))
    (remhash location +emacs--store-table)
    (when (file-exists-p path)
      (delete-file path)
      t)))
