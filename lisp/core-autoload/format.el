;; lisp/autoloads/format.el -*- lexical-binding: t; -*-

(defvar +emacs-format-ansi-alist
  '(;; fx
    (bold       1 :weight bold)
    (dark       2)
    (italic     3 :slant italic)
    (underscore 4 :underline t)
    (blink      5)
    (rapid      6)
    (contrary   7)
    (concealed  8)
    (strike     9 :strike-through t)
    ;; fg
    (black      30 term-color-black)
    (red        31 term-color-red)
    (green      32 term-color-green)
    (yellow     33 term-color-yellow)
    (blue       34 term-color-blue)
    (magenta    35 term-color-magenta)
    (cyan       36 term-color-cyan)
    (white      37 term-color-white)
    ;; bg
    (on-black   40 term-color-black)
    (on-red     41 term-color-red)
    (on-green   42 term-color-green)
    (on-yellow  43 term-color-yellow)
    (on-blue    44 term-color-blue)
    (on-magenta 45 term-color-magenta)
    (on-cyan    46 term-color-cyan)
    (on-white   47 term-color-white))
  "An alist of fg/bg/fx names mapped to ansi codes and term-color-* variables.
This serves as the cipher for converting (COLOR ...) function
calls in `print!' and `format!' into colored output, where COLOR
is any car of this list.")

(defvar +emacs-format-class-alist
  `((color . +emacs--format-color)
    (class . +emacs--format-class)
    (indent . +emacs--format-indent)
    (autofill . +emacs--format-autofill)

    (success . (lambda (str &rest args)
                 (apply #'+emacs--format-color 'green (format "✓ %s" str) args)))
    (warn    . (lambda (str &rest args)
                 (apply #'+emacs--format-color 'yellow (format "! %s" str) args)))
    (error   . (lambda (str &rest args)
                 (apply #'+emacs--format-color 'red (format "x %s" str) args)))
    (info    . (lambda (str &rest args)
                 (concat "- " (if args (apply #'format str args) str))))
    (start    . (lambda (str &rest args)
                  (concat "> " (if args (apply #'format str args) str))))
    (debug   . (lambda (str &rest args)
                 (if +emacs-debug-p
                     (if args
                         (apply #'format str args)
                       (format "%s" str))
                   "")))
    (path    . abbreviate-file-name)
    (symbol . symbol-name)
    (relpath . (lambda (str &optional dir)
                 (if (or (not str)
                         (not (stringp str))
                         (string-empty-p str))
                     str
                   (let ((dir (or dir (file-truename default-directory)))
                         (str (file-truename str)))
                     (if (file-in-directory-p str dir)
                         (file-relative-name str dir)
                       (abbreviate-file-name str))))))
    (filename . file-name-nondirectory)
    (dirname . (lambda (path)
                 (unless (file-directory-p path)
                   (setq path (file-name-directory path)))
                 (directory-file-name path))))
  "An alist of text classes that map to transformation functions.
Any of these classes can be called like functions from within
`format!' and `print!' calls, which will transform their input.")

(defvar +emacs-format-indent 0
  "Level to rigidly indent text returned by `format!' and `print!'.")

(defvar +emacs-format-indent-increment 2
  "Steps in which to increment `+emacs-format-indent' for
consecutive levels.")

(defvar +emacs-format-backend
  (if noninteractive 'ansi 'text-properties)
  "Determines whether to print colors with ANSI codes or with text properties.
Accepts 'ansi and 'text-properties. nil means don't render
colors.")

;;; Library

;;;###autoload
(defun +emacs--format (output)
  (if (string-empty-p (string-trim output))
      ""
    (concat (make-string +emacs-format-indent 32)
            (replace-regexp-in-string
             "\n" (concat "\n" (make-string +emacs-format-indent 32))
             output t t))))

;;;###autoload
(defun +emacs--format-print (output)
  (unless (string-empty-p output)
    (if (not noninteractive)
        (message "%s" output)
      (princ output)
      (terpri)) ; newline
    t))

;;;###autoload
(defun +emacs--format-indent (width text &optional prefix)
  "Indent TEXT by WIDTH spaces. If ARGS, format TEXT with them."
  (with-temp-buffer
    (setq text (format "%s" text))
    (insert text)
    (indent-rigidly (point-min) (point-max) width)
    (when (stringp prefix)
      (when (> width 2)
        (goto-char (point-min))
        (beginning-of-line-text)
        (delete-char (- (length prefix)))
        (insert prefix)))
    (buffer-string)))

;;;###autoload
(defun +emacs--format-autofill (&rest msgs)
  "Ensure MSG is split into lines no longer than `fill-column'."
  (with-temp-buffer
    (let ((fill-column 76))
      (dolist (line msgs)
        (when line
          (insert (format "%s" line))))
      (fill-region (point-min) (point-max))
      (buffer-string))))

;;;###autoload
(defun +emacs--format-color (style format &rest args)
  "Apply STYLE to formatted MESSAGE with ARGS.
STYLE is a symbol that correlates to `+emacs-format-ansi-alist'.
In a noninteractive session, this wraps the result in ansi color
codes. Otherwise, it maps colors to a term-color-* face."
  (let* ((code (cadr (assq style +emacs-format-ansi-alist)))
         (format (format "%s" format))
         (message (if args (apply #'format format args) format)))
    (unless code
      (error "%S is an invalid color" style))
    (pcase +emacs-format-backend
      (`ansi
       (format "\e[%dm%s\e[%dm" code message 0))
      (`text-properties
       (require 'term)  ; piggyback on term's color faces
       (propertize
        message
        'face
        (append (get-text-property 0 'face format)
                (cond ((>= code 40)
                       `(:background ,(caddr (assq style +emacs-format-ansi-alist))))
                      ((>= code 30)
                       `(:foreground ,(face-foreground (caddr (assq style +emacs-format-ansi-alist)))))
                      ((cddr (assq style +emacs-format-ansi-alist)))))))
      (_ message))))

;;;###autoload
(defun +emacs--format-class (class format &rest args)
  "Apply CLASS to formatted format with ARGS.
CLASS is derived from `+emacs-format-class-alist', and can
contain any arbitrary, transformative logic."
  (let (fn)
    (cond ((setq fn (cdr (assq class +emacs-format-class-alist)))
           (if (functionp fn)
               (apply fn format args)
             (error "%s does not have a function" class)))
          (args (apply #'format format args))
          (format))))

;;;###autoload
(defun +emacs--format-apply (forms &optional sub)
  "Replace color-name functions with calls to
`+emacs--format-color'."
  (cond ((null forms) nil)
        ((listp forms)
         (append (cond ((not (symbolp (car forms)))
                        (list (+emacs--format-apply (car forms))))
                       (sub
                        (list (car forms)))
                       ((assq (car forms) +emacs-format-ansi-alist)
                        `(+emacs--format-color ',(car forms)))
                       ((assq (car forms) +emacs-format-class-alist)
                        `(+emacs--format-class ',(car forms)))
                       ((list (car forms))))
                 (+emacs--format-apply (cdr forms) t)
                 nil))
        (forms)))

;;;###autoload
(defmacro format! (message &rest args)
  "An alternative to `format' that understands (color ...) and
converts them into faces or ANSI codes depending on the type of
sesssion we're in."
  `(+emacs--format (format ,@(+emacs--format-apply `(,message ,@args)))))

;;;###autoload
(defmacro print-group! (&rest body)
  "Indents any `print!' or `format!' output within BODY."
  (declare (debug (body)))
  `(let ((+emacs-format-indent (+ +emacs-format-indent-increment +emacs-format-indent)))
     ,@body))

;;;###autoload
(defmacro print! (message &rest args)
  "Uses `message' in interactive sessions and `princ'
otherwise (prints to standard out). Can be colored using (color
...) blocks:
  (print! \"Hello %s\" (bold (blue \"How are you?\")))
  (print! \"Hello %s\" (red \"World\"))
  (print! (green \"Great %s!\") \"success\")
Uses faces in interactive sessions and ANSI codes otherwise."
  `(+emacs--format-print (format! ,message ,@args)))

;;;###autoload
(defmacro insert! (message &rest args)
  "Like `insert'; the last argument must be format arguments for MESSAGE.
\(fn MESSAGE... ARGS)"
  `(insert (format! (concat ,message ,@(butlast args))
                    ,@(car (last args)))))

;;;###autoload
(defmacro error! (message &rest args)
  "Like `error', but with the power of `format!'."
  `(error (format! ,message ,@args)))

;;;###autoload
(defmacro user-error! (message &rest args)
  "Like `user-error', but with the power of `format!'."
  `(user-error (format! ,message ,@args)))
