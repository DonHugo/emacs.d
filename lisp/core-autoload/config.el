;; lisp/autoloads/config.el -*- lexical-binding: t; -*-

;;;###autoload
(defvar +emacs-reload-hook nil
  "A list of hooks to run when `+emacs/reload' is called.")

;;;###autoload
(defvar +emacs-reloading-p nil
  "TODO")

;;;###autoload
(defun +emacs/open-local-config ()
  "Browse your `+emacs-private-dir'."
  (interactive)
  (unless (file-directory-p +emacs-private-dir)
    (make-directory +emacs-private-dir t))
  (+emacs-project-browse +emacs-private-dir))

;;;###autoload
(defun +emacs/find-file-in-local-config ()
  "Search for a file in `+emacs-private-dir'."
  (interactive)
  (+emacs-project-find-file +emacs-private-dir))

;;;###autoload
(defun +emacs/goto-local-init-file ()
  "Open your local init.el file and jumps to the `modules!'
block."
  (interactive)
  (find-file (expand-file-name "init.el" +emacs-private-dir))
  (goto-char
   (or (save-excursion
         (goto-char (point-min))
         (search-forward "(modules!" nil t))
       (point))))

;;;###autoload
(defun +emacs/goto-local-config-file ()
  "Open your private config.el file."
  (interactive)
  (find-file (expand-file-name "config.el" +emacs-private-dir)))

;;;###autoload
(defun +emacs/goto-local-packages-file ()
  "Open your private packages.el file."
  (interactive)
  (find-file (expand-file-name "packages.el" +emacs-private-dir)))

;;;###autoload
(defun +emacs/open-in-magit ()
  "Open magit in .emacs.d."
  (interactive)
  (magit-status +emacs-dir))

;;;###autoload
(defun +emacs/search-in-emacsd (&optional arg)
  "Conduct a text search in `user-emacs-directory'. If
ARG (universal argument), include all files."
  (interactive "P")
  (let ((dir +emacs-dir))
    (cond ((featurep! :completion ivy)
           (+ivy/project-search arg nil dir))
          ((featurep! :completion helm)
           (+helm/project-search arg nil dir))
          ((rgrep (regexp-quote symbol) nil dir)))))
