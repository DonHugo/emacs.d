;; lisp/cli/byte-compile.el -*- lexical-binding: t; no-byte-compile: t; -*-

(cl-defun +emacs-cli-byte-compile ()
  "Byte compiles your emacs configuration."
  (let ((default-directory +emacs-dir)
        (byte-compile-verbose +emacs-debug-p)
        (byte-compile-warnings '(not free-vars unresolved noruntime lexical make-local))

        ;; In case it is changed during compile-time
        (auto-mode-alist auto-mode-alist)
        (noninteractive t)

        (targets (list (+emacs-glob +emacs-core-dir "init-core.el"))))

    ;; But first we must be sure that configs have been fully loaded. Which
    ;; usually aren't so in an noninteractive session.
    (let ((+emacs-interactive-p 'byte-compile))
      (+emacs-initialize)
      (+emacs-initialize-packages)
      (load (concat user-emacs-directory "lisp/init-core-interactive")
            nil 'nomessage)
      (load (concat user-emacs-directory "lisp/init-core-modules")
            nil 'nomessage)
      (+emacs-initialize-modules))

    (delq nil targets)
    ;; Assemble el files we want to compile
    (appendq! targets
              (+emacs-files-in (append (list +emacs-core-dir) +emacs-modules-dirs)
                               :match "\\.el$"
                               :filter #'+emacs--byte-compile-ignore-file-p
                               :depth 0))

    (unless targets
      (print!
       (if targets
           (warn "Couldn't find any valid targets")
         (info "No targets to compile")))
      (cl-return nil))

    (print!
     (info "Byte-compiling your config (may take a while)..."))
    (print-group!
     (require 'use-package)
     (condition-case e
         (let ((total-ok   0)
               (total-fail 0)
               (total-noop 0)
               (use-package-defaults use-package-defaults)
               (use-package-expand-minimally t)
               kill-emacs-hook kill-buffer-query-functions)
           ;; Prevent packages from being loaded at compile time if they
           ;; don't meet their own predicates.
           (push (list :no-require t
                   (lambda (_name args)
                     (or (when-let (pred (or (plist-get args :if)
                                             (plist-get args :when)))
                           (not (eval pred t)))
                         (when-let (pred (plist-get args :unless))
                           (eval pred t)))))
                 use-package-defaults)

           (+emacs-cli-clean-byte-compiled-files)

           (dolist (target (delete-dups (delq nil targets)))
             (cl-incf
              (if (let ((elc-file (byte-compile-dest-file target)))
                    (and (file-exists-p elc-file)
                         (file-newer-than-file-p target elc-file)))
                  total-noop
                (pcase (byte-compile-file target)
                  (`no-byte-compile
                   (print! (info "Ignored %s") (relpath target))
                   total-noop)
                  (`nil
                   (print! (error "Failed to compile %s") (relpath target))
                   total-fail)
                  (_
                   (print! (success "Compiled %s") (relpath target))
                   (load target t t)
                   total-ok)))))
           (print! (class (if (= total-fail 0) 'success 'error)
                          "%s %d/%d file(s) (%d ignored)")
                   "Compiled"
                   total-ok (- (length targets) total-noop)
                   total-noop)
           t)
       ((debug error)
        (print! (error "\nThere were breaking errors.\n\n%s")
                "Reverting changes...")
        (signal '+emacs-error (list 'byte-compile e)))))))

(defun +emacs-cli-clean-byte-compiled-files ()
  "Delete all the compiled elc files in your Emacs configuration and private
module. This does not include your byte-compiled, third party packages.'"
  (print! (start "Cleaning .elc files"))
  (print-group!
   (cl-loop with default-directory = +emacs-dir
            with success = nil
            for path
            in (append (+emacs-glob +emacs-dir "*.elc")
                       (+emacs-files-in +emacs-private-dir :match "\\.elc$" :depth 1)
                       (+emacs-files-in +emacs-core-dir :match "\\.elc$")
                       (+emacs-files-in +emacs-modules-dirs :match "\\.elc$" :depth 4))
            if (file-exists-p path)
            do (delete-file path)
            and do (print! (success "Deleted %s") (relpath path))
            and do (setq success t)
            finally do
            (print! (if success
                        (success "All elc files deleted")
                      (info "No elc files to clean"))))))
