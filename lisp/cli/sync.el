;; lisp/cli/sync.el -*- lexical-binding: t; no-byte-compile: t; -*-

;; Two ways to sync the emacs config:
;;
;; - refreshing it, thus taking care that all packages are built and autoloads
;;   are generated
;;
;; - syncing it, thus taking care that all packages are on the same version as
;;   specified in the lockfiles and then refreshing plus cleaning up the
;;   repositories

(defun +emacs-cli-refresh (&optional no-envvar-p)
  "Refresh Emacs config."
  (run-hooks '+emacs-refresh-pre-hook)
  (print! (start "Refreshing your Emacs config..."))
  (unwind-protect
      (print-group!
       (mapc #'+emacs-autoloads--delete-file
             (list +emacs-autoloads-file))
       (when (and (not no-envvar-p)
                  (file-exists-p +emacs-env-file))
         (+emacs-cli-reload-env-file 'force))
       (+emacs-cli-clean-byte-compiled-files)
       (+emacs-read-modules)
       (+emacs-cli-packages-install)
       (+emacs-cli-packages-build)
       (+emacs-packages-purge t t t)
       (run-hooks '+emacs-refresh-post-hook)
       (when (+emacs-autoloads-reload)
         (print! (info "Restart Emacs for changes to take effect")))
       t)))

(defun +emacs-cli-sync (&optional no-envvar-p)
  "Refresh Emacs config."
  (run-hooks '+emacs-sync-pre-hook)
  (print! (start "Synchronizing your Emacs config..."))
  (unwind-protect
      (print-group!
       (mapc #'+emacs-autoloads--delete-file
             (list +emacs-autoloads-file))
       (when (and (not no-envvar-p)
                  (file-exists-p +emacs-env-file))
         (+emacs-cli-reload-env-file 'force))
       (+emacs-cli-clean-byte-compiled-files)
       (+emacs-read-modules)
       (+emacs-cli-packages-install)
       (+emacs-cli-packages-build)
       (+emacs-cli-packages-update)
       (+emacs-packages-purge t t t)
       (run-hooks '+emacs-sync-post-hook)
       (when (+emacs-autoloads-reload)
         (print! (info "Restart Emacs for changes to take effect")))
       t)))
