;; lisp/cli/deploy.el -*- lexical-binding: t; no-byte-compile: t; -*-

(defun +emacs-cli-deploy ()
    (print! (green "Deploying Emacs config!\n"))
    ;; Read modules
    (+emacs-read-modules)

    ;; Env file
    (if (file-exists-p +emacs-env-file)
        (print! (info "Envvar file already exists, skipping"))
      (when (or +emacs-auto-accept
                (y-or-n-p "Generate an envvar file?"))
        (+emacs-cli-reload-env-file 'force-p)))

    ;; Initialize packages
    (print! "Installing packges")
    (+emacs-cli-packages-install)

    (print! "Regenerating autoloads files")
    (+emacs-autoloads-reload)

    ;; Install `all-the-icons' fonts
    (when (or +emacs-auto-accept
              (y-or-n-p "Download and install all-the-icon's fonts?"))
      (require 'all-the-icons)
      (let ((window-system (cond (IS-MAC 'ns)
                                 (IS-LINUX 'x))))
        (all-the-icons-install-fonts 'yes)))

    (when (file-exists-p "~/.emacs")
      (print! (warn "A ~/.emacs file was detected. This should be deleted!")))

        (print! (success "Finished! Emacs is ready to go!\n")))
