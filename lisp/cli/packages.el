;; lisp/cli/packages.el -*- lexical-binding: t; no-byte-compile: t; -*-

;;
;;; Helpers

(defun +emacs--same-commit-p (abbrev-ref ref)
  (and (stringp abbrev-ref)
       (stringp ref)
       (string-match-p (concat "^" (regexp-quote abbrev-ref))
                       ref)))

(defun +emacs--abbrev-commit (commit &optional full)
  (if full commit (substring commit 0 7)))

(defun +emacs--commit-log-between (start-ref end-ref)
  (straight--process-with-result
      (straight--process-run
       "git" "log" "--oneline" "--no-merges"
       "-n" "26" end-ref start-ref)
    (if success
        (let* ((output (string-trim-right (or stdout "")))
               (lines (split-string output "\n")))
          (if (> (length lines) 25)
              (concat (string-join (butlast lines 1) "\n") "\n[...]")
            output))
      (format "ERROR: Couldn't collect commit list because: %s" stderr))))

(defmacro +emacs--straight-with (form &rest body)
  (declare (indent 1))
  `(let-alist
       (let* ((buffer (straight--process-buffer))
              (start (with-current-buffer buffer (point-max)))
              (retval ,form)
              (output (with-current-buffer buffer (buffer-substring start (point-max)))))
         (save-match-data
           (list (cons 'it retval)
                 (cons 'stdout (substring-no-properties output))
                 (cons 'success (if (string-match "\n+\\[Return code: \\([0-9-]+\\)\\]\n+" output)
                                    (string-to-number (match-string 1 output))))
                 (cons 'output (string-trim output
                                            "^\\(\\$ [^\n]+\n\\)*\n+"
                                            "\n+\\[Return code: [0-9-]+\\]\n+")))))
     ,@body))

(defmacro +emacs--with-package-recipes (recipes binds &rest body)
  "TODO"
  (declare (indent 2) (debug (form sexp body)))
  (let ((recipe-var  (make-symbol "recipe"))
        (recipes-var (make-symbol "recipes")))
    `(let* ((,recipes-var ,recipes)
            (built ())
            (straight-use-package-pre-build-functions
             (cons (lambda (pkg) (cl-pushnew pkg built :test #'equal))
                   straight-use-package-pre-build-functions)))
       (dolist (,recipe-var ,recipes-var)
         (cl-block nil
           (straight--with-plist (append (list :recipe ,recipe-var) ,recipe-var)
               ,(+emacs-enlist binds)
             ,@body)))
       (nreverse built))))

(defvar +emacs--cli-updated-recipes nil)
(defun +emacs--cli-recipes-update ()
  "Updates straight and recipe repos."
  (unless +emacs--cli-updated-recipes
    (straight--make-build-cache-available)
    (print! (start "Updating recipe repos..."))
    (print-group!
     (+emacs--with-package-recipes
      (delq
       nil (mapcar (+emacs-rpartial #'gethash straight--repo-cache)
                   (mapcar #'symbol-name straight-recipe-repositories)))
      (recipe package type local-repo)
      (let ((esc (unless +emacs-debug-p "\033[1A"))
            (ref (straight-vc-get-commit type local-repo))
            newref output)
        (print! (start "\033[KUpdating recipes for %s...%s") package esc)
        (+emacs--straight-with (straight-vc-fetch-from-remote recipe)
          (when .it
            (setq output .output)
            (straight-merge-package package)
            (unless (equal ref (setq newref (straight-vc-get-commit type local-repo)))
              (print! (success "\033[K%s updated (%s -> %s)")
                      package
                      (+emacs--abbrev-commit ref)
                      (+emacs--abbrev-commit newref))
              (unless (string-empty-p output)
                (print-group! (print! (info "%s" output))))))))))
    (setq straight--recipe-lookup-cache (make-hash-table :test #'eq)
          +emacs--cli-updated-recipes t)))

(defun +emacs-package-recipe-list ()
  "Return straight recipes for non-builtin packages with a local-repo."
  (let (recipes)
    (dolist (recipe (hash-table-values straight--recipe-cache))
      (cl-destructuring-bind (&key local-repo type &allow-other-keys)
          recipe
        (unless (or (null local-repo)
                    (eq type 'built-in))
          (push recipe recipes))))
    (nreverse recipes)))

(defun +emacs--barf-if-incomplete-packages ()
  (let ((straight-safe-mode t))
    (condition-case _ (straight-check-all)
      (error (user-error "Package state is incomplete. Run 'make clean' in cmdline.")))))

(defun +emacs--elc-file-outdated-p (file)
  "Check whether the corresponding .elc for `file' is outdated."
  (let ((elc-file (byte-compile-dest-file file)))
    ;; NOTE Ignore missing elc files, they could be missing due to
    ;; `no-byte-compile'. Rebuilding unnecessarily is expensive.
    (when (and (file-exists-p elc-file)
               (file-newer-than-file-p file elc-file))
      (+emacs-log "%s is newer than %s" file elc-file)
      t)))


;;
;;; Build

(defun +emacs-cli-packages-install ()
  "Installs missing packages.
This function will install any primary package (i.e. a package
with a `package!' declaration) or dependency thereof that hasn't
already been."
  (require 'init-packages)
  (+emacs-initialize-packages)
  (print! (start "Installing packages..."))
  (let ((pinned (straight--lockfile-read-all)))
    (print-group!
     (if-let (built
              (+emacs--with-package-recipes (+emacs-package-recipe-list)
                  (recipe package type local-repo)
                (unless (file-directory-p (straight--repos-dir local-repo))
                  (+emacs--cli-recipes-update))
                (condition-case-unless-debug e
                    (let ((straight-use-package-pre-build-functions
                           (cons (lambda (pkg &rest _)
                                   (when-let (commit (cdr (assoc pkg pinned)))
                                     (print! (info "Checked out %s: %s") pkg commit)))
                                 straight-use-package-pre-build-functions)))
                      (straight-use-package (intern package))
                      ;; HACK Line encoding issues can plague repos with dirty
                      ;;      worktree prompts when updating packages or "Local
                      ;;      variables entry is missing the suffix" errors when
                      ;;      installing them (see hlissner/+emacs-emacs#2637), so
                      ;;      have git handle conversion by force.
                      (when (and IS-WINDOWS (stringp local-repo))
                        (let ((default-directory (straight--repos-dir local-repo)))
                          (when (file-in-directory-p default-directory straight-base-dir)
                            (straight--process-run "git" "config" "core.autocrlf" "true")))))
                  (error
                   (signal '+emacs-package-error (list package e))))))
         (print! (success "\033[KInstalled %d packages") (length built))
       (print! (info "No packages need to be installed"))
       nil))))


;;
;;; Build

(defun +emacs-cli-packages-build (&optional force-p)
  "(Re)build all packages."
  (require 'init-packages)
  (+emacs-initialize-packages)
  (print! (start "(Re)building %spackages...") (if force-p "all " ""))
  (print-group!
   (let ((straight-check-for-modifications
          (when (file-directory-p (straight--modified-dir))
            '(find-when-checking)))
         (straight--allow-find
          (and straight-check-for-modifications
               (executable-find straight-find-executable)
               t))
         (straight--packages-not-to-rebuild
          (or straight--packages-not-to-rebuild (make-hash-table :test #'equal)))
         (straight--packages-to-rebuild
          (or (if force-p :all straight--packages-to-rebuild)
              (make-hash-table :test #'equal)))
         (recipes (+emacs-package-recipe-list)))
     (unless force-p
       (straight--make-build-cache-available))
     (if-let (built
              (+emacs--with-package-recipes recipes (package local-repo recipe)
                (unless force-p
                  ;; Ensure packages with outdated files/bytecode are rebuilt
                  (let* ((build-dir (straight--build-dir package))
                         (repo-dir  (straight--repos-dir local-repo))
                         (build (if (plist-member recipe :build)
                                    (plist-get recipe :build)
                                  t))
                         (want-byte-compile
                          (or (eq build t)
                              (memq 'compile build))))
                    (when (eq (car-safe build) :not)
                      (setq want-byte-compile (not want-byte-compile)))
                    (and want-byte-compile
                         (or (file-newer-than-file-p repo-dir build-dir)
                             (file-exists-p (straight--modified-dir (or local-repo package)))
                             (cl-loop with outdated = nil
                                      for file in (+emacs-files-in build-dir :match "\\.el$" :full t)
                                      if (if want-byte-compile   (+emacs--elc-file-outdated-p file))
                                      do (setq outdated t)
                                      finally return outdated))
                         (puthash package t straight--packages-to-rebuild))))
                (straight-use-package (intern package))))
         (progn
           (delete-directory (straight--modified-dir) 'recursive)
           (print! (success "\033[KRebuilt %d package(s)") (length built)))
       (print! (info "No packages need rebuilding"))
       nil))))


;;
;;; Update

(defun +emacs-cli-packages-update ()
  "Updates packages."
  (+emacs-initialize-packages)
  (+emacs--barf-if-incomplete-packages)
  (let* ((repo-dir (straight--repos-dir))
         (pinned (straight--lockfile-read-all))
         (recipes (+emacs-package-recipe-list))
         (packages-to-rebuild (make-hash-table :test 'equal))
         (repos-to-rebuild (make-hash-table :test 'equal))
         (total (length recipes))
         (esc (unless +emacs-debug-p "\033[1A"))
         (i 0)
         errors)
    (when recipes
      (+emacs--cli-recipes-update))
    (print! (start "Updating packages (this may take a while)..."))
    (+emacs--with-package-recipes recipes (recipe package type local-repo)
      (cl-incf i)
      (print-group!
       (unless (straight--repository-is-available-p recipe)
         (print! (error "(%d/%d) Couldn't find local repo for %s") i total package)
         (cl-return))
       (when (gethash local-repo repos-to-rebuild)
         (puthash package t packages-to-rebuild)
         (print! (success "(%d/%d) %s was updated indirectly (with %s)") i total package local-repo)
         (cl-return))
       (let ((default-directory (straight--repos-dir local-repo)))
         (unless (file-in-directory-p default-directory repo-dir)
           (print! (warn "(%d/%d) Skipping %s because it is local") i total package)
           (cl-return))
         (when (eq type 'git)
           (unless (file-exists-p ".git")
             (error "%S is not a valid repository" package)))
         (condition-case-unless-debug e
             (let ((ref (straight-vc-get-commit type local-repo))
                   (target-ref
                    (cdr (or (assoc local-repo pinned)
                             (assoc package pinned))))
                   output)
               (or (cond
                    ((not (stringp target-ref))
                     (print! (start "\033[K(%d/%d) Fetching %s...%s") i total package esc)
                     (+emacs--straight-with (straight-vc-fetch-from-remote recipe)
                       (when .it
                         (setq output .output)
                         (straight-merge-package package)
                         (setq target-ref (straight-vc-get-commit type local-repo))
                         (or (not (+emacs--same-commit-p target-ref ref))
                             (cl-return)))))

                    ((+emacs--same-commit-p target-ref ref)
                     (print! (info "\033[K(%d/%d) %s is up-to-date...%s") i total package esc)
                     (cl-return))

                    ((if (straight-vc-commit-present-p recipe target-ref)
                         (print! (start "\033[K(%d/%d) Checking out %s (%s)...%s")
                                 i total package (+emacs--abbrev-commit target-ref) esc)
                       (print! (start "\033[K(%d/%d) Fetching %s...%s") i total package esc)
                       (and (straight-vc-fetch-from-remote recipe)
                            (straight-vc-commit-present-p recipe target-ref)))
                     (straight-vc-check-out-commit recipe target-ref)
                     (or (not (eq type 'git))
                         (setq output (+emacs--commit-log-between ref target-ref)))
                     (+emacs--same-commit-p target-ref (straight-vc-get-commit type local-repo)))

                    ((print! (start "\033[K(%d/%d) Re-cloning %s...") i total local-repo esc)
                     (let ((repo (straight--repos-dir local-repo))
                           (straight-vc-git-default-clone-depth 'full))
                       (delete-directory repo 'recursive)
                       (print-group!
                        (straight-use-package (intern package) nil 'no-build))
                       (prog1 (file-directory-p repo)
                         (or (not (eq type 'git))
                             (setq output (+emacs--commit-log-between ref target-ref)))))))
                   (progn
                     (print! (warn "\033[K(%d/%d) Failed to fetch %s")
                             i total local-repo)
                     (unless (string-empty-p output)
                       (print-group! (print! (info "%s" output))))
                     (cl-return)))
               (puthash local-repo t repos-to-rebuild)
               (puthash package t packages-to-rebuild)
               (print! (success "\033[K(%d/%d) %s updated (%s -> %s)")
                       i total local-repo
                       (+emacs--abbrev-commit ref)
                       (+emacs--abbrev-commit target-ref))
               (unless (string-empty-p output)
                 (print-group! (print! "%s" (indent 2 output)))))
           (user-error
            (signal 'user-error (error-message-string e)))
           (error
            (signal '+emacs-package-error (list package e)))))))
    (print-group!
     (princ "\033[K")
     (if (hash-table-empty-p packages-to-rebuild)
         (ignore (print! (success "All %d packages are up-to-date") total))
       (straight--transaction-finalize)
       (let ((default-directory (straight--build-dir)))
         (mapc (+emacs-rpartial #'delete-directory 'recursive)
               (hash-table-keys packages-to-rebuild)))
       (print! (success "Updated %d package(s)")
               (hash-table-count packages-to-rebuild))
       (+emacs-cli-packages-build)
       t))))


;;
;;; Purge

(defun +emacs--packages-purge-build (build)
  (let ((build-dir (straight--build-dir build)))
    (delete-directory build-dir 'recursive)
    (if (file-directory-p build-dir)
        (ignore (print! (error "Failed to purg build/%s" build)))
      (print! (success "Purged build/%s" build))
      t)))

(defun +emacs--packages-purge-builds (builds)
  (if (not builds)
      (prog1 0
        (print! (info "No builds to purge")))
    (print! (start "Purging straight builds..." (length builds)))
    (print-group!
     (length
      (delq nil (mapcar #'+emacs--packages-purge-build builds))))))

(cl-defun +emacs--packages-regraft-repo (repo)
  (let ((default-directory (straight--repos-dir repo)))
    (unless (file-directory-p ".git")
      (print! (warn "\033[Krepos/%s is not a git repo, skipping" repo))
      (cl-return))
    (unless (file-in-directory-p default-directory straight-base-dir)
      (print! (warn "\033[KSkipping repos/%s because it is local" repo))
      (cl-return))
    (let ((before-size (+emacs-directory-size default-directory)))
      (+emacs-call-process "git" "reset" "--hard")
      (+emacs-call-process "git" "clean" "-ffd")
      (if (not (zerop (car (+emacs-call-process "git" "replace" "--graft" "HEAD"))))
          (print! (info "\033[Krepos/%s is already compact\033[1A" repo))
        (+emacs-call-process "git" "reflog" "expire" "--expire=all" "--all")
        (+emacs-call-process "git" "gc" "--prune=now")
        (let ((after-size (+emacs-directory-size default-directory)))
          (if (equal after-size before-size)
              (print! (success "\033[Krepos/%s cannot be compacted further" repo))
            (print! (success "\033[KRegrafted repos/%s (from %0.1fKB to %0.1fKB)")
                    repo before-size after-size)))))
    t))

(defun +emacs--packages-regraft-repos (repos)
  (if (not repos)
      (prog1 0
        (print! (info "No repos to regraft")))
    (print! (start "Regrafting %d repos..." (length repos)))
    (let ((before-size (+emacs-directory-size (straight--repos-dir))))
      (print-group!
       (prog1 (delq nil (mapcar #'+emacs--packages-regraft-repo repos))
         (princ "\033[K")
         (let ((after-size (+emacs-directory-size (straight--repos-dir))))
           (print! (success "Finished regrafting. Size before: %0.1fKB and after: %0.1fKB (%0.1fKB)")
                   before-size after-size
                   (- after-size before-size))))))))

(defun +emacs--packages-purge-repo (repo)
  (let ((repo-dir (straight--repos-dir repo)))
    (when (file-directory-p repo-dir)
      (delete-directory repo-dir 'recursive)
      (delete-file (straight--modified-file repo))
      (if (file-directory-p repo-dir)
          (ignore (print! (error "Failed to purge repos/%s" repo)))
        (print! (success "Purged repos/%s" repo))
        t))))

(defun +emacs--packages-purge-repos (repos)
  (if (not repos)
      (prog1 0
        (print! (info "No repos to purge")))
    (print! (start "Purging straight repositories..."))
    (print-group!
     (length
      (delq nil (mapcar #'+emacs--packages-purge-repo repos))))))

(defun +emacs--packages-purge-elpa ()
  (require 'init-packages)
  (let ((dirs (+emacs-files-in package-user-dir :type t :depth 0)))
    (if (not dirs)
        (prog1 0
          (print! (info "No ELPA packages to purge")))
      (print! (start "Purging ELPA packages..."))
      (dolist (path dirs (length dirs))
        (condition-case e
            (print-group!
             (if (file-directory-p path)
                 (delete-directory path 'recursive)
               (delete-file path))
             (print! (success "Deleted %s") (filename path)))
          (error
           (print! (error "Failed to delete %s because: %s")
                   (filename path)
                   e)))))))

(defun +emacs-packages-purge (&optional elpa-p builds-p repos-p regraft-repos-p)
  "Auto-removes orphaned packages and repos.
An orphaned package is a package that isn't a primary package (i.e. doesn't have
a `package!' declaration) or isn't depended on by another primary package.
If BUILDS-P, include straight package builds.
If REPOS-P, include straight repos.
If ELPA-P, include packages installed with package.el (M-x package-install)."
  (+emacs-initialize-packages)
  (+emacs--barf-if-incomplete-packages)
  (print! (start "Purging orphaned packages (for the emperor)..."))
  (cl-destructuring-bind (&optional builds-to-purge repos-to-purge repos-to-regraft)
      (let ((rdirs
             (and (or repos-p regraft-repos-p)
                  (straight--directory-files (straight--repos-dir) nil nil 'sort))))
        (list (when builds-p
                (let ((default-directory (straight--build-dir)))
                  (seq-filter #'file-directory-p
                              (seq-remove (+emacs-rpartial #'gethash straight--profile-cache)
                                          (straight--directory-files default-directory nil nil 'sort)))))
              (when repos-p
                (seq-remove (+emacs-rpartial #'straight--checkhash straight--repo-cache)
                            rdirs))
              (when regraft-repos-p
                (seq-filter (+emacs-rpartial #'straight--checkhash straight--repo-cache)
                            rdirs))))
    (print-group!
     (delq
      nil (list
           (if (not builds-p)
               (ignore (print! (info "Skipping builds")))
             (and (/= 0 (+emacs--packages-purge-builds builds-to-purge))
                  (straight-prune-build-cache)))
           (if (not elpa-p)
               (ignore (print! (info "Skipping elpa packages")))
             (/= 0 (+emacs--packages-purge-elpa)))
           (if (not repos-p)
               (ignore (print! (info "Skipping repos")))
             (/= 0 (+emacs--packages-purge-repos repos-to-purge)))
           (if (not regraft-repos-p)
               (ignore (print! (info "Skipping regrafting")))
             (+emacs--packages-regraft-repos repos-to-regraft)))))))
