Modules can be deactivated by using the cookie

```{emacs-lisp}
;;;###deactivatemodule
```

This needs to be done in `site-lisp/modules/modules.el`. See
`+emacs-module-deactivate-cookies` for details. If you want to replace a module,
deactivate it first and then create it anew in a different category, e.g.
`:local MODULE`.

## Included Modules

Installed by default.

### `:completion`

- company 
- ivy

### `:ui`

- dashboard
- hl-todo
- evil-goggles
- highlight-symbol
- popup
- pretty-code
- transient-state
- treemacs
- vc-gutter
- window-select
- workspaces

### `:editor`

- cleverlispy
- evil
- file-templates
- fold
- multiple-cursors
- rotate-text
- snippets

### `:emacs`

- dired
- electric
- ibuffer
- vc

### `:term`

- eshell
- tty

### `:tools`

- backups
- eval
- lookup
- magit

### `:org`

- org

### `:lang`

- emacs-lisp
- sh

### `:config`

- default
  - +smartparens

## Variable modules

### `:input`

- deDE

### `:completion`

- company 
  - +childframe
  - +tng

### `:ui`

- childframes
- deft
- doom-theming
- golden-ratio
- modeline
- popup
  - +all
- unicode
- zen

### `:editor`

- editor
  - +holy
- format
  - +onsave
- word-wrap

### `checkers`

- grammar
- spelling
- syntax

### `:tools`

- arch-linux
- copy-as-format
- direnv
- editorconfig
- ein
- lsp
- pandoc
- pdf

### `:org`

- org
  - +dragndrop
  - +jupyter
  - +pomodoro
  - +present
  - +org-ql
  - +ref
  - +tufte

### `:lang`

- common-lisp
- data
- ess
  - +lsp
- latex
  - +fold
  - +latexmk
- markdown
- nix
- python
- scheme
  - +guile
  - +racket
- sh
  - +fish
