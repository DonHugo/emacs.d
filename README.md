# Emacs configuration

My little Emacs config: Parentheses is Magic.

-------------------------------------------------------------------------------

## Recommended dependencies

* ripgrep
* watchexec
* fd

-------------------------------------------------------------------------------

Inspired by
* [Doom Emacs](https://github.com/hlissner/doom-emacs/tree/develop)
* [Spacemacs](https://github.com/syl20bnr/spacemacs/tree/develop)
* [Radian Emacs](https://github.com/raxod502/radian/tree/develop/emacs)
* [Joe Schafer's config](https://github.com/jschaf/dotfiles/tree/master/emacs)
