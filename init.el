;; init.el -*- lexical-binding: t; -*-

;; Defer garbage collection further back in the startup process
(setq gc-cons-threshold most-positive-fixnum)

;; In noninteractive sessions, prioritize non-byte-compiled source files to
;; prevent the use of stale byte-code.
(setq load-prefer-newer noninteractive)

(let (file-name-handler-alist)
  ;; Ensure Emacs is running out of this file's directory
  (setq user-emacs-directory (file-name-directory load-file-name)))

;; Make sure not to load an out-of-date .elc file. Since byte-compilation
;; happens asynchronously in the background after init succeeds, this case
;; can happen.
(let ((stale-bytecode t))
  (catch 'stale-bytecode
    (dolist (file (list (concat user-emacs-directory "lisp/init-core.el")
                        (concat user-emacs-directory "lisp/init-core-lib.el")
                        (concat user-emacs-directory "lisp/init-core-interactive.el")
                        (concat user-emacs-directory "lisp/init-core-modules.el")
                        (concat user-emacs-directory "lisp/modules/modules.el")
                        (concat user-emacs-directory "site-lisp/modules/modules.el")))
      (let ((elc-file (concat (file-name-sans-extension file) ".elc")))
        (when (and (file-exists-p elc-file)
                   (file-newer-than-file-p file elc-file))
          (throw 'stale-bytecode nil))))

    ;; Load the heart of Emacs
    (load (concat user-emacs-directory "lisp/init-core")
          nil 'nomessage)
    (+emacs-initialize)

    (setq stale-bytecode nil))
  (when stale-bytecode
    ;; Don't bother trying to recompile, we are going to handle that later,
    ;; asynchronously.
    (ignore-errors
      (dolist (file (append (directory-files (concat user-emacs-directory "lisp/") t ".+\\.elc$")
                            (directory-files (concat user-emacs-directory "lisp/modules/") t ".+\\.elc$")
                            (directory-files (concat user-emacs-directory "site-lisp/") t ".+\\.elc$")))
        (delete-file file)))

    (load (concat user-emacs-directory "lisp/init-core.el")
          nil 'nomessage 'nosuffix t)
    (+emacs-initialize)))

;; We should only get here if init was successful. If we do, byte-compile
;; asynchronously in a subprocess using the Makefile. That way, the next startup
;; will be fast(er).
(when (and (not (or (getenv "DEBUG") init-file-debug))
           (not noninteractive)
           (not (daemonp)))
  (run-with-idle-timer
   1 nil
   #'+emacs-config-byte-compile t))
