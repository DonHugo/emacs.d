;; init-minimal.el -*- lexical-binding: t; -*-

;; A minimal configuration for low-resource systems and terminals

;; In the strange case that early-init.el wasn't loaded, we do it explicitly:
(unless (boundp '+emacs-core-loaded)
  (load (concat (file-name-directory load-file-name) "early-init")
        nil t))

;; Load autoloads
(load (string-remove-suffix ".el" +emacs-autoloads-file)
      nil 'nomessage)

;; Load custom file
(load custom-file 'noerror t)


;;
;;; Core settings which were not covered already

(setq uniquify-buffer-name-style 'forward
      ;; no beeping or blinking please
      ring-bell-function #'ignore
      visible-bell nil
      ;; Save before killing
      save-interprogram-paste-before-kill t
      ;; Eliminate duplicates in the kill ring. That is, if you kill the same
      ;; thing twice, you won't have to use M-y twice to get past it to older
      ;; entries in the kill ring.
      kill-do-not-save-duplicates t
      x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)
      ;; Formating
      sentence-end-double-space nil
      delete-trailing-lines nil
      require-final-newline t
      ;; No tabs
      indent-tabs-mode nil
      ;; make scrolling sane
      scroll-margin 0
      scroll-conservatively 10
      hscroll-step 1
      mouse-wheel-progressive-speed nil
      mouse-wheel-scroll-amount '(5 ((shift) . 1)))

;; Backups & Autosave

(setq create-lockfiles nil
      make-backup-files t
      ;; Some sensible defaults:
      version-control t     ; number each backup file
      backup-by-copying t   ; instead of renaming current file (clobbers links)
      delete-old-versions t ; clean up after itself
      kept-old-versions 5
      kept-new-versions 5
      vc-make-backup-files t ; If we backup, backup (nearly) everything
      backup-directory-alist (list (cons "." (concat +emacs-cache-dir "backup/")))
      tramp-backup-directory-alist backup-directory-alist)

;; Turn on auto-save, so we have a fallback in case of crashes or lost data. Use
;; `recover-file' or `recover-session' to recover them.
(setq auto-save-default t
      ;; Don't auto-disable auto-save after deleting big chunks. This defeats
      ;; the purpose of a failsafe. This adds the risk of losing the data we
      ;; just deleted, but I believe that's VCS's jurisdiction, not ours.
      auto-save-include-big-deletions t
      ;; Keep it out of `+emacs-dir' or the local directory.
      auto-save-list-file-prefix (concat +emacs-cache-dir "autosave/")
      tramp-auto-save-directory  (concat +emacs-cache-dir "tramp-autosave/")
      auto-save-file-name-transforms
      (list (list "\\`/[^/]*:\\([^/]*/\\)*\\([^/]*\\)\\'"
                  ;; Prefix tramp autosaves to prevent conflicts with local ones
                  (concat auto-save-list-file-prefix "tramp-\\2") t)
            (list ".*" auto-save-list-file-prefix t)))

;;
;;; UX/UI

;; Easier question
(defalias 'yes-or-no-p #'y-or-n-p)

;; Reduce the clutter in the fringes; we'd like to reserve that space for more
;; useful information, like git-gutter and flycheck.
(setq indicate-buffer-boundaries nil
      indicate-empty-lines nil)

;; remove continuation arrow on right fringe
(delq! 'continuation fringe-indicator-alist 'assq)
;; Simple frame title
(setq frame-title-format '("" invocation-name " - λ : "
                           (:eval (if (buffer-file-name)
                                      (abbreviate-file-name (buffer-file-name))
                                    "%b")))
      icon-title-format frame-title-format)

;; Disable tool and scrollbars, and encourage keyboard-centric workflows (the
;; scrollbar also impacts performance).
(push '(menu-bar-lines . 0)   default-frame-alist)
(push '(tool-bar-lines . 0)   default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)

;; These are disabled directly through their frame parameters, to avoid the
;; extra work their minor modes do, but we have to unset these variables
;; ourselves, otherwise users will have to cycle them twice to re-enable them.
(setq menu-bar-mode nil
      tool-bar-mode nil
      scroll-bar-mode nil)

;; The native border "consumes" a pixel of the fringe on righter-most splits,
;; `window-divider' does not. Available since Emacs 25.1.
(setq window-divider-default-places t
      window-divider-default-bottom-width 1
      window-divider-default-right-width 1)
(add-hook 'window-setup-hook #'window-divider-mode)

(add-hook 'window-configuration-change-hook #'winner-mode)

;; always avoid GUI
(setq use-dialog-box nil)
;; Don't display floating tooltips; display their contents in the echo-area.
(if (bound-and-true-p tooltip-mode) (tooltip-mode -1))
;; native linux tooltips are ugly
(when IS-LINUX
  (setq x-gtk-use-system-tooltips nil))

;; No blinking cursor
(blink-cursor-mode -1)
;; Allow deletion of selection
(delete-selection-mode +1)
;; Highlight line
(global-hl-line-mode +1)
;; Show column numbers
(column-number-mode +1)
;; Enable mouse
(xterm-mouse-mode +1)
;; Enable icomplete
(icomplete-mode +1)
;; Enable visual line
(global-visual-line-mode +1)

;; Enable semantic code LALR(1) parser
(add-hook 'prog-mode-hook #'semantic-mode)
(with-eval-after-load 'semantic
  (global-semanticdb-minor-mode +1)
  (global-semantic-idle-scheduler-mode +1)
  (global-semantic-idle-summary-mode +1))

;; Enable auto-save
(add-hook 'find-file-hook #'auto-save-mode)
;; Enable auto-revert
(add-hook 'find-file-hook #'global-auto-revert-mode)

(defhook! +emacs--hook-generic-term-init ()
  "Generic settings for terminals."
  (term-mode-hook shell-mode-hook eshell-mode-hook)
  (visual-line-mode -1)
  (setq-local global-hl-line-mode nil)
  (setq-local scroll-margin 0))

;; Hippie expand
(setq hippie-expand-try-functions-list '(try-expand-dabbrev
                                         try-expand-dabbrev-from-kill
                                         try-expand-dabbrev-all-buffers
                                         try-complete-file-name-partially
                                         try-complete-file-name
                                         try-expand-all-abbrevs
                                         try-expand-list
                                         try-expand-line
                                         try-complete-lisp-symbol-partially
                                         try-complete-lisp-symbol))

(global-set-key (kbd "C-.") #'hippie-expand)

;;
;;; TTY support

;; This config will often be used in a terminal
;; Keep window title up-to-date; should fail gracefully in non-xterm terminals.
(setq xterm-set-window-title t)
(defadvice! +tty--advice-only-set-window-title-in-tty (&optional terminal)
  "`xterm-set-window-title' fails in GUI Emacs."
  :before-until #'xterm-set-window-title
  (not (display-graphic-p terminal)))

;; Some terminals offer two different cursors: a "visible" static cursor and a
;; "very visible" blinking one. By default, Emacs uses the very visible cursor
;; and will switch back to it when Emacs is started or resumed. A nil
;; `visible-cursor' prevents this.
(setq visible-cursor nil)

;; Enable the mouse in terminal Emacs
(add-hook 'tty-setup-hook #'xterm-mouse-mode)

(defhook! +tty--hook-init-clipboard-in-tty-emacs ()
  "Require `xclip' for that."
  'tty-setup-hook
  (and (require 'xclip nil t)
       (with-demoted-errors "%s" (xclip-mode +1))))


;;
;;; Keys

;; interpreted as C-<Arrow> in a terminal
(global-set-key (kbd "M-[ d") (kbd "<C-left>"))
(global-set-key (kbd "M-[ c") (kbd "<C-right>"))
(global-set-key (kbd "M-[ a") (kbd "<C-up>"))
(global-set-key (kbd "M-[ b") (kbd "<C-down"))

;; and parsed normally in GUI
(global-set-key (kbd "C-<left>") #'windmove-left)
(global-set-key (kbd "C-<right>") #'windmove-right)
(global-set-key (kbd "C-<up>") #'windmove-up)
(global-set-key (kbd "C-<down>") #'windmove-down)

(global-set-key (kbd "<remap> <just-one-space>") #'cycle-spacing)
(global-set-key (kbd "<remap> <delete-horizontal-space>") #'cycle-spacing)
(global-set-key (kbd "C-0") #'delete-window)
(global-set-key (kbd "C-1") #'delete-other-windows)
(global-set-key (kbd "C-2") #'split-window-below)
(global-set-key (kbd "C-3") #'split-window-right)
(global-set-key (kbd "C-4") #'find-file-other-window)
(global-set-key (kbd "C-5") #'make-frame-command)


;;
;;; Theme
(load-theme 'modus-vivendi)

;;
;;; GC
(gcmh-mode +1)

;; test
